//
//  PlayUDict.h
//  Interchange
//
//  Created by whiznic on 1/8/18.
//  Copyright © 2018 com.production. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary(GSM)

-(id)objectForKeyWithNull:(id)aKey;
-(NSString*)stringForKeyWithNull:(id)aKey;
- (instancetype)dictByReplacingNulls;

@end
