//
//  PlayUDict.m
//  Interchange
//
//  Created by whiznic on 1/8/18.
//  Copyright © 2018 com.production. All rights reserved.
//

#import "NSDictionary(GSM).h"
#import "AppDelegate.h"

@implementation NSDictionary(PlayU)
-(id)objectForKeyWithNull:(id)aKey{
    NSString *value = [self objectForKey:aKey];
    if ([value isKindOfClass:[NSNull class]]) {
        return nil;
    }
    return value;
}

-(NSString*)stringForKeyWithNull:(id)aKey{
    NSString *value = [self objectForKeyWithNull:aKey];
    if (value) {
        return value;
    }else{
        return @"";
    }
}

- (instancetype)dictByReplacingNulls {
    NSMutableDictionary *mDict = [NSMutableDictionary dictionaryWithDictionary:self];
    NSNull *null = NSNull.null;
    NSString *strBlank = @"";
    
    for (NSString *key in self) {
        const id object = [self objectForKey:key];
        
        if ([object isEqual:null]) {
            [mDict setObject:strBlank
                      forKey:key];
        }
        else if ([object isKindOfClass:NSArray.class]) {
            NSMutableArray *mArr = [NSMutableArray arrayWithArray:object];
            
            for (int i = 0; i < [object count]; i++) {
                id obj = [object objectAtIndex:i];
                
                if ([obj isKindOfClass:NSDictionary.class]) {
                    [mArr replaceObjectAtIndex:i
                                    withObject:[obj dictByReplacingNulls]];
                }
                else if ([obj isEqual:null]) {
                    [mArr replaceObjectAtIndex:i
                                    withObject:strBlank];
                }
            }
            
            [mDict setObject:mArr
                      forKey:key];
        }
        else if ([object isKindOfClass:NSDictionary.class]) {
            [mDict setObject:[object dictByReplacingNulls]
                      forKey:key];
        }
        else if ([object isKindOfClass:NSNumber.class]) {
            [mDict setObject:[NSString stringWithFormat:@"%@", object]
                      forKey:key];
        }
    }
    return [NSDictionary dictionaryWithDictionary:mDict];
}

@end
