//
//  CommanMethods.m
//  Interchange
//
//  Created by whiznic on 1/8/18.
//  Copyright © 2018 com.production. All rights reserved.
//

#import "CommanMethods.h"
#import <UIKit/UIKit.h>
#import "Reachability.h"

@implementation CommanMethods

+(void) displayAlertMessage:(NSString *)title message:(NSString *)message currentRef:(id)currentRef {
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:title
                                 message:message
                                 preferredStyle:UIAlertControllerStyleAlert];
    //Add Buttons
    UIAlertAction* yesButton = [UIAlertAction
                                actionWithTitle:@"OK"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action) {
                                    //Handle your yes please button action here
                                }];
    [alert addAction:yesButton];
    [currentRef presentViewController:alert animated:YES completion:nil];
}

+(void) displayAlertMessageWithCurrentController:(NSString *)title message:(NSString *)message currentRef:(id)currentRef {
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:title
                                 message:message
                                 preferredStyle:UIAlertControllerStyleAlert];
    //Add Buttons
    UIAlertAction* yesButton = [UIAlertAction
                                actionWithTitle:@"OK"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action) {
                                    //Handle your yes please button action here
                                }];
    [alert addAction:yesButton];
    
    
    
    UIViewController *VC = [UIApplication sharedApplication].keyWindow.rootViewController;
    if ([VC isKindOfClass:[UIViewController class]]) {
        [VC presentViewController:alert animated:YES completion:nil];
    }
}

+(void) displayAlertMessageWithCallBack:(NSString *)title message:(NSString *)message refrence:(id)currentRef Success:(void (^)(void))success {
    UIAlertController * alertController = [UIAlertController alertControllerWithTitle: title
                                                          message: message
                                                   preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                               handler:^(UIAlertAction * action){
                                                   //Do Some action here
                                                   [alertController dismissViewControllerAnimated:YES completion:nil];
                                                   success();
                                               }];
    
    [alertController addAction:ok];
    [currentRef presentViewController:alertController animated:YES completion:nil];
}

+(BOOL) NSStringIsValidEmail:(NSString *)checkString {
    BOOL stricterFilter = NO;
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

+(BOOL)isNetworkRechable {
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    [reachability startNotifier];
    NetworkStatus status = [reachability currentReachabilityStatus];
    
    if(status == NotReachable) {
        return  false;
    } else {
        return  true;
    }
}

+(BOOL)isStringIsEmpty:(NSString *)string {

    NSString *strTitle = [string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    if (strTitle.length == 0) {
        return true;
    } else {
        return false;
    }
}

+(NSString *)getBlankStringIfStringEmpty:(NSString *)string {
    
    if (string.length == 0) {
        return @"";
    } else {
        NSString *strTitle = [string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        return strTitle;
    }
}

+(NSString *)trimString:(NSString *)string {
    
    NSString *strTitle = [string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    return strTitle;
    
}

@end
