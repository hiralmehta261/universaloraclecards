//
//  CommanMethods.h
//  Interchange
//
//  Created by whiznic on 1/8/18.
//  Copyright © 2018 com.production. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

// Alert message
#define kInternetConnectionMessage  @"Please check internet connection and try again."

// Alert title
#define Key_Alert                     @"Alert"
#define key_Informaction              @"Information"

@interface CommanMethods : NSObject

+(void) displayAlertMessage:(NSString *)title message:(NSString *)message currentRef:(id)currentRef;
+(void) displayAlertMessageWithCurrentController:(NSString *)title message:(NSString *)message currentRef:(id)currentRef;
+(void) displayAlertMessageWithCallBack:(NSString *)title message:(NSString *)message refrence:(id)currentRef Success:(void (^)(void))success;
+(BOOL) NSStringIsValidEmail:(NSString *)checkString;
+(BOOL)isNetworkRechable;

+(void)setButtonBorder:(UIButton *)button;
+(void)setTextFieldBorder:(UITextField *)textfield;
+(void)setTextFieldBorderWithoutPadding:(UITextField *)textfield;
+(void)setTextViewBorder:(UITextView *)textview;
+(void)setViewBorder:(UIView *)view;
+(BOOL)isStringIsEmpty:(NSString *)string;
+(NSString *)getBlankStringIfStringEmpty:(NSString *)string;
+(NSString *)trimString:(NSString *)string;

@end
