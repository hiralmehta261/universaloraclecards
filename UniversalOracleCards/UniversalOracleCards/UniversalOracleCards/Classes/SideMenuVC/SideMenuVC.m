//
//  SideMenuVC.m
//  Interchange
//
//  Created by whiznic on 1/6/18.
//  Copyright © 2018 com.production. All rights reserved.
//

#import "SideMenuVC.h"
#import "SMHeaderVw.h"
#import "SMHeaderVw2.h"
#import "SMCell.h"
#import "AppDelegate.h"
#import "CommanMethods.h"
#import "HomeVC.h"
#import "SocialIconCell.h"
#import "SignInVC.h"
#import "AboutTheArtistVC.h"
#import "SettingVC.h"
#import "CommanMethods.h"
#import "CommunicationHandler.h"
#import "MyJournalListVC.h"
#import "PrivacyVC.h"
#import "ContactUsVC.h"

#define SCREEN_HEIGHT [[UIScreen mainScreen] bounds].size.height
#define SCREEN_WIDTH [[UIScreen mainScreen] bounds].size.width
#define SCREEN_SCALE UIScreen.mainScreen.scale

static NSDate *lastTouchTime;
static int speed;
static int startXposition;
static int currentXposition;
@interface SideMenuTblVw ()

@end


@implementation SideMenuTblVw

- (void)drawRect:(CGRect)rect {
    [super drawRect:rect];
    
    CGRect pathRect = CGRectMake(rect.size.width, -rect.size.height,
                                 1.5, rect.size.height*5.0);
    
    UIBezierPath *bezierPath = [UIBezierPath bezierPathWithRect:pathRect];
    self.layer.masksToBounds = NO;
    self.layer.shadowColor = UIColor.blackColor.CGColor;
    self.layer.shadowOpacity = 1.0;
    self.layer.shadowPath = bezierPath.CGPath;
}

@end

@interface SideMenuVC () {
    BOOL isSideMenuExpanded;
    NSMutableArray *arrCategory;
    NSDictionary *dcitData;
}

@end

@implementation SideMenuVC

@synthesize langStrings = _langStrings;
@synthesize arrMenuOptions = _arrMenuOptions;
@synthesize arrMainMenu = _arrMainMenu;
@synthesize tblMenu = _tblMenu;

@synthesize isOpen = _isOpen;
@synthesize arrMenuOptionID = _arrMenuOptionID;
@synthesize arrMenuIcon = _arrMenuIcon;

#pragma mark- ViewController

+ (instancetype)sharedSideMenu {
    static SideMenuVC *sideMenuVC = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        sideMenuVC = [[SideMenuVC alloc] initWithNibName:NSStringFromClass(SideMenuVC.class)
                                                  bundle:nil];
    });
    
    return sideMenuVC;
}

- (instancetype)initWithNibName:(NSString *)nibNameOrNil
                         bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil
                           bundle:nibBundleOrNil];
    
    if (self) {
        _arrMenuOptions = NSMutableArray.new;
        _arrMainMenu = NSMutableArray.new;
        _arrMenuIcon = NSMutableArray.new;
    }
    
    return self;
}

+ (void)addToViewController:(UIViewController *)vC {
    [AppDelegate addVwSideMenuBgToVw:vC.view];
    SideMenuVC *sideMenuVC = SideMenuVC.sharedSideMenu;
    
    ((TouchVw *)vC.view).slider = sideMenuVC;
    ((TouchVw *)vC.view).shouldShowSlider = true;
    
    [vC addChildViewController:sideMenuVC];
    [vC.view addSubview:sideMenuVC.view];
}

+ (void)remove {
    SideMenuVC *sideMenuVC = SideMenuVC.sharedSideMenu;
    
    [sideMenuVC removeFromParentViewController];
    [sideMenuVC.view removeFromSuperview];
    [AppDelegate removeVwSideMenuBg];
}

+ (void)defaultVwFrame:(CGRect)frame {
    frame.origin.x -= CGRectGetWidth(frame);
    SideMenuVC.sharedSideMenu.view.frame = frame;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view from its nib.
    self.view.backgroundColor = UIColor.clearColor;
    
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
        UINib *nib = [UINib nibWithNibName:@"SMHeaderVw" bundle:nil];
        [_tblMenu registerNib:nib forHeaderFooterViewReuseIdentifier:SMHeaderVwId];
    } else {
        UINib *nib = [UINib nibWithNibName:@"SMHeaderVw_iPad" bundle:nil];
        [_tblMenu registerNib:nib forHeaderFooterViewReuseIdentifier:SMHeaderVwId];
    }
    
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
        UINib *nib = [UINib nibWithNibName:@"SMHeaderVw2" bundle:nil];
        [_tblMenu registerNib:nib forHeaderFooterViewReuseIdentifier:SMHeaderVw2Id];
    } else {
        UINib *nib = [UINib nibWithNibName:@"SMHeaderVw2" bundle:nil];
        [_tblMenu registerNib:nib forHeaderFooterViewReuseIdentifier:SMHeaderVw2Id];
    }
    
    
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
        UINib *cellnib = [UINib nibWithNibName:@"SMCell" bundle:nil];
        [_tblMenu registerNib:cellnib forCellReuseIdentifier:SMCellId];
    } else {
        UINib *cellnib = [UINib nibWithNibName:@"SMCell_iPad" bundle:nil];
        [_tblMenu registerNib:cellnib forCellReuseIdentifier:SMCellId];
        
    }
    
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
        UINib *cellnib = [UINib nibWithNibName:@"SocialIconCell" bundle:nil];
        [_tblMenu registerNib:cellnib forCellReuseIdentifier:SocialIconCellId];
    } else {
        UINib *cellnib = [UINib nibWithNibName:@"SocialIconCell_iPad" bundle:nil];
        [_tblMenu registerNib:cellnib forCellReuseIdentifier:SocialIconCellId];
        
    }
    
    self.dsipalymenu.layer.zPosition = 1;
    
    lastTouchTime = [NSDate date];
    speed = 0;
    startXposition = 0;
    currentXposition = 0;
    
   
    [self.tblMenu setBackgroundColor:[UIColor clearColor]];
    UIImageView *tableBackgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"dark_background"]];
    [tableBackgroundView setFrame: self.tblMenu.frame];
    [self.tblMenu setBackgroundView:tableBackgroundView];
    
   
    arrCategory = [[NSMutableArray alloc] init];
   
}

-(void) reloadSideMenu {

    _arrMainMenu = [[NSMutableArray alloc] init];
    [_arrMainMenu addObject:@"Home"];
    [_arrMainMenu addObject:@"My Journal"];
    [_arrMainMenu addObject:@"Shop"];
    [_arrMainMenu addObject:@"About the Artist"];
    [_arrMainMenu addObject:@"Get in Touch"];
    [_arrMainMenu addObject:@"Privacy & Terms"];
    [_arrMainMenu addObject:@"Share App With Friends"];
    [_arrMainMenu addObject:@"Logout"];
    
    _arrMenuIcon = [[NSMutableArray alloc] init];
    [_arrMenuIcon addObject:@"m-home"];
    [_arrMenuIcon addObject:@"m-myjournal"];
    [_arrMenuIcon addObject:@"m-shop"];
    [_arrMenuIcon addObject:@"m-aboutartist"];
    [_arrMenuIcon addObject:@"m-getintouch"];
    [_arrMenuIcon addObject:@"m-privacy"];
    [_arrMenuIcon addObject:@"m-share"];
    [_arrMenuIcon addObject:@"m-logout"];

    [self viewWillAppear:YES];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    isSideMenuExpanded = false;
    [_tblMenu reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark- Actions
- (void)animateStatusBar {
    [self setNeedsStatusBarAppearanceUpdate];
}

- (void)show {
    _isOpen = YES;
    [self.parentViewController setNeedsStatusBarAppearanceUpdate];
    
    [self animateStatusBar];
    [UIView animateWithDuration:0.3
                     animations:^{
                         CGRect currentRect = self.view.frame;
                         currentRect.origin.x = 0.0;
                         self.view.frame = currentRect;
                         vwSideMenuBg.alpha = 0.7;
                         //self.parentViewController.view.alpha = 0.7;
                     }
                     completion:^(BOOL finished) {
                         
                         [_tblMenu reloadData];
                     }];
}

- (void)showWithTouches:(CGPoint)point {
    [self.parentViewController setNeedsStatusBarAppearanceUpdate];
    float diff = SCREEN_WIDTH - 252;
    CGRect currentRect = self.view.frame;
    if (point.x <= SCREEN_WIDTH){
       [[NSNotificationCenter defaultCenter] postNotificationName:@"notification_side_menu_open" object:nil];
        currentRect.origin.x = (point.x-SCREEN_WIDTH + diff) > 0 ? 0 : point.x-SCREEN_WIDTH + diff;
        int diffrence = startXposition - currentXposition;
        if (self.isOpen && diffrence > 0) {
            currentRect.origin.x = -(diffrence);
        }else if (self.isOpen && diffrence <= 0){
            currentRect.origin.x = 0;
        }

        self.view.frame = currentRect;
         if (!(self.isOpen && diffrence <= 0)){
        float nextAlpha = (point.x*0.7)/(SCREEN_WIDTH-diff);
        vwSideMenuBg.alpha = nextAlpha > 0.7 ? 0.7 : nextAlpha;
         }
    }
}

- (void)hide {
    _isOpen = NO;
    [self.parentViewController setNeedsStatusBarAppearanceUpdate];

    [UIView animateWithDuration:0.4
                     animations:^{
                         CGRect currentRect = self.view.frame;
                         currentRect.origin.x -= SCREEN_WIDTH;
                         self.view.frame = currentRect;
                         self.view.alpha = 0.0;
                         //self.parentViewController.view.alpha = 1.0;
                     }
                     completion:^(BOOL finished) {
                         
//                         vwSideMenuBg.alpha = 1.0;
                         self.view.alpha = 1.0;
                         [UIView animateWithDuration:0.3
                                          animations:^{
                                              [self.view layoutIfNeeded];
                                          }
                                          completion:^(BOOL finished) {
                                              [_tblMenu reloadData];
                                          }];
//                         self.parentViewController.accessibilityHint = @"";
//                         [self animateStatusBar];
                     }];
}

#pragma mark- TableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return _arrMainMenu.count;
    } else {
        return 1;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section == 0) {
        SMCell *cell = [tableView dequeueReusableCellWithIdentifier:SMCellId forIndexPath:indexPath];
        cell.lblTitle.text = [_arrMainMenu objectAtIndex:indexPath.row];
        cell.imgIcon.image = [UIImage imageNamed:[_arrMenuIcon objectAtIndex:indexPath.row]];
        return cell;
    } else {
        SocialIconCell *cell = [tableView dequeueReusableCellWithIdentifier:SocialIconCellId forIndexPath:indexPath];
        cell.btnInsta.tag = indexPath.row;
        cell.btnGoogle.tag = indexPath.row;
        cell.btnFaceBook.tag = indexPath.row;
        [cell.btnFaceBook addTarget:self action:@selector(btnFacebookClick:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btnGoogle addTarget:self action:@selector(btnGoogleClick:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btnInsta addTarget:self action:@selector(btnInstaClick:) forControlEvents:UIControlEventTouchUpInside];
        return cell;
    }
}

#pragma mark- TableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone )
    {
        if (section == 0) {
            return 130.0;
        } else {
            return 0.0;
        }
    }
    else{
        if (section == 0) {
            return 200.0;
        } else {
            return 0.0;
        }
    }
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
    return 0.0;
    } else {
        return 0.0;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    if (section == 0) {
        SMHeaderVw *header = [tableView dequeueReusableHeaderFooterViewWithIdentifier:SMHeaderVwId];
        
        if ([CommanMethods isNetworkRechable]) {
            [self callForGetDetails];
        } else {
            [CommanMethods displayAlertMessage:key_Informaction message:kInternetConnectionMessage currentRef:self];
        }
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        NSDictionary *dictData = [userDefaults objectForKey:@"getdata"];
         NSLog(@" dictData :- %@", dictData);
         header.lblName.text = [dictData objectForKey:@"full_name"];
         header.lblEmail.text = [dictData objectForKey:@"email"];
        
        
        header.btnSetting.tag = section;
        header.btnSetting.accessibilityHint = @"0";
        [header.btnSetting addTarget:self action:@selector(btnSettingClick:) forControlEvents:UIControlEventTouchUpInside];
        return header;
    } else {
        SMHeaderVw2 *header = [tableView dequeueReusableHeaderFooterViewWithIdentifier:SMHeaderVw2Id];
        return header;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
        if (indexPath.section == 0) {
            return 70;
        } else {
            return 58;
        }
    } else {
        if (indexPath.section == 0) {
            return 108;
        } else {
            return 100;
        }
    }
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section == 0) {
        
        if (indexPath.row == 0) {
            if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
                HomeVC *homeVC = [[HomeVC alloc] initWithNibName:@"HomeVC" bundle:nil];
                [self.navigationController pushViewController:homeVC animated:YES];
            } else {
                HomeVC *homeVC = [[HomeVC alloc] initWithNibName:@"HomeVC_IPad" bundle:nil];
                [self.navigationController pushViewController:homeVC animated:YES];
            }
        } else if (indexPath.row == 1) {
            if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
                MyJournalListVC *journalVC = [[MyJournalListVC alloc] initWithNibName:@"MyJournalListVC" bundle:nil];
                [self.navigationController pushViewController:journalVC animated:YES];
            } else {
                MyJournalListVC *journalVC = [[MyJournalListVC alloc] initWithNibName:@"MyJournalListVC_IPad" bundle:nil];
                [self.navigationController pushViewController:journalVC animated:YES];
            }
        } else if (indexPath.row == 2) {
             [[UIApplication sharedApplication] openURL:[NSURL URLWithString: @"https://google.com"] options:@{} completionHandler:nil];
        } else if (indexPath.row == 3) {
            if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
                AboutTheArtistVC *aboutVC = [[AboutTheArtistVC alloc] initWithNibName:@"AboutTheArtistVC" bundle:nil];
                [self.navigationController pushViewController:aboutVC animated:YES];
            } else {
                AboutTheArtistVC *aboutVC = [[AboutTheArtistVC alloc] initWithNibName:@"AboutTheArtistVC_IPad" bundle:nil];
                [self.navigationController pushViewController:aboutVC animated:YES];
            }
        } else if (indexPath.row == 4) {
            if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
                ContactUsVC *aboutVC = [[ContactUsVC alloc] initWithNibName:@"ContactUsVC" bundle:nil];
                [self.navigationController pushViewController:aboutVC animated:YES];
            } else {
                ContactUsVC *aboutVC = [[ContactUsVC alloc] initWithNibName:@"ContactUsVC_IPad" bundle:nil];
                [self.navigationController pushViewController:aboutVC animated:YES];
            }
        } else if (indexPath.row == 5) {
            if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
                PrivacyVC *aboutVC = [[PrivacyVC alloc] initWithNibName:@"PrivacyVC" bundle:nil];
                [self.navigationController pushViewController:aboutVC animated:YES];
            } else {
                PrivacyVC *aboutVC = [[PrivacyVC alloc] initWithNibName:@"PrivacyVC_IPad" bundle:nil];
                [self.navigationController pushViewController:aboutVC animated:YES];
            }
        } else if (indexPath.row == 6) {
            NSString *url=@"https://www.google.com/";
            NSString * title =[NSString stringWithFormat:@"Download app from %@",url];
            NSArray* dataToShare = @[title];
            UIActivityViewController *activityViewController = [[UIActivityViewController alloc] initWithActivityItems:dataToShare applicationActivities:nil];
            activityViewController.popoverPresentationController.sourceView = self.view;
            [self presentViewController:activityViewController animated:YES completion:nil];
        } else {
            UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"UOC" message:@"Are you sure you want to logout?" preferredStyle:UIAlertControllerStyleAlert];
            
            [actionSheet addAction:[UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
                
                [self dismissViewControllerAnimated:YES completion:^{
                }];
            }]];
            
            
            [actionSheet addAction:[UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                
                if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
                    [self ClearAllUserDetaults];
                    SignInVC *objcLogin = [[SignInVC alloc] initWithNibName:@"SignInVC" bundle:nil];
                    self.navi1 = [[UINavigationController alloc] initWithRootViewController:objcLogin];
                    self.navi1.navigationBarHidden = TRUE;
                    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
                    appDelegate.window.rootViewController = self.navi1;
                } else {
                    [self ClearAllUserDetaults];
                    SignInVC *objcLogin = [[SignInVC alloc] initWithNibName:@"SignInVC_IPad" bundle:nil];
                    self.navi1 = [[UINavigationController alloc] initWithRootViewController:objcLogin];
                    self.navi1.navigationBarHidden = TRUE;
                    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
                    appDelegate.window.rootViewController = self.navi1;
                }
                
                
            }]];
            
            // Present action sheet.
            [self presentViewController:actionSheet animated:YES completion:nil];
            
        }
   
    } else {
        NSLog(@"Social icon");
    }
}





-(IBAction)btnSettingClick:(id)sender {
    /*UIButton *buttonPressed = (UIButton *)sender;
     if (buttonPressed.tag == 1) {
            UIStoryboard *mainStoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            HomeVC *vc = [mainStoryBoard instantiateViewControllerWithIdentifier: @"HomeVC"];
            [self.navigationController pushViewController:vc animated: YES];
        }*/
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
        SettingVC *settingVC = [[SettingVC alloc] initWithNibName:@"SettingVC" bundle:nil];
        [self.navigationController pushViewController:settingVC animated:YES];
    } else {
        SettingVC *settingVC = [[SettingVC alloc] initWithNibName:@"SettingVC_IPad" bundle:nil];
        [self.navigationController pushViewController:settingVC animated:YES];
    }
}

-(IBAction)btnFacebookClick:(id)sender {
    NSLog(@"Facebook");
    
}

-(IBAction)btnGoogleClick:(id)sender {
    NSLog(@"Google");
    
}

-(IBAction)btnInstaClick:(id)sender {
    NSLog(@"Insta");
}


-(void)ClearAllUserDetaults {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults removeObjectForKey:@"username"];
    [userDefaults removeObjectForKey:@"user_data"];
    [userDefaults removeObjectForKey:@"HomeScreen"];
    [userDefaults removeObjectForKey:@"CheckboxDailyCard"];
    [userDefaults removeObjectForKey:@"CheckboxGeneral"];
    [userDefaults removeObjectForKey:@"CheckboxInner"];
    [userDefaults removeObjectForKey:@"CheckboxConnection"];
    [userDefaults removeObjectForKey:@"CheckboxLife"];
    [userDefaults removeObjectForKey:@"audio"];
    [userDefaults removeObjectForKey:@"journaldata"];
    [userDefaults removeObjectForKey:@"spreaddata"];
}

#pragma CallAPI

-(void)callForGetDetails  {
    NSMutableDictionary *dcitParams = [[NSMutableDictionary alloc] init];
    
    //NSLog(@" dcitParams :-%@", dcitParams);
    
    [AppDelegate addVwLoading];
    [CommunicationHandler callForGetDetails:dcitParams Sucess:^(NSDictionary *dict, NSURLResponse *response) {
        //NSLog(@"dict :- %@", dict);
        dispatch_async(dispatch_get_main_queue(), ^{
            [AppDelegate removeVwLoading];
            if ([[dict objectForKey:@"error"] boolValue] == false) {
                self->dcitData = [[NSDictionary alloc] initWithDictionary:[dict objectForKey:@"data"] copyItems:YES];
                NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
                [userDefaults setObject:self->dcitData forKey:@"getdata"];
                [userDefaults synchronize];
                
            } else {
                [CommanMethods displayAlertMessage:@"UOC" message:[dict objectForKey:@"message"] currentRef:self];
            }
        });
    } Error:^(NSError *error, NSURLResponse *response) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [AppDelegate removeVwLoading];
        });
    }];
}


@end

@interface TouchVw ()

@end

@implementation TouchVw

@synthesize slider = _slider;
@synthesize shouldShowSlider = _shouldShowSlider;

- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event {
    if ((point.x < 10.0) && (point.y >= 85.0)) {
        _shouldShowSlider = true;
        return self;
    }
    else {
        if (_slider.isOpen) {
            _shouldShowSlider = true;
            //
            if ([[super hitTest:point withEvent:event] isKindOfClass:[UITableView class]])
            {
                return self;
            } else {
                if (CGRectContainsPoint(_slider.view.frame, point)) {
                    return [super hitTest:point withEvent:event];
                }
                else {
                    return self;
                }
            }
        }
        else {
            _shouldShowSlider = false;
        }
        
        return [super hitTest:point withEvent:event];
    }
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [super touchesBegan:touches withEvent:event];
    CGPoint endPoint = [touches.anyObject locationInView:self];
    startXposition = endPoint.x;
    currentXposition = endPoint.x;
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    [super touchesMoved:touches withEvent:event];
    
    CGPoint endPoint = [touches.anyObject locationInView:self];
    currentXposition = endPoint.x;
    
    if (_shouldShowSlider) {
        [_slider showWithTouches:[touches.anyObject locationInView:self]];
    }
    
    NSArray *touchesArray = [touches allObjects];
    UITouch *touch;
    CGPoint ptTouch;
    CGPoint ptPrevious;
    NSDate *now = [[NSDate alloc] init];
    NSTimeInterval interval = [now timeIntervalSinceDate:lastTouchTime];
    lastTouchTime = [[NSDate alloc] init];
    touch = [touchesArray objectAtIndex:0];
    ptTouch = [touch locationInView:self];
    ptPrevious = [touch previousLocationInView:self];
    
    CGFloat xMove = ptTouch.x - ptPrevious.x;
    CGFloat yMove = ptTouch.y - ptPrevious.y;
    CGFloat distance = sqrt ((xMove * xMove) + (yMove * yMove));
    
//    NSLog (@"TimeInterval:%f", interval);
//    NSLog (@"Move:%5.2f, %5.2f", xMove, yMove);
//    NSLog (@"Distance:%5.2f", distance);
//    NSLog (@"Speed:%5.2f", distance / interval);
    speed =  (int)(distance / interval);
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    [super touchesEnded:touches withEvent:event];
    
    if (_shouldShowSlider) {
        CGPoint endPoint = [touches.anyObject locationInView:self];
        
        if (_slider.isOpen) {
            int diffrence = startXposition - currentXposition;
            if (speed >= 2000 && diffrence > 0) {
                [_slider hide];
            }
            else if (endPoint.x <= 125.0 && diffrence > 0) {
                [_slider hide];
            }
            else{
//                [_slider show];
                if (!CGRectContainsPoint(_slider.tblMenu.frame, endPoint)) {
                    [_slider hide];
                }
                else {
                    [_slider show];
                }
            }
        }
        else {
            if (speed >= 2000) {
                [_slider show];
            }
            else if (endPoint.x >= 125.0){
                 [_slider show];
            }
            else {
                [_slider hide];
            }
        }
    }
}



@end
