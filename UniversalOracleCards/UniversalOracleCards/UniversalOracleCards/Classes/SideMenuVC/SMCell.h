//
//  SMCell.h
//  Interchange
//
//  Created by whiznic on 1/6/18.
//  Copyright © 2018 com.production. All rights reserved.
//

#import <UIKit/UIKit.h>

#define SMCellId @"SMCellId"

@interface SMCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UILabel *lblTitle;
@property (strong, nonatomic) IBOutlet UIImageView *imgIcon;


@end
