//
//  SMHeaderVw2.h
//  ios-personalityDevelopment
//
//  Created by Ankit Vadalia on 31/01/19.
//  Copyright © 2019 whiznic. All rights reserved.
//

#import <UIKit/UIKit.h>

#define SMHeaderVw2Id @"SMHeaderVw2Id"

@interface SMHeaderVw2 : UITableViewHeaderFooterView

@property (nonatomic, strong) IBOutlet UIView *vwBg;

@end

