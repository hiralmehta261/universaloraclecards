//
//  SMHeaderVw.m
//  Interchange
//
//  Created by whiznic on 1/6/18.
//  Copyright © 2018 com.production. All rights reserved.
//

#import "SMHeaderVw.h"

@implementation SMHeaderVw

@synthesize vwBg = _vwBg;
@synthesize lblName = _lblName;

- (void)awakeFromNib {
    [super awakeFromNib];
    [self layoutIfNeeded];
}

@end
