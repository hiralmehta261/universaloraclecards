//
//  SMHeaderVw.h
//  Interchange
//
//  Created by whiznic on 1/6/18.
//  Copyright © 2018 com.production. All rights reserved.
//

#import <UIKit/UIKit.h>

#define SMHeaderVwId @"SMHeaderVwId"

@interface SMHeaderVw : UITableViewHeaderFooterView

@property (nonatomic, strong) IBOutlet UIView *vwBg;
@property (nonatomic, strong) IBOutlet UILabel *lblName;
@property (strong, nonatomic) IBOutlet UIView *ViewImage;
@property (strong, nonatomic) IBOutlet UILabel *lblEmail;
@property (strong, nonatomic) IBOutlet UIButton *btnSetting;

@end
