//
//  SMCell.m
//  Interchange
//
//  Created by whiznic on 1/6/18.
//  Copyright © 2018 com.production. All rights reserved.
//

#import "SMCell.h"

@implementation SMCell

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

@end
