//
//  SocialIconCell.h
//  UniversalOracleCards
//
//  Created by whiznic on 05/03/19.
//  Copyright © 2019 whiznic. All rights reserved.
//

#import <UIKit/UIKit.h>

#define SocialIconCellId @"SocialIconCellId"


@interface SocialIconCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIButton *btnFaceBook;
@property (strong, nonatomic) IBOutlet UIButton *btnGoogle;
@property (strong, nonatomic) IBOutlet UIButton *btnInsta;

@end


