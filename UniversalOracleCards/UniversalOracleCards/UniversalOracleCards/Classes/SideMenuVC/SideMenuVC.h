//
//  SideMenuVC.h
//  Interchange
//
//  Created by whiznic on 1/6/18.
//  Copyright © 2018 com.production. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SideMenuTblVw : UITableView

@end

@class LanguageStrings;

@interface SideMenuVC : UIViewController
<UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) LanguageStrings *langStrings;
@property (nonatomic, strong) NSMutableArray *arrMenuOptions ,*arrMainMenu, *arrMenuOptionID, *arrMenuIcon;
@property (nonatomic, strong) IBOutlet SideMenuTblVw *tblMenu;
@property (nonatomic, assign) BOOL isOpen;
@property (nonatomic, strong) NSDictionary *param;

+ (instancetype)sharedSideMenu;
- (instancetype)initWithNibName:(NSString *)nibNameOrNil
                         bundle:(NSBundle *)nibBundleOrNil;
+ (void)addToViewController:(UIViewController *)vC;
+ (void)remove;
+ (void)defaultVwFrame:(CGRect)frame;
- (void)show;
- (void)showWithTouches:(CGPoint)point;
- (void)hide;
-(void) reloadSideMenu;
@property (nonatomic, strong) UINavigationController *navi1;
@property (nonatomic, strong) IBOutlet UIWindow* window;
@property (strong, nonatomic) IBOutlet UIView *dsipalymenu;

@end

@interface TouchVw : UIView

@property (nonatomic, strong) SideMenuVC *slider;
@property (nonatomic, assign) BOOL shouldShowSlider;

@end
