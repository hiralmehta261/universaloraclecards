//
//  WebAPIDelegate.h
//  Interchange
//
//  Created by whiznic on 1/8/18.
//  Copyright © 2018 com.production. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WebAPIDelegate : NSObject <NSURLSessionTaskDelegate,NSURLSessionDataDelegate>

@property (nonatomic, strong) NSURLResponse *respHeader;
@property (nonatomic, strong) NSMutableData *conData;
@property(nonatomic, copy) void (^success)(NSData *dict, NSURLResponse *response);
@property(nonatomic, copy) void (^error)(NSError *error, NSURLResponse *response);

-(id)initWithCallBackSuccess:(void (^)(NSData *data, NSURLResponse *response))success
               callBackError:(void (^)(NSError *error, NSURLResponse *response))error;
@end
