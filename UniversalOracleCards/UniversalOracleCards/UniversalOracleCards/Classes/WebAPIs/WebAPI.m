//
//  WebAPI.m
//  Interchange
//
//  Created by whiznic on 1/8/18.
//  Copyright © 2018 com.production. All rights reserved.
//

#import "WebAPI.h"
#import "WebAPIDelegate.h"
#import "CommanMethods.h"
#import "AppDelegate.h"

@implementation WebAPI

+(NSMutableURLRequest *)makeRequest:(NSString*)url withHeaders:(NSDictionary *)keyVal{
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]];
    if (keyVal != nil) {
        for (NSString *key in [keyVal allKeys]) {
            [request setValue:[keyVal objectForKey:key] forHTTPHeaderField:key];
        }
    }
    return request;
}

+(void)callGET:(NSString*)url withQuery:(NSString *)query
   withHeaders:(NSDictionary *)keyVal
callBackSuccess:(void (^)(NSData *dict, NSURLResponse *response))success
 callBackError:(void (^)(NSError *error, NSURLResponse *response))error {
    
    if (![CommanMethods isNetworkRechable]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [AppDelegate removeVwLoading];
            [CommanMethods displayAlertMessageWithCurrentController:key_Informaction message:kInternetConnectionMessage currentRef:self];
        });
    }
    
    NSString *web_url = [[NSString stringWithFormat:@"%@%@",url,query] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSMutableURLRequest *request = [WebAPI makeRequest:web_url withHeaders:keyVal];
    [request setHTTPMethod:@"GET"];
    request.timeoutInterval = 100;
    NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration] delegate:[[WebAPIDelegate alloc] initWithCallBackSuccess:^(NSData *data, NSURLResponse *response) {
        success(data,response);
    } callBackError:^(NSError *err, NSURLResponse *response) {
        error(err,response);
    }] delegateQueue:nil];
    
    [[session dataTaskWithRequest:request] resume];
}

+(void)callPOSTWithParameter:(NSString*)url withParams:(NSMutableDictionary *)params
                 withHeaders:(NSDictionary *)keyVal
             callBackSuccess:(void (^)(NSData *dict, NSURLResponse *response))success
               callBackError:(void (^)(NSError *error, NSURLResponse *response))error{
    
    if (![CommanMethods isNetworkRechable]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [AppDelegate removeVwLoading];
            [CommanMethods displayAlertMessageWithCurrentController:key_Informaction message:kInternetConnectionMessage currentRef:self];
        });
    }
    
    NSString *boundary = @"--------------------------f98e103630d0f2c3";
    
    // post body
    NSMutableData *body = NSMutableData.data;
    NSArray *paramsAllKeys = params.allKeys;
    
    // add params (all params are strings) ////////////
    for (NSString *param in paramsAllKeys) {
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary]
                          dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param]
                          dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@\r\n", [params objectForKey:param]]
                          dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary]
                      dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    NSMutableDictionary *headers = NSMutableDictionary.new;
    [headers setObject:@"keep-alive" forKey:@"Connection"];
    [headers setObject:contentType forKey:@"Content-Type"];
    [headers setObject:[NSString stringWithFormat:@"%llu", (unsigned long long)body.length]
                forKey:@"Content-Length"];
   // [headers setObject:@"9f03af9e4c9d0da7a26af71c2a8da2da" forKey:@"api-key"];
    [headers setObject:[keyVal objectForKey:@"api-key"] forKey:@"api-key"];
    
    NSString *web_url = [url stringByAddingPercentEncodingWithAllowedCharacters:NSCharacterSet.URLFragmentAllowedCharacterSet];
    NSMutableURLRequest *request = [WebAPI makeRequest:web_url withHeaders:headers];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:body];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration] delegate:[[WebAPIDelegate alloc] initWithCallBackSuccess:^(NSData *data, NSURLResponse *response) {
        success(data,response);
    } callBackError:^(NSError *err, NSURLResponse *response) {
        error(err,response);
    }] delegateQueue:nil];
    
    [[session dataTaskWithRequest:request] resume];
}


+(void)callPOSTWithParameterRegisterImage:(NSString*)url withParams:(NSMutableDictionary *)params
                      withHeaders:(NSDictionary *)keyVal
                  callBackSuccess:(void (^)(NSData *dict, NSURLResponse *response))success
                    callBackError:(void (^)(NSError *error, NSURLResponse *response))error{
    
    if (![CommanMethods isNetworkRechable]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [AppDelegate removeVwLoading];
            [CommanMethods displayAlertMessageWithCurrentController:key_Informaction message:kInternetConnectionMessage currentRef:self];
        });
    }
    
    NSString *boundary = @"--------------------------f98e103630d0f2c3";
    
    // post body
    NSMutableData *body = NSMutableData.data;
    NSArray *paramsAllKeys = params.allKeys;
    
    // add params (all params are strings) ////////////
    for (NSString *param in paramsAllKeys) {
        if ([param isEqualToString:@"recording"] || [param isEqualToString:@"profile_image"] || [param isEqualToString:@"profile_img"]) {
            [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\";filename=\"cover123.mp3\"\r\n",param]
                              dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[@"Content-Type: image/jpg\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[NSData dataWithData:[params objectForKey:param]]];
            [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            
        }else {
            [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary]
                              dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param]
                              dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"%@\r\n", [params objectForKey:param]]
                              dataUsingEncoding:NSUTF8StringEncoding]];
        }
        
    }
    
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary]
                      dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    NSMutableDictionary *headers = NSMutableDictionary.new;
    [headers setObject:@"keep-alive" forKey:@"Connection"];
    [headers setObject:contentType forKey:@"Content-Type"];
    [headers setObject:[NSString stringWithFormat:@"%llu", (unsigned long long)body.length]
                forKey:@"Content-Length"];
    [headers setObject:[keyVal objectForKey:@"api-key"] forKey:@"api-key"];
    NSString *web_url = [url stringByAddingPercentEncodingWithAllowedCharacters:NSCharacterSet.URLFragmentAllowedCharacterSet];
    NSMutableURLRequest *request = [WebAPI makeRequest:web_url withHeaders:headers];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:body];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration] delegate:[[WebAPIDelegate alloc] initWithCallBackSuccess:^(NSData *data, NSURLResponse *response) {
        success(data,response);
    } callBackError:^(NSError *err, NSURLResponse *response) {
        error(err,response);
    }] delegateQueue:nil];
    
    [[session dataTaskWithRequest:request] resume];
}



+(void)callPOSTWithParameterImage:(NSString*)url withParams:(NSMutableDictionary *)params
                 withHeaders:(NSDictionary *)keyVal
             callBackSuccess:(void (^)(NSData *dict, NSURLResponse *response))success
               callBackError:(void (^)(NSError *error, NSURLResponse *response))error{
    
    if (![CommanMethods isNetworkRechable]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [AppDelegate removeVwLoading];
            [CommanMethods displayAlertMessageWithCurrentController:key_Informaction message:kInternetConnectionMessage currentRef:self];
        });
    }
    
    NSString *boundary = @"--------------------------f98e103630d0f2c3";
    
    // post body
    NSMutableData *body = NSMutableData.data;
    NSArray *paramsAllKeys = params.allKeys;
    
    // add params (all params are strings) ////////////
    for (NSString *param in paramsAllKeys) {
        if ([param isEqualToString:@"cover_image"] || [param isEqualToString:@"profile_image"] || [param isEqualToString:@"gallery_img"]) {
            [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\";filename=\"cover123.jpg\"\r\n",param]
                              dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[@"Content-Type: image/jpg\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[NSData dataWithData:[params objectForKey:param]]];
            [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];

        }else {
            [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary]
                              dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param]
                              dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"%@\r\n", [params objectForKey:param]]
                              dataUsingEncoding:NSUTF8StringEncoding]];
        }
        
    }
    
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary]
                      dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    NSMutableDictionary *headers = NSMutableDictionary.new;
    [headers setObject:@"keep-alive" forKey:@"Connection"];
    [headers setObject:contentType forKey:@"Content-Type"];
    [headers setObject:[NSString stringWithFormat:@"%llu", (unsigned long long)body.length]
                forKey:@"Content-Length"];
    
    NSString *web_url = [url stringByAddingPercentEncodingWithAllowedCharacters:NSCharacterSet.URLFragmentAllowedCharacterSet];
    NSMutableURLRequest *request = [WebAPI makeRequest:web_url withHeaders:headers];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:body];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration] delegate:[[WebAPIDelegate alloc] initWithCallBackSuccess:^(NSData *data, NSURLResponse *response) {
        success(data,response);
    } callBackError:^(NSError *err, NSURLResponse *response) {
        error(err,response);
    }] delegateQueue:nil];
    
    [[session dataTaskWithRequest:request] resume];
}

+(void)callPOSTWithParameterImageDict:(NSString*)url withParams:(NSMutableDictionary *)params dictImages:(NSMutableDictionary *)dictImages
                      withHeaders:(NSDictionary *)keyVal
                  callBackSuccess:(void (^)(NSData *dict, NSURLResponse *response))success
                    callBackError:(void (^)(NSError *error, NSURLResponse *response))error{
    
    if (![CommanMethods isNetworkRechable]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [AppDelegate removeVwLoading];
            [CommanMethods displayAlertMessageWithCurrentController:key_Informaction message:kInternetConnectionMessage currentRef:self];
        });
    }
    
    NSString *boundary = @"--------------------------f98e103630d0f2c3";
    
    // post body
    NSMutableData *body = NSMutableData.data;
    NSArray *paramsAllKeys = params.allKeys;
    
    //add attached images
    for (NSString *param in dictImages) {
        if ([param isEqualToString:@"profile_photo"]) {
            
            NSString *name;
            if (name.length == 0) {
                NSUInteger r = arc4random_uniform(25566);
                name = [NSString stringWithFormat:@"%lu",(unsigned long)r];
            }
            
            [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\";filename=\"%@cover.jpg\"\r\n",param,name]
                              dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[@"Content-Type: image/jpg\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[NSData dataWithData:[dictImages objectForKey:param]]];
            [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        } else {
            [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\";filename=\"cover.jpg\"\r\n",param]
                              dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[@"Content-Type: image/jpg\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[NSData dataWithData:[dictImages objectForKey:param]]];
            [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        }
    }
    
    // add params (all params are strings) ////////////
    for (NSString *param in paramsAllKeys) {
        if ([param isEqualToString:@"cover_image"] || [param isEqualToString:@"profile_image"]) {
            [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\";filename=\"cover.jpg\"\r\n",param]
                              dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[@"Content-Type: image/jpg\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[NSData dataWithData:[params objectForKey:param]]];
            [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            
        }else {
            [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary]
                              dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param]
                              dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"%@\r\n", [params objectForKey:param]]
                              dataUsingEncoding:NSUTF8StringEncoding]];
        }
        
    }
    
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary]
                      dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    NSMutableDictionary *headers = NSMutableDictionary.new;
    [headers setObject:@"keep-alive" forKey:@"Connection"];
    [headers setObject:contentType forKey:@"Content-Type"];
    [headers setObject:[NSString stringWithFormat:@"%llu", (unsigned long long)body.length]
                forKey:@"Content-Length"];
    
    NSString *web_url = [url stringByAddingPercentEncodingWithAllowedCharacters:NSCharacterSet.URLFragmentAllowedCharacterSet];
    NSMutableURLRequest *request = [WebAPI makeRequest:web_url withHeaders:headers];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:body];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration] delegate:[[WebAPIDelegate alloc] initWithCallBackSuccess:^(NSData *data, NSURLResponse *response) {
        success(data,response);
    } callBackError:^(NSError *err, NSURLResponse *response) {
        error(err,response);
    }] delegateQueue:nil];
    
    [[session dataTaskWithRequest:request] resume];
}

+(void)callPOSTWithParameterImage1:(NSString*)url withParams:(NSMutableDictionary *)params
                            imageDict:(NSMutableDictionary *)imageDict
                              fileName:(NSString *)fileName
                           withHeaders:(NSDictionary *)keyVal
                       callBackSuccess:(void (^)(NSData *dict, NSURLResponse *response))success
                         callBackError:(void (^)(NSError *error, NSURLResponse *response))error{
    
    if (![CommanMethods isNetworkRechable]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [AppDelegate removeVwLoading];
            [CommanMethods displayAlertMessageWithCurrentController:key_Informaction message:kInternetConnectionMessage currentRef:self];
        });
    }
    
    NSString *boundary = @"--------------------------f98e103630d0f2c3";
    
    // post body
    NSMutableData *body = NSMutableData.data;
    NSArray *paramsAllKeys = params.allKeys;
    
    //add attached images
    for (NSString *param in imageDict) {
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\";filename=\"cover.jpg\"\r\n",param]
                          dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"Content-Type: image/jpg\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[NSData dataWithData:[imageDict objectForKey:param]]];
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    // add params (all params are strings) ////////////
    for (NSString *param in paramsAllKeys) {
        if ([param isEqualToString:@"profile_photo"]) {
            [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\";filename=\"%@\"\r\n",param,fileName]
                              dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[@"Content-Type: image/png\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[NSData dataWithData:[params objectForKey:param]]];
            [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            
        }else {
            [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary]
                              dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param]
                              dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"%@\r\n", [params objectForKey:param]]
                              dataUsingEncoding:NSUTF8StringEncoding]];
        }
    }
    
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary]
                      dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    NSMutableDictionary *headers = NSMutableDictionary.new;
    [headers setObject:@"keep-alive" forKey:@"Connection"];
    [headers setObject:contentType forKey:@"Content-Type"];
    [headers setObject:[NSString stringWithFormat:@"%llu", (unsigned long long)body.length]
                forKey:@"Content-Length"];
    
    NSString *web_url = [url stringByAddingPercentEncodingWithAllowedCharacters:NSCharacterSet.URLFragmentAllowedCharacterSet];
    NSMutableURLRequest *request = [WebAPI makeRequest:web_url withHeaders:headers];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:body];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration] delegate:[[WebAPIDelegate alloc] initWithCallBackSuccess:^(NSData *data, NSURLResponse *response) {
        success(data,response);
    } callBackError:^(NSError *err, NSURLResponse *response) {
        error(err,response);
    }] delegateQueue:nil];
    
    [[session dataTaskWithRequest:request] resume];
}

+(void)callPOSTWithParameterAttachment:(NSString*)url withParams:(NSMutableDictionary *)params
                              fileName:(NSString *)fileName
                      withHeaders:(NSDictionary *)keyVal
                  callBackSuccess:(void (^)(NSData *dict, NSURLResponse *response))success
                    callBackError:(void (^)(NSError *error, NSURLResponse *response))error{
    
    if (![CommanMethods isNetworkRechable]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [AppDelegate removeVwLoading];
            [CommanMethods displayAlertMessageWithCurrentController:key_Informaction message:kInternetConnectionMessage currentRef:self];
        });
    }
    
    NSString *boundary = @"--------------------------f98e103630d0f2c3";
    
    // post body
    NSMutableData *body = NSMutableData.data;
    NSArray *paramsAllKeys = params.allKeys;
    
    // add params (all params are strings) ////////////
    for (NSString *param in paramsAllKeys) {
        if ([param isEqualToString:@"content"]) {
            [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\";filename=\"%@\"\r\n",@"content",fileName]
                              dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[@"scope: crmapi\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[NSData dataWithData:[params objectForKey:param]]];
            [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            
        }else {
            [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary]
                              dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param]
                              dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"%@\r\n", [params objectForKey:param]]
                              dataUsingEncoding:NSUTF8StringEncoding]];
        }
        
    }
    
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary]
                      dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    NSMutableDictionary *headers = NSMutableDictionary.new;
    [headers setObject:@"keep-alive" forKey:@"Connection"];
    [headers setObject:contentType forKey:@"Content-Type"];
    [headers setObject:[NSString stringWithFormat:@"%llu", (unsigned long long)body.length]
                forKey:@"Content-Length"];
    
    NSString *web_url = [url stringByAddingPercentEncodingWithAllowedCharacters:NSCharacterSet.URLFragmentAllowedCharacterSet];
    NSMutableURLRequest *request = [WebAPI makeRequest:web_url withHeaders:headers];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:body];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration] delegate:[[WebAPIDelegate alloc] initWithCallBackSuccess:^(NSData *data, NSURLResponse *response) {
        success(data,response);
    } callBackError:^(NSError *err, NSURLResponse *response) {
        error(err,response);
    }] delegateQueue:nil];
    
    [[session dataTaskWithRequest:request] resume];
}



+(void)callPOSTWithParameterPhoto:(NSString*)url withParams:(NSMutableDictionary *)params
                         fileName:(NSString *)fileName
                      withHeaders:(NSDictionary *)keyVal
                  callBackSuccess:(void (^)(NSData *dict, NSURLResponse *response))success
                    callBackError:(void (^)(NSError *error, NSURLResponse *response))error
{
    
    if (![CommanMethods isNetworkRechable]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [AppDelegate removeVwLoading];
            [CommanMethods displayAlertMessageWithCurrentController:key_Informaction message:kInternetConnectionMessage currentRef:self];
        });
    }
    
    NSString *boundary = @"--------------------------f98e103630d0f2c3";
    
    // post body
    NSMutableData *body = NSMutableData.data;
    NSArray *paramsAllKeys = params.allKeys;
    
    // add params (all params are strings) ////////////
    for (NSString *param in paramsAllKeys) {
        if ([param isEqualToString:@"file"]) {
            [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\";filename=\"%@\"\r\n",@"file",fileName]
                              dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[@"scope: crmapi\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[NSData dataWithData:[params objectForKey:param]]];
            [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            
        }else {
            [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary]
                              dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param]
                              dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"%@\r\n", [params objectForKey:param]]
                              dataUsingEncoding:NSUTF8StringEncoding]];
        }
        
    }
    
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary]
                      dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    NSMutableDictionary *headers = NSMutableDictionary.new;
    [headers setObject:@"keep-alive" forKey:@"Connection"];
    [headers setObject:contentType forKey:@"Content-Type"];
    [headers setObject:[keyVal valueForKey:@"authtoken"] forKey:@"Authorization"];
    [headers setObject:[NSString stringWithFormat:@"%llu", (unsigned long long)body.length]
                forKey:@"Content-Length"];
    
    NSString *web_url = [url stringByAddingPercentEncodingWithAllowedCharacters:NSCharacterSet.URLFragmentAllowedCharacterSet];
    NSMutableURLRequest *request = [WebAPI makeRequest:web_url withHeaders:headers];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:body];

    NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration] delegate:[[WebAPIDelegate alloc] initWithCallBackSuccess:^(NSData *data, NSURLResponse *response) {
        success(data,response);
    } callBackError:^(NSError *err, NSURLResponse *response) {
        error(err,response);
    }] delegateQueue:nil];
    
    [[session dataTaskWithRequest:request] resume];
}


+(void)callPOSTWithParameterAudioFile:(NSString*)url withParams:(NSMutableDictionary *)params
                          withHeaders:(NSDictionary *)keyVal
                      callBackSuccess:(void (^)(NSData *dict, NSURLResponse *response))success
                        callBackError:(void (^)(NSError *error, NSURLResponse *response))error{
    
    if (![CommanMethods isNetworkRechable]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [AppDelegate removeVwLoading];
            [CommanMethods displayAlertMessageWithCurrentController:key_Informaction message:kInternetConnectionMessage currentRef:self];
        });
    }
    
    NSString *boundary = @"--------------------------f98e103630d0f2c3";
    
    // post body
    NSMutableData *body = NSMutableData.data;
    NSArray *paramsAllKeys = params.allKeys;
    
    // add params (all params are strings) ////////////
    for (NSString *param in paramsAllKeys) {
        if ([param isEqualToString:@"recording"]) {
            [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\";filename=\"test.m4a\"\r\n",param]
                              dataUsingEncoding:NSUTF8StringEncoding]];
            //[body appendData:[@"scope: crmapi\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[@"Content-Type: audio/m4a\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[NSData dataWithData:[params objectForKey:param]]];
            [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            
        }else {
            [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary]
                              dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param]
                              dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"%@\r\n", [params objectForKey:param]]
                              dataUsingEncoding:NSUTF8StringEncoding]];
        }
        
    }
    
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary]
                      dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    NSMutableDictionary *headers = NSMutableDictionary.new;
    [headers setObject:@"keep-alive" forKey:@"Connection"];
    [headers setObject:contentType forKey:@"Content-Type"];
    [headers setObject:[NSString stringWithFormat:@"%llu", (unsigned long long)body.length]
                forKey:@"Content-Length"];
    // [headers setObject:@"9f03af9e4c9d0da7a26af71c2a8da2da" forKey:@"api-key"];
    [headers setObject:[keyVal objectForKey:@"api-key"] forKey:@"api-key"];
    
    NSString *web_url = [url stringByAddingPercentEncodingWithAllowedCharacters:NSCharacterSet.URLFragmentAllowedCharacterSet];
    NSMutableURLRequest *request = [WebAPI makeRequest:web_url withHeaders:headers];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:body];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration] delegate:[[WebAPIDelegate alloc] initWithCallBackSuccess:^(NSData *data, NSURLResponse *response) {
        success(data,response);
    } callBackError:^(NSError *err, NSURLResponse *response) {
        error(err,response);
    }] delegateQueue:nil];
    
    [[session dataTaskWithRequest:request] resume];
}
@end
