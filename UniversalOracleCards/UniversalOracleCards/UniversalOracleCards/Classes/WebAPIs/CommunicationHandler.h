//
//  CommunicationHandler.h
//  Interchange
//
//  Created by whiznic on 1/8/18.
//  Copyright © 2018 com.production. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CommunicationHandler : NSObject

+(void) callForLogin:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
               Error:(void (^)(NSError *error, NSURLResponse *response))error;

+(void) callForRegister:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
               Error:(void (^)(NSError *error, NSURLResponse *response))error;

+(void) callForSaveReading:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                  Error:(void (^)(NSError *error, NSURLResponse *response))error;

+(void) callForGetNotesList:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                     Error:(void (^)(NSError *error, NSURLResponse *response))error;

+(void) callForDeleteNotes:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                      Error:(void (^)(NSError *error, NSURLResponse *response))error;


+(void) callForUpdateNotes:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                     Error:(void (^)(NSError *error, NSURLResponse *response))error;



+(void) callForSaveAudio:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                     Error:(void (^)(NSError *error, NSURLResponse *response))error;


+(void) callForGetMyJournalList:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                     Error:(void (^)(NSError *error, NSURLResponse *response))error;

+(void) callForGetMySpreadList:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                          Error:(void (^)(NSError *error, NSURLResponse *response))error;

+(void) callForDeleteSpread:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                         Error:(void (^)(NSError *error, NSURLResponse *response))error;


+(void) callForDeleteJournal:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                          Error:(void (^)(NSError *error, NSURLResponse *response))error;

+(void) callForCreateSpread:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                       Error:(void (^)(NSError *error, NSURLResponse *response))error;

+(void) callForSaveNotes:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                     Error:(void (^)(NSError *error, NSURLResponse *response))error;

+(void) callForVerifyOTP:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
               Error:(void (^)(NSError *error, NSURLResponse *response))error;

+(void) callForResetPassword:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                   Error:(void (^)(NSError *error, NSURLResponse *response))error;

+(void) callForChangePassword:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                       Error:(void (^)(NSError *error, NSURLResponse *response))error;

+(void) callForUpdateProfile:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                    Error:(void (^)(NSError *error, NSURLResponse *response))error;

+(void) callForRemoveFacebook:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                       Error:(void (^)(NSError *error, NSURLResponse *response))error;

+(void) callForRemoveGoogle:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                       Error:(void (^)(NSError *error, NSURLResponse *response))error;

+(void) callForGetDetails:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                     Error:(void (^)(NSError *error, NSURLResponse *response))error;

+(void) callForLogout:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                Error:(void (^)(NSError *error, NSURLResponse *response))error;

+(void) callForTotalCount:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                Error:(void (^)(NSError *error, NSURLResponse *response))error;

+(void) callForFriendList:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                    Error:(void (^)(NSError *error, NSURLResponse *response))error;

+(void) callForGetData:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                    Error:(void (^)(NSError *error, NSURLResponse *response))error;

+(void) callForGroupChat:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                 Error:(void (^)(NSError *error, NSURLResponse *response))error;

+(void) callForViewProfile:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                     Error:(void (^)(NSError *error, NSURLResponse *response))error;

+(void) callForChangePassword1:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                        Error:(void (^)(NSError *error, NSURLResponse *response))error;

+(void) callForNotification:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                        Error:(void (^)(NSError *error, NSURLResponse *response))error;

+(void) callForPrivacySetting:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                      Error:(void (^)(NSError *error, NSURLResponse *response))error;

+(void) callForBlockUser:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                        Error:(void (^)(NSError *error, NSURLResponse *response))error;

+(void) callForContactUs:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                   Error:(void (^)(NSError *error, NSURLResponse *response))error;

+(void) callForReportAbuse:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                   Error:(void (^)(NSError *error, NSURLResponse *response))error;

+(void) callForMessageList:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                     Error:(void (^)(NSError *error, NSURLResponse *response))error;

+(void) callForWinkList:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                     Error:(void (^)(NSError *error, NSURLResponse *response))error;

+(void) callForWink:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                     Error:(void (^)(NSError *error, NSURLResponse *response))error;

+(void) callForSendFriendRequest:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
              Error:(void (^)(NSError *error, NSURLResponse *response))error;

+(void) callForAddToFavorite:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                           Error:(void (^)(NSError *error, NSURLResponse *response))error;

+(void) callForSendMessage:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                       Error:(void (^)(NSError *error, NSURLResponse *response))error;
+(void) callForMatchAlert:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                     Error:(void (^)(NSError *error, NSURLResponse *response))error;

+(void) callForNewMemberList:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                    Error:(void (^)(NSError *error, NSURLResponse *response))error;

+(void) callForVirtualGiftList:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                       Error:(void (^)(NSError *error, NSURLResponse *response))error;

+(void) callForOnlineList:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                         Error:(void (^)(NSError *error, NSURLResponse *response))error;

+(void) callForMatchList:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                    Error:(void (^)(NSError *error, NSURLResponse *response))error;

+(void) callForCountryList:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                    Error:(void (^)(NSError *error, NSURLResponse *response))error;

+(void) callForEditProfile:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                     Error:(void (^)(NSError *error, NSURLResponse *response))error;

+(void) callForGetCityData:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                     Error:(void (^)(NSError *error, NSURLResponse *response))error;

+(void) callForSearchByUsername:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                     Error:(void (^)(NSError *error, NSURLResponse *response))error;

+(void) callForSearchByZipcode:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                          Error:(void (^)(NSError *error, NSURLResponse *response))error;

+(void) callForEmailExist:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                         Error:(void (^)(NSError *error, NSURLResponse *response))error;

+(void) callForGalleries:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                    Error:(void (^)(NSError *error, NSURLResponse *response))error;

+(void) callForAddImagesToGalleries:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                              Error:(void (^)(NSError *error, NSURLResponse *response))error;

+(void) callForAddUser:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                              Error:(void (^)(NSError *error, NSURLResponse *response))error;

+(void) callForSearchList:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                     Error:(void (^)(NSError *error, NSURLResponse *response))error;

+(void) callForForgotPassword:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                        Error:(void (^)(NSError *error, NSURLResponse *response))error;

+(void) callForFavoriteList:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                        Error:(void (^)(NSError *error, NSURLResponse *response))error;

+(void) callForMenuList:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                  Error:(void (^)(NSError *error, NSURLResponse *response))error;

+(void) callForAddEditProfile:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                        Error:(void (^)(NSError *error, NSURLResponse *response))error;

+(void) callForGetProfileList:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                        Error:(void (^)(NSError *error, NSURLResponse *response))error;

+(void) callForGetProfileDetail:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                          Error:(void (^)(NSError *error, NSURLResponse *response))error;

+(void) callForDeleteProfile:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                       Error:(void (^)(NSError *error, NSURLResponse *response))error;

+(void) callForAddStaff:(NSMutableDictionary *)paramDict imageDict:(NSMutableDictionary *)imageDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                  Error:(void (^)(NSError *error, NSURLResponse *response))error;

+(void) callForGetUserTypeList:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                         Error:(void (^)(NSError *error, NSURLResponse *response))error;

+(void) callForGetStaffList:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                         Error:(void (^)(NSError *error, NSURLResponse *response))error;

+(void) callForDeleteStaff:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                     Error:(void (^)(NSError *error, NSURLResponse *response))error;

+(void) callForUpdateStaff:(NSMutableDictionary *)paramDict imageDict:(NSMutableDictionary *)imageDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                     Error:(void (^)(NSError *error, NSURLResponse *response))error;

+(void) callForGetParticipantList:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                            Error:(void (^)(NSError *error, NSURLResponse *response))error;

+(void) callForGetTeamCoachList:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                            Error:(void (^)(NSError *error, NSURLResponse *response))error;

+(void) callForGetHealthAlertList:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                            Error:(void (^)(NSError *error, NSURLResponse *response))error;

+(void) callForDeleteHealthAlert:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                           Error:(void (^)(NSError *error, NSURLResponse *response))error;

+(void) callForAddEditHealthAlert:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                            Error:(void (^)(NSError *error, NSURLResponse *response))error;

+(void) callForSearchParticipant:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                           Error:(void (^)(NSError *error, NSURLResponse *response))error;

+(void) callForSearchTeamCoach:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                           Error:(void (^)(NSError *error, NSURLResponse *response))error;

+(void) callForGetParticipantFullDetail:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                                  Error:(void (^)(NSError *error, NSURLResponse *response))error;

+(void) callForGetTeamCoachFullDetail:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                                  Error:(void (^)(NSError *error, NSURLResponse *response))error;

+(void) callForGetNoteList:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                     Error:(void (^)(NSError *error, NSURLResponse *response))error;

+(void) callForGetNoteAttachmentsList:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                                Error:(void (^)(NSError *error, NSURLResponse *response))error;

+(void) callForAddNote:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                 Error:(void (^)(NSError *error, NSURLResponse *response))error;

+(void) callForEditNote:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                  Error:(void (^)(NSError *error, NSURLResponse *response))error;

+(void) callForDeleteNote:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                    Error:(void (^)(NSError *error, NSURLResponse *response))error;

+(void) callForGetAssessmentList:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                           Error:(void (^)(NSError *error, NSURLResponse *response))error;

+(void) callForCreateAssessment:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                          Error:(void (^)(NSError *error, NSURLResponse *response))error;

+(void) callForEditAssessment:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                        Error:(void (^)(NSError *error, NSURLResponse *response))error;

+(void) callForDeleteAssessment:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                          Error:(void (^)(NSError *error, NSURLResponse *response))error;

+(void) callForGetAssessmentDetail:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                             Error:(void (^)(NSError *error, NSURLResponse *response))error;

+(void) callForAddEditLearningInfo:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                             Error:(void (^)(NSError *error, NSURLResponse *response))error;

+(void) callForAddEditAssessmentSubDetail:(NSMutableDictionary *)paramDict apiName:(NSString *)apiName Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                          Error:(void (^)(NSError *error, NSURLResponse *response))error;

+(void) callForGetStaffUserDetails:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                             Error:(void (^)(NSError *error, NSURLResponse *response))error;

+(void) callForUpdateStaffPersonalProfile:(NSMutableDictionary *)paramDict imageDict:(NSMutableDictionary *)imageDict fileName:(NSString *)fileName Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                                    Error:(void (^)(NSError *error, NSURLResponse *response))error;

+(void) callForGetAttachmentsList:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                            Error:(void (^)(NSError *error, NSURLResponse *response))error;

+(void) callForDeleteAttachment:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                          Error:(void (^)(NSError *error, NSURLResponse *response))error;

+(void) callForUploadAttachment:(NSMutableDictionary *)paramDict fileName:(NSString *)fileName Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                          Error:(void (^)(NSError *error, NSURLResponse *response))error;

+(void) callForUploadPhoto:(NSMutableDictionary *)paramDict fileName:(NSString *)fileName  strId:(NSString *)strId  Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                          Error:(void (^)(NSError *error, NSURLResponse *response))error;



+(void) callForGetCarerList:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                      Error:(void (^)(NSError *error, NSURLResponse *response))error;

+(void) callForDeleteCarer:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                     Error:(void (^)(NSError *error, NSURLResponse *response))error;

+(void) callForAddEditCarer:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                   Error:(void (^)(NSError *error, NSURLResponse *response))error;

+(void) callForGetPersonalCareList:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                             Error:(void (^)(NSError *error, NSURLResponse *response))error;

+(void) callForAddEditPersonalCare:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                                Error:(void (^)(NSError *error, NSURLResponse *response))error;

+(void) callForDeletePersonalCare:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                               Error:(void (^)(NSError *error, NSURLResponse *response))error;

+(void) callForGetConcernList:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                        Error:(void (^)(NSError *error, NSURLResponse *response))error;

+(void) callForAddEditConcern:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                        Error:(void (^)(NSError *error, NSURLResponse *response))error;

+(void) callForDeleteConcern:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                       Error:(void (^)(NSError *error, NSURLResponse *response))error;

+(void) callForGetConfigData:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                       Error:(void (^)(NSError *error, NSURLResponse *response))error;

+(void) callForGetDynamicData:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                        Error:(void (^)(NSError *error, NSURLResponse *response))error;

+(void) callForUpdateParticipantEmergencyInfo:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                                        Error:(void (^)(NSError *error, NSURLResponse *response))error;

+(void) callForUpdateParticipantHealthInfo:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                                     Error:(void (^)(NSError *error, NSURLResponse *response))error;

+(void) callForUpdateParticipantPersonalInfo:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                                       Error:(void (^)(NSError *error, NSURLResponse *response))error;

+(void) callForGetKnownDoctorList:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                            Error:(void (^)(NSError *error, NSURLResponse *response))error;


+(void) callForUploadPhotoEmployee:(NSMutableDictionary *)paramDict fileName:(NSString *)fileName strId:(NSString *)strId Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                             Error:(void (^)(NSError *error, NSURLResponse *response))error;
@end
