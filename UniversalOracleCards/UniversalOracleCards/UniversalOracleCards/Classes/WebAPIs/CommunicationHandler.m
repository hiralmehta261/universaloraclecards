//
//  CommunicationHandler.m
//  Interchange
//
//  Created by whiznic on 1/8/18.
//  Copyright © 2018 com.production. All rights reserved.
//

#import "CommunicationHandler.h"
#import "WebAPI.h"
#import "NSDictionary(GSM).h"

#define APP_NAME @"EXIST"
#define LOGIN_URL @"login"
#define LOGOUT_URL @"logout"
#define CHANGE_PASSWORD_URL @"change_password"
#define FORGOT_PASSWORD_URL @"forgot_password"
#define MENU_LIST_URL @"getmenulist"
#define ADD_EDIT_PROFILE_URL @"addeditprofile"
#define GET_PROFILE_LIST_URL @"getprofilelist"
#define GET_PROFILE_DETAIL_URL @"get_user_profile_settings"
#define DELETE_PROFILE_URL @"delete_user_profile"
#define ADD_STAFF_URL @"addeditEmployee"
#define GET_USER_TYPE_LIST_URL @"get_user_type_list"
#define GET_ADD_EDIT_HEALTH_URL @"addeditHealth_Alert"

//#define GET_EMPLOYEE_LIST_URL @"list_staff"
#define GET_EMPLOYEE_LIST_URL @"employee_list"

#define DELETE_STAFF_URL @"delete_staff_profile"
#define DELETE_HEALTH_ALERT @"delete_Health_Alert"
#define DELETE_employee @"delete_employee"


#define UPDATE_STAFF_URL @"update_staff_profile"
#define GET_PARTICIPANT_LIST_URL @"participate_quick_view"
#define GET_HealthAlert_list_URL @"HealthAlert_list"
#define SEARCH_PARTICIPANT_URL @"participate_search"
#define SEARCH_TEAMCOACH_URL @"teamcoach_search"
#define GET_PARTICIPANT_FULL_DETAIL_URL @"participate_full_details"
#define GET_TEAMCOACH_FULL_DETAIL_URL @"view_tc_profile"
#define GET_PARTICIPANT_NOTE_LIST_URL @"participate_notes_list"
#define GET_PARTICIPANT_NOTE_ATTACHMENT_LIST_URL @"participate_attachments_list"
#define ADD_NOTE_URL @"addNotes"
#define EDIT_NOTE_URL @"editNotes"
#define DELETE_NOTE_URL @"deleteNotes"
#define GET_ASSESSMENT_LIST_URL @"participate_assessment_view"
#define ADD_ASSESSMENT_URL @"add_assessment"
#define EDIT_ASSESSMENT_URL @"edit_assessment"
#define DELETE_ASSESSMENT_URL @"delete_assessment"
#define GET_ASSESSMENT_DETAIL_URL @"view_assessment"
#define ADD_EDIT_LEARNING_APPLYING_KNOWLEDGE_URL @"Learning_applying_knowledge"
#define GET_STAFF_PERSONAL_PROFILE_URL @"get_staff_personal_profile"
#define UPDATE_STAFF_PERSONAL_PROFILE_URL @"staff_personal_profile_update"
#define GET_ATTACHMENT_LIST_URL @"participate_attachments_list"
#define DELETE_ATTACHMENT_URL @"delete_attachment"
#define CARER_LIST_URL @"carers_list"
#define DELETE_CARER_URL @"delete_carers"
#define ADD_EDIT_CARER_URL @"addeditCarers"
#define GET_PERSONAL_CARE_LIST_URL @"PersonalCare_list"
#define ADD_EDIT_PERSONAL_CARE_URL @"addeditPersonal_Care"
#define DELETE_PERSONAL_CARE_URL @"delete_Personal_Care"
#define GET_CONCERN_LIST_URL @"ConcernCarer_list"
#define ADD_EDIT_CONCERN_URL @"addeditConcern_Carer"
#define DELETE_CONCERN_URL @"delete_Concern_Carer"
#define GET_CONFIG_DATA_URL @"config_data"
#define UPDATE_PARTICIPANT_EMERGENCY_INFO @"updateEmergencyInfo"
#define UPDATE_PARTICIPANT_HEALTH_INFO @"updateHelthInfo"
#define UPDATE_PARTICIPANT_PERSONAL_INFO @"updatePersonalInfo"
#define GET_KNOWN_DOCTOR_LIST_URL @"doctor_list"
#define GET_TEAM_COACH_LIST_URL @"user_tc_quick_view"

@implementation CommunicationHandler

+(void) callForLogin:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                      Error:(void (^)(NSError *error, NSURLResponse *response))error {
    
    NSMutableDictionary *headersKey = NSMutableDictionary.new;
    [headersKey setObject:@"9f03af9e4c9d0da7a26af71c2a8da2da" forKey:@"api-key"];
    NSString *url = @"http://surfportable.com/Universal-Oracle-Cards/index.php/SignIn";
    
    /*[WebAPI callPOSTWithParameter:url withParams:paramDict withHeaders:nil callBackSuccess:^(NSData *dict, NSURLResponse *response) {
        NSString* myString;
        myString = [[NSString alloc] initWithData:dict encoding:NSASCIIStringEncoding];
        NSLog(@"data in string = %@",myString);
        
        if (myString.length > 0) {
            myString = [myString substringWithRange:NSMakeRange(1, myString.length-2)];
        }
        
        NSError *err = nil;
        NSArray *array = [NSJSONSerialization JSONObjectWithData:[myString dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&err];
        NSDictionary *dictionary = [array objectAtIndex:0];
        
        success(dictionary.dictByReplacingNulls,response);
    } callBackError:^(NSError *err, NSURLResponse *response) {
        error(err,response);
    }];*/
    [WebAPI callPOSTWithParameter:url withParams:paramDict withHeaders:headersKey callBackSuccess:^(NSData *dict, NSURLResponse *response) {
        NSError *error = nil;
        NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:dict
                                                                     options:NSJSONReadingAllowFragments
                                                                       error:&error];
        success(dictResponse.dictByReplacingNulls,response);
    } callBackError:^(NSError *err, NSURLResponse *response) {
        error(err,response);
    }];
}

+(void) callForRegister:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                  Error:(void (^)(NSError *error, NSURLResponse *response))error {
    NSMutableDictionary *headersKey = NSMutableDictionary.new;
    [headersKey setObject:@"9f03af9e4c9d0da7a26af71c2a8da2da" forKey:@"api-key"];
    NSString *url = @"http://surfportable.com/Universal-Oracle-Cards/index.php/SignUp";
    
    /*[WebAPI callPOSTWithParameter:url withParams:paramDict withHeaders:nil callBackSuccess:^(NSData *dict, NSURLResponse *response) {
        NSLog(@" response :- %@", response);
        
        NSString* myString;
        myString = [[NSString alloc] initWithData:dict encoding:NSASCIIStringEncoding];
        NSLog(@"data in string = %@",myString);
        
        NSError *err = nil;
        NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:[myString dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&err];
        
        success(dictionary.dictByReplacingNulls,response);
    } callBackError:^(NSError *err, NSURLResponse *response) {
        error(err,response);
    }];*/
    [WebAPI callPOSTWithParameter:url withParams:paramDict withHeaders:headersKey callBackSuccess:^(NSData *dict, NSURLResponse *response) {
        NSError *error = nil;
        NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:dict
                                                                     options:NSJSONReadingAllowFragments
                                                                       error:&error];
        success(dictResponse.dictByReplacingNulls,response);
    } callBackError:^(NSError *err, NSURLResponse *response) {
        error(err,response);
    }];
}

+(void) callForForgotPassword:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                        Error:(void (^)(NSError *error, NSURLResponse *response))error {
    NSMutableDictionary *headersKey = NSMutableDictionary.new;
    [headersKey setObject:@"9f03af9e4c9d0da7a26af71c2a8da2da" forKey:@"api-key"];
    NSString *url = @"http://surfportable.com/Universal-Oracle-Cards/index.php/SignIn/forgotPassword";
    
    /*[WebAPI callPOSTWithParameter:url withParams:paramDict withHeaders:nil callBackSuccess:^(NSData *dict, NSURLResponse *response) {
     NSLog(@" response :- %@", response);
     
     NSString* myString;
     myString = [[NSString alloc] initWithData:dict encoding:NSASCIIStringEncoding];
     NSLog(@"data in string = %@",myString);
     
     NSError *err = nil;
     NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:[myString dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&err];
     
     success(dictionary.dictByReplacingNulls,response);
     } callBackError:^(NSError *err, NSURLResponse *response) {
     error(err,response);
     }];*/
    [WebAPI callPOSTWithParameter:url withParams:paramDict withHeaders:headersKey callBackSuccess:^(NSData *dict, NSURLResponse *response) {
        NSError *error = nil;
        NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:dict
                                                                     options:NSJSONReadingAllowFragments
                                                                       error:&error];
        success(dictResponse.dictByReplacingNulls,response);
    } callBackError:^(NSError *err, NSURLResponse *response) {
        error(err,response);
    }];
}

+(void) callForVerifyOTP:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                        Error:(void (^)(NSError *error, NSURLResponse *response))error {
    NSMutableDictionary *headersKey = NSMutableDictionary.new;
    [headersKey setObject:@"9f03af9e4c9d0da7a26af71c2a8da2da" forKey:@"api-key"];
    NSString *url = @"http://surfportable.com/Universal-Oracle-Cards/index.php/SignIn/verifyPassword";
    
    /*[WebAPI callPOSTWithParameter:url withParams:paramDict withHeaders:nil callBackSuccess:^(NSData *dict, NSURLResponse *response) {
     NSLog(@" response :- %@", response);
     
     NSString* myString;
     myString = [[NSString alloc] initWithData:dict encoding:NSASCIIStringEncoding];
     NSLog(@"data in string = %@",myString);
     
     NSError *err = nil;
     NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:[myString dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&err];
     
     success(dictionary.dictByReplacingNulls,response);
     } callBackError:^(NSError *err, NSURLResponse *response) {
     error(err,response);
     }];*/
    [WebAPI callPOSTWithParameter:url withParams:paramDict withHeaders:headersKey callBackSuccess:^(NSData *dict, NSURLResponse *response) {
        NSError *error = nil;
        NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:dict
                                                                     options:NSJSONReadingAllowFragments
                                                                       error:&error];
        success(dictResponse.dictByReplacingNulls,response);
    } callBackError:^(NSError *err, NSURLResponse *response) {
        error(err,response);
    }];
}

+(void) callForResetPassword:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                        Error:(void (^)(NSError *error, NSURLResponse *response))error {
    NSMutableDictionary *headersKey = NSMutableDictionary.new;
    [headersKey setObject:@"9f03af9e4c9d0da7a26af71c2a8da2da" forKey:@"api-key"];
    NSString *url = @"http://surfportable.com/Universal-Oracle-Cards/index.php/SignIn/resetPassword";
    
    /*[WebAPI callPOSTWithParameter:url withParams:paramDict withHeaders:nil callBackSuccess:^(NSData *dict, NSURLResponse *response) {
     NSLog(@" response :- %@", response);
     
     NSString* myString;
     myString = [[NSString alloc] initWithData:dict encoding:NSASCIIStringEncoding];
     NSLog(@"data in string = %@",myString);
     
     NSError *err = nil;
     NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:[myString dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&err];
     
     success(dictionary.dictByReplacingNulls,response);
     } callBackError:^(NSError *err, NSURLResponse *response) {
     error(err,response);
     }];*/
    [WebAPI callPOSTWithParameter:url withParams:paramDict withHeaders:headersKey callBackSuccess:^(NSData *dict, NSURLResponse *response) {
        NSError *error = nil;
        NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:dict
                                                                     options:NSJSONReadingAllowFragments
                                                                       error:&error];
        success(dictResponse.dictByReplacingNulls,response);
    } callBackError:^(NSError *err, NSURLResponse *response) {
        error(err,response);
    }];
}

+(void) callForChangePassword:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                       Error:(void (^)(NSError *error, NSURLResponse *response))error {
    NSMutableDictionary *headersKey = NSMutableDictionary.new;
    [headersKey setObject:@"9f03af9e4c9d0da7a26af71c2a8da2da" forKey:@"api-key"];
    NSString *url = @"http://surfportable.com/Universal-Oracle-Cards/index.php/SignIn/changePassword";
    
    /*[WebAPI callPOSTWithParameter:url withParams:paramDict withHeaders:nil callBackSuccess:^(NSData *dict, NSURLResponse *response) {
     NSLog(@" response :- %@", response);
     
     NSString* myString;
     myString = [[NSString alloc] initWithData:dict encoding:NSASCIIStringEncoding];
     NSLog(@"data in string = %@",myString);
     
     NSError *err = nil;
     NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:[myString dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&err];
     
     success(dictionary.dictByReplacingNulls,response);
     } callBackError:^(NSError *err, NSURLResponse *response) {
     error(err,response);
     }];*/
    [WebAPI callPOSTWithParameter:url withParams:paramDict withHeaders:headersKey callBackSuccess:^(NSData *dict, NSURLResponse *response) {
        NSError *error = nil;
        NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:dict
                                                                     options:NSJSONReadingAllowFragments
                                                                       error:&error];
        success(dictResponse.dictByReplacingNulls,response);
    } callBackError:^(NSError *err, NSURLResponse *response) {
        error(err,response);
    }];
}


+(void) callForGetDetails:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                     Error:(void (^)(NSError *error, NSURLResponse *response))error {
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *dictData = [userDefaults objectForKey:@"user_data"];
    NSMutableDictionary *headersKey = NSMutableDictionary.new;
    [headersKey setObject:[dictData objectForKey:@"api_key"] forKey:@"api-key"];
    NSString *url = @"http://surfportable.com/Universal-Oracle-Cards/index.php/Api/get_profile";
    
    /*[WebAPI callPOSTWithParameter:url withParams:paramDict withHeaders:nil callBackSuccess:^(NSData *dict, NSURLResponse *response) {
        NSString* myString;
        myString = [[NSString alloc] initWithData:dict encoding:NSASCIIStringEncoding];
        
        if (myString.length > 0) {
            myString = [myString substringWithRange:NSMakeRange(1, myString.length-2)];
        }
        
        NSError *err = nil;
        NSArray *array = [NSJSONSerialization JSONObjectWithData:[myString dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&err];
        NSDictionary *dictionary = [array objectAtIndex:0];
        
        success(dictionary.dictByReplacingNulls,response);
    } callBackError:^(NSError *err, NSURLResponse *response) {
        error(err,response);
    }];*/
    [WebAPI callPOSTWithParameter:url withParams:paramDict withHeaders:headersKey callBackSuccess:^(NSData *dict, NSURLResponse *response) {
     NSError *error = nil;
     NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:dict
     options:NSJSONReadingAllowFragments
     error:&error];
     success(dictResponse.dictByReplacingNulls,response);
     } callBackError:^(NSError *err, NSURLResponse *response) {
     error(err,response);
     }];
}

+(void) callForGetMyJournalList:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                    Error:(void (^)(NSError *error, NSURLResponse *response))error {
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *dictData = [userDefaults objectForKey:@"user_data"];
    NSMutableDictionary *headersKey = NSMutableDictionary.new;
    [headersKey setObject:[dictData objectForKey:@"api_key"] forKey:@"api-key"];
    NSString *url = @"http://surfportable.com/Universal-Oracle-Cards/index.php/Api/Reading_list";
    
    /*[WebAPI callPOSTWithParameter:url withParams:paramDict withHeaders:nil callBackSuccess:^(NSData *dict, NSURLResponse *response) {
     NSString* myString;
     myString = [[NSString alloc] initWithData:dict encoding:NSASCIIStringEncoding];
     
     if (myString.length > 0) {
     myString = [myString substringWithRange:NSMakeRange(1, myString.length-2)];
     }
     
     NSError *err = nil;
     NSArray *array = [NSJSONSerialization JSONObjectWithData:[myString dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&err];
     NSDictionary *dictionary = [array objectAtIndex:0];
     
     success(dictionary.dictByReplacingNulls,response);
     } callBackError:^(NSError *err, NSURLResponse *response) {
     error(err,response);
     }];*/
    [WebAPI callPOSTWithParameter:url withParams:paramDict withHeaders:headersKey callBackSuccess:^(NSData *dict, NSURLResponse *response) {
        NSError *error = nil;
        NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:dict
                                                                     options:NSJSONReadingAllowFragments
                                                                       error:&error];
        success(dictResponse.dictByReplacingNulls,response);
    } callBackError:^(NSError *err, NSURLResponse *response) {
        error(err,response);
    }];
}

+(void) callForGetNotesList:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                         Error:(void (^)(NSError *error, NSURLResponse *response))error {
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *dictData = [userDefaults objectForKey:@"user_data"];
    NSMutableDictionary *headersKey = NSMutableDictionary.new;
    [headersKey setObject:[dictData objectForKey:@"api_key"] forKey:@"api-key"];
    NSString *url = @"http://surfportable.com/Universal-Oracle-Cards/index.php/Api/Notes_list";
    
    /*[WebAPI callPOSTWithParameter:url withParams:paramDict withHeaders:nil callBackSuccess:^(NSData *dict, NSURLResponse *response) {
     NSString* myString;
     myString = [[NSString alloc] initWithData:dict encoding:NSASCIIStringEncoding];
     
     if (myString.length > 0) {
     myString = [myString substringWithRange:NSMakeRange(1, myString.length-2)];
     }
     
     NSError *err = nil;
     NSArray *array = [NSJSONSerialization JSONObjectWithData:[myString dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&err];
     NSDictionary *dictionary = [array objectAtIndex:0];
     
     success(dictionary.dictByReplacingNulls,response);
     } callBackError:^(NSError *err, NSURLResponse *response) {
     error(err,response);
     }];*/
    [WebAPI callPOSTWithParameter:url withParams:paramDict withHeaders:headersKey callBackSuccess:^(NSData *dict, NSURLResponse *response) {
        NSError *error = nil;
        NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:dict
                                                                     options:NSJSONReadingAllowFragments
                                                                       error:&error];
        success(dictResponse.dictByReplacingNulls,response);
    } callBackError:^(NSError *err, NSURLResponse *response) {
        error(err,response);
    }];
}


+(void) callForDeleteNotes:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                      Error:(void (^)(NSError *error, NSURLResponse *response))error {
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *dictData = [userDefaults objectForKey:@"user_data"];
    NSMutableDictionary *headersKey = NSMutableDictionary.new;
    [headersKey setObject:[dictData objectForKey:@"api_key"] forKey:@"api-key"];
    NSString *url = @"http://surfportable.com/Universal-Oracle-Cards/index.php/Api/delete_Notes";
    
    /*[WebAPI callPOSTWithParameter:url withParams:paramDict withHeaders:nil callBackSuccess:^(NSData *dict, NSURLResponse *response) {
     NSString* myString;
     myString = [[NSString alloc] initWithData:dict encoding:NSASCIIStringEncoding];
     
     if (myString.length > 0) {
     myString = [myString substringWithRange:NSMakeRange(1, myString.length-2)];
     }
     
     NSError *err = nil;
     NSArray *array = [NSJSONSerialization JSONObjectWithData:[myString dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&err];
     NSDictionary *dictionary = [array objectAtIndex:0];
     
     success(dictionary.dictByReplacingNulls,response);
     } callBackError:^(NSError *err, NSURLResponse *response) {
     error(err,response);
     }];*/
    [WebAPI callPOSTWithParameter:url withParams:paramDict withHeaders:headersKey callBackSuccess:^(NSData *dict, NSURLResponse *response) {
        NSError *error = nil;
        NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:dict
                                                                     options:NSJSONReadingAllowFragments
                                                                       error:&error];
        success(dictResponse.dictByReplacingNulls,response);
    } callBackError:^(NSError *err, NSURLResponse *response) {
        error(err,response);
    }];
}



+(void) callForGetMySpreadList:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                          Error:(void (^)(NSError *error, NSURLResponse *response))error {
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *dictData = [userDefaults objectForKey:@"user_data"];
    NSMutableDictionary *headersKey = NSMutableDictionary.new;
    [headersKey setObject:[dictData objectForKey:@"api_key"] forKey:@"api-key"];
    NSString *url = @"http://surfportable.com/Universal-Oracle-Cards/index.php/Api/Spread_list";
    
    /*[WebAPI callPOSTWithParameter:url withParams:paramDict withHeaders:nil callBackSuccess:^(NSData *dict, NSURLResponse *response) {
     NSString* myString;
     myString = [[NSString alloc] initWithData:dict encoding:NSASCIIStringEncoding];
     
     if (myString.length > 0) {
     myString = [myString substringWithRange:NSMakeRange(1, myString.length-2)];
     }
     
     NSError *err = nil;
     NSArray *array = [NSJSONSerialization JSONObjectWithData:[myString dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&err];
     NSDictionary *dictionary = [array objectAtIndex:0];
     
     success(dictionary.dictByReplacingNulls,response);
     } callBackError:^(NSError *err, NSURLResponse *response) {
     error(err,response);
     }];*/
    [WebAPI callPOSTWithParameter:url withParams:paramDict withHeaders:headersKey callBackSuccess:^(NSData *dict, NSURLResponse *response) {
        NSError *error = nil;
        NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:dict
                                                                     options:NSJSONReadingAllowFragments
                                                                       error:&error];
        success(dictResponse.dictByReplacingNulls,response);
    } callBackError:^(NSError *err, NSURLResponse *response) {
        error(err,response);
    }];
}



+(void) callForDeleteJournal:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                          Error:(void (^)(NSError *error, NSURLResponse *response))error {
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *dictData = [userDefaults objectForKey:@"user_data"];
    NSMutableDictionary *headersKey = NSMutableDictionary.new;
    [headersKey setObject:[dictData objectForKey:@"api_key"] forKey:@"api-key"];
    NSString *url = @"http://surfportable.com/Universal-Oracle-Cards/index.php/Api/delete_Reading";
    
    /*[WebAPI callPOSTWithParameter:url withParams:paramDict withHeaders:nil callBackSuccess:^(NSData *dict, NSURLResponse *response) {
     NSString* myString;
     myString = [[NSString alloc] initWithData:dict encoding:NSASCIIStringEncoding];
     
     if (myString.length > 0) {
     myString = [myString substringWithRange:NSMakeRange(1, myString.length-2)];
     }
     
     NSError *err = nil;
     NSArray *array = [NSJSONSerialization JSONObjectWithData:[myString dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&err];
     NSDictionary *dictionary = [array objectAtIndex:0];
     
     success(dictionary.dictByReplacingNulls,response);
     } callBackError:^(NSError *err, NSURLResponse *response) {
     error(err,response);
     }];*/
    [WebAPI callPOSTWithParameter:url withParams:paramDict withHeaders:headersKey callBackSuccess:^(NSData *dict, NSURLResponse *response) {
        NSError *error = nil;
        NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:dict
                                                                     options:NSJSONReadingAllowFragments
                                                                       error:&error];
        success(dictResponse.dictByReplacingNulls,response);
    } callBackError:^(NSError *err, NSURLResponse *response) {
        error(err,response);
    }];
}


+(void) callForCreateSpread:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                       Error:(void (^)(NSError *error, NSURLResponse *response))error {
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *dictData = [userDefaults objectForKey:@"user_data"];
    NSMutableDictionary *headersKey = NSMutableDictionary.new;
    [headersKey setObject:[dictData objectForKey:@"api_key"] forKey:@"api-key"];
    NSString *url = @"http://surfportable.com/Universal-Oracle-Cards/index.php/Api/create_spread";
    
    /*[WebAPI callPOSTWithParameter:url withParams:paramDict withHeaders:nil callBackSuccess:^(NSData *dict, NSURLResponse *response) {
     NSString* myString;
     myString = [[NSString alloc] initWithData:dict encoding:NSASCIIStringEncoding];
     
     if (myString.length > 0) {
     myString = [myString substringWithRange:NSMakeRange(1, myString.length-2)];
     }
     
     NSError *err = nil;
     NSArray *array = [NSJSONSerialization JSONObjectWithData:[myString dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&err];
     NSDictionary *dictionary = [array objectAtIndex:0];
     
     success(dictionary.dictByReplacingNulls,response);
     } callBackError:^(NSError *err, NSURLResponse *response) {
     error(err,response);
     }];*/
    [WebAPI callPOSTWithParameter:url withParams:paramDict withHeaders:headersKey callBackSuccess:^(NSData *dict, NSURLResponse *response) {
        NSError *error = nil;
        NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:dict
                                                                     options:NSJSONReadingAllowFragments
                                                                       error:&error];
        success(dictResponse.dictByReplacingNulls,response);
    } callBackError:^(NSError *err, NSURLResponse *response) {
        error(err,response);
    }];
}



+(void) callForRemoveFacebook:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                       Error:(void (^)(NSError *error, NSURLResponse *response))error {
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *dictData = [userDefaults objectForKey:@"user_data"];
    NSMutableDictionary *headersKey = NSMutableDictionary.new;
    [headersKey setObject:[dictData objectForKey:@"api_key"] forKey:@"api-key"];
    NSString *url = @"http://surfportable.com/Universal-Oracle-Cards/index.php/Api/remove_facebook";
    
    /*[WebAPI callPOSTWithParameter:url withParams:paramDict withHeaders:nil callBackSuccess:^(NSData *dict, NSURLResponse *response) {
     NSString* myString;
     myString = [[NSString alloc] initWithData:dict encoding:NSASCIIStringEncoding];
     
     if (myString.length > 0) {
     myString = [myString substringWithRange:NSMakeRange(1, myString.length-2)];
     }
     
     NSError *err = nil;
     NSArray *array = [NSJSONSerialization JSONObjectWithData:[myString dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&err];
     NSDictionary *dictionary = [array objectAtIndex:0];
     
     success(dictionary.dictByReplacingNulls,response);
     } callBackError:^(NSError *err, NSURLResponse *response) {
     error(err,response);
     }];*/
    [WebAPI callPOSTWithParameter:url withParams:paramDict withHeaders:headersKey callBackSuccess:^(NSData *dict, NSURLResponse *response) {
        NSError *error = nil;
        NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:dict
                                                                     options:NSJSONReadingAllowFragments
                                                                       error:&error];
        success(dictResponse.dictByReplacingNulls,response);
    } callBackError:^(NSError *err, NSURLResponse *response) {
        error(err,response);
    }];
}

+(void) callForRemoveGoogle:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                        Error:(void (^)(NSError *error, NSURLResponse *response))error {
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *dictData = [userDefaults objectForKey:@"user_data"];
    NSMutableDictionary *headersKey = NSMutableDictionary.new;
    [headersKey setObject:[dictData objectForKey:@"api_key"] forKey:@"api-key"];
    NSString *url = @"http://surfportable.com/Universal-Oracle-Cards/index.php/Api/remove_google";
    
    /*[WebAPI callPOSTWithParameter:url withParams:paramDict withHeaders:nil callBackSuccess:^(NSData *dict, NSURLResponse *response) {
     NSString* myString;
     myString = [[NSString alloc] initWithData:dict encoding:NSASCIIStringEncoding];
     
     if (myString.length > 0) {
     myString = [myString substringWithRange:NSMakeRange(1, myString.length-2)];
     }
     
     NSError *err = nil;
     NSArray *array = [NSJSONSerialization JSONObjectWithData:[myString dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&err];
     NSDictionary *dictionary = [array objectAtIndex:0];
     
     success(dictionary.dictByReplacingNulls,response);
     } callBackError:^(NSError *err, NSURLResponse *response) {
     error(err,response);
     }];*/
    [WebAPI callPOSTWithParameter:url withParams:paramDict withHeaders:headersKey callBackSuccess:^(NSData *dict, NSURLResponse *response) {
        NSError *error = nil;
        NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:dict
                                                                     options:NSJSONReadingAllowFragments
                                                                       error:&error];
        success(dictResponse.dictByReplacingNulls,response);
    } callBackError:^(NSError *err, NSURLResponse *response) {
        error(err,response);
    }];
}


+(void) callForUpdateProfile:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                    Error:(void (^)(NSError *error, NSURLResponse *response))error {
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *dictData = [userDefaults objectForKey:@"user_data"];
    NSMutableDictionary *headersKey = NSMutableDictionary.new;
    [headersKey setObject:[dictData objectForKey:@"api_key"] forKey:@"api-key"];
    NSString *url = @"http://surfportable.com/Universal-Oracle-Cards/index.php/Api/update_profile";
    
    /*[WebAPI callPOSTWithParameter:url withParams:paramDict withHeaders:nil callBackSuccess:^(NSData *dict, NSURLResponse *response) {
     NSString* myString;
     myString = [[NSString alloc] initWithData:dict encoding:NSASCIIStringEncoding];
     
     if (myString.length > 0) {
     myString = [myString substringWithRange:NSMakeRange(1, myString.length-2)];
     }
     
     NSError *err = nil;
     NSArray *array = [NSJSONSerialization JSONObjectWithData:[myString dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&err];
     NSDictionary *dictionary = [array objectAtIndex:0];
     
     success(dictionary.dictByReplacingNulls,response);
     } callBackError:^(NSError *err, NSURLResponse *response) {
     error(err,response);
     }];*/
    [WebAPI callPOSTWithParameter:url withParams:paramDict withHeaders:headersKey callBackSuccess:^(NSData *dict, NSURLResponse *response) {
        NSError *error = nil;
        NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:dict
                                                                     options:NSJSONReadingAllowFragments
                                                                       error:&error];
        success(dictResponse.dictByReplacingNulls,response);
    } callBackError:^(NSError *err, NSURLResponse *response) {
        error(err,response);
    }];
}




+(void) callForSaveReading:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
               Error:(void (^)(NSError *error, NSURLResponse *response))error {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *dictData = [userDefaults objectForKey:@"user_data"];
    NSMutableDictionary *headersKey = NSMutableDictionary.new;
    [headersKey setObject:[dictData objectForKey:@"api_key"] forKey:@"api-key"];
    NSString *url = @"http://surfportable.com/Universal-Oracle-Cards/index.php/Api/Save_Reading";
    
    /*[WebAPI callPOSTWithParameter:url withParams:paramDict withHeaders:nil callBackSuccess:^(NSData *dict, NSURLResponse *response) {
     NSString* myString;
     myString = [[NSString alloc] initWithData:dict encoding:NSASCIIStringEncoding];
     
     if (myString.length > 0) {
     myString = [myString substringWithRange:NSMakeRange(1, myString.length-2)];
     }
     
     NSError *err = nil;
     NSArray *array = [NSJSONSerialization JSONObjectWithData:[myString dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&err];
     NSDictionary *dictionary = [array objectAtIndex:0];
     
     success(dictionary.dictByReplacingNulls,response);
     } callBackError:^(NSError *err, NSURLResponse *response) {
     error(err,response);
     }];*/
    [WebAPI callPOSTWithParameter:url withParams:paramDict withHeaders:headersKey callBackSuccess:^(NSData *dict, NSURLResponse *response) {
        NSError *error = nil;
        NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:dict
                                                                     options:NSJSONReadingAllowFragments
                                                                       error:&error];
        success(dictResponse.dictByReplacingNulls,response);
    } callBackError:^(NSError *err, NSURLResponse *response) {
        error(err,response);
    }];
}


+(void) callForDeleteSpread:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                   Error:(void (^)(NSError *error, NSURLResponse *response))error {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *dictData = [userDefaults objectForKey:@"user_data"];
    NSMutableDictionary *headersKey = NSMutableDictionary.new;
    [headersKey setObject:[dictData objectForKey:@"api_key"] forKey:@"api-key"];
    NSString *url = @"http://surfportable.com/Universal-Oracle-Cards/index.php/Api/delete_Spread";
    
    /*[WebAPI callPOSTWithParameter:url withParams:paramDict withHeaders:nil callBackSuccess:^(NSData *dict, NSURLResponse *response) {
     NSString* myString;
     myString = [[NSString alloc] initWithData:dict encoding:NSASCIIStringEncoding];
     
     if (myString.length > 0) {
     myString = [myString substringWithRange:NSMakeRange(1, myString.length-2)];
     }
     
     NSError *err = nil;
     NSArray *array = [NSJSONSerialization JSONObjectWithData:[myString dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&err];
     NSDictionary *dictionary = [array objectAtIndex:0];
     
     success(dictionary.dictByReplacingNulls,response);
     } callBackError:^(NSError *err, NSURLResponse *response) {
     error(err,response);
     }];*/
    [WebAPI callPOSTWithParameter:url withParams:paramDict withHeaders:headersKey callBackSuccess:^(NSData *dict, NSURLResponse *response) {
        NSError *error = nil;
        NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:dict
                                                                     options:NSJSONReadingAllowFragments
                                                                       error:&error];
        success(dictResponse.dictByReplacingNulls,response);
    } callBackError:^(NSError *err, NSURLResponse *response) {
        error(err,response);
    }];
}



+(void) callForSaveNotes:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                     Error:(void (^)(NSError *error, NSURLResponse *response))error {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *dictData = [userDefaults objectForKey:@"user_data"];
    NSMutableDictionary *headersKey = NSMutableDictionary.new;
    [headersKey setObject:[dictData objectForKey:@"api_key"] forKey:@"api-key"];
    NSString *url = @"http://surfportable.com/Universal-Oracle-Cards/index.php/Api/Save_Notes";
    
    /*[WebAPI callPOSTWithParameter:url withParams:paramDict withHeaders:nil callBackSuccess:^(NSData *dict, NSURLResponse *response) {
     NSString* myString;
     myString = [[NSString alloc] initWithData:dict encoding:NSASCIIStringEncoding];
     
     if (myString.length > 0) {
     myString = [myString substringWithRange:NSMakeRange(1, myString.length-2)];
     }
     
     NSError *err = nil;
     NSArray *array = [NSJSONSerialization JSONObjectWithData:[myString dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&err];
     NSDictionary *dictionary = [array objectAtIndex:0];
     
     success(dictionary.dictByReplacingNulls,response);
     } callBackError:^(NSError *err, NSURLResponse *response) {
     error(err,response);
     }];*/
    [WebAPI callPOSTWithParameter:url withParams:paramDict withHeaders:headersKey callBackSuccess:^(NSData *dict, NSURLResponse *response) {
        NSError *error = nil;
        NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:dict
                                                                     options:NSJSONReadingAllowFragments
                                                                       error:&error];
        success(dictResponse.dictByReplacingNulls,response);
    } callBackError:^(NSError *err, NSURLResponse *response) {
        error(err,response);
    }];
}

+(void) callForUpdateNotes:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                   Error:(void (^)(NSError *error, NSURLResponse *response))error {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *dictData = [userDefaults objectForKey:@"user_data"];
    NSMutableDictionary *headersKey = NSMutableDictionary.new;
    [headersKey setObject:[dictData objectForKey:@"api_key"] forKey:@"api-key"];
    NSString *url = @"http://surfportable.com/Universal-Oracle-Cards/index.php/Api/Update_Notes";
    
    /*[WebAPI callPOSTWithParameter:url withParams:paramDict withHeaders:nil callBackSuccess:^(NSData *dict, NSURLResponse *response) {
     NSString* myString;
     myString = [[NSString alloc] initWithData:dict encoding:NSASCIIStringEncoding];
     
     if (myString.length > 0) {
     myString = [myString substringWithRange:NSMakeRange(1, myString.length-2)];
     }
     
     NSError *err = nil;
     NSArray *array = [NSJSONSerialization JSONObjectWithData:[myString dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&err];
     NSDictionary *dictionary = [array objectAtIndex:0];
     
     success(dictionary.dictByReplacingNulls,response);
     } callBackError:^(NSError *err, NSURLResponse *response) {
     error(err,response);
     }];*/
    [WebAPI callPOSTWithParameter:url withParams:paramDict withHeaders:headersKey callBackSuccess:^(NSData *dict, NSURLResponse *response) {
        NSError *error = nil;
        NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:dict
                                                                     options:NSJSONReadingAllowFragments
                                                                       error:&error];
        success(dictResponse.dictByReplacingNulls,response);
    } callBackError:^(NSError *err, NSURLResponse *response) {
        error(err,response);
    }];
}

+(void) callForSaveAudio:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                   Error:(void (^)(NSError *error, NSURLResponse *response))error {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *dictData = [userDefaults objectForKey:@"user_data"];
    NSMutableDictionary *headersKey = NSMutableDictionary.new;
    [headersKey setObject:[dictData objectForKey:@"api_key"] forKey:@"api-key"];
    NSString *url = @"http://surfportable.com/Universal-Oracle-Cards/index.php/Api/Save_Notes";
    
    /*[WebAPI callPOSTWithParameter:url withParams:paramDict withHeaders:nil callBackSuccess:^(NSData *dict, NSURLResponse *response) {
     NSString* myString;
     myString = [[NSString alloc] initWithData:dict encoding:NSASCIIStringEncoding];
     
     if (myString.length > 0) {
     myString = [myString substringWithRange:NSMakeRange(1, myString.length-2)];
     }
     
     NSError *err = nil;
     NSArray *array = [NSJSONSerialization JSONObjectWithData:[myString dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&err];
     NSDictionary *dictionary = [array objectAtIndex:0];
     
     success(dictionary.dictByReplacingNulls,response);
     } callBackError:^(NSError *err, NSURLResponse *response) {
     error(err,response);
     }];*/
    [WebAPI callPOSTWithParameterAudioFile:url withParams:paramDict withHeaders:headersKey callBackSuccess:^(NSData *dict, NSURLResponse *response) {
        NSError *error = nil;
        NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:dict
                                                                     options:NSJSONReadingAllowFragments
                                                                       error:&error];
        success(dictResponse.dictByReplacingNulls,response);
    } callBackError:^(NSError *err, NSURLResponse *response) {
        error(err,response);
    }];
}


+(void) callForAddUser:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                              Error:(void (^)(NSError *error, NSURLResponse *response))error {
    
    NSString *url = @"http://modjes.com/wp-content/plugins/dsp_dating/api_v7/sign_up.php";
    
    [WebAPI callPOSTWithParameterRegisterImage:url withParams:paramDict withHeaders:nil callBackSuccess:^(NSData *dict, NSURLResponse *response) {
        
        NSError *error = nil;
        NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:dict
                                                                     options:NSJSONReadingAllowFragments
                                                                       error:&error];
        success(dictResponse.dictByReplacingNulls,response);
        
    } callBackError:^(NSError *err, NSURLResponse *response) {
        error(err,response);
    }];
}


+(void) callForLogout:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                  Error:(void (^)(NSError *error, NSURLResponse *response))error {
    
    NSString *url = @"http://modjes.com/wp-content/plugins/dsp_dating/api_v7/logout.php";
    
    [WebAPI callPOSTWithParameter:url withParams:paramDict withHeaders:nil callBackSuccess:^(NSData *dict, NSURLResponse *response) {
        NSError *error = nil;
        NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:dict
                                                                     options:NSJSONReadingAllowFragments
                                                                       error:&error];
        success(dictResponse.dictByReplacingNulls,response);
    } callBackError:^(NSError *err, NSURLResponse *response) {
        error(err,response);
    }];
}

+(void) callForTotalCount:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                Error:(void (^)(NSError *error, NSURLResponse *response))error {
    
    NSString *url = @"http://modjes.com/wp-content/plugins/dsp_dating/api_v7/counter.php";
    
    [WebAPI callPOSTWithParameter:url withParams:paramDict withHeaders:nil callBackSuccess:^(NSData *dict, NSURLResponse *response) {
        NSError *error = nil;
        NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:dict
                                                                     options:NSJSONReadingAllowFragments
                                                                       error:&error];
        success(dictResponse.dictByReplacingNulls,response);
    } callBackError:^(NSError *err, NSURLResponse *response) {
        error(err,response);
    }];
}

+(void) callForChangePassword1:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
               Error:(void (^)(NSError *error, NSURLResponse *response))error {
    
    NSString *url = @"http://modjes.com/wp-content/plugins/dsp_dating/api_v7/change_password.php";
    
    [WebAPI callPOSTWithParameter:url withParams:paramDict withHeaders:nil callBackSuccess:^(NSData *dict, NSURLResponse *response) {
        NSError *error = nil;
        NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:dict
                                                                     options:NSJSONReadingAllowFragments
                                                                       error:&error];
        success(dictResponse.dictByReplacingNulls,response);
    } callBackError:^(NSError *err, NSURLResponse *response) {
        error(err,response);
    }];
}

+(void) callForNotification:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                        Error:(void (^)(NSError *error, NSURLResponse *response))error {
    
    NSString *url = @"http://modjes.com/wp-content/plugins/dsp_dating/api_v7/notifications_setting.php";
    
    [WebAPI callPOSTWithParameter:url withParams:paramDict withHeaders:nil callBackSuccess:^(NSData *dict, NSURLResponse *response) {
        NSError *error = nil;
        NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:dict
                                                                     options:NSJSONReadingAllowFragments
                                                                       error:&error];
        success(dictResponse.dictByReplacingNulls,response);
    } callBackError:^(NSError *err, NSURLResponse *response) {
        error(err,response);
    }];
}

+(void) callForGetCityData:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                      Error:(void (^)(NSError *error, NSURLResponse *response))error {
    
    NSString *url = @"http://modjes.com/wp-content/plugins/dsp_dating/api_v7/get_data.php";
    
    [WebAPI callPOSTWithParameter:url withParams:paramDict withHeaders:nil callBackSuccess:^(NSData *dict, NSURLResponse *response) {
        NSError *error = nil;
        NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:dict
                                                                     options:NSJSONReadingAllowFragments
                                                                       error:&error];
        success(dictResponse.dictByReplacingNulls,response);
    } callBackError:^(NSError *err, NSURLResponse *response) {
        error(err,response);
    }];
}

+(void) callForBlockUser:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                      Error:(void (^)(NSError *error, NSURLResponse *response))error {
    
    NSString *url = @"http://modjes.com/wp-content/plugins/dsp_dating/api_v7/block_unblock_user.php";
    
    [WebAPI callPOSTWithParameter:url withParams:paramDict withHeaders:nil callBackSuccess:^(NSData *dict, NSURLResponse *response) {
        NSError *error = nil;
        NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:dict
                                                                     options:NSJSONReadingAllowFragments
                                                                       error:&error];
        success(dictResponse.dictByReplacingNulls,response);
    } callBackError:^(NSError *err, NSURLResponse *response) {
        error(err,response);
    }];
}

+(void) callForContactUs:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                   Error:(void (^)(NSError *error, NSURLResponse *response))error {
    
    NSString *url = @"http://modjes.com/wp-content/plugins/dsp_dating/api_v7/contact_us.php";
    
    [WebAPI callPOSTWithParameter:url withParams:paramDict withHeaders:nil callBackSuccess:^(NSData *dict, NSURLResponse *response) {
        NSError *error = nil;
        NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:dict
                                                                     options:NSJSONReadingAllowFragments
                                                                       error:&error];
        success(dictResponse.dictByReplacingNulls,response);
    } callBackError:^(NSError *err, NSURLResponse *response) {
        error(err,response);
    }];
}

+(void) callForGalleries:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                   Error:(void (^)(NSError *error, NSURLResponse *response))error {
    
    NSString *url = @"http://modjes.com/wp-content/plugins/dsp_dating/api_v7/gallery.php";
    
    [WebAPI callPOSTWithParameter:url withParams:paramDict withHeaders:nil callBackSuccess:^(NSData *dict, NSURLResponse *response) {
        NSError *error = nil;
        NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:dict
                                                                     options:NSJSONReadingAllowFragments
                                                                       error:&error];
        success(dictResponse.dictByReplacingNulls,response);
    } callBackError:^(NSError *err, NSURLResponse *response) {
        error(err,response);
    }];
}

+(void) callForAddImagesToGalleries:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                   Error:(void (^)(NSError *error, NSURLResponse *response))error {
    
    NSString *url = @"http://modjes.com/wp-content/plugins/dsp_dating/api_v7/gallery.php";
    
    [WebAPI callPOSTWithParameterImage:url withParams:paramDict withHeaders:nil callBackSuccess:^(NSData *dict, NSURLResponse *response) {
        
        NSError *error = nil;
        NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:dict
                                                                     options:NSJSONReadingAllowFragments
                                                                       error:&error];
        success(dictResponse.dictByReplacingNulls,response);
        
    } callBackError:^(NSError *err, NSURLResponse *response) {
        error(err,response);
    }];
}


+(void) callForMatchAlert:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                   Error:(void (^)(NSError *error, NSURLResponse *response))error {
    
    NSString *url = @"http://modjes.com/wp-content/plugins/dsp_dating/api_v7/match_alert.php";
    
    [WebAPI callPOSTWithParameter:url withParams:paramDict withHeaders:nil callBackSuccess:^(NSData *dict, NSURLResponse *response) {
        NSError *error = nil;
        NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:dict
                                                                     options:NSJSONReadingAllowFragments
                                                                       error:&error];
        success(dictResponse.dictByReplacingNulls,response);
    } callBackError:^(NSError *err, NSURLResponse *response) {
        error(err,response);
    }];
}

+(void) callForSendMessage:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                   Error:(void (^)(NSError *error, NSURLResponse *response))error {
    
    NSString *url = @"http://modjes.com/wp-content/plugins/dsp_dating/api_v7/message.php";
    
    [WebAPI callPOSTWithParameter:url withParams:paramDict withHeaders:nil callBackSuccess:^(NSData *dict, NSURLResponse *response) {
        NSError *error = nil;
        NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:dict
                                                                     options:NSJSONReadingAllowFragments
                                                                       error:&error];
        success(dictResponse.dictByReplacingNulls,response);
    } callBackError:^(NSError *err, NSURLResponse *response) {
        error(err,response);
    }];
}

+(void) callForReportAbuse:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                   Error:(void (^)(NSError *error, NSURLResponse *response))error {
    
    NSString *url = @"http://modjes.com/wp-content/plugins/dsp_dating/api_v7/report_abuse.php";
    
    [WebAPI callPOSTWithParameter:url withParams:paramDict withHeaders:nil callBackSuccess:^(NSData *dict, NSURLResponse *response) {
        NSError *error = nil;
        NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:dict
                                                                     options:NSJSONReadingAllowFragments
                                                                       error:&error];
        success(dictResponse.dictByReplacingNulls,response);
    } callBackError:^(NSError *err, NSURLResponse *response) {
        error(err,response);
    }];
}


+(void) callForPrivacySetting:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                      Error:(void (^)(NSError *error, NSURLResponse *response))error {
    
    NSString *url = @"http://modjes.com/wp-content/plugins/dsp_dating/api_v7/privacy_setting.php";
    
    [WebAPI callPOSTWithParameter:url withParams:paramDict withHeaders:nil callBackSuccess:^(NSData *dict, NSURLResponse *response) {
        NSError *error = nil;
        NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:dict
                                                                     options:NSJSONReadingAllowFragments
                                                                       error:&error];
        success(dictResponse.dictByReplacingNulls,response);
    } callBackError:^(NSError *err, NSURLResponse *response) {
        error(err,response);
    }];
}

+(void) callForWink:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                        Error:(void (^)(NSError *error, NSURLResponse *response))error {
    
    NSString *url = @"http://modjes.com/wp-content/plugins/dsp_dating/api_v7/wink.php";
    
    [WebAPI callPOSTWithParameter:url withParams:paramDict withHeaders:nil callBackSuccess:^(NSData *dict, NSURLResponse *response) {
        NSError *error = nil;
        NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:dict
                                                                     options:NSJSONReadingAllowFragments
                                                                       error:&error];
        success(dictResponse.dictByReplacingNulls,response);
    } callBackError:^(NSError *err, NSURLResponse *response) {
        error(err,response);
    }];
}



+(void) callForFriendList:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                        Error:(void (^)(NSError *error, NSURLResponse *response))error {
    
    NSString *url = @"http://modjes.com/wp-content/plugins/dsp_dating/api_v7/friend.php";
    
    [WebAPI callPOSTWithParameter:url withParams:paramDict withHeaders:nil callBackSuccess:^(NSData *dict, NSURLResponse *response) {
        NSError *error = nil;
        NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:dict
                                                                     options:NSJSONReadingAllowFragments
                                                                       error:&error];
        success(dictResponse.dictByReplacingNulls,response);
    } callBackError:^(NSError *err, NSURLResponse *response) {
        error(err,response);
    }];
}

+(void) callForFavoriteList:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                    Error:(void (^)(NSError *error, NSURLResponse *response))error {
    
    NSString *url = @"http://modjes.com/wp-content/plugins/dsp_dating/api_v7/favourites.php";
    
    [WebAPI callPOSTWithParameter:url withParams:paramDict withHeaders:nil callBackSuccess:^(NSData *dict, NSURLResponse *response) {
        NSError *error = nil;
        NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:dict
                                                                     options:NSJSONReadingAllowFragments
                                                                       error:&error];
        success(dictResponse.dictByReplacingNulls,response);
    } callBackError:^(NSError *err, NSURLResponse *response) {
        error(err,response);
    }];
}

+(void) callForMessageList:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                      Error:(void (^)(NSError *error, NSURLResponse *response))error {
    
    NSString *url = @"http://modjes.com/wp-content/plugins/dsp_dating/api_v7/message.php";
    
    [WebAPI callPOSTWithParameter:url withParams:paramDict withHeaders:nil callBackSuccess:^(NSData *dict, NSURLResponse *response) {
        NSError *error = nil;
        NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:dict
                                                                     options:NSJSONReadingAllowFragments
                                                                       error:&error];
        success(dictResponse.dictByReplacingNulls,response);
    } callBackError:^(NSError *err, NSURLResponse *response) {
        error(err,response);
    }];
}

+(void) callForSearchByUsername:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                      Error:(void (^)(NSError *error, NSURLResponse *response))error {
    
    NSString *url = @"http://modjes.com/wp-content/plugins/dsp_dating/api_v7/username_search.php";
    
    [WebAPI callPOSTWithParameter:url withParams:paramDict withHeaders:nil callBackSuccess:^(NSData *dict, NSURLResponse *response) {
        NSError *error = nil;
        NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:dict
                                                                     options:NSJSONReadingAllowFragments
                                                                       error:&error];
        success(dictResponse.dictByReplacingNulls,response);
    } callBackError:^(NSError *err, NSURLResponse *response) {
        error(err,response);
    }];
}

+(void) callForSearchByZipcode:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                          Error:(void (^)(NSError *error, NSURLResponse *response))error {
    
    NSString *url = @"http://modjes.com/wp-content/plugins/dsp_dating/api_v7/zipcode_search.php";
    
    [WebAPI callPOSTWithParameter:url withParams:paramDict withHeaders:nil callBackSuccess:^(NSData *dict, NSURLResponse *response) {
        NSError *error = nil;
        NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:dict
                                                                     options:NSJSONReadingAllowFragments
                                                                       error:&error];
        success(dictResponse.dictByReplacingNulls,response);
    } callBackError:^(NSError *err, NSURLResponse *response) {
        error(err,response);
    }];
}


+(void) callForVirtualGiftList:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                      Error:(void (^)(NSError *error, NSURLResponse *response))error {
    
    NSString *url = @"http://modjes.com/wp-content/plugins/dsp_dating/api_v7/virtual_gift.php";
    
    [WebAPI callPOSTWithParameter:url withParams:paramDict withHeaders:nil callBackSuccess:^(NSData *dict, NSURLResponse *response) {
        NSError *error = nil;
        NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:dict
                                                                     options:NSJSONReadingAllowFragments
                                                                       error:&error];
        success(dictResponse.dictByReplacingNulls,response);
    } callBackError:^(NSError *err, NSURLResponse *response) {
        error(err,response);
    }];
}

+(void) callForWinkList:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                         Error:(void (^)(NSError *error, NSURLResponse *response))error {
    
    NSString *url = @"http://modjes.com/wp-content/plugins/dsp_dating/api_v7/wink.php";
    
    [WebAPI callPOSTWithParameter:url withParams:paramDict withHeaders:nil callBackSuccess:^(NSData *dict, NSURLResponse *response) {
        NSError *error = nil;
        NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:dict
                                                                     options:NSJSONReadingAllowFragments
                                                                       error:&error];
        success(dictResponse.dictByReplacingNulls,response);
    } callBackError:^(NSError *err, NSURLResponse *response) {
        error(err,response);
    }];
}

+(void) callForOnlineList:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                         Error:(void (^)(NSError *error, NSURLResponse *response))error {
    
    NSString *url = @"http://modjes.com/wp-content/plugins/dsp_dating/api_v7/online.php";
    
    [WebAPI callPOSTWithParameter:url withParams:paramDict withHeaders:nil callBackSuccess:^(NSData *dict, NSURLResponse *response) {
        NSError *error = nil;
        NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:dict
                                                                     options:NSJSONReadingAllowFragments
                                                                       error:&error];
        success(dictResponse.dictByReplacingNulls,response);
    } callBackError:^(NSError *err, NSURLResponse *response) {
        error(err,response);
    }];
}

+(void) callForMatchList:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                    Error:(void (^)(NSError *error, NSURLResponse *response))error {
    
    NSString *url = @"http://modjes.com/wp-content/plugins/dsp_dating/api_v7/my_matches.php";
    
    [WebAPI callPOSTWithParameter:url withParams:paramDict withHeaders:nil callBackSuccess:^(NSData *dict, NSURLResponse *response) {
        NSError *error = nil;
        NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:dict
                                                                     options:NSJSONReadingAllowFragments
                                                                       error:&error];
        success(dictResponse.dictByReplacingNulls,response);
    } callBackError:^(NSError *err, NSURLResponse *response) {
        error(err,response);
    }];
}

+(void) callForNewMemberList:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                      Error:(void (^)(NSError *error, NSURLResponse *response))error {
    
    NSString *url = @"http://modjes.com/wp-content/plugins/dsp_dating/api_v7/new_member.php";
    
    [WebAPI callPOSTWithParameter:url withParams:paramDict withHeaders:nil callBackSuccess:^(NSData *dict, NSURLResponse *response) {
        NSError *error = nil;
        NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:dict
                                                                     options:NSJSONReadingAllowFragments
                                                                       error:&error];
        success(dictResponse.dictByReplacingNulls,response);
    } callBackError:^(NSError *err, NSURLResponse *response) {
        error(err,response);
    }];
}

+(void) callForCountryList:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                       Error:(void (^)(NSError *error, NSURLResponse *response))error {
    
    NSString *url = @"http://modjes.com/wp-content/plugins/dsp_dating/api_v7/block_countries.php";
    
    [WebAPI callPOSTWithParameter:url withParams:paramDict withHeaders:nil callBackSuccess:^(NSData *dict, NSURLResponse *response) {
        NSError *error = nil;
        NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:dict
                                                                     options:NSJSONReadingAllowFragments
                                                                       error:&error];
        success(dictResponse.dictByReplacingNulls,response);
    } callBackError:^(NSError *err, NSURLResponse *response) {
        error(err,response);
    }];
}

+(void) callForSearchList:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                     Error:(void (^)(NSError *error, NSURLResponse *response))error {
    
    NSString *url = @"http://modjes.com/wp-content/plugins/dsp_dating/api_v7/basic_search.php";
    
    [WebAPI callPOSTWithParameter:url withParams:paramDict withHeaders:nil callBackSuccess:^(NSData *dict, NSURLResponse *response) {
        NSError *error = nil;
        NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:dict
                                                                     options:NSJSONReadingAllowFragments
                                                                       error:&error];
        success(dictResponse.dictByReplacingNulls,response);
    } callBackError:^(NSError *err, NSURLResponse *response) {
        error(err,response);
    }];
}

+(void) callForEmailExist:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                    Error:(void (^)(NSError *error, NSURLResponse *response))error {
    
    NSString *url = @"http://modjes.com/wp-content/plugins/dsp_dating/api_v7/sign_up.php";
    
    [WebAPI callPOSTWithParameter:url withParams:paramDict withHeaders:nil callBackSuccess:^(NSData *dict, NSURLResponse *response) {
        NSError *error = nil;
        NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:dict
                                                                     options:NSJSONReadingAllowFragments
                                                                       error:&error];
        success(dictResponse.dictByReplacingNulls,response);
    } callBackError:^(NSError *err, NSURLResponse *response) {
        error(err,response);
    }];
}


+(void) callForGroupChat:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                    Error:(void (^)(NSError *error, NSURLResponse *response))error {
    
    NSString *url = @"http://modjes.com/wp-content/plugins/dsp_dating/api_v7/group_chat.php";
    
    [WebAPI callPOSTWithParameter:url withParams:paramDict withHeaders:nil callBackSuccess:^(NSData *dict, NSURLResponse *response) {
        NSError *error = nil;
        NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:dict
                                                                     options:NSJSONReadingAllowFragments
                                                                       error:&error];
        success(dictResponse.dictByReplacingNulls,response);
    } callBackError:^(NSError *err, NSURLResponse *response) {
        error(err,response);
    }];
}

+(void) callForViewProfile:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                    Error:(void (^)(NSError *error, NSURLResponse *response))error {
    
    NSString *url = @"http://modjes.com/wp-content/plugins/dsp_dating/api_v7/viewProfile.php";
    
    [WebAPI callPOSTWithParameter:url withParams:paramDict withHeaders:nil callBackSuccess:^(NSData *dict, NSURLResponse *response) {
        NSError *error = nil;
        NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:dict
                                                                     options:NSJSONReadingAllowFragments
                                                                       error:&error];
        success(dictResponse.dictByReplacingNulls,response);
    } callBackError:^(NSError *err, NSURLResponse *response) {
        error(err,response);
    }];
}

+(void) callForEditProfile:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                     Error:(void (^)(NSError *error, NSURLResponse *response))error {
    
    NSString *url = @"http://modjes.com/wp-content/plugins/dsp_dating/api_v7/edit_profile.php";
    
    [WebAPI callPOSTWithParameter:url withParams:paramDict withHeaders:nil callBackSuccess:^(NSData *dict, NSURLResponse *response) {
        NSError *error = nil;
        NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:dict
                                                                     options:NSJSONReadingAllowFragments
                                                                       error:&error];
        success(dictResponse.dictByReplacingNulls,response);
    } callBackError:^(NSError *err, NSURLResponse *response) {
        error(err,response);
    }];
}

+(void) callForSendFriendRequest:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                     Error:(void (^)(NSError *error, NSURLResponse *response))error {
    
    NSString *url = @"http://modjes.com/wp-content/plugins/dsp_dating/api_v7/friend.php";
    
    [WebAPI callPOSTWithParameter:url withParams:paramDict withHeaders:nil callBackSuccess:^(NSData *dict, NSURLResponse *response) {
        NSError *error = nil;
        NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:dict
                                                                     options:NSJSONReadingAllowFragments
                                                                       error:&error];
        success(dictResponse.dictByReplacingNulls,response);
    } callBackError:^(NSError *err, NSURLResponse *response) {
        error(err,response);
    }];
}

+(void) callForAddToFavorite:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                           Error:(void (^)(NSError *error, NSURLResponse *response))error {
    
    NSString *url = @"http://modjes.com/wp-content/plugins/dsp_dating/api_v7/favourites.php";
    
    [WebAPI callPOSTWithParameter:url withParams:paramDict withHeaders:nil callBackSuccess:^(NSData *dict, NSURLResponse *response) {
        NSError *error = nil;
        NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:dict
                                                                     options:NSJSONReadingAllowFragments
                                                                       error:&error];
        success(dictResponse.dictByReplacingNulls,response);
    } callBackError:^(NSError *err, NSURLResponse *response) {
        error(err,response);
    }];
}

+(void) callForGetData:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                     Error:(void (^)(NSError *error, NSURLResponse *response))error {
    
    NSString *url = @"http://modjes.com/wp-content/plugins/dsp_dating/api_v7/get_data.php";
    
    [WebAPI callPOSTWithParameter:url withParams:paramDict withHeaders:nil callBackSuccess:^(NSData *dict, NSURLResponse *response) {
        NSError *error = nil;
        NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:dict
                                                                     options:NSJSONReadingAllowFragments
                                                                       error:&error];
        success(dictResponse.dictByReplacingNulls,response);
    } callBackError:^(NSError *err, NSURLResponse *response) {
        error(err,response);
    }];
}


+(void) callForMenuList:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
               Error:(void (^)(NSError *error, NSURLResponse *response))error {
    
    NSString *url = [NSString stringWithFormat:@"%@%@",@"",MENU_LIST_URL];
    
    [WebAPI callPOSTWithParameter:url withParams:paramDict withHeaders:nil callBackSuccess:^(NSData *dict, NSURLResponse *response) {
        NSError *error = nil;
        NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:dict
                                                                     options:NSJSONReadingAllowFragments
                                                                       error:&error];
        success(dictResponse.dictByReplacingNulls,response);
    } callBackError:^(NSError *err, NSURLResponse *response) {
        error(err,response);
    }];
}

+(void) callForAddEditProfile:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                  Error:(void (^)(NSError *error, NSURLResponse *response))error {
    
    NSString *url = [NSString stringWithFormat:@"%@%@",@"",ADD_EDIT_PROFILE_URL];
    
    [WebAPI callPOSTWithParameter:url withParams:paramDict withHeaders:nil callBackSuccess:^(NSData *dict, NSURLResponse *response) {
        NSError *error = nil;
        NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:dict
                                                                     options:NSJSONReadingAllowFragments
                                                                       error:&error];
        success(dictResponse.dictByReplacingNulls,response);
    } callBackError:^(NSError *err, NSURLResponse *response) {
        error(err,response);
    }];
}

+(void) callForGetProfileList:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                        Error:(void (^)(NSError *error, NSURLResponse *response))error {
    
    NSString *url = [NSString stringWithFormat:@"%@%@",@"",GET_PROFILE_LIST_URL];
    
    [WebAPI callPOSTWithParameter:url withParams:paramDict withHeaders:nil callBackSuccess:^(NSData *dict, NSURLResponse *response) {
        NSError *error = nil;
        NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:dict
                                                                     options:NSJSONReadingAllowFragments
                                                                       error:&error];
        success(dictResponse.dictByReplacingNulls,response);
    } callBackError:^(NSError *err, NSURLResponse *response) {
        error(err,response);
    }];
}

+(void) callForGetProfileDetail:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                        Error:(void (^)(NSError *error, NSURLResponse *response))error {
    
    NSString *url = [NSString stringWithFormat:@"%@%@",@"",GET_PROFILE_DETAIL_URL];
    
    [WebAPI callPOSTWithParameter:url withParams:paramDict withHeaders:nil callBackSuccess:^(NSData *dict, NSURLResponse *response) {
        NSError *error = nil;
        NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:dict
                                                                     options:NSJSONReadingAllowFragments
                                                                       error:&error];
        success(dictResponse.dictByReplacingNulls,response);
    } callBackError:^(NSError *err, NSURLResponse *response) {
        error(err,response);
    }];
}

+(void) callForDeleteProfile:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                          Error:(void (^)(NSError *error, NSURLResponse *response))error {
    
    NSString *url = [NSString stringWithFormat:@"%@%@",@"",DELETE_PROFILE_URL];
    
    [WebAPI callPOSTWithParameter:url withParams:paramDict withHeaders:nil callBackSuccess:^(NSData *dict, NSURLResponse *response) {
        NSError *error = nil;
        NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:dict
                                                                     options:NSJSONReadingAllowFragments
                                                                       error:&error];
        success(dictResponse.dictByReplacingNulls,response);
    } callBackError:^(NSError *err, NSURLResponse *response) {
        error(err,response);
    }];
}

+(void) callForAddStaff:(NSMutableDictionary *)paramDict imageDict:(NSMutableDictionary *)imageDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                  Error:(void (^)(NSError *error, NSURLResponse *response))error {
    
    NSString *url = [NSString stringWithFormat:@"%@%@",@"",ADD_STAFF_URL];
    
    
    [WebAPI callPOSTWithParameter:url withParams:paramDict withHeaders:nil callBackSuccess:^(NSData *dict, NSURLResponse *response) {
        NSError *error = nil;
        NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:dict
                                                                     options:NSJSONReadingAllowFragments
                                                                       error:&error];
        success(dictResponse.dictByReplacingNulls,response);
    } callBackError:^(NSError *err, NSURLResponse *response) {
        error(err,response);
    }];
    
//    [WebAPI callPOSTWithParameterImageDict:url withParams:paramDict dictImages:imageDict withHeaders:nil callBackSuccess:^(NSData *dict, NSURLResponse *response) {
//
//        NSError *error = nil;
//
//        NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:dict
//                                                                     options:NSJSONReadingAllowFragments
//                                                                       error:&error];
//
//        if (error) {
//            NSString *str = [[NSString alloc] initWithData:dict encoding:NSUTF8StringEncoding];
//            NSLog(@"%@",str);
//            NSLog(@"Error : %@",error.localizedDescription);
//        }
//
//        success(dictResponse.dictByReplacingNulls,response);
//
//    } callBackError:^(NSError *err, NSURLResponse *response) {
//        error(err,response);
//    }];
}


+(void) callForGetUserTypeList:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                  Error:(void (^)(NSError *error, NSURLResponse *response))error {
    
    NSString *url = [NSString stringWithFormat:@"%@%@",@"",GET_USER_TYPE_LIST_URL];
    
    [WebAPI callPOSTWithParameter:url withParams:paramDict withHeaders:nil callBackSuccess:^(NSData *dict, NSURLResponse *response) {
        NSError *error = nil;
        NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:dict
                                                                     options:NSJSONReadingAllowFragments
                                                                       error:&error];
        success(dictResponse.dictByReplacingNulls,response);
    } callBackError:^(NSError *err, NSURLResponse *response) {
        error(err,response);
    }];
}




+(void) callForAddEditHealthAlert:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                         Error:(void (^)(NSError *error, NSURLResponse *response))error {
    
    NSString *url = [NSString stringWithFormat:@"%@%@",@"",GET_ADD_EDIT_HEALTH_URL];
    
    [WebAPI callPOSTWithParameter:url withParams:paramDict withHeaders:nil callBackSuccess:^(NSData *dict, NSURLResponse *response) {
        NSError *error = nil;
        NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:dict
                                                                     options:NSJSONReadingAllowFragments
                                                                       error:&error];
        success(dictResponse.dictByReplacingNulls,response);
    } callBackError:^(NSError *err, NSURLResponse *response) {
        error(err,response);
    }];
}

+(void) callForGetStaffList:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                         Error:(void (^)(NSError *error, NSURLResponse *response))error {
    
    NSString *url = [NSString stringWithFormat:@"%@%@",@"",GET_EMPLOYEE_LIST_URL];
    
    [WebAPI callPOSTWithParameter:url withParams:paramDict withHeaders:nil callBackSuccess:^(NSData *dict, NSURLResponse *response) {
        NSError *error = nil;
        NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:dict
                                                                     options:NSJSONReadingAllowFragments
                                                                       error:&error];
        NSLog(@" dictResponse :- %@", dictResponse);
        success(dictResponse.dictByReplacingNulls,response);
    } callBackError:^(NSError *err, NSURLResponse *response) {
        error(err,response);
    }];
}

+(void) callForDeleteStaff:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                      Error:(void (^)(NSError *error, NSURLResponse *response))error {
    
    NSString *url = [NSString stringWithFormat:@"%@%@",@"",DELETE_employee];
    
    [WebAPI callPOSTWithParameter:url withParams:paramDict withHeaders:nil callBackSuccess:^(NSData *dict, NSURLResponse *response) {
        NSError *error = nil;
        NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:dict
                                                                     options:NSJSONReadingAllowFragments
                                                                       error:&error];
        success(dictResponse.dictByReplacingNulls,response);
    } callBackError:^(NSError *err, NSURLResponse *response) {
        error(err,response);
    }];
}



+(void) callForUpdateStaff:(NSMutableDictionary *)paramDict imageDict:(NSMutableDictionary *)imageDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                     Error:(void (^)(NSError *error, NSURLResponse *response))error {
    
    NSString *url = [NSString stringWithFormat:@"%@%@",@"",UPDATE_STAFF_URL];
    
    [WebAPI callPOSTWithParameterImageDict:url withParams:paramDict dictImages:imageDict withHeaders:nil callBackSuccess:^(NSData *dict, NSURLResponse *response) {
        
        NSError *error = nil;
        
        NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:dict
                                                                     options:NSJSONReadingAllowFragments
                                                                       error:&error];
        
        if (error) {
            NSString *str = [[NSString alloc] initWithData:dict encoding:NSUTF8StringEncoding];
            NSLog(@"%@",str);
            NSLog(@"Error : %@",error.localizedDescription);
        }
        
        success(dictResponse.dictByReplacingNulls,response);
        
    } callBackError:^(NSError *err, NSURLResponse *response) {
        error(err,response);
    }];
}

+(void) callForGetParticipantList:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                      Error:(void (^)(NSError *error, NSURLResponse *response))error {
    
    NSString *url = [NSString stringWithFormat:@"%@%@",@"",GET_PARTICIPANT_LIST_URL];
    
    [WebAPI callPOSTWithParameter:url withParams:paramDict withHeaders:nil callBackSuccess:^(NSData *dict, NSURLResponse *response) {
        NSError *error = nil;
        
        NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:dict
                                                                     options:NSJSONReadingAllowFragments
                                                                       error:&error];
        
        if (error) {
            NSString *str = [[NSString alloc] initWithData:dict encoding:NSUTF8StringEncoding];
            NSLog(@"%@",str);
            NSLog(@"Error : %@",error.localizedDescription);
        }
        
        success(dictResponse.dictByReplacingNulls,response);
    } callBackError:^(NSError *err, NSURLResponse *response) {
        error(err,response);
    }];
}

+(void) callForGetTeamCoachList:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                          Error:(void (^)(NSError *error, NSURLResponse *response))error {
    
    NSString *url = [NSString stringWithFormat:@"%@%@",@"",GET_TEAM_COACH_LIST_URL];
    
    [WebAPI callPOSTWithParameter:url withParams:paramDict withHeaders:nil callBackSuccess:^(NSData *dict, NSURLResponse *response) {
        NSError *error = nil;
        
        NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:dict
                                                                     options:NSJSONReadingAllowFragments
                                                                       error:&error];
        
        if (error) {
            NSString *str = [[NSString alloc] initWithData:dict encoding:NSUTF8StringEncoding];
            NSLog(@"%@",str);
            NSLog(@"Error : %@",error.localizedDescription);
        }
        
        success(dictResponse.dictByReplacingNulls,response);
    } callBackError:^(NSError *err, NSURLResponse *response) {
        error(err,response);
    }];
}

+(void) callForGetHealthAlertList:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                            Error:(void (^)(NSError *error, NSURLResponse *response))error {
    
    NSString *url = [NSString stringWithFormat:@"%@%@",@"",GET_HealthAlert_list_URL];
    
    [WebAPI callPOSTWithParameter:url withParams:paramDict withHeaders:nil callBackSuccess:^(NSData *dict, NSURLResponse *response) {
        NSError *error = nil;
        
        NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:dict
                                                                     options:NSJSONReadingAllowFragments
                                                                       error:&error];
        
        if (error) {
            NSString *str = [[NSString alloc] initWithData:dict encoding:NSUTF8StringEncoding];
            NSLog(@"%@",str);
            NSLog(@"Error : %@",error.localizedDescription);
        }
        
        success(dictResponse.dictByReplacingNulls,response);
    } callBackError:^(NSError *err, NSURLResponse *response) {
        error(err,response);
    }];
}



+(void) callForDeleteHealthAlert:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                     Error:(void (^)(NSError *error, NSURLResponse *response))error {
    
    NSString *url = [NSString stringWithFormat:@"%@%@",@"",DELETE_HEALTH_ALERT];
    
    [WebAPI callPOSTWithParameter:url withParams:paramDict withHeaders:nil callBackSuccess:^(NSData *dict, NSURLResponse *response) {
        NSError *error = nil;
        NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:dict
                                                                     options:NSJSONReadingAllowFragments
                                                                       error:&error];
        success(dictResponse.dictByReplacingNulls,response);
    } callBackError:^(NSError *err, NSURLResponse *response) {
        error(err,response);
    }];
}


+(void) callForSearchParticipant:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                            Error:(void (^)(NSError *error, NSURLResponse *response))error {
    
    NSString *url = [NSString stringWithFormat:@"%@%@",@"",SEARCH_PARTICIPANT_URL];
    
    [WebAPI callPOSTWithParameter:url withParams:paramDict withHeaders:nil callBackSuccess:^(NSData *dict, NSURLResponse *response) {
        NSError *error = nil;
        
        NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:dict
                                                                     options:NSJSONReadingAllowFragments
                                                                       error:&error];
        
        if (error) {
            NSString *str = [[NSString alloc] initWithData:dict encoding:NSUTF8StringEncoding];
            NSLog(@"%@",str);
            NSLog(@"Error : %@",error.localizedDescription);
        }
        
        success(dictResponse.dictByReplacingNulls,response);
    } callBackError:^(NSError *err, NSURLResponse *response) {
        error(err,response);
    }];
}

+(void) callForSearchTeamCoach:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                           Error:(void (^)(NSError *error, NSURLResponse *response))error {
    
    NSString *url = [NSString stringWithFormat:@"%@%@",@"",SEARCH_TEAMCOACH_URL];
    
    [WebAPI callPOSTWithParameter:url withParams:paramDict withHeaders:nil callBackSuccess:^(NSData *dict, NSURLResponse *response) {
        NSError *error = nil;
        
        NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:dict
                                                                     options:NSJSONReadingAllowFragments
                                                                       error:&error];
        
        if (error) {
            NSString *str = [[NSString alloc] initWithData:dict encoding:NSUTF8StringEncoding];
            NSLog(@"%@",str);
            NSLog(@"Error : %@",error.localizedDescription);
        }
        
        success(dictResponse.dictByReplacingNulls,response);
    } callBackError:^(NSError *err, NSURLResponse *response) {
        error(err,response);
    }];
}

+(void) callForGetParticipantFullDetail:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                            Error:(void (^)(NSError *error, NSURLResponse *response))error {
    
    NSString *url = [NSString stringWithFormat:@"%@%@",@"",GET_PARTICIPANT_FULL_DETAIL_URL];
    
    [WebAPI callPOSTWithParameter:url withParams:paramDict withHeaders:nil callBackSuccess:^(NSData *dict, NSURLResponse *response) {
        NSError *error = nil;
        
        NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:dict
                                                                     options:NSJSONReadingAllowFragments
                                                                       error:&error];
        
        if (error) {
            NSString *str = [[NSString alloc] initWithData:dict encoding:NSUTF8StringEncoding];
            NSLog(@"%@",str);
            NSLog(@"Error : %@",error.localizedDescription);
        }
        
        success(dictResponse.dictByReplacingNulls,response);
    } callBackError:^(NSError *err, NSURLResponse *response) {
        error(err,response);
    }];
}

+(void) callForGetTeamCoachFullDetail:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                                  Error:(void (^)(NSError *error, NSURLResponse *response))error {
    
    NSString *url = [NSString stringWithFormat:@"%@%@",@"",GET_TEAMCOACH_FULL_DETAIL_URL];
   
    [WebAPI callPOSTWithParameter:url withParams:paramDict withHeaders:nil callBackSuccess:^(NSData *dict, NSURLResponse *response) {
        NSError *error = nil;
        
        NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:dict
                                                                     options:NSJSONReadingAllowFragments
                                                                       error:&error];
        if (error) {
            NSString *str = [[NSString alloc] initWithData:dict encoding:NSUTF8StringEncoding];
            NSLog(@"%@",str);
            NSLog(@"Error : %@",error.localizedDescription);
        }
        
        success(dictResponse.dictByReplacingNulls,response);
    } callBackError:^(NSError *err, NSURLResponse *response) {
        error(err,response);
    }];
}

+(void) callForGetNoteList:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                            Error:(void (^)(NSError *error, NSURLResponse *response))error {
    
    NSString *url = [NSString stringWithFormat:@"%@%@",@"",GET_PARTICIPANT_NOTE_LIST_URL];
    
    [WebAPI callPOSTWithParameter:url withParams:paramDict withHeaders:nil callBackSuccess:^(NSData *dict, NSURLResponse *response) {
        NSError *error = nil;
        
        NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:dict
                                                                     options:NSJSONReadingAllowFragments
                                                                       error:&error];
        
        if (error) {
            NSString *str = [[NSString alloc] initWithData:dict encoding:NSUTF8StringEncoding];
            NSLog(@"%@",str);
            NSLog(@"Error : %@",error.localizedDescription);
        }
        
        success(dictResponse.dictByReplacingNulls,response);
    } callBackError:^(NSError *err, NSURLResponse *response) {
        error(err,response);
    }];
}

+(void) callForGetNoteAttachmentsList:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                     Error:(void (^)(NSError *error, NSURLResponse *response))error {
    
    NSString *url = [NSString stringWithFormat:@"%@%@",@"",GET_PARTICIPANT_NOTE_ATTACHMENT_LIST_URL];
    
    [WebAPI callPOSTWithParameter:url withParams:paramDict withHeaders:nil callBackSuccess:^(NSData *dict, NSURLResponse *response) {
        NSError *error = nil;
        
        NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:dict
                                                                     options:NSJSONReadingAllowFragments
                                                                       error:&error];
        
        if (error) {
            NSString *str = [[NSString alloc] initWithData:dict encoding:NSUTF8StringEncoding];
            NSLog(@"%@",str);
            NSLog(@"Error : %@",error.localizedDescription);
        }
        
        success(dictResponse.dictByReplacingNulls,response);
    } callBackError:^(NSError *err, NSURLResponse *response) {
        error(err,response);
    }];
}

+(void) callForAddNote:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                                Error:(void (^)(NSError *error, NSURLResponse *response))error {
    
    NSString *url = [NSString stringWithFormat:@"%@%@",@"",ADD_NOTE_URL];
    
    [WebAPI callPOSTWithParameter:url withParams:paramDict withHeaders:nil callBackSuccess:^(NSData *dict, NSURLResponse *response) {
        NSError *error = nil;
        
        NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:dict
                                                                     options:NSJSONReadingAllowFragments
                                                                       error:&error];
        
        if (error) {
            NSString *str = [[NSString alloc] initWithData:dict encoding:NSUTF8StringEncoding];
            NSLog(@"%@",str);
            NSLog(@"Error : %@",error.localizedDescription);
        }
        
        success(dictResponse.dictByReplacingNulls,response);
    } callBackError:^(NSError *err, NSURLResponse *response) {
        error(err,response);
    }];
}

+(void) callForEditNote:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                 Error:(void (^)(NSError *error, NSURLResponse *response))error {
    
    NSString *url = [NSString stringWithFormat:@"%@%@",@"",EDIT_NOTE_URL];
    
    [WebAPI callPOSTWithParameter:url withParams:paramDict withHeaders:nil callBackSuccess:^(NSData *dict, NSURLResponse *response) {
        NSError *error = nil;
        
        NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:dict
                                                                     options:NSJSONReadingAllowFragments
                                                                       error:&error];
        
        if (error) {
            NSString *str = [[NSString alloc] initWithData:dict encoding:NSUTF8StringEncoding];
            NSLog(@"%@",str);
            NSLog(@"Error : %@",error.localizedDescription);
        }
        
        success(dictResponse.dictByReplacingNulls,response);
    } callBackError:^(NSError *err, NSURLResponse *response) {
        error(err,response);
    }];
}

+(void) callForDeleteNote:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                 Error:(void (^)(NSError *error, NSURLResponse *response))error {
    
    NSString *url = [NSString stringWithFormat:@"%@%@",@"",DELETE_NOTE_URL];
    
    [WebAPI callPOSTWithParameter:url withParams:paramDict withHeaders:nil callBackSuccess:^(NSData *dict, NSURLResponse *response) {
        NSError *error = nil;
        
        NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:dict
                                                                     options:NSJSONReadingAllowFragments
                                                                       error:&error];
        
        if (error) {
            NSString *str = [[NSString alloc] initWithData:dict encoding:NSUTF8StringEncoding];
            NSLog(@"%@",str);
            NSLog(@"Error : %@",error.localizedDescription);
        }
        
        success(dictResponse.dictByReplacingNulls,response);
    } callBackError:^(NSError *err, NSURLResponse *response) {
        error(err,response);
    }];
}

+(void) callForGetAssessmentList:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                            Error:(void (^)(NSError *error, NSURLResponse *response))error {
    
    NSString *url = [NSString stringWithFormat:@"%@%@",@"",GET_ASSESSMENT_LIST_URL];
    
    [WebAPI callPOSTWithParameter:url withParams:paramDict withHeaders:nil callBackSuccess:^(NSData *dict, NSURLResponse *response) {
        NSError *error = nil;
        
        NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:dict
                                                                     options:NSJSONReadingAllowFragments
                                                                       error:&error];
        
        if (error) {
            NSString *str = [[NSString alloc] initWithData:dict encoding:NSUTF8StringEncoding];
            NSLog(@"%@",str);
            NSLog(@"Error : %@",error.localizedDescription);
        }
        
        success(dictResponse.dictByReplacingNulls,response);
    } callBackError:^(NSError *err, NSURLResponse *response) {
        error(err,response);
    }];
}

+(void) callForCreateAssessment:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                           Error:(void (^)(NSError *error, NSURLResponse *response))error {
    
    NSString *url = [NSString stringWithFormat:@"%@%@",@"",ADD_ASSESSMENT_URL];
    
    [WebAPI callPOSTWithParameter:url withParams:paramDict withHeaders:nil callBackSuccess:^(NSData *dict, NSURLResponse *response) {
        NSError *error = nil;
        
        NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:dict
                                                                     options:NSJSONReadingAllowFragments
                                                                       error:&error];
        
        if (error) {
            NSString *str = [[NSString alloc] initWithData:dict encoding:NSUTF8StringEncoding];
            NSLog(@"%@",str);
            NSLog(@"Error : %@",error.localizedDescription);
        }
        
        success(dictResponse.dictByReplacingNulls,response);
    } callBackError:^(NSError *err, NSURLResponse *response) {
        error(err,response);
    }];
}

+(void) callForEditAssessment:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                          Error:(void (^)(NSError *error, NSURLResponse *response))error {
    
    NSString *url = [NSString stringWithFormat:@"%@%@",@"",EDIT_ASSESSMENT_URL];
    
    [WebAPI callPOSTWithParameter:url withParams:paramDict withHeaders:nil callBackSuccess:^(NSData *dict, NSURLResponse *response) {
        NSError *error = nil;
        
        NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:dict
                                                                     options:NSJSONReadingAllowFragments
                                                                       error:&error];
        
        if (error) {
            NSString *str = [[NSString alloc] initWithData:dict encoding:NSUTF8StringEncoding];
            NSLog(@"%@",str);
            NSLog(@"Error : %@",error.localizedDescription);
        }
        
        success(dictResponse.dictByReplacingNulls,response);
    } callBackError:^(NSError *err, NSURLResponse *response) {
        error(err,response);
    }];
}

+(void) callForDeleteAssessment:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                          Error:(void (^)(NSError *error, NSURLResponse *response))error {
    
    NSString *url = [NSString stringWithFormat:@"%@%@",@"",DELETE_ASSESSMENT_URL];
    
    [WebAPI callPOSTWithParameter:url withParams:paramDict withHeaders:nil callBackSuccess:^(NSData *dict, NSURLResponse *response) {
        NSError *error = nil;
        
        NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:dict
                                                                     options:NSJSONReadingAllowFragments
                                                                       error:&error];
        
        if (error) {
            NSString *str = [[NSString alloc] initWithData:dict encoding:NSUTF8StringEncoding];
            NSLog(@"%@",str);
            NSLog(@"Error : %@",error.localizedDescription);
        }
        
        success(dictResponse.dictByReplacingNulls,response);
    } callBackError:^(NSError *err, NSURLResponse *response) {
        error(err,response);
    }];
}

+(void) callForGetAssessmentDetail:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                          Error:(void (^)(NSError *error, NSURLResponse *response))error {
    
    NSString *url = [NSString stringWithFormat:@"%@%@",@"",GET_ASSESSMENT_DETAIL_URL];
    
    [WebAPI callPOSTWithParameter:url withParams:paramDict withHeaders:nil callBackSuccess:^(NSData *dict, NSURLResponse *response) {
        NSError *error = nil;
        
        NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:dict
                                                                     options:NSJSONReadingAllowFragments
                                                                       error:&error];
        
        if (error) {
            NSString *str = [[NSString alloc] initWithData:dict encoding:NSUTF8StringEncoding];
            NSLog(@"%@",str);
            NSLog(@"Error : %@",error.localizedDescription);
        }
        
        success(dictResponse.dictByReplacingNulls,response);
    } callBackError:^(NSError *err, NSURLResponse *response) {
        error(err,response);
    }];
}

+(void) callForAddEditLearningInfo:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                          Error:(void (^)(NSError *error, NSURLResponse *response))error {
    
    NSString *url = [NSString stringWithFormat:@"%@%@",@"",ADD_EDIT_LEARNING_APPLYING_KNOWLEDGE_URL];
    
    [WebAPI callPOSTWithParameter:url withParams:paramDict withHeaders:nil callBackSuccess:^(NSData *dict, NSURLResponse *response) {
        NSError *error = nil;
        
        NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:dict
                                                                     options:NSJSONReadingAllowFragments
                                                                       error:&error];
        
        if (error) {
            NSString *str = [[NSString alloc] initWithData:dict encoding:NSUTF8StringEncoding];
            NSLog(@"%@",str);
            NSLog(@"Error : %@",error.localizedDescription);
        }
        
        success(dictResponse.dictByReplacingNulls,response);
    } callBackError:^(NSError *err, NSURLResponse *response) {
        error(err,response);
    }];
}

+(void) callForAddEditAssessmentSubDetail:(NSMutableDictionary *)paramDict apiName:(NSString *)apiName Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                             Error:(void (^)(NSError *error, NSURLResponse *response))error {
    
    NSString *url = [NSString stringWithFormat:@"%@%@",@"",apiName];
    
    [WebAPI callPOSTWithParameter:url withParams:paramDict withHeaders:nil callBackSuccess:^(NSData *dict, NSURLResponse *response) {
        NSError *error = nil;
        
        NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:dict
                                                                     options:NSJSONReadingAllowFragments
                                                                       error:&error];
        
        if (error) {
            NSString *str = [[NSString alloc] initWithData:dict encoding:NSUTF8StringEncoding];
            NSLog(@"%@",str);
            NSLog(@"Error : %@",error.localizedDescription);
        }
        
        success(dictResponse.dictByReplacingNulls,response);
    } callBackError:^(NSError *err, NSURLResponse *response) {
        error(err,response);
    }];
}

+(void) callForGetStaffUserDetails:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                     Error:(void (^)(NSError *error, NSURLResponse *response))error {
    
    NSString *url = [NSString stringWithFormat:@"%@%@",@"",GET_STAFF_PERSONAL_PROFILE_URL];
    
    [WebAPI callPOSTWithParameter:url withParams:paramDict withHeaders:nil callBackSuccess:^(NSData *dict, NSURLResponse *response) {
        NSError *error = nil;
        NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:dict
                                                                     options:NSJSONReadingAllowFragments
                                                                       error:&error];
        success(dictResponse.dictByReplacingNulls,response);
    } callBackError:^(NSError *err, NSURLResponse *response) {
        error(err,response);
    }];
}

+(void) callForUpdateStaffPersonalProfile:(NSMutableDictionary *)paramDict imageDict:(NSMutableDictionary *)imageDict fileName:(NSString *)fileName Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                     Error:(void (^)(NSError *error, NSURLResponse *response))error {
    
    NSString *url = [NSString stringWithFormat:@"%@%@",@"",UPDATE_STAFF_PERSONAL_PROFILE_URL];
    
    [WebAPI callPOSTWithParameterImage1:url withParams:paramDict imageDict:imageDict fileName:fileName withHeaders:nil callBackSuccess:^(NSData *dict, NSURLResponse *response) {
        
        NSError *error = nil;
        NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:dict
                                                                     options:NSJSONReadingAllowFragments
                                                                       error:&error];
        
        if (error) {
            NSString *str = [[NSString alloc] initWithData:dict encoding:NSUTF8StringEncoding];
            NSLog(@"%@",str);
            NSLog(@"Error : %@",error.localizedDescription);
        }
        
        success(dictResponse.dictByReplacingNulls,response);
    } callBackError:^(NSError *err, NSURLResponse *response) {
        error(err,response);
    }];
}

+(void) callForGetAttachmentsList:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                                Error:(void (^)(NSError *error, NSURLResponse *response))error {
    
    NSString *url = [NSString stringWithFormat:@"%@%@",@"",GET_ATTACHMENT_LIST_URL];
    
    [WebAPI callPOSTWithParameter:url withParams:paramDict withHeaders:nil callBackSuccess:^(NSData *dict, NSURLResponse *response) {
        NSError *error = nil;
        
        NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:dict
                                                                     options:NSJSONReadingAllowFragments
                                                                       error:&error];
        
        if (error) {
            NSString *str = [[NSString alloc] initWithData:dict encoding:NSUTF8StringEncoding];
            NSLog(@"%@",str);
            NSLog(@"Error : %@",error.localizedDescription);
        }
        
        success(dictResponse.dictByReplacingNulls,response);
    } callBackError:^(NSError *err, NSURLResponse *response) {
        error(err,response);
    }];
}

+(void) callForDeleteAttachment:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                            Error:(void (^)(NSError *error, NSURLResponse *response))error {
    
    NSString *url = [NSString stringWithFormat:@"%@%@",@"",DELETE_ATTACHMENT_URL];
    
    [WebAPI callPOSTWithParameter:url withParams:paramDict withHeaders:nil callBackSuccess:^(NSData *dict, NSURLResponse *response) {
        NSError *error = nil;
        
        NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:dict
                                                                     options:NSJSONReadingAllowFragments
                                                                       error:&error];
        
        if (error) {
            NSString *str = [[NSString alloc] initWithData:dict encoding:NSUTF8StringEncoding];
            NSLog(@"%@",str);
            NSLog(@"Error : %@",error.localizedDescription);
        }
        
        success(dictResponse.dictByReplacingNulls,response);
    } callBackError:^(NSError *err, NSURLResponse *response) {
        error(err,response);
    }];
}

+(void) callForUploadAttachment:(NSMutableDictionary *)paramDict fileName:(NSString *)fileName Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                          Error:(void (^)(NSError *error, NSURLResponse *response))error
{
    NSString *url = [NSString stringWithFormat:@"%@",@""];
    
    [WebAPI callPOSTWithParameterAttachment:url withParams:paramDict fileName:fileName withHeaders:nil callBackSuccess:^(NSData *dict, NSURLResponse *response) {
        
        NSError *error = nil;
        NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:dict
                                                                     options:NSJSONReadingAllowFragments
                                                                       error:&error];
        
        if (error) {
            NSString *str = [[NSString alloc] initWithData:dict encoding:NSUTF8StringEncoding];
            NSLog(@"%@",str);
            NSLog(@"Error : %@",error.localizedDescription);
        }
        
        success(dictResponse.dictByReplacingNulls,response);
        
    } callBackError:^(NSError *err, NSURLResponse *response) {
        error(err,response);
    }];
}

+(void) callForUploadPhoto:(NSMutableDictionary *)paramDict fileName:(NSString *)fileName strId:(NSString *)strId Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                     Error:(void (^)(NSError *error, NSURLResponse *response))error{
    
    NSString *url = [NSString stringWithFormat:@"https://www.zohoapis.com/crm/v2/Contacts/%@/photo",strId];
    NSMutableDictionary *dcitParams = [[NSMutableDictionary alloc] init];
    [dcitParams setValue:@"" forKey:@"authtoken"];
    [WebAPI callPOSTWithParameterPhoto:url withParams:paramDict fileName:fileName withHeaders:dcitParams callBackSuccess:^(NSData *dict, NSURLResponse *response) {
        NSError *error = nil;
        NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:dict
                                                                     options:NSJSONReadingAllowFragments
                                                                       error:&error];
        
        if (error) {
            NSString *str = [[NSString alloc] initWithData:dict encoding:NSUTF8StringEncoding];
            NSLog(@"%@",str);
            NSLog(@"Error : %@",error.localizedDescription);
        }
        
        success(dictResponse.dictByReplacingNulls,response);
    } callBackError:^(NSError *err, NSURLResponse *response) {
        error(err,response);

    }];
    
}


+(void) callForUploadPhotoEmployee:(NSMutableDictionary *)paramDict fileName:(NSString *)fileName strId:(NSString *)strId Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                     Error:(void (^)(NSError *error, NSURLResponse *response))error{
    
    NSString *url = [NSString stringWithFormat:@"https://www.zohoapis.com/crm/v2/CustomModule5/%@/photo",strId];
    NSMutableDictionary *dcitParams = [[NSMutableDictionary alloc] init];
    [dcitParams setValue:@"" forKey:@"authtoken"];
    [WebAPI callPOSTWithParameterPhoto:url withParams:paramDict fileName:fileName withHeaders:dcitParams callBackSuccess:^(NSData *dict, NSURLResponse *response) {
        NSError *error = nil;
        NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:dict
                                                                     options:NSJSONReadingAllowFragments
                                                                       error:&error];
        
        if (error) {
            NSString *str = [[NSString alloc] initWithData:dict encoding:NSUTF8StringEncoding];
            NSLog(@"%@",str);
            NSLog(@"Error : %@",error.localizedDescription);
        }
        
        success(dictResponse.dictByReplacingNulls,response);
    } callBackError:^(NSError *err, NSURLResponse *response) {
        error(err,response);
        
    }];
    
}






+(void) callForGetCarerList:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                          Error:(void (^)(NSError *error, NSURLResponse *response))error {
    
    NSString *url = [NSString stringWithFormat:@"%@%@",@"",CARER_LIST_URL];
    
    [WebAPI callPOSTWithParameter:url withParams:paramDict withHeaders:nil callBackSuccess:^(NSData *dict, NSURLResponse *response) {
        NSError *error = nil;
        
        NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:dict
                                                                     options:NSJSONReadingAllowFragments
                                                                       error:&error];
        
        if (error) {
            NSString *str = [[NSString alloc] initWithData:dict encoding:NSUTF8StringEncoding];
            NSLog(@"%@",str);
            NSLog(@"Error : %@",error.localizedDescription);
        }
        
        success(dictResponse.dictByReplacingNulls,response);
    } callBackError:^(NSError *err, NSURLResponse *response) {
        error(err,response);
    }];
}

+(void) callForDeleteCarer:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                      Error:(void (^)(NSError *error, NSURLResponse *response))error {
    
    NSString *url = [NSString stringWithFormat:@"%@%@",@"",DELETE_CARER_URL];
    
    [WebAPI callPOSTWithParameter:url withParams:paramDict withHeaders:nil callBackSuccess:^(NSData *dict, NSURLResponse *response) {
        NSError *error = nil;
        
        NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:dict
                                                                     options:NSJSONReadingAllowFragments
                                                                       error:&error];
        
        if (error) {
            NSString *str = [[NSString alloc] initWithData:dict encoding:NSUTF8StringEncoding];
            NSLog(@"%@",str);
            NSLog(@"Error : %@",error.localizedDescription);
        }
        
        success(dictResponse.dictByReplacingNulls,response);
    } callBackError:^(NSError *err, NSURLResponse *response) {
        error(err,response);
    }];
}

+(void) callForAddEditCarer:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                     Error:(void (^)(NSError *error, NSURLResponse *response))error {
    
    NSString *url = [NSString stringWithFormat:@"%@%@",@"",ADD_EDIT_CARER_URL];
    
    [WebAPI callPOSTWithParameter:url withParams:paramDict withHeaders:nil callBackSuccess:^(NSData *dict, NSURLResponse *response) {
        NSError *error = nil;
        
        NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:dict
                                                                     options:NSJSONReadingAllowFragments
                                                                       error:&error];
        
        if (error) {
            NSString *str = [[NSString alloc] initWithData:dict encoding:NSUTF8StringEncoding];
            NSLog(@"%@",str);
            NSLog(@"Error : %@",error.localizedDescription);
        }
        
        success(dictResponse.dictByReplacingNulls,response);
    } callBackError:^(NSError *err, NSURLResponse *response) {
        error(err,response);
    }];
}

+(void) callForGetPersonalCareList:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                      Error:(void (^)(NSError *error, NSURLResponse *response))error {
    
    NSString *url = [NSString stringWithFormat:@"%@%@",@"",GET_PERSONAL_CARE_LIST_URL];
    
    [WebAPI callPOSTWithParameter:url withParams:paramDict withHeaders:nil callBackSuccess:^(NSData *dict, NSURLResponse *response) {
        NSError *error = nil;
        
        NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:dict
                                                                     options:NSJSONReadingAllowFragments
                                                                       error:&error];
        
        if (error) {
            NSString *str = [[NSString alloc] initWithData:dict encoding:NSUTF8StringEncoding];
            NSLog(@"%@",str);
            NSLog(@"Error : %@",error.localizedDescription);
        }
        
        success(dictResponse.dictByReplacingNulls,response);
    } callBackError:^(NSError *err, NSURLResponse *response) {
        error(err,response);
    }];
}

+(void) callForAddEditPersonalCare:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                             Error:(void (^)(NSError *error, NSURLResponse *response))error {
    
    NSString *url = [NSString stringWithFormat:@"%@%@",@"",ADD_EDIT_PERSONAL_CARE_URL];
    
    [WebAPI callPOSTWithParameter:url withParams:paramDict withHeaders:nil callBackSuccess:^(NSData *dict, NSURLResponse *response) {
        NSError *error = nil;
        
        NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:dict
                                                                     options:NSJSONReadingAllowFragments
                                                                       error:&error];
        
        if (error) {
            NSString *str = [[NSString alloc] initWithData:dict encoding:NSUTF8StringEncoding];
            NSLog(@"%@",str);
            NSLog(@"Error : %@",error.localizedDescription);
        }
        
        success(dictResponse.dictByReplacingNulls,response);
    } callBackError:^(NSError *err, NSURLResponse *response) {
        error(err,response);
    }];
}

+(void) callForDeletePersonalCare:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                                Error:(void (^)(NSError *error, NSURLResponse *response))error {
    
    NSString *url = [NSString stringWithFormat:@"%@%@",@"",DELETE_PERSONAL_CARE_URL];
    
    [WebAPI callPOSTWithParameter:url withParams:paramDict withHeaders:nil callBackSuccess:^(NSData *dict, NSURLResponse *response) {
        NSError *error = nil;
        
        NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:dict
                                                                     options:NSJSONReadingAllowFragments
                                                                       error:&error];
        
        if (error) {
            NSString *str = [[NSString alloc] initWithData:dict encoding:NSUTF8StringEncoding];
            NSLog(@"%@",str);
            NSLog(@"Error : %@",error.localizedDescription);
        }
        
        success(dictResponse.dictByReplacingNulls,response);
    } callBackError:^(NSError *err, NSURLResponse *response) {
        error(err,response);
    }];
}

+(void) callForGetConcernList:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                            Error:(void (^)(NSError *error, NSURLResponse *response))error {
    
    NSString *url = [NSString stringWithFormat:@"%@%@",@"",GET_CONCERN_LIST_URL];
    
    [WebAPI callPOSTWithParameter:url withParams:paramDict withHeaders:nil callBackSuccess:^(NSData *dict, NSURLResponse *response) {
        NSError *error = nil;
        
        NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:dict
                                                                     options:NSJSONReadingAllowFragments
                                                                       error:&error];
        
        if (error) {
            NSString *str = [[NSString alloc] initWithData:dict encoding:NSUTF8StringEncoding];
            NSLog(@"%@",str);
            NSLog(@"Error : %@",error.localizedDescription);
        }
        
        success(dictResponse.dictByReplacingNulls,response);
    } callBackError:^(NSError *err, NSURLResponse *response) {
        error(err,response);
    }];
}

+(void) callForAddEditConcern:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                        Error:(void (^)(NSError *error, NSURLResponse *response))error {
    
    NSString *url = [NSString stringWithFormat:@"%@%@",@"",ADD_EDIT_CONCERN_URL];
    
    [WebAPI callPOSTWithParameter:url withParams:paramDict withHeaders:nil callBackSuccess:^(NSData *dict, NSURLResponse *response) {
        NSError *error = nil;
        
        NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:dict
                                                                     options:NSJSONReadingAllowFragments
                                                                       error:&error];
        
        if (error) {
            NSString *str = [[NSString alloc] initWithData:dict encoding:NSUTF8StringEncoding];
            NSLog(@"%@",str);
            NSLog(@"Error : %@",error.localizedDescription);
        }
        
        success(dictResponse.dictByReplacingNulls,response);
    } callBackError:^(NSError *err, NSURLResponse *response) {
        error(err,response);
    }];
}

+(void) callForDeleteConcern:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                        Error:(void (^)(NSError *error, NSURLResponse *response))error {
    
    NSString *url = [NSString stringWithFormat:@"%@%@",@"",DELETE_CONCERN_URL];
    
    [WebAPI callPOSTWithParameter:url withParams:paramDict withHeaders:nil callBackSuccess:^(NSData *dict, NSURLResponse *response) {
        NSError *error = nil;
        
        NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:dict
                                                                     options:NSJSONReadingAllowFragments
                                                                       error:&error];
        
        if (error) {
            NSString *str = [[NSString alloc] initWithData:dict encoding:NSUTF8StringEncoding];
            NSLog(@"%@",str);
            NSLog(@"Error : %@",error.localizedDescription);
        }
        
        success(dictResponse.dictByReplacingNulls,response);
    } callBackError:^(NSError *err, NSURLResponse *response) {
        error(err,response);
    }];
}

+(void) callForGetConfigData:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                       Error:(void (^)(NSError *error, NSURLResponse *response))error {
    
    NSString *url = [NSString stringWithFormat:@"%@%@",@"",GET_CONFIG_DATA_URL];
    NSLog(@" url for config data :- %@", url);
    
    [WebAPI callPOSTWithParameter:url withParams:paramDict withHeaders:nil callBackSuccess:^(NSData *dict, NSURLResponse *response) {
        NSError *error = nil;
        
        NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:dict
                                                                     options:NSJSONReadingAllowFragments
                                                                       error:&error];
        
        if (error) {
            NSString *str = [[NSString alloc] initWithData:dict encoding:NSUTF8StringEncoding];
            NSLog(@"%@",str);
            NSLog(@"Error : %@",error.localizedDescription);
        }
        
        success(dictResponse.dictByReplacingNulls,response);
    } callBackError:^(NSError *err, NSURLResponse *response) {
        error(err,response);
    }];
}

+(void) callForGetDynamicData:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                           Error:(void (^)(NSError *error, NSURLResponse *response))error {
    
    NSString *url = [NSString stringWithFormat:@"%@%@",@"",@"all_fileds"];
    
    [WebAPI callPOSTWithParameter:url withParams:paramDict withHeaders:nil callBackSuccess:^(NSData *dict, NSURLResponse *response) {
        NSError *error = nil;
        
        NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:dict
                                                                     options:NSJSONReadingAllowFragments
                                                                       error:&error];
        
        if (error) {
            NSString *str = [[NSString alloc] initWithData:dict encoding:NSUTF8StringEncoding];
            NSLog(@"%@",str);
            NSLog(@"Error : %@",error.localizedDescription);
        }
        
        success(dictResponse.dictByReplacingNulls,response);
    } callBackError:^(NSError *err, NSURLResponse *response) {
        error(err,response);
    }];
}

+(void) callForUpdateParticipantEmergencyInfo:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                        Error:(void (^)(NSError *error, NSURLResponse *response))error {
    
    NSString *url = [NSString stringWithFormat:@"%@%@",@"",UPDATE_PARTICIPANT_EMERGENCY_INFO];
    
    [WebAPI callPOSTWithParameter:url withParams:paramDict withHeaders:nil callBackSuccess:^(NSData *dict, NSURLResponse *response) {
        NSError *error = nil;
        
        NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:dict
                                                                     options:NSJSONReadingAllowFragments
                                                                       error:&error];
        
        if (error) {
            NSString *str = [[NSString alloc] initWithData:dict encoding:NSUTF8StringEncoding];
            NSLog(@"%@",str);
            NSLog(@"Error : %@",error.localizedDescription);
        }
        
        success(dictResponse.dictByReplacingNulls,response);
    } callBackError:^(NSError *err, NSURLResponse *response) {
        error(err,response);
    }];
}

+(void) callForUpdateParticipantHealthInfo:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                                        Error:(void (^)(NSError *error, NSURLResponse *response))error {
    
    NSString *url = [NSString stringWithFormat:@"%@%@",@"",UPDATE_PARTICIPANT_HEALTH_INFO];
    
    [WebAPI callPOSTWithParameter:url withParams:paramDict withHeaders:nil callBackSuccess:^(NSData *dict, NSURLResponse *response) {
        NSError *error = nil;
        
        NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:dict
                                                                     options:NSJSONReadingAllowFragments
                                                                       error:&error];
        
        if (error) {
            NSString *str = [[NSString alloc] initWithData:dict encoding:NSUTF8StringEncoding];
            NSLog(@"%@",str);
            NSLog(@"Error : %@",error.localizedDescription);
        }
        
        success(dictResponse.dictByReplacingNulls,response);
    } callBackError:^(NSError *err, NSURLResponse *response) {
        error(err,response);
    }];
}

+(void) callForUpdateParticipantPersonalInfo:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                                     Error:(void (^)(NSError *error, NSURLResponse *response))error {
    
    NSString *url = [NSString stringWithFormat:@"%@%@",@"",UPDATE_PARTICIPANT_PERSONAL_INFO];
    
    [WebAPI callPOSTWithParameter:url withParams:paramDict withHeaders:nil callBackSuccess:^(NSData *dict, NSURLResponse *response) {
        NSError *error = nil;
        
        NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:dict
                                                                     options:NSJSONReadingAllowFragments
                                                                       error:&error];
        
        if (error) {
            NSString *str = [[NSString alloc] initWithData:dict encoding:NSUTF8StringEncoding];
            NSLog(@"%@",str);
            NSLog(@"Error : %@",error.localizedDescription);
        }
        
        success(dictResponse.dictByReplacingNulls,response);
    } callBackError:^(NSError *err, NSURLResponse *response) {
        error(err,response);
    }];
}

+(void) callForGetKnownDoctorList:(NSMutableDictionary *)paramDict Sucess:(void (^)(NSDictionary *dict, NSURLResponse *response))success
                                     Error:(void (^)(NSError *error, NSURLResponse *response))error {
    
    NSString *url = [NSString stringWithFormat:@"%@%@",@"",GET_KNOWN_DOCTOR_LIST_URL];
    
    [WebAPI callPOSTWithParameter:url withParams:paramDict withHeaders:nil callBackSuccess:^(NSData *dict, NSURLResponse *response) {
        NSError *error = nil;
        
        NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:dict
                                                                     options:NSJSONReadingAllowFragments
                                                                       error:&error];
        
        if (error) {
            NSString *str = [[NSString alloc] initWithData:dict encoding:NSUTF8StringEncoding];
            NSLog(@"%@",str);
            NSLog(@"Error : %@",error.localizedDescription);
        }
        
        success(dictResponse.dictByReplacingNulls,response);
    } callBackError:^(NSError *err, NSURLResponse *response) {
        error(err,response);
    }];
}

@end
