//
//  WebAPI.h
//  Interchange
//
//  Created by whiznic on 1/8/18.
//  Copyright © 2018 com.production. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WebAPI : NSObject

+(NSMutableURLRequest *)makeRequest:(NSString*)url withHeaders:(NSDictionary *)keyVal;

+(void)callGET:(NSString*)url withQuery:(NSString *)query
   withHeaders:(NSDictionary *)keyVal
callBackSuccess:(void (^)(NSData *dict, NSURLResponse *response))success
 callBackError:(void (^)(NSError *error, NSURLResponse *response))error;

+(void)callPOSTWithParameter:(NSString*)url withParams:(NSMutableDictionary *)dict
    withHeaders:(NSDictionary *)keyVal
callBackSuccess:(void (^)(NSData *dict, NSURLResponse *response))success
  callBackError:(void (^)(NSError *error, NSURLResponse *response))error;

+(void)callPOSTWithParameter1:(NSString*)url withParams:(NSMutableDictionary *)dict
                 withHeaders:(NSDictionary *)keyVal
             callBackSuccess:(void (^)(NSData *dict, NSURLResponse *response))success
               callBackError:(void (^)(NSError *error, NSURLResponse *response))error;

+(void)callPOSTWithParameterImage:(NSString*)url withParams:(NSMutableDictionary *)params
                      withHeaders:(NSDictionary *)keyVal
                  callBackSuccess:(void (^)(NSData *dict, NSURLResponse *response))success
                    callBackError:(void (^)(NSError *error, NSURLResponse *response))error;

+(void)callPOSTWithParameterRegisterImage:(NSString*)url withParams:(NSMutableDictionary *)params
                      withHeaders:(NSDictionary *)keyVal
                  callBackSuccess:(void (^)(NSData *dict, NSURLResponse *response))success
                    callBackError:(void (^)(NSError *error, NSURLResponse *response))error;

+(void)callPOSTWithParameterImage1:(NSString*)url withParams:(NSMutableDictionary *)params
                         imageDict:(NSMutableDictionary *)imageDict
                          fileName:(NSString *)fileName
                       withHeaders:(NSDictionary *)keyVal
                   callBackSuccess:(void (^)(NSData *dict, NSURLResponse *response))success
                     callBackError:(void (^)(NSError *error, NSURLResponse *response))error;

+(void)callPOSTWithParameterAttachment:(NSString*)url withParams:(NSMutableDictionary *)params
                              fileName:(NSString *)fileName
                           withHeaders:(NSDictionary *)keyVal
                       callBackSuccess:(void (^)(NSData *dict, NSURLResponse *response))success
                         callBackError:(void (^)(NSError *error, NSURLResponse *response))error;

+(void)callPOSTWithParameterPhoto:(NSString*)url withParams:(NSMutableDictionary *)params
                              fileName:(NSString *)fileName
                           withHeaders:(NSDictionary *)keyVal
                       callBackSuccess:(void (^)(NSData *dict, NSURLResponse *response))success
                         callBackError:(void (^)(NSError *error, NSURLResponse *response))error;


+(void)callPOSTWithParameterImageDict:(NSString*)url withParams:(NSMutableDictionary *)params dictImages:(NSMutableDictionary *)dictImages
                          withHeaders:(NSDictionary *)keyVal
                      callBackSuccess:(void (^)(NSData *dict, NSURLResponse *response))success
                        callBackError:(void (^)(NSError *error, NSURLResponse *response))error;

+(void)callPOSTWithParameterAudioFile:(NSString*)url withParams:(NSMutableDictionary *)params
                          withHeaders:(NSDictionary *)keyVal
                      callBackSuccess:(void (^)(NSData *dict, NSURLResponse *response))success
                        callBackError:(void (^)(NSError *error, NSURLResponse *response))error;
@end
