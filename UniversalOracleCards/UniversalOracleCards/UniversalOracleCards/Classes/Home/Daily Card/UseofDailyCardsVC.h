//
//  UseofDailyCardsVC.h
//  UniversalOracleCards
//
//  Created by whiznic on 05/03/19.
//  Copyright © 2019 whiznic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <AVKit/AVKit.h>


@interface UseofDailyCardsVC : UIViewController
@property (strong, nonatomic) IBOutlet UIButton *btnCheck;
@property (strong, nonatomic) IBOutlet UIImageView *imgUncheck;
@property (strong, nonatomic) IBOutlet UIImageView *imgCheck;
@property (strong, nonatomic) IBOutlet UILabel *lblTitle;
@property (nonatomic) AVPlayer *playerViewController;
@property (nonatomic) AVPlayerLayer *player;
@property (strong, nonatomic) IBOutlet UIView *viewDisply;
@property (strong, nonatomic) NSString *Value,*btnShowPage;
@end


