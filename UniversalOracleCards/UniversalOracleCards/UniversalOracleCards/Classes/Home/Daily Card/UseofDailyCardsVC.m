//
//  UseofDailyCardsVC.m
//  UniversalOracleCards
//
//  Created by whiznic on 05/03/19.
//  Copyright © 2019 whiznic. All rights reserved.
//

#import "UseofDailyCardsVC.h"
#import "BreathingExerciseVC.h"
#import "BrethingOutVC.h"
#import <AudioToolbox/AudioServices.h>
#import "SharkfoodMuteSwitchDetector.h"
@import MediaPlayer;

@interface UseofDailyCardsVC ()
{
    NSMutableArray *value;
}
@property (nonatomic,strong) SharkfoodMuteSwitchDetector* detector;
@end

@implementation UseofDailyCardsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    /*self.detector = [SharkfoodMuteSwitchDetector shared];
    __weak UseofDailyCardsVC* sself = self;
    self.detector.silentNotify = ^(BOOL silent){
        if (silent) {
            
            NSLog(@"device is silent");
        } else {
            
            NSLog(@"device is in ringing mode");
            
        }
    };*/
    
    NSMutableAttributedString *text =
    [[NSMutableAttributedString alloc]
     initWithAttributedString: self.lblTitle.attributedText];
    
    [text addAttribute:NSForegroundColorAttributeName
                 value:[UIColor colorWithRed:173/255.0 green:173/255.0 blue:173/255. alpha:1.0]
                 range:NSMakeRange(0, 6)];
    [self.lblTitle setAttributedText: text];
    

    
    
    value = [[NSMutableArray alloc] init];
    
    [value addObject:@"1"];
    [value addObject:@"2"];
    [value addObject:@"3"];
    [value addObject:@"4"];
    [value addObject:@"5"];
    [value addObject:@"6"];
    [value addObject:@"7"];
    NSLog(@" before random :- %@", value);
    
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    NSString *stringVideoName = @"stars_animated.mp4";
    NSString *filepath = [[NSBundle mainBundle] pathForResource:stringVideoName ofType:nil];
    
    NSURL *fileURL = [NSURL fileURLWithPath:filepath];
    self.playerViewController = [AVPlayer playerWithURL:fileURL];
    self.playerViewController.actionAtItemEnd = AVPlayerActionAtItemEndNone;
    AVPlayerLayer *videoLayer = [AVPlayerLayer playerLayerWithPlayer:self.playerViewController];
    videoLayer.frame = self.view.bounds;
    videoLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
    [self.view.layer addSublayer:videoLayer];
    
    [self.playerViewController play];
    
    
    NSString *savedValue1 = [[NSUserDefaults standardUserDefaults]
                             stringForKey:@"audio"];
    if([savedValue1 isEqualToString:@"off"]) {
        
        [self.playerViewController setVolume:0.0];
    } else {
        
        [self.playerViewController setVolume:1.0];
    }
    
    
    
    
    self.viewDisply.layer.zPosition = 1;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(itemDidFinishPlaying:) name:AVPlayerItemDidPlayToEndTimeNotification object:[self.playerViewController currentItem]];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationDidBecomeActive:) name:UIApplicationDidEnterBackgroundNotification object:nil];
    
    [[UIApplication sharedApplication] beginReceivingRemoteControlEvents];
    [self becomeFirstResponder];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [self.playerViewController pause];
    [super viewWillDisappear:animated];
    [[UIApplication sharedApplication] endReceivingRemoteControlEvents];
    [self resignFirstResponder];
}


- (void)remoteControlReceivedWithEvent:(UIEvent *)event {
    switch (event.subtype) {
        case UIEventSubtypeRemoteControlTogglePlayPause:
            if([self.playerViewController rate] == 0){
                [self.playerViewController play];
            } else {
                [self.playerViewController pause];
            }
            break;
        case UIEventSubtypeRemoteControlPlay:
            [self.playerViewController play];
            break;
        case UIEventSubtypeRemoteControlPause:
            [self.playerViewController pause];
            break;
        default:
            break;
    }
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    self.detector = [SharkfoodMuteSwitchDetector shared];
    __weak UseofDailyCardsVC* sself = self;
    self.detector.silentNotify = ^(BOOL silent){
        if (silent) {
            
            NSLog(@"device is silent");
        } else {
            
            NSLog(@"device is in ringing mode");
            [self.playerViewController performSelector:@selector(play) withObject:nil afterDelay:0.01];
        }
    };
}



- (void)itemDidFinishPlaying:(NSNotification *)notification {
    AVPlayerItem *player = [notification object];
    [player seekToTime:kCMTimeZero];
}

#pragma Mark - Action Button


- (IBAction)btnBack:(id)sender {
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
        [self.navigationController popViewControllerAnimated:YES];
    } else {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (IBAction)btnCheck:(id)sender {
    if ([self.btnCheck.accessibilityHint isEqualToString:@"1"]) {
        self.btnCheck.accessibilityHint = @"0";
        self.imgUncheck.hidden = FALSE;
        self.imgCheck.hidden = TRUE;
    } else {
        self.btnCheck.accessibilityHint = @"1";
        self.imgUncheck.hidden = TRUE;
        self.imgCheck.hidden = FALSE;
        if ([self.Value isEqualToString:@"1"]) {
            NSUserDefaults *imgcheck = [NSUserDefaults standardUserDefaults];
            [imgcheck setValue:@"1" forKey:@"CheckboxDailyCard"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
        else if ([self.Value isEqualToString:@"3"]) {
            NSUserDefaults *imgcheck = [NSUserDefaults standardUserDefaults];
            [imgcheck setValue:@"1" forKey:@"CheckboxGeneral"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
        else if ([self.Value isEqualToString:@"6"]) {
            NSUserDefaults *imgcheck = [NSUserDefaults standardUserDefaults];
            [imgcheck setValue:@"1" forKey:@"CheckboxInner"];
            NSLog(@"imgcheck :- %@", imgcheck);
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
        else if ([self.Value isEqualToString:@"5C"]) {
            NSUserDefaults *imgcheck = [NSUserDefaults standardUserDefaults];
            [imgcheck setValue:@"1" forKey:@"CheckboxConnection"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
        else if ([self.Value isEqualToString:@"5L"]) {
            NSUserDefaults *imgcheck = [NSUserDefaults standardUserDefaults];
            [imgcheck setValue:@"1" forKey:@"CheckboxLife"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
    }
}

- (IBAction)btnContinue:(id)sender {
    
    /*for (int x = 0; x < [value count]; x++) {
        int randInt = (arc4random() % ([value count] - x)) + x;
        [value exchangeObjectAtIndex:x withObjectAtIndex:randInt];
        NSLog(@" after random :- %@", value);
    }*/
    
    
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
        BrethingOutVC *BreathingoutVC = [[BrethingOutVC alloc] initWithNibName:@"BrethingOutVC" bundle:nil];
        BreathingoutVC.get_Value = self.Value;
        [self.navigationController pushViewController:BreathingoutVC animated:YES];
    } else {
        BrethingOutVC *BreathingoutVC = [[BrethingOutVC alloc] initWithNibName:@"BrethingOutVC_IPad" bundle:nil];
        BreathingoutVC.get_Value = self.Value;
        [self.navigationController pushViewController:BreathingoutVC animated:YES];
    }
}

@end
