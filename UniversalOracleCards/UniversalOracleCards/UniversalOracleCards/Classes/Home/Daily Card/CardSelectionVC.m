//
//  CardSelectionVC.m
//  UniversalOracleCards
//
//  Created by whiznic on 11/03/19.
//  Copyright © 2019 whiznic. All rights reserved.
//

#import "CardSelectionVC.h"
#import "DailyCardVC.h"
#import "GeneralAdviceCardVC.h"
#import "InnerWorkCardVC.h"
#import "LifePurposeCardVC.h"
#import "ConnectionCardVC.h"
#import "GeneralAdviceCardVC.h"
#import "InnerWorkCardVC.h"
#import "LifePurposeCardVC.h"
#import "ConnectionCardVC.h"
#import <AudioToolbox/AudioServices.h>
#import "SharkfoodMuteSwitchDetector.h"
#import "CustomSpreadCardDisplayVC.h"
#import "PaymentVC.h"
#import "AppDelegate.h"
#import "CommanMethods.h"
#import "CommunicationHandler.h"
@import MediaPlayer;


@interface CardSelectionVC ()
{
    int temp, count;
}
@property (nonatomic, assign) BOOL wrap;
@property (nonatomic, strong) NSMutableArray *imgarr, *imgSelectarr,*cardID,*tepmArr,*arrShuffle;
@property (nonatomic,strong) SharkfoodMuteSwitchDetector* detector;
@end

@implementation CardSelectionVC


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    
    NSLog(@" spread_Name :- %@", self.spread_Name);
    
    
    /*self.detector = [SharkfoodMuteSwitchDetector shared];
    __weak CardSelectionVC* sself = self;
    self.detector.silentNotify = ^(BOOL silent){
        if (silent) {
            [sself.playerViewController setVolume:0.0];
            sself.btnMute.hidden = FALSE;
            sself.btnVolume.hidden = TRUE;
            NSLog(@"device is silent");
        } else {
            [sself.playerViewController setVolume:1.0];
            sself.btnMute.hidden = TRUE;
            sself.btnVolume.hidden = FALSE;
            NSLog(@"device is in ringing mode");
        }
    };*/
    
    
    if ([self.get_Selected_Value isEqualToString:@"1"]) {
        UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake(10, 40, 20, 20)];
        img.image = [UIImage imageNamed:@"card_not_selected"];
        [self.sclCardSelection addSubview:img];
    } else if ([self.get_Selected_Value isEqualToString:@"3"]) {
        for (int i=0; i<3; i++) {
            UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake((i*35)+10, 40, 20, 20)];
            img.image = [UIImage imageNamed:@"card_not_selected"];
            [self.sclCardSelection addSubview:img];
        }
    } else if ([self.get_Selected_Value isEqualToString:@"6"]) {
        for (int i=0; i<6; i++) {
            UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake((i*35)+10, 40, 20, 20)];
            img.image = [UIImage imageNamed:@"card_not_selected"];
            [self.sclCardSelection addSubview:img];
        }
    } else if ([self.get_Selected_Value isEqualToString:@"5L"]) {
        for (int i=0; i<5; i++) {
            UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake((i*35)+10, 40, 20, 20)];
            img.image = [UIImage imageNamed:@"card_not_selected"];
            [self.sclCardSelection addSubview:img];
        }
    } else if ([self.get_Selected_Value isEqualToString:@"5C"]) {
        for (int i=0; i<5; i++) {
            UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake((i*35)+10, 40, 20, 20)];
            img.image = [UIImage imageNamed:@"card_not_selected"];
            [self.sclCardSelection addSubview:img];
        }
    } else if ([self.get_Selected_Value isEqualToString:@"2x2"]) {
        for (int i=0; i<self.getToatalCards.count; i++) {
            UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake((i*35)+10, 40, 20, 20)];
            img.image = [UIImage imageNamed:@"card_not_selected"];
            [self.sclCardSelection addSubview:img];
        }
    } else if ([self.get_Selected_Value isEqualToString:@"3x3"]) {
        for (int i=0; i<self.getToatalCards.count; i++) {
            UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake((i*35)+10, 40, 20, 20)];
            img.image = [UIImage imageNamed:@"card_not_selected"];
            [self.sclCardSelection addSubview:img];
        }
    }  else if ([self.get_Selected_Value isEqualToString:@"4x4"]) {
        for (int i=0; i<self.getToatalCards.count; i++) {
            UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake((i*35)+10, 40, 20, 20)];
            img.image = [UIImage imageNamed:@"card_not_selected"];
            [self.sclCardSelection addSubview:img];
        }
    } else if ([self.get_Selected_Value isEqualToString:@"5x5"]) {
        for (int i=0; i<self.getToatalCards.count; i++) {
            UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake((i*35)+10, 40, 20, 20)];
            img.image = [UIImage imageNamed:@"card_not_selected"];
            [self.sclCardSelection addSubview:img];
        }
    }  else if ([self.get_Selected_Value isEqualToString:@"6x6"]) {
        for (int i=0; i<self.getToatalCards.count; i++) {
            UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake((i*35)+10, 40, 20, 20)];
            img.image = [UIImage imageNamed:@"card_not_selected"];
            [self.sclCardSelection addSubview:img];
        }
    }
    
    CGRect screenSizeRect = [[UIScreen mainScreen] bounds];
    CGFloat screenHeight = screenSizeRect.size.height;
    
    self.imgSelectarr = [[NSMutableArray alloc]init];
    self.cardID = [[NSMutableArray alloc]init];
    self.tepmArr = [[NSMutableArray alloc]init];
    for (int i = 0; i < 51; i++) {
        [self.imgSelectarr addObject:@"0"];
    }
    
    self.arrShuffle = [[NSMutableArray alloc] init];
    
    for (int i = 0; i<15; i++) {
        [self.arrShuffle addObject:[NSDecimalNumber numberWithInt:i]];
        [self.tepmArr addObject:[NSDecimalNumber numberWithInt:i]];
    }
    
    for (int x = 0; x< [self.arrShuffle count] ; x++) {
        int randInt = (arc4random() % ([_arrShuffle count] - x)) + x;
        [_arrShuffle exchangeObjectAtIndex:x withObjectAtIndex:randInt];
        [self.tepmArr exchangeObjectAtIndex:x withObjectAtIndex:randInt];
        NSLog(@" after random :- %@", self.tepmArr);
    }
    
    for (int i = 15; i<51; i++) {
        [self.tepmArr addObject:[NSDecimalNumber numberWithInt:i]];
    }
    
    
    
    
    for (int i=0; i<51; i++) {
        if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
            if ([self.get_Selected_Value isEqualToString:@"1"] || [self.get_Selected_Value isEqualToString:@"3"] || [self.get_Selected_Value isEqualToString:@"6"]) {
                if (i == 0) {
                    UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake(0, (screenHeight-290)/2, 168, 338)];
                    img.image = [UIImage imageNamed:@"third"];
                    [self.srlv addSubview:img];
                    
                    UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(0, (screenHeight-290)/2, 168, 338)];
                    btn.tag = i;
                    temp = 1;
                    [btn addTarget:self action:@selector(btnCardClicked:) forControlEvents:UIControlEventTouchUpInside];
                    [self.srlv addSubview:btn];
                    
                } else if (i < 14) {
                    UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake((i*66)+102, (screenHeight-290)/2, 66, 338)];
                    img.image = [UIImage imageNamed:@"second"];
                    [self.srlv addSubview:img];
                    
                    UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake((i*66)+102, (screenHeight-290)/2, 66, 338)];
                    btn.tag = i;
                    
                    [btn addTarget:self action:@selector(btnCardClicked:) forControlEvents:UIControlEventTouchUpInside];
                    [self.srlv addSubview:btn];
                } else {
                    UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake((i*66)+102, (screenHeight-290)/2, 66, 338)];
                    img.image = [UIImage imageNamed:@"second_lock"];
                    [self.srlv addSubview:img];
                    
                    UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake((i*66)+102, (screenHeight-290)/2, 66, 338)];
                    btn.tag = i;
                    
                    [btn addTarget:self action:@selector(btnCardClicked:) forControlEvents:UIControlEventTouchUpInside];
                    [self.srlv addSubview:btn];
                }
                [self.srlv setContentSize:CGSizeMake((66*49)+168, screenHeight)];
            } else {
                if (i == 0) {
                    UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake(0, (screenHeight-290)/2, 168, 338)];
                    img.image = [UIImage imageNamed:@"third"];
                    [self.srlv addSubview:img];
                    
                    UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(0, (screenHeight-290)/2, 168, 338)];
                    btn.tag = i;
                    temp = 1;
                    [btn addTarget:self action:@selector(btnCardClicked:) forControlEvents:UIControlEventTouchUpInside];
                    [self.srlv addSubview:btn];
                    
                } else {
                    UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake((i*66)+102, (screenHeight-290)/2, 66, 338)];
                    img.image = [UIImage imageNamed:@"second"];
                    [self.srlv addSubview:img];
                    
                    UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake((i*66)+102, (screenHeight-290)/2, 66, 338)];
                    btn.tag = i;
                    
                    [btn addTarget:self action:@selector(btnCardClicked:) forControlEvents:UIControlEventTouchUpInside];
                    [self.srlv addSubview:btn];
                    [self.srlv setContentSize:CGSizeMake((66*49)+168, screenHeight)];
                }
            }
        } else {
            if ([self.get_Selected_Value isEqualToString:@"1"] || [self.get_Selected_Value isEqualToString:@"3"] || [self.get_Selected_Value isEqualToString:@"6"]) {
                if (i == 0) {
                    UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake(0, (screenHeight-900)/2, 168, 400)];
                    img.image = [UIImage imageNamed:@"third"];
                    [self.srlv addSubview:img];
                    
                    UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(0, (screenHeight-900)/2, 168, 400)];
                    btn.tag = i;
                    temp = 1;
                    [btn addTarget:self action:@selector(btnCardClicked:) forControlEvents:UIControlEventTouchUpInside];
                    [self.srlv addSubview:btn];
                    
                } else if (i < 14) {
                    UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake((i*66)+102, (screenHeight-900)/2, 66, 400)];
                    img.image = [UIImage imageNamed:@"second"];
                    [self.srlv addSubview:img];
                    
                    UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake((i*66)+102, (screenHeight-900)/2, 66, 400)];
                    btn.tag = i;
                    
                    [btn addTarget:self action:@selector(btnCardClicked:) forControlEvents:UIControlEventTouchUpInside];
                    [self.srlv addSubview:btn];
                } else {
                    UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake((i*66)+102, (screenHeight-900)/2, 66, 400)];
                    img.image = [UIImage imageNamed:@"second_lock"];
                    [self.srlv addSubview:img];
                    
                    UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake((i*66)+102, (screenHeight-900)/2, 66, 400)];
                    btn.tag = i;
                    
                    [btn addTarget:self action:@selector(btnCardClicked:) forControlEvents:UIControlEventTouchUpInside];
                    [self.srlv addSubview:btn];
                }
                [self.srlv setContentSize:CGSizeMake((66*49)+168, self.srlv.frame.size.height)];
            } else {
                if (i == 0) {
                    UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake(0, (screenHeight-900)/2, 168, 500)];
                    img.image = [UIImage imageNamed:@"third"];
                    [self.srlv addSubview:img];
                    
                    UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(0, (screenHeight-900)/2, 168, 500)];
                    btn.tag = i;
                    temp = 1;
                    [btn addTarget:self action:@selector(btnCardClicked:) forControlEvents:UIControlEventTouchUpInside];
                    [self.srlv addSubview:btn];
                    
                } else {
                    UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake((i*66)+102, (screenHeight-900)/2, 66, 400)];
                    img.image = [UIImage imageNamed:@"second"];
                    [self.srlv addSubview:img];
                    img.backgroundColor = UIColor.orangeColor;
                    
                    UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake((i*66)+102, (screenHeight-900)/2, 66, 400)];
                    btn.tag = i;
                    
                    [btn addTarget:self action:@selector(btnCardClicked:) forControlEvents:UIControlEventTouchUpInside];
                    [self.srlv addSubview:btn];
                    [self.srlv setContentSize:CGSizeMake((66*49)+168, self.srlv.frame.size.height)];
                }
            }
        }
   
    }
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationDidBecomeActive:) name:UIApplicationDidEnterBackgroundNotification object:nil];

}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    NSString *stringVideoName = @"stars_animated.mp4";
    NSString *filepath = [[NSBundle mainBundle] pathForResource:stringVideoName ofType:nil];
    NSLog(@" filepath :- %@", filepath);
    NSURL *fileURL = [NSURL fileURLWithPath:filepath];
    self.playerViewController = [AVPlayer playerWithURL:fileURL];
    self.playerViewController.actionAtItemEnd = AVPlayerActionAtItemEndNone;
    AVPlayerLayer *videoLayer = [AVPlayerLayer playerLayerWithPlayer:self.playerViewController];
    videoLayer.frame = self.view.bounds;
    videoLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
    [self.view.layer addSublayer:videoLayer];
    
    [self.playerViewController play];
    
    
    
    NSString *savedValue = [[NSUserDefaults standardUserDefaults]
                            stringForKey:@"audio"];
    if([savedValue isEqualToString:@"off"]) {
        self.btnMute.hidden = FALSE;
        self.btnVolume.hidden = TRUE;
        [self.playerViewController setVolume:0.0];
    } else {
        self.btnMute.hidden = TRUE;
        self.btnVolume.hidden = FALSE;
        [self.playerViewController setVolume:1.0];
    }
    
    NSLog(@" savedValue :- %@", savedValue);
    
    self.viewDisply.layer.zPosition = 1;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(itemDidFinishPlaying:) name:AVPlayerItemDidPlayToEndTimeNotification object:[self.playerViewController currentItem]];

    
    [[UIApplication sharedApplication] beginReceivingRemoteControlEvents];
    [self becomeFirstResponder];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [self.playerViewController pause];
    [super viewWillDisappear:animated];
    [[UIApplication sharedApplication] endReceivingRemoteControlEvents];
    [self resignFirstResponder];
}


- (void)remoteControlReceivedWithEvent:(UIEvent *)event {
    switch (event.subtype) {
        case UIEventSubtypeRemoteControlTogglePlayPause:
            if([self.playerViewController rate] == 0){
                [self.playerViewController play];
            } else {
                [self.playerViewController pause];
            }
            break;
        case UIEventSubtypeRemoteControlPlay:
            [self.playerViewController play];
            break;
        case UIEventSubtypeRemoteControlPause:
            [self.playerViewController pause];
            break;
        default:
            break;
    }
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    /*self.detector = [SharkfoodMuteSwitchDetector shared];
    __weak CardSelectionVC* sself = self;
    self.detector.silentNotify = ^(BOOL silent){
        if (silent) {
            
            NSLog(@"device is silent");
        } else {
            
            NSLog(@"device is in ringing mode");
            [self.playerViewController performSelector:@selector(play) withObject:nil afterDelay:0.01];
        }
    };*/
    
    NSString *savedValue = [[NSUserDefaults standardUserDefaults]
                            stringForKey:@"audio"];
    if([savedValue isEqualToString:@"off"]) {
        self.btnMute.hidden = FALSE;
        self.btnVolume.hidden = TRUE;
        [self.playerViewController setVolume:0.0];
    } else {
        self.btnMute.hidden = TRUE;
        self.btnVolume.hidden = FALSE;
        [self.playerViewController setVolume:1.0];
    }
}




-(IBAction)btnCardClicked:(id)sender {
    
   
    
    if ([self.get_Selected_Value isEqualToString:@"1"]) {
        
        if ([sender tag] < 14) {
            self.lblQuestion.text = @"What is today's energy?";
            UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake(10, 40, 20, 20)];
            img.image = [UIImage imageNamed:@"card_selected"];
            [self.sclCardSelection addSubview:img];
            [self.cardID addObject:[_tepmArr objectAtIndex:[sender tag]]];
            [_tepmArr removeObjectAtIndex:[sender tag]];
            
            if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
                DailyCardVC *dailycardVC = [[DailyCardVC alloc] initWithNibName:@"DailyCardVC" bundle:nil];
                dailycardVC.get_cardID = self.cardID;
                [self.navigationController pushViewController:dailycardVC animated:YES];
            } else {
                DailyCardVC *dailycardVC = [[DailyCardVC alloc] initWithNibName:@"DailyCardVC_IPad" bundle:nil];
                dailycardVC.get_cardID = self.cardID;
                [self.navigationController pushViewController:dailycardVC animated:YES];
            }
        }
        else {
            if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
                PaymentVC *vc = [[PaymentVC alloc] initWithNibName:@"PaymentVC" bundle:nil];
                [self addChildViewController:vc];
                self.viewDisply.layer.zPosition = 0;
                vc.view.frame = self.view.frame;
                [self.view addSubview:vc.view];
                [vc didMoveToParentViewController:self];
            }
        }
    } else if ([self.get_Selected_Value isEqualToString:@"3"]) {
         if ([sender tag] < 14) {
            [self.imgSelectarr replaceObjectAtIndex:([sender tag]) withObject:@"1"];
            [self.cardID addObject:[_tepmArr objectAtIndex:[sender tag]]];
            [_tepmArr removeObjectAtIndex:[sender tag]];
            int count = 0;
            for (int i=0; i<self.imgSelectarr.count; i++) {
                if ([[self.imgSelectarr objectAtIndex:i] isEqualToString:@"1"]) {
                    
                    if (count == 0) {
                        self.lblQuestion.text = @"How can i make the last of the situation?";
                        UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake((count*35)+10, 40, 20, 20)];
                        img.image = [UIImage imageNamed:@"card_selected"];
                        [self.sclCardSelection addSubview:img];
                        
                        count = count + 1;
                    } else if (count == 1) {
                        self.lblQuestion.text = @"Positive Energy of the situation?";
                        UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake((count*35)+10, 40, 20, 20)];
                        img.image = [UIImage imageNamed:@"card_selected"];
                        [self.sclCardSelection addSubview:img];
                        
                        count = count + 1;
                    } else if (count == 2) {
                        self.lblQuestion.text = @"Negative Energy of the situation?";
                        UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake((count*35)+10, 40, 20, 20)];
                        img.image = [UIImage imageNamed:@"card_selected"];
                        [self.sclCardSelection addSubview:img];
                        NSLog(@"cardID :- %@", self.cardID);
                        
                        if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
                            GeneralAdviceCardVC *generaladvicecardVC = [[GeneralAdviceCardVC alloc] initWithNibName:@"GeneralAdviceCardVC" bundle:nil];
                            generaladvicecardVC.get_cardID = self.cardID;
                            [self.navigationController pushViewController:generaladvicecardVC animated:YES];
                        } else {
                            GeneralAdviceCardVC *generaladvicecardVC = [[GeneralAdviceCardVC alloc] initWithNibName:@"GeneralAdviceCardVC_IPad" bundle:nil];
                            generaladvicecardVC.get_cardID = self.cardID;
                            [self.navigationController pushViewController:generaladvicecardVC animated:YES];
                        }
                    }
                }
            }
        }
        else {
            if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
                PaymentVC *vc = [[PaymentVC alloc] initWithNibName:@"PaymentVC" bundle:nil];
                [self addChildViewController:vc];
                self.viewDisply.layer.zPosition = 0;
                vc.view.frame = self.view.frame;
                [self.view addSubview:vc.view];
                [vc didMoveToParentViewController:self];
            }
        }
        
    } else if ([self.get_Selected_Value isEqualToString:@"6"]) {
        if ([sender tag] < 14) {
            [self.imgSelectarr replaceObjectAtIndex:([sender tag]) withObject:@"1"];
            [self.cardID addObject:[_tepmArr objectAtIndex:[sender tag]]];
            [_tepmArr removeObjectAtIndex:[sender tag]];
            int count = 0;
            for (int i=0; i<self.imgSelectarr.count; i++) {
                if ([[self.imgSelectarr objectAtIndex:i] isEqualToString:@"1"]) {
                    if (count == 0) {
                        self.lblQuestion.text = @"What do I need to heal?";
                        UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake((count*35)+10, 40, 20, 20)];
                        img.image = [UIImage imageNamed:@"card_selected"];
                         [self.sclCardSelection addSubview:img];
                        count = count + 1;
                    } else if (count == 1) {
                        UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake((count*35)+10, 40, 20, 20)];
                        img.image = [UIImage imageNamed:@"card_selected"];
                        self.lblQuestion.text = @"What do I need to relase?";
                        [self.sclCardSelection addSubview:img];
                        count = count + 1;
                    } else if (count == 2) {
                        self.lblQuestion.text = @"What do I need to accept?";
                        UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake((count*35)+10, 40, 20, 20)];
                        img.image = [UIImage imageNamed:@"card_selected"];
                        [self.sclCardSelection addSubview:img];
                        count = count + 1;
                    } else if (count == 3) {
                        self.lblQuestion.text = @"What do I need to express?";
                        UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake((count*35)+10, 40, 20, 20)];
                        img.image = [UIImage imageNamed:@"card_selected"];
                        [self.sclCardSelection addSubview:img];
                        count = count + 1;
                    } else if (count == 4) {
                        self.lblQuestion.text = @"Where/How can i get help and support?";
                        UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake((count*35)+10, 40, 20, 20)];
                        img.image = [UIImage imageNamed:@"card_selected"];
                        [self.sclCardSelection addSubview:img];
                        count = count + 1;
                    } else if (count == 5) {
                        self.lblQuestion.text = @"How can I help myself?";
                        UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake((count*35)+10, 40, 20, 20)];
                        img.image = [UIImage imageNamed:@"card_selected"];
                        [self.sclCardSelection addSubview:img];
                        if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
                            InnerWorkCardVC *innerworkcardVC = [[InnerWorkCardVC alloc] initWithNibName:@"InnerWorkCardVC" bundle:nil];
                            innerworkcardVC.get_cardID = self.cardID;
                            [self.navigationController pushViewController:innerworkcardVC animated:YES];
                        } else {
                            InnerWorkCardVC *innerworkcardVC = [[InnerWorkCardVC alloc] initWithNibName:@"InnerWorkCardVC_IPad" bundle:nil];
                            innerworkcardVC.get_cardID = self.cardID;
                            [self.navigationController pushViewController:innerworkcardVC animated:YES];
                        }
                    }
                }
            }
         }
        else {
            if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
                PaymentVC *vc = [[PaymentVC alloc] initWithNibName:@"PaymentVC" bundle:nil];
                [self addChildViewController:vc];
                self.viewDisply.layer.zPosition = 0;
                vc.view.frame = self.view.frame;
                [self.view addSubview:vc.view];
                [vc didMoveToParentViewController:self];
            }
        }
    }
    
    else if ([self.get_Selected_Value isEqualToString:@"5C"]) {
        if ([sender tag] < 51) {
            [self.imgSelectarr replaceObjectAtIndex:([sender tag]) withObject:@"1"];
            [self.cardID addObject:[_tepmArr objectAtIndex:[sender tag]]];
            [_tepmArr removeObjectAtIndex:[sender tag]];
            int count = 0;
            for (int i=0; i<self.imgSelectarr.count; i++) {
                if ([[self.imgSelectarr objectAtIndex:i] isEqualToString:@"1"]) {
                    if (count == 0) {
                        self.lblQuestion.text = @"How can I contribute to the world currently?";
                        UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake((count*35)+10, 40, 20, 20)];
                        img.image = [UIImage imageNamed:@"card_selected"];
                        [self.sclCardSelection addSubview:img];
                        count = count + 1;
                    } else if (count == 1) {
                        self.lblQuestion.text = @"What should I focus on in my journey righty now?";
                        UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake((count*35)+10, 40, 20, 20)];
                        img.image = [UIImage imageNamed:@"card_selected"];
                        [self.sclCardSelection addSubview:img];
                        count = count + 1;
                    } else if (count == 2) {
                        self.lblQuestion.text = @"Where can I expand my skillset?";
                        UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake((count*35)+10, 40, 20, 20)];
                        img.image = [UIImage imageNamed:@"card_selected"];
                        [self.sclCardSelection addSubview:img];
                        count = count + 1;
                    } else if (count == 3) {
                        self.lblQuestion.text = @"Where should I ask  for help advice?";
                        UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake((count*35)+10, 40, 20, 20)];
                        img.image = [UIImage imageNamed:@"card_selected"];
                        [self.sclCardSelection addSubview:img];
                        count = count + 1;
                    } else if (count == 4) {
                        self.lblQuestion.text = @"How can I express my talents?";
                        UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake((count*35)+10, 40, 20, 20)];
                        img.image = [UIImage imageNamed:@"card_selected"];
                        [self.sclCardSelection addSubview:img];
                        if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
                            ConnectionCardVC *connectionVC = [[ConnectionCardVC alloc] initWithNibName:@"ConnectionCardVC" bundle:nil];
                            connectionVC.get_cardID = self.cardID;
                            [self.navigationController pushViewController:connectionVC animated:YES];
                        } else {
                            ConnectionCardVC *connectionVC = [[ConnectionCardVC alloc] initWithNibName:@"ConnectionCardVC_IPad" bundle:nil];
                            connectionVC.get_cardID = self.cardID;
                            [self.navigationController pushViewController:connectionVC animated:YES];
                        }
                    }
                }
            }
        }
        
    }
    
    else if ([self.get_Selected_Value isEqualToString:@"5L"]) {
         if ([sender tag] < 60) {
             [self.imgSelectarr replaceObjectAtIndex:([sender tag]) withObject:@"1"];
             [self.cardID addObject:[_tepmArr objectAtIndex:[sender tag]]];
             [_tepmArr removeObjectAtIndex:[sender tag]];
             int count = 0;
             for (int i=0; i<self.imgSelectarr.count; i++) {
                 if ([[self.imgSelectarr objectAtIndex:i] isEqualToString:@"1"]) {
                     if (count == 0) {
                         self.lblQuestion.text = @"General energy of the connection?";
                         UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake((count*35)+10, 40, 20, 20)];
                         img.image = [UIImage imageNamed:@"card_selected"];
                         [self.sclCardSelection addSubview:img];
                         count = count + 1;
                     } else if (count == 1) {
                         self.lblQuestion.text = @"What is this person teaching me?";
                         UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake((count*35)+10, 40, 20, 20)];
                         img.image = [UIImage imageNamed:@"card_selected"];
                         [self.sclCardSelection addSubview:img];
                         count = count + 1;
                     } else if (count == 2) {
                         self.lblQuestion.text = @"What am i teaching this person?";
                         UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake((count*35)+10, 40, 20, 20)];
                         img.image = [UIImage imageNamed:@"card_selected"];
                         [self.sclCardSelection addSubview:img];
                         count = count + 1;
                     } else if (count == 3) {
                         self.lblQuestion.text = @"What is the purpose of this connection?";
                         UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake((count*35)+10, 40, 20, 20)];
                         img.image = [UIImage imageNamed:@"card_selected"];
                         [self.sclCardSelection addSubview:img];
                         count = count + 1;
                     } else if (count == 4) {
                         self.lblQuestion.text = @"How can we connect more deeply?";
                         UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake((count*35)+10, 40, 20, 20)];
                         img.image = [UIImage imageNamed:@"card_selected"];
                         [self.sclCardSelection addSubview:img];
                         if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
                             LifePurposeCardVC *lifepurposeVC = [[LifePurposeCardVC alloc] initWithNibName:@"LifePurposeCardVC" bundle:nil];
                             lifepurposeVC.get_cardID = self.cardID;
                             [self.navigationController pushViewController:lifepurposeVC animated:YES];
                         } else {
                             LifePurposeCardVC *lifepurposeVC = [[LifePurposeCardVC alloc] initWithNibName:@"LifePurposeCardVC_IPad" bundle:nil];
                             lifepurposeVC.get_cardID = self.cardID;
                             [self.navigationController pushViewController:lifepurposeVC animated:YES];
                         }
                     }
                 }
             }
         }
      
    }
    else if ([self.get_Selected_Value isEqualToString:@"2x2"]){
        if ([sender tag] < 51) {
            [self.imgSelectarr replaceObjectAtIndex:([sender tag]) withObject:@"1"];
            [self.cardID addObject:[_tepmArr objectAtIndex:[sender tag]]];
            [_tepmArr removeObjectAtIndex:[sender tag]];
            int count = 0;
            for (int i=0; i<self.imgSelectarr.count; i++) {
                if ([[self.imgSelectarr objectAtIndex:i] isEqualToString:@"1"]) {
                    if (self.getToatalCards.count == count+1) {
                        if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
                            CustomSpreadCardDisplayVC *customspreadVC = [[CustomSpreadCardDisplayVC alloc] initWithNibName:@"CustomSpreadCardDisplayVC" bundle:nil];
                            customspreadVC.get_cardID = self.cardID;
                            customspreadVC.Title = self.spread_Name;
                            customspreadVC.grid = 2.0;
                            customspreadVC.get_CardPosition = self.getToatalCards;
                            customspreadVC.get_spread_id= self.spread_id;
                            [self.navigationController pushViewController:customspreadVC animated:YES];
                        } else {
                            CustomSpreadCardDisplayVC *customspreadVC = [[CustomSpreadCardDisplayVC alloc] initWithNibName:@"CustomSpreadCardDisplayVC_IPad" bundle:nil];
                            customspreadVC.get_cardID = self.cardID;
                            customspreadVC.Title = self.spread_Name;
                            customspreadVC.grid = 2.0;
                            customspreadVC.get_CardPosition = self.getToatalCards;
                            customspreadVC.get_spread_id= self.spread_id;
                            [self.navigationController pushViewController:customspreadVC animated:YES];
                        }
                    } else
                    {
                        if (count == 0) {
                            UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake((count*35)+10, 40, 20, 20)];
                            img.image = [UIImage imageNamed:@"card_selected"];
                            [self.sclCardSelection addSubview:img];
                            count = count + 1;
                        } else if (count == 1) {
                            UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake((count*35)+10, 40, 20, 20)];
                            img.image = [UIImage imageNamed:@"card_selected"];
                            [self.sclCardSelection addSubview:img];
                            count = count + 1;
                        } else if (count == 2) {
                            UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake((count*35)+10, 40, 20, 20)];
                            img.image = [UIImage imageNamed:@"card_selected"];
                            [self.sclCardSelection addSubview:img];
                            count = count + 1;
                        } else if (count == 3) {
                            UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake((count*35)+10, 40, 20, 20)];
                            img.image = [UIImage imageNamed:@"card_selected"];
                            [self.sclCardSelection addSubview:img];
                            if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
                                CustomSpreadCardDisplayVC *customspreadVC = [[CustomSpreadCardDisplayVC alloc] initWithNibName:@"CustomSpreadCardDisplayVC" bundle:nil];
                                customspreadVC.get_cardID = self.cardID;
                                customspreadVC.Title = self.spread_Name;
                                customspreadVC.grid = 2.0;
                                customspreadVC.get_CardPosition = self.getToatalCards;
                                customspreadVC.get_spread_id= self.spread_id;
                                [self.navigationController pushViewController:customspreadVC animated:YES];
                            } else {
                                CustomSpreadCardDisplayVC *customspreadVC = [[CustomSpreadCardDisplayVC alloc] initWithNibName:@"CustomSpreadCardDisplayVC_IPad" bundle:nil];
                                customspreadVC.get_cardID = self.cardID;
                                customspreadVC.Title = self.spread_Name;
                                customspreadVC.grid = 2.0;
                                customspreadVC.get_CardPosition = self.getToatalCards;
                                customspreadVC.get_spread_id= self.spread_id;
                                [self.navigationController pushViewController:customspreadVC animated:YES];
                            }
                        }
                    }
                    
                }
            }
        }
        
    }
    
    else if ([self.get_Selected_Value isEqualToString:@"3x3"]){
        if ([sender tag] < 51) {
            [self.imgSelectarr replaceObjectAtIndex:([sender tag]) withObject:@"1"];
            [self.cardID addObject:[_tepmArr objectAtIndex:[sender tag]]];
            [_tepmArr removeObjectAtIndex:[sender tag]];
            int count = 0;
            for (int i=0; i<self.imgSelectarr.count; i++) {
                if ([[self.imgSelectarr objectAtIndex:i] isEqualToString:@"1"]) {
                    if (self.getToatalCards.count == count+1) {
                        if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
                            CustomSpreadCardDisplayVC *customspreadVC = [[CustomSpreadCardDisplayVC alloc] initWithNibName:@"CustomSpreadCardDisplayVC" bundle:nil];
                            customspreadVC.get_cardID = self.cardID;
                            customspreadVC.Title = self.spread_Name;
                            customspreadVC.grid = 3.0;
                            customspreadVC.get_CardPosition = self.getToatalCards;
                            customspreadVC.get_spread_id= self.spread_id;
                            [self.navigationController pushViewController:customspreadVC animated:YES];
                        } else {
                            CustomSpreadCardDisplayVC *customspreadVC = [[CustomSpreadCardDisplayVC alloc] initWithNibName:@"CustomSpreadCardDisplayVC_IPad" bundle:nil];
                            customspreadVC.get_cardID = self.cardID;
                            customspreadVC.Title = self.spread_Name;
                            customspreadVC.grid = 3.0;
                            customspreadVC.get_CardPosition = self.getToatalCards;
                            customspreadVC.get_spread_id= self.spread_id;
                            [self.navigationController pushViewController:customspreadVC animated:YES];
                        }
                    } else {
                        if (count == 0) {
                            UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake((count*35)+10, 40, 20, 20)];
                            img.image = [UIImage imageNamed:@"card_selected"];
                            [self.sclCardSelection addSubview:img];
                            count = count + 1;
                        } else if (count == 1) {
                            UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake((count*35)+10, 40, 20, 20)];
                            img.image = [UIImage imageNamed:@"card_selected"];
                            [self.sclCardSelection addSubview:img];
                            count = count + 1;
                        } else if (count == 2) {
                            UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake((count*35)+10, 40, 20, 20)];
                            img.image = [UIImage imageNamed:@"card_selected"];
                            [self.sclCardSelection addSubview:img];
                            count = count + 1;
                        } else if (count == 3) {
                            UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake((count*35)+10, 40, 20, 20)];
                            img.image = [UIImage imageNamed:@"card_selected"];
                            [self.sclCardSelection addSubview:img];
                            count = count + 1;
                        } else if (count == 4) {
                            UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake((count*35)+10, 40, 20, 20)];
                            img.image = [UIImage imageNamed:@"card_selected"];
                            [self.sclCardSelection addSubview:img];
                            count = count + 1;
                        } else if (count == 5) {
                            UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake((count*35)+10, 40, 20, 20)];
                            img.image = [UIImage imageNamed:@"card_selected"];
                            [self.sclCardSelection addSubview:img];
                            count = count + 1;
                        } else if (count == 6) {
                            UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake((count*35)+10, 40, 20, 20)];
                            img.image = [UIImage imageNamed:@"card_selected"];
                            [self.sclCardSelection addSubview:img];
                            count = count + 1;
                        } else if (count == 7) {
                            UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake((count*35)+10, 40, 20, 20)];
                            img.image = [UIImage imageNamed:@"card_selected"];
                            [self.sclCardSelection addSubview:img];
                            count = count + 1;
                        } else if (count == 8) {
                            UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake((count*35)+10, 40, 20, 20)];
                            img.image = [UIImage imageNamed:@"card_selected"];
                            [self.sclCardSelection addSubview:img];
                            if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
                                CustomSpreadCardDisplayVC *customspreadVC = [[CustomSpreadCardDisplayVC alloc] initWithNibName:@"CustomSpreadCardDisplayVC" bundle:nil];
                                customspreadVC.get_cardID = self.cardID;
                                customspreadVC.Title = self.spread_Name;
                                customspreadVC.grid = 3.0;
                                customspreadVC.get_CardPosition = self.getToatalCards;
                                customspreadVC.get_spread_id= self.spread_id;
                                [self.navigationController pushViewController:customspreadVC animated:YES];
                            } else {
                                CustomSpreadCardDisplayVC *customspreadVC = [[CustomSpreadCardDisplayVC alloc] initWithNibName:@"CustomSpreadCardDisplayVC_IPad" bundle:nil];
                                customspreadVC.get_cardID = self.cardID;
                                customspreadVC.Title = self.spread_Name;
                                customspreadVC.grid = 3.0;
                                customspreadVC.get_CardPosition = self.getToatalCards;
                                customspreadVC.get_spread_id= self.spread_id;
                                [self.navigationController pushViewController:customspreadVC animated:YES];
                            }
                        }
                        
                    }
                }
            }
        }
        
}
    
    
    else if ([self.get_Selected_Value isEqualToString:@"4x4"]){
        [self.imgSelectarr replaceObjectAtIndex:([sender tag]) withObject:@"1"];
        [self.cardID addObject:[_tepmArr objectAtIndex:[sender tag]]];
        [_tepmArr removeObjectAtIndex:[sender tag]];
        int count = 0;
        for (int i=0; i<self.imgSelectarr.count; i++) {
            if ([[self.imgSelectarr objectAtIndex:i] isEqualToString:@"1"]) {
                if (self.getToatalCards.count == count+1) {
                    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
                        CustomSpreadCardDisplayVC *customspreadVC = [[CustomSpreadCardDisplayVC alloc] initWithNibName:@"CustomSpreadCardDisplayVC" bundle:nil];
                        customspreadVC.get_cardID = self.cardID;
                        customspreadVC.Title = self.spread_Name;
                        customspreadVC.grid = 4.0;
                        customspreadVC.get_CardPosition = self.getToatalCards;
                        customspreadVC.get_spread_id= self.spread_id;
                        [self.navigationController pushViewController:customspreadVC animated:YES];
                    } else {
                        CustomSpreadCardDisplayVC *customspreadVC = [[CustomSpreadCardDisplayVC alloc] initWithNibName:@"CustomSpreadCardDisplayVC_IPad" bundle:nil];
                        customspreadVC.get_cardID = self.cardID;
                        customspreadVC.Title = self.spread_Name;
                        customspreadVC.grid = 4.0;
                        customspreadVC.get_CardPosition = self.getToatalCards;
                        customspreadVC.get_spread_id= self.spread_id;
                        [self.navigationController pushViewController:customspreadVC animated:YES];
                    }
                } else {
                    if ([[self.imgSelectarr objectAtIndex:i] isEqualToString:@"1"]) {
                        if (count == 0) {
                            UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake((count*35)+10, 40, 20, 20)];
                            img.image = [UIImage imageNamed:@"card_selected"];
                            [self.sclCardSelection addSubview:img];
                            count = count + 1;
                        } else if (count == 1) {
                            UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake((count*35)+10, 40, 20, 20)];
                            img.image = [UIImage imageNamed:@"card_selected"];
                            [self.sclCardSelection addSubview:img];
                            count = count + 1;
                        } else if (count == 2) {
                            UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake((count*35)+10, 40, 20, 20)];
                            img.image = [UIImage imageNamed:@"card_selected"];
                            [self.sclCardSelection addSubview:img];
                            count = count + 1;
                        } else if (count == 3) {
                            UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake((count*35)+10, 40, 20, 20)];
                            img.image = [UIImage imageNamed:@"card_selected"];
                            [self.sclCardSelection addSubview:img];
                            count = count + 1;
                        } else if (count == 4) {
                            UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake((count*35)+10, 40, 20, 20)];
                            img.image = [UIImage imageNamed:@"card_selected"];
                            [self.sclCardSelection addSubview:img];
                            count = count + 1;
                        } else if (count == 5) {
                            UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake((count*35)+10, 40, 20, 20)];
                            img.image = [UIImage imageNamed:@"card_selected"];
                            [self.sclCardSelection addSubview:img];
                            count = count + 1;
                        } else if (count == 6) {
                            UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake((count*35)+10, 40, 20, 20)];
                            img.image = [UIImage imageNamed:@"card_selected"];
                            [self.sclCardSelection addSubview:img];
                            count = count + 1;
                        } else if (count == 7) {
                            UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake((count*35)+10, 40, 20, 20)];
                            img.image = [UIImage imageNamed:@"card_selected"];
                            [self.sclCardSelection addSubview:img];
                            count = count + 1;
                        } else if (count == 8) {
                            UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake((count*35)+10, 40, 20, 20)];
                            img.image = [UIImage imageNamed:@"card_selected"];
                            [self.sclCardSelection addSubview:img];
                            count = count + 1;
                        } else if (count == 9) {
                            UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake((count*35)+10, 40, 20, 20)];
                            img.image = [UIImage imageNamed:@"card_selected"];
                            [self.sclCardSelection addSubview:img];
                            count = count + 1;
                        } else if (count == 10) {
                            UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake((count*35)+10, 40, 20, 20)];
                            img.image = [UIImage imageNamed:@"card_selected"];
                            [self.sclCardSelection addSubview:img];
                            count = count + 1;
                        } else if (count == 11) {
                            UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake((count*35)+10, 40, 20, 20)];
                            img.image = [UIImage imageNamed:@"card_selected"];
                            [self.sclCardSelection addSubview:img];
                            count = count + 1;
                        } else if (count == 12) {
                            UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake((count*35)+10, 40, 20, 20)];
                            img.image = [UIImage imageNamed:@"card_selected"];
                            [self.sclCardSelection addSubview:img];
                            count = count + 1;
                        } else if (count == 13) {
                            UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake((count*35)+10, 40, 20, 20)];
                            img.image = [UIImage imageNamed:@"card_selected"];
                            [self.sclCardSelection addSubview:img];
                            count = count + 1;
                        } else if (count == 14) {
                            UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake((count*35)+10, 40, 20, 20)];
                            img.image = [UIImage imageNamed:@"card_selected"];
                            [self.sclCardSelection addSubview:img];
                            if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
                                CustomSpreadCardDisplayVC *customspreadVC = [[CustomSpreadCardDisplayVC alloc] initWithNibName:@"CustomSpreadCardDisplayVC" bundle:nil];
                                customspreadVC.get_cardID = self.cardID;
                                customspreadVC.Title = self.spread_Name;
                                customspreadVC.grid = 4.0;
                                customspreadVC.get_CardPosition = self.getToatalCards;
                                customspreadVC.get_spread_id= self.spread_id;
                                [self.navigationController pushViewController:customspreadVC animated:YES];
                            } else {
                                CustomSpreadCardDisplayVC *customspreadVC = [[CustomSpreadCardDisplayVC alloc] initWithNibName:@"CustomSpreadCardDisplayVC_IPad" bundle:nil];
                                customspreadVC.get_cardID = self.cardID;
                                customspreadVC.Title = self.spread_Name;
                                customspreadVC.grid = 4.0;
                                customspreadVC.get_CardPosition = self.getToatalCards;
                                customspreadVC.get_spread_id= self.spread_id;
                                [self.navigationController pushViewController:customspreadVC animated:YES];
                            }
                        }
                }
                
                }
            }
        }
        
    }
    
    else if ([self.get_Selected_Value isEqualToString:@"5x5"]){
        if ([sender tag] < 51) {
            [self.imgSelectarr replaceObjectAtIndex:([sender tag]) withObject:@"1"];
            [self.cardID addObject:[_tepmArr objectAtIndex:[sender tag]]];
            [_tepmArr removeObjectAtIndex:[sender tag]];
            int count = 0;
            for (int i=0; i<self.imgSelectarr.count; i++) {
                if ([[self.imgSelectarr objectAtIndex:i] isEqualToString:@"1"]) {
                    if (self.getToatalCards.count == count+1) {
                        if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
                            CustomSpreadCardDisplayVC *customspreadVC = [[CustomSpreadCardDisplayVC alloc] initWithNibName:@"CustomSpreadCardDisplayVC" bundle:nil];
                            customspreadVC.get_cardID = self.cardID;
                            customspreadVC.Title = self.spread_Name;
                            customspreadVC.grid = 5.0;
                            customspreadVC.get_CardPosition = self.getToatalCards;
                            customspreadVC.get_spread_id= self.spread_id;
                            [self.navigationController pushViewController:customspreadVC animated:YES];
                        } else {
                            CustomSpreadCardDisplayVC *customspreadVC = [[CustomSpreadCardDisplayVC alloc] initWithNibName:@"CustomSpreadCardDisplayVC_IPad" bundle:nil];
                            customspreadVC.get_cardID = self.cardID;
                            customspreadVC.Title = self.spread_Name;
                            customspreadVC.grid = 5.0;
                            customspreadVC.get_CardPosition = self.getToatalCards;
                            customspreadVC.get_spread_id= self.spread_id;
                            [self.navigationController pushViewController:customspreadVC animated:YES];
                        }
                    } else {
                        if (count == 0) {
                            UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake((count*35)+10, 40, 20, 20)];
                            img.image = [UIImage imageNamed:@"card_selected"];
                            [self.sclCardSelection addSubview:img];
                            count = count + 1;
                        } else if (count == 1) {
                            UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake((count*35)+10, 40, 20, 20)];
                            img.image = [UIImage imageNamed:@"card_selected"];
                            [self.sclCardSelection addSubview:img];
                            count = count + 1;
                        } else if (count == 2) {
                            UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake((count*35)+10, 40, 20, 20)];
                            img.image = [UIImage imageNamed:@"card_selected"];
                            [self.sclCardSelection addSubview:img];
                            count = count + 1;
                        } else if (count == 3) {
                            UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake((count*35)+10, 40, 20, 20)];
                            img.image = [UIImage imageNamed:@"card_selected"];
                            [self.sclCardSelection addSubview:img];
                            count = count + 1;
                        } else if (count == 4) {
                            UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake((count*35)+10, 40, 20, 20)];
                            img.image = [UIImage imageNamed:@"card_selected"];
                            [self.sclCardSelection addSubview:img];
                            count = count + 1;
                        } else if (count == 5) {
                            UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake((count*35)+10, 40, 20, 20)];
                            img.image = [UIImage imageNamed:@"card_selected"];
                            [self.sclCardSelection addSubview:img];
                            count = count + 1;
                        } else if (count == 6) {
                            UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake((count*35)+10, 40, 20, 20)];
                            img.image = [UIImage imageNamed:@"card_selected"];
                            [self.sclCardSelection addSubview:img];
                            count = count + 1;
                        } else if (count == 7) {
                            UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake((count*35)+10, 40, 20, 20)];
                            img.image = [UIImage imageNamed:@"card_selected"];
                            [self.sclCardSelection addSubview:img];
                            count = count + 1;
                        } else if (count == 8) {
                            UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake((count*35)+10, 40, 20, 20)];
                            img.image = [UIImage imageNamed:@"card_selected"];
                            [self.sclCardSelection addSubview:img];
                            count = count + 1;
                        } else if (count == 9) {
                            UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake((count*35)+10, 40, 20, 20)];
                            img.image = [UIImage imageNamed:@"card_selected"];
                            [self.sclCardSelection addSubview:img];
                            count = count + 1;
                        } else if (count == 10) {
                            UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake((count*35)+10, 40, 20, 20)];
                            img.image = [UIImage imageNamed:@"card_selected"];
                            [self.sclCardSelection addSubview:img];
                            count = count + 1;
                        } else if (count == 11) {
                            UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake((count*35)+10, 40, 20, 20)];
                            img.image = [UIImage imageNamed:@"card_selected"];
                            [self.sclCardSelection addSubview:img];
                            count = count + 1;
                        } else if (count == 12) {
                            UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake((count*35)+10, 40, 20, 20)];
                            img.image = [UIImage imageNamed:@"card_selected"];
                            [self.sclCardSelection addSubview:img];
                            count = count + 1;
                        } else if (count == 13) {
                            UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake((count*35)+10, 40, 20, 20)];
                            img.image = [UIImage imageNamed:@"card_selected"];
                            [self.sclCardSelection addSubview:img];
                            count = count + 1;
                        } else if (count == 14) {
                            UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake((count*35)+10, 40, 20, 20)];
                            img.image = [UIImage imageNamed:@"card_selected"];
                            [self.sclCardSelection addSubview:img];
                            count = count + 1;
                        } else if (count == 15) {
                            UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake((count*35)+10, 40, 20, 20)];
                            img.image = [UIImage imageNamed:@"card_selected"];
                            [self.sclCardSelection addSubview:img];
                            count = count + 1;
                        } else if (count == 16) {
                            UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake((count*35)+10, 40, 20, 20)];
                            img.image = [UIImage imageNamed:@"card_selected"];
                            [self.sclCardSelection addSubview:img];
                            count = count + 1;
                        } else if (count == 17) {
                            UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake((count*35)+10, 40, 20, 20)];
                            img.image = [UIImage imageNamed:@"card_selected"];
                            [self.sclCardSelection addSubview:img];
                            count = count + 1;
                        } else if (count == 18) {
                            UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake((count*35)+10, 40, 20, 20)];
                            img.image = [UIImage imageNamed:@"card_selected"];
                            [self.sclCardSelection addSubview:img];
                            count = count + 1;
                        } else if (count == 19) {
                            UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake((count*35)+10, 40, 20, 20)];
                            img.image = [UIImage imageNamed:@"card_selected"];
                            [self.sclCardSelection addSubview:img];
                            count = count + 1;
                        } else if (count == 20) {
                            UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake((count*35)+10, 40, 20, 20)];
                            img.image = [UIImage imageNamed:@"card_selected"];
                            [self.sclCardSelection addSubview:img];
                            count = count + 1;
                        } else if (count == 21) {
                            UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake((count*35)+10, 40, 20, 20)];
                            img.image = [UIImage imageNamed:@"card_selected"];
                            [self.sclCardSelection addSubview:img];
                            count = count + 1;
                        } else if (count == 22) {
                            UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake((count*35)+10, 40, 20, 20)];
                            img.image = [UIImage imageNamed:@"card_selected"];
                            [self.sclCardSelection addSubview:img];
                            count = count + 1;
                        } else if (count == 23) {
                            UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake((count*35)+10, 40, 20, 20)];
                            img.image = [UIImage imageNamed:@"card_selected"];
                            [self.sclCardSelection addSubview:img];
                            count = count + 1;
                        } else if (count == 24) {
                            UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake((count*35)+10, 40, 20, 20)];
                            img.image = [UIImage imageNamed:@"card_selected"];
                            [self.sclCardSelection addSubview:img];
                            if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
                                CustomSpreadCardDisplayVC *customspreadVC = [[CustomSpreadCardDisplayVC alloc] initWithNibName:@"CustomSpreadCardDisplayVC" bundle:nil];
                                customspreadVC.get_cardID = self.cardID;
                                customspreadVC.Title = self.spread_Name;
                                customspreadVC.grid = 5.0;
                                customspreadVC.get_CardPosition = self.getToatalCards;
                                customspreadVC.get_spread_id= self.spread_id;
                                [self.navigationController pushViewController:customspreadVC animated:YES];
                            } else {
                                CustomSpreadCardDisplayVC *customspreadVC = [[CustomSpreadCardDisplayVC alloc] initWithNibName:@"CustomSpreadCardDisplayVC_IPad" bundle:nil];
                                customspreadVC.get_cardID = self.cardID;
                                customspreadVC.Title = self.spread_Name;
                                customspreadVC.grid = 5.0;
                                customspreadVC.get_CardPosition = self.getToatalCards;
                                customspreadVC.get_spread_id= self.spread_id;
                                [self.navigationController pushViewController:customspreadVC animated:YES];
                            }
                        }
                    }

                }
            }
        }
        
    }
    
    else if ([self.get_Selected_Value isEqualToString:@"6x6"]){
         if ([sender tag] < 51) {
             [self.imgSelectarr replaceObjectAtIndex:([sender tag]) withObject:@"1"];
             [self.cardID addObject:[_tepmArr objectAtIndex:[sender tag]]];
             [_tepmArr removeObjectAtIndex:[sender tag]];
             int count = 0;
             for (int i=0; i<self.imgSelectarr.count; i++) {
                 if ([[self.imgSelectarr objectAtIndex:i] isEqualToString:@"1"]) {
                     if (self.getToatalCards.count == count+1) {
                         if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
                             CustomSpreadCardDisplayVC *customspreadVC = [[CustomSpreadCardDisplayVC alloc] initWithNibName:@"CustomSpreadCardDisplayVC" bundle:nil];
                             customspreadVC.get_cardID = self.cardID;
                             customspreadVC.Title = self.spread_Name;
                             customspreadVC.grid = 6.0;
                             customspreadVC.get_CardPosition = self.getToatalCards;
                             customspreadVC.get_spread_id= self.spread_id;
                             [self.navigationController pushViewController:customspreadVC animated:YES];
                         } else {
                             CustomSpreadCardDisplayVC *customspreadVC = [[CustomSpreadCardDisplayVC alloc] initWithNibName:@"CustomSpreadCardDisplayVC_IPad" bundle:nil];
                             customspreadVC.get_cardID = self.cardID;
                             customspreadVC.Title = self.spread_Name;
                             customspreadVC.grid = 6.0;
                             customspreadVC.get_CardPosition = self.getToatalCards;
                             customspreadVC.get_spread_id= self.spread_id;
                             [self.navigationController pushViewController:customspreadVC animated:YES];
                         }
                     } else {
                         if (count == 0) {
                             UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake((count*35)+10, 40, 20, 20)];
                             img.image = [UIImage imageNamed:@"card_selected"];
                             [self.sclCardSelection addSubview:img];
                             count = count + 1;
                         } else if (count == 1) {
                             UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake((count*35)+10, 40, 20, 20)];
                             img.image = [UIImage imageNamed:@"card_selected"];
                             [self.sclCardSelection addSubview:img];
                             count = count + 1;
                         } else if (count == 2) {
                             UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake((count*35)+10, 40, 20, 20)];
                             img.image = [UIImage imageNamed:@"card_selected"];
                             [self.sclCardSelection addSubview:img];
                             count = count + 1;
                         } else if (count == 3) {
                             UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake((count*35)+10, 40, 20, 20)];
                             img.image = [UIImage imageNamed:@"card_selected"];
                             [self.sclCardSelection addSubview:img];
                             count = count + 1;
                         } else if (count == 4) {
                             UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake((count*35)+10, 40, 20, 20)];
                             img.image = [UIImage imageNamed:@"card_selected"];
                             [self.sclCardSelection addSubview:img];
                             count = count + 1;
                         } else if (count == 5) {
                             UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake((count*35)+10, 40, 20, 20)];
                             img.image = [UIImage imageNamed:@"card_selected"];
                             [self.sclCardSelection addSubview:img];
                             count = count + 1;
                         } else if (count == 6) {
                             UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake((count*35)+10, 40, 20, 20)];
                             img.image = [UIImage imageNamed:@"card_selected"];
                             [self.sclCardSelection addSubview:img];
                             count = count + 1;
                         } else if (count == 7) {
                             UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake((count*35)+10, 40, 20, 20)];
                             img.image = [UIImage imageNamed:@"card_selected"];
                             [self.sclCardSelection addSubview:img];
                             count = count + 1;
                         } else if (count == 8) {
                             UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake((count*35)+10, 40, 20, 20)];
                             img.image = [UIImage imageNamed:@"card_selected"];
                             [self.sclCardSelection addSubview:img];
                             count = count + 1;
                         } else if (count == 9) {
                             UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake((count*35)+10, 40, 20, 20)];
                             img.image = [UIImage imageNamed:@"card_selected"];
                             [self.sclCardSelection addSubview:img];
                             count = count + 1;
                         } else if (count == 10) {
                             UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake((count*35)+10, 40, 20, 20)];
                             img.image = [UIImage imageNamed:@"card_selected"];
                             [self.sclCardSelection addSubview:img];
                             count = count + 1;
                         } else if (count == 11) {
                             UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake((count*35)+10, 40, 20, 20)];
                             img.image = [UIImage imageNamed:@"card_selected"];
                             [self.sclCardSelection addSubview:img];
                             count = count + 1;
                         } else if (count == 12) {
                             UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake((count*35)+10, 40, 20, 20)];
                             img.image = [UIImage imageNamed:@"card_selected"];
                             [self.sclCardSelection addSubview:img];
                             count = count + 1;
                         } else if (count == 13) {
                             UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake((count*35)+10, 40, 20, 20)];
                             img.image = [UIImage imageNamed:@"card_selected"];
                             [self.sclCardSelection addSubview:img];
                             count = count + 1;
                         } else if (count == 14) {
                             UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake((count*35)+10, 40, 20, 20)];
                             img.image = [UIImage imageNamed:@"card_selected"];
                             [self.sclCardSelection addSubview:img];
                             count = count + 1;
                         } else if (count == 15) {
                             UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake((count*35)+10, 40, 20, 20)];
                             img.image = [UIImage imageNamed:@"card_selected"];
                             [self.sclCardSelection addSubview:img];
                             count = count + 1;
                         } else if (count == 16) {
                             UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake((count*35)+10, 40, 20, 20)];
                             img.image = [UIImage imageNamed:@"card_selected"];
                             [self.sclCardSelection addSubview:img];
                             count = count + 1;
                         } else if (count == 17) {
                             UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake((count*35)+10, 40, 20, 20)];
                             img.image = [UIImage imageNamed:@"card_selected"];
                             [self.sclCardSelection addSubview:img];
                             count = count + 1;
                         } else if (count == 18) {
                             UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake((count*35)+10, 40, 20, 20)];
                             img.image = [UIImage imageNamed:@"card_selected"];
                             [self.sclCardSelection addSubview:img];
                             count = count + 1;
                         } else if (count == 19) {
                             UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake((count*35)+10, 40, 20, 20)];
                             img.image = [UIImage imageNamed:@"card_selected"];
                             [self.sclCardSelection addSubview:img];
                             count = count + 1;
                         } else if (count == 20) {
                             UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake((count*35)+10, 40, 20, 20)];
                             img.image = [UIImage imageNamed:@"card_selected"];
                             [self.sclCardSelection addSubview:img];
                             count = count + 1;
                         } else if (count == 21) {
                             UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake((count*35)+10, 40, 20, 20)];
                             img.image = [UIImage imageNamed:@"card_selected"];
                             [self.sclCardSelection addSubview:img];
                             count = count + 1;
                         } else if (count == 22) {
                             UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake((count*35)+10, 40, 20, 20)];
                             img.image = [UIImage imageNamed:@"card_selected"];
                             [self.sclCardSelection addSubview:img];
                             count = count + 1;
                         } else if (count == 23) {
                             UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake((count*35)+10, 40, 20, 20)];
                             img.image = [UIImage imageNamed:@"card_selected"];
                             [self.sclCardSelection addSubview:img];
                             count = count + 1;
                         } else if (count == 24) {
                             UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake((count*35)+10, 40, 20, 20)];
                             img.image = [UIImage imageNamed:@"card_selected"];
                             [self.sclCardSelection addSubview:img];
                             count = count + 1;
                         } else if (count == 25) {
                             UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake((count*35)+10, 40, 20, 20)];
                             img.image = [UIImage imageNamed:@"card_selected"];
                             [self.sclCardSelection addSubview:img];
                             count = count + 1;
                         } else if (count == 26) {
                             UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake((count*35)+10, 40, 20, 20)];
                             img.image = [UIImage imageNamed:@"card_selected"];
                             [self.sclCardSelection addSubview:img];
                             count = count + 1;
                         } else if (count == 27) {
                             UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake((count*35)+10, 40, 20, 20)];
                             img.image = [UIImage imageNamed:@"card_selected"];
                             [self.sclCardSelection addSubview:img];
                             count = count + 1;
                         } else if (count == 28) {
                             UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake((count*35)+10, 40, 20, 20)];
                             img.image = [UIImage imageNamed:@"card_selected"];
                             [self.sclCardSelection addSubview:img];
                             count = count + 1;
                         } else if (count == 29) {
                             UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake((count*35)+10, 40, 20, 20)];
                             img.image = [UIImage imageNamed:@"card_selected"];
                             [self.sclCardSelection addSubview:img];
                             count = count + 1;
                         } else if (count == 30) {
                             UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake((count*35)+10, 40, 20, 20)];
                             img.image = [UIImage imageNamed:@"card_selected"];
                             [self.sclCardSelection addSubview:img];
                             count = count + 1;
                         } else if (count == 31) {
                             UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake((count*35)+10, 40, 20, 20)];
                             img.image = [UIImage imageNamed:@"card_selected"];
                             [self.sclCardSelection addSubview:img];
                             count = count + 1;
                         } else if (count == 32) {
                             UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake((count*35)+10, 40, 20, 20)];
                             img.image = [UIImage imageNamed:@"card_selected"];
                             [self.sclCardSelection addSubview:img];
                             count = count + 1;
                         } else if (count == 33) {
                             UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake((count*35)+10, 40, 20, 20)];
                             img.image = [UIImage imageNamed:@"card_selected"];
                             [self.sclCardSelection addSubview:img];
                             count = count + 1;
                         } else if (count == 34) {
                             UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake((count*35)+10, 40, 20, 20)];
                             img.image = [UIImage imageNamed:@"card_selected"];
                             [self.sclCardSelection addSubview:img];
                             count = count + 1;
                         } else if (count == 35) {
                             UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake((count*35)+10, 40, 20, 20)];
                             img.image = [UIImage imageNamed:@"card_selected"];
                             [self.sclCardSelection addSubview:img];
                             if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
                                 CustomSpreadCardDisplayVC *customspreadVC = [[CustomSpreadCardDisplayVC alloc] initWithNibName:@"CustomSpreadCardDisplayVC" bundle:nil];
                                 customspreadVC.get_cardID = self.cardID;
                                 customspreadVC.Title = self.spread_Name;
                                 customspreadVC.grid = 6.0;
                                 customspreadVC.get_CardPosition = self.getToatalCards;
                                 customspreadVC.get_spread_id= self.spread_id;
                                 [self.navigationController pushViewController:customspreadVC animated:YES];
                             } else {
                                 CustomSpreadCardDisplayVC *customspreadVC = [[CustomSpreadCardDisplayVC alloc] initWithNibName:@"CustomSpreadCardDisplayVC_IPad" bundle:nil];
                                 customspreadVC.get_cardID = self.cardID;
                                 customspreadVC.Title = self.spread_Name;
                                 customspreadVC.grid = 6.0;
                                 customspreadVC.get_CardPosition = self.getToatalCards;
                                 customspreadVC.get_spread_id= self.spread_id;
                                 [self.navigationController pushViewController:customspreadVC animated:YES];
                             }
                         }
                     }
          
                 }
             }
         }
        
    }
    
}

- (void)itemDidFinishPlaying:(NSNotification *)notification {
    AVPlayerItem *player = [notification object];
    [player seekToTime:kCMTimeZero];
}




- (IBAction)btnBack:(id)sender {
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
        [self.navigationController popViewControllerAnimated:YES];
    } else {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (IBAction)btnVolume:(id)sender {
    UIButton *buttonPressed = (UIButton *)sender;
    if ([self.btnVolume.accessibilityHint isEqualToString:@"1"]) {
        self.btnVolume.accessibilityHint = @"0";
        self.btnVolume.hidden = TRUE;
        self.btnMute.hidden = FALSE;
        [self.playerViewController setVolume:0.0];
        NSUserDefaults *audiocheck = [NSUserDefaults standardUserDefaults];
        [audiocheck setValue:@"off" forKey:@"audio"];
        NSLog(@"audiocheck :- %@", audiocheck);
        [[NSUserDefaults standardUserDefaults] synchronize];
        
    } else {
        self.btnVolume.accessibilityHint = @"1";
        self.btnVolume.hidden = FALSE;
        self.btnMute.hidden = TRUE;
        [self.playerViewController setVolume:1.0];
        NSUserDefaults *audiocheck = [NSUserDefaults standardUserDefaults];
        [audiocheck setValue:@"on" forKey:@"audio"];
        NSLog(@"audiocheck :- %@", audiocheck);
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}


- (IBAction)btnSuffle:(id)sender {
    
    [UIView animateWithDuration:3
                          delay:1
                        options:UIViewAnimationCurveLinear
                     animations:^{
                         [self.srlv scrollRectToVisible:CGRectMake(self.srlv.contentSize.width-self.srlv.frame.size.width, 0, self.srlv.frame.size.width, self.srlv.frame.size.height) animated:YES];
                     }
                     completion:^(BOOL finished) {
                         double delayInSeconds = 1.0;
                         dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
                         dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                             [self.srlv scrollRectToVisible:CGRectMake(0, 0, self.srlv.frame.size.width, self.srlv.frame.size.height) animated:YES];
                             for (int x = 0; x< [self.arrShuffle count] ; x++) {
                              int randInt = (arc4random() % ([_arrShuffle count] - x)) + x;
                              [_arrShuffle exchangeObjectAtIndex:x withObjectAtIndex:randInt];
                              [self.tepmArr exchangeObjectAtIndex:x withObjectAtIndex:randInt];
                              NSLog(@" after random :- %@", self.tepmArr);
                              // [CommanMethods displayAlertMessage:@"UOC" message:@"Card Shuffle" currentRef:self];
                              [self ShowAlert:@"Card Shuffle"];
                              }
                         });
                     }];
}



- (void) ShowAlert:(NSString *)Message {
    UIAlertController * alert=[UIAlertController alertControllerWithTitle:nil
                                                                  message:@""
                                                           preferredStyle:UIAlertControllerStyleAlert];
    UIView *firstSubview = alert.view.subviews.firstObject;
    UIView *alertContentView = firstSubview.subviews.firstObject;
    for (UIView *subSubView in alertContentView.subviews) {
        subSubView.backgroundColor = [UIColor colorWithRed:173/255.0f green:173/255.0f blue:173/255.0f alpha:1.0f];
    }
    NSMutableAttributedString *AS = [[NSMutableAttributedString alloc] initWithString:Message];
    [AS addAttribute: NSForegroundColorAttributeName value: [UIColor blackColor] range: NSMakeRange(0,AS.length)];
    [alert setValue:AS forKey:@"attributedTitle"];
    [self presentViewController:alert animated:YES completion:nil];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [alert dismissViewControllerAnimated:YES completion:^{
        }];
    });
}
@end
