//
//  CardSelectionVC.h
//  UniversalOracleCards
//
//  Created by whiznic on 11/03/19.
//  Copyright © 2019 whiznic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "iCarousel.h"
#import <AVFoundation/AVFoundation.h>
#import <AVKit/AVKit.h>

@interface CardSelectionVC : UIViewController <iCarouselDataSource, iCarouselDelegate>
@property (strong, nonatomic) IBOutlet iCarousel *demoiCarousel;
@property (nonatomic) AVPlayer *playerViewController;
@property (strong, nonatomic) IBOutlet UIView *viewDisply;
@property (strong, nonatomic) NSString *get_Selected_Value,*spread_Name;
@property (strong, nonatomic) NSMutableArray *getToatalCards,*getCard_Title;
@property (weak, nonatomic) IBOutlet UIScrollView *srlv;
@property (weak, nonatomic) IBOutlet UIButton *btnMute;
@property (weak, nonatomic) IBOutlet UIScrollView *sclCardSelection;
@property (weak, nonatomic) IBOutlet UIButton *btnVolume,*spread_id;
@property (weak, nonatomic) IBOutlet UILabel *lblQuestion;
@end


