//
//  DailyCardVC.h
//  UniversalOracleCards
//
//  Created by whiznic on 06/03/19.
//  Copyright © 2019 whiznic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <AVKit/AVKit.h>

@interface DailyCardVC : UIViewController <UITextFieldDelegate>
@property (strong, nonatomic) IBOutlet UIView *ViewBorder;
@property (strong, nonatomic) IBOutlet UIImageView *imgCard;
@property (nonatomic) AVPlayer *playerViewController;
@property (strong, nonatomic) IBOutlet UIView *viewDisply;
@property (strong, nonatomic) IBOutlet UITextField *txtTitle;
@property (strong, nonatomic) NSMutableArray *get_cardID;
@property (strong, nonatomic) NSDictionary *get_Data;
@property (strong, nonatomic) NSString *getvalue;
@end

