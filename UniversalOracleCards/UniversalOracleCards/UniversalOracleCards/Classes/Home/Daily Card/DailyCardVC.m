//
//  DailyCardVC.m
//  UniversalOracleCards
//
//  Created by whiznic on 06/03/19.
//  Copyright © 2019 whiznic. All rights reserved.
//

#import "DailyCardVC.h"
#import "HomeVC.h"
#import "GeneralAdviceVC.h"
#import <IQKeyboardManager/IQKeyboardManager.h>
#import "CommanMethods.h"
#import "AppDelegate.h"
#import "CommunicationHandler.h"
#import <AudioToolbox/AudioServices.h>
#import "SharkfoodMuteSwitchDetector.h"
#import "CardDisplywithNamesVC.h"
@import MediaPlayer;

@interface DailyCardVC ()
{
    NSLocale* currentLocale;
    NSDateFormatter *dateFormatter;
    NSString *joinedString;
    NSString *readingid;
    int pass;
}
@property (nonatomic,strong) SharkfoodMuteSwitchDetector* detector;
@end

@implementation DailyCardVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
        self.detector = [SharkfoodMuteSwitchDetector shared];
        __weak DailyCardVC* sself = self;
        self.detector.silentNotify = ^(BOOL silent){
            if (silent) {
                
                NSLog(@"device is silent");
            } else {
                
                NSLog(@"device is in ringing mode");
                [self.playerViewController performSelector:@selector(play) withObject:nil afterDelay:0.01];
            }
        };
    

    
    self.ViewBorder.layer.borderColor = [UIColor colorWithRed:173/255.0 green:173/255.0 blue:173/255.0 alpha:1.0].CGColor;
    self.ViewBorder.layer.borderWidth = 1;
    self.ViewBorder.layer.cornerRadius = 20;
    
    
     NSLog(@"self.get_Data :- %@", self.get_Data);
    
    CALayer *border1 = [CALayer layer];
    CGFloat borderWidth1 = 1;
    border1.borderColor = [UIColor colorWithRed:173/255.0 green:173/255.0 blue:173/255.0 alpha:1.0].CGColor;
    border1.frame = CGRectMake(0, self.txtTitle.frame.size.height - borderWidth1, self.txtTitle.frame.size.width, self.txtTitle.frame.size.height);
    border1.borderWidth = borderWidth1;
    [self.txtTitle.layer addSublayer:border1];
    self.txtTitle.layer.masksToBounds = YES;
    

    
    currentLocale = [NSLocale currentLocale];
    [[NSDate date] descriptionWithLocale:currentLocale];
    
    dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];

    if (self.get_Data == 0) {
        self.txtTitle.text = [dateFormatter stringFromDate:[NSDate date]];
    } else {
        self.txtTitle.text = [self.get_Data objectForKey:@"save_name"];
    }
    if (self.get_Data == 0) {
        joinedString = [self.get_cardID componentsJoinedByString:@","];
        
        self.imgCard.image = [UIImage imageNamed:joinedString];
        
    } else {
     
        joinedString = [self.get_Data objectForKey:@"card_number"];
        
        self.imgCard.image = [UIImage imageNamed:joinedString];
    }
    
    self.txtTitle.delegate = self;
     [[IQKeyboardManager sharedManager] setEnable:true];

  
    
    
    
    
    
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    NSString *stringVideoName = @"stars_animated.mp4";
    NSString *filepath = [[NSBundle mainBundle] pathForResource:stringVideoName ofType:nil];
    NSLog(@" filepath :- %@", filepath);
    NSURL *fileURL = [NSURL fileURLWithPath:filepath];
    self.playerViewController = [AVPlayer playerWithURL:fileURL];
    self.playerViewController.actionAtItemEnd = AVPlayerActionAtItemEndNone;
    AVPlayerLayer *videoLayer = [AVPlayerLayer playerLayerWithPlayer:self.playerViewController];
    videoLayer.frame = self.view.bounds;
    videoLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
    [self.view.layer addSublayer:videoLayer];
    
    [self.playerViewController play];
    
    NSString *savedValue = [[NSUserDefaults standardUserDefaults]
                            stringForKey:@"audio"];
    if([savedValue isEqualToString:@"off"]) {
        
        [self.playerViewController setVolume:0.0];
    } else {
        
        [self.playerViewController setVolume:1.0];
    }
    
    NSLog(@" savedValue :- %@", savedValue);
    
    
    NSLog(@"self.get_cardID :- %@", self.get_cardID);
    
    self.viewDisply.layer.zPosition = 1;
    
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationDidBecomeActive:) name:UIApplicationDidEnterBackgroundNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(itemDidFinishPlaying:) name:AVPlayerItemDidPlayToEndTimeNotification object:[self.playerViewController currentItem]];
    
    [[UIApplication sharedApplication] beginReceivingRemoteControlEvents];
    [self becomeFirstResponder];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [self.playerViewController pause];
    [super viewWillDisappear:animated];
    [[UIApplication sharedApplication] endReceivingRemoteControlEvents];
    [self resignFirstResponder];
}


- (void)remoteControlReceivedWithEvent:(UIEvent *)event {
    switch (event.subtype) {
        case UIEventSubtypeRemoteControlTogglePlayPause:
            if([self.playerViewController rate] == 0){
                [self.playerViewController play];
            } else {
                [self.playerViewController pause];
            }
            break;
        case UIEventSubtypeRemoteControlPlay:
            [self.playerViewController play];
            break;
        case UIEventSubtypeRemoteControlPause:
            [self.playerViewController pause];
            break;
        default:
            break;
    }
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    self.detector = [SharkfoodMuteSwitchDetector shared];
    __weak DailyCardVC* sself = self;
    self.detector.silentNotify = ^(BOOL silent){
        if (silent) {
            
            NSLog(@"device is silent");
        } else {
            
            NSLog(@"device is in ringing mode");
            [self.playerViewController performSelector:@selector(play) withObject:nil afterDelay:0.01];
        }
    };
}



- (void)itemDidFinishPlaying:(NSNotification *)notification {
    AVPlayerItem *player = [notification object];
    [player seekToTime:kCMTimeZero];
}

#pragma TEXTFIELD-DELEGATE

- (BOOL) textFieldShouldBeginEditing:(UITextField *)textField {
    self.txtTitle.text = @"";
   
    return YES;
}

-(void) textFieldDidChange:(UITextField *)textField {
    
    if(self.txtTitle.text.length == 0) {
        self.txtTitle.text = [dateFormatter stringFromDate:[NSDate date]];
       
    }
}

-(BOOL) textFieldShouldEndEditing:(UITextField *)textField {
    
    if(self.txtTitle.text.length == 0) {
        self.txtTitle.text = [dateFormatter stringFromDate:[NSDate date]];
       
    }
    return YES;
}

#pragma Mark - Button Action

- (IBAction)btnMyJournal:(id)sender {
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
        GeneralAdviceVC *generalVC = [[GeneralAdviceVC alloc] initWithNibName:@"GeneralAdviceVC" bundle:nil];
        generalVC.get_Selected_Value = @"1";
        generalVC.title = @"Daily Card";
        generalVC.spread_type = @"Daily";
        if (self.get_Data == 0) {
            generalVC.get_cardData = self.get_cardID;
        } else {
            joinedString = [self.get_Data objectForKey:@"card_number"];
            NSString *timeCreated = joinedString;
            NSArray *timeArray = [timeCreated componentsSeparatedByString:@","];
            NSMutableArray *data = [[NSMutableArray alloc] init];
            [data addObjectsFromArray:timeArray];
            generalVC.get_cardData = data;
        }
        generalVC.get_Selected_Value = @"1";
        if (readingid.length == 0) {
            if ([CommanMethods isNetworkRechable]) {
                [self callForSaveReading:self.txtTitle.text];
                generalVC.getid = readingid;
            } else {
                [CommanMethods displayAlertMessage:key_Informaction message:kInternetConnectionMessage currentRef:self];
            }
        } else {
            generalVC.getid = readingid;
            [self.navigationController pushViewController:generalVC animated:YES];
        }
        
        
    } else {
        GeneralAdviceVC *generalVC = [[GeneralAdviceVC alloc] initWithNibName:@"GeneralAdviceVC_IPad" bundle:nil];
        generalVC.get_Selected_Value = @"1";
        generalVC.title = @"Daily Card";
        generalVC.spread_type = @"Daily";
        if (self.get_Data == 0) {
            generalVC.get_cardData = self.get_cardID;
        } else {
            joinedString = [self.get_Data objectForKey:@"card_number"];
            NSString *timeCreated = joinedString;
            NSArray *timeArray = [timeCreated componentsSeparatedByString:@","];
            NSMutableArray *data = [[NSMutableArray alloc] init];
            [data addObjectsFromArray:timeArray];
            generalVC.get_cardData = data;
        }
           generalVC.get_Selected_Value = @"1";
        if (readingid.length == 0) {
            if ([CommanMethods isNetworkRechable]) {
                [self callForSaveReading:self.txtTitle.text];
                generalVC.getid = readingid;
            } else {
                [CommanMethods displayAlertMessage:key_Informaction message:kInternetConnectionMessage currentRef:self];
            }
        } else {
            generalVC.getid = readingid;
            [self.navigationController pushViewController:generalVC animated:YES];
        }
        
        
    }
}

- (IBAction)btnSaveReading:(id)sender {
    pass = 1;
    if (self.get_Data == 0) {
        if ([CommanMethods isNetworkRechable]) {
            [self callForSaveReading:self.txtTitle.text];
        } else {
            [CommanMethods displayAlertMessage:key_Informaction message:kInternetConnectionMessage currentRef:self];
        }
    } else {
        if ([CommanMethods isNetworkRechable]) {
            [self callForEditReading:self.txtTitle.text];
        } else {
            [CommanMethods displayAlertMessage:key_Informaction message:kInternetConnectionMessage currentRef:self];
        }
    }
}

- (IBAction)btnEdit:(id)sender {
    [self.txtTitle becomeFirstResponder];
}

- (IBAction)btnShare:(id)sender {
    NSMutableArray *activityItems= [NSMutableArray arrayWithObjects:self.imgCard.image, nil];
    UIActivityViewController *activityViewController = [[UIActivityViewController alloc] initWithActivityItems:activityItems applicationActivities:nil];
    activityViewController.excludedActivityTypes = @[UIActivityTypePostToWeibo, UIActivityTypePrint, UIActivityTypeCopyToPasteboard, UIActivityTypeAssignToContact, UIActivityTypeSaveToCameraRoll, UIActivityTypeAddToReadingList, UIActivityTypePostToFlickr, UIActivityTypePostToVimeo, UIActivityTypePostToTencentWeibo, UIActivityTypeAirDrop];
    [self presentViewController:activityViewController animated:YES completion:nil];
}

- (IBAction)btnBack:(id)sender {
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
        [self.navigationController popViewControllerAnimated:YES];
    } else {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

#pragma CallAPI

-(void)callForSaveReading:(NSString*)title  {
    NSMutableDictionary *dcitParams = [[NSMutableDictionary alloc] init];
    [dcitParams setValue:@"" forKey:@"custom_spread_id"];
    [dcitParams setValue:@"Daily" forKey:@"spread_type"];
    [dcitParams setValue:title forKey:@"save_name"];
    [dcitParams setValue:joinedString forKey:@"card_number"];
    [dcitParams setValue:@"1" forKey:@"no_of_cards"];
    
  
    
    NSLog(@" dcitParams :-%@", dcitParams);
    
    [AppDelegate addVwLoading];
    [CommunicationHandler callForSaveReading:dcitParams Sucess:^(NSDictionary *dict, NSURLResponse *response) {
        NSLog(@"%@", dict);
        dispatch_async(dispatch_get_main_queue(), ^{
            [AppDelegate removeVwLoading];
            if ([[dict objectForKey:@"error"] boolValue] == false) {
                    [CommanMethods displayAlertMessageWithCallBack:nil message:[dict objectForKey:@"message"] refrence:self Success:^{
                        readingid = [dict objectForKey:@"save_reading_id"];
                        if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
                            GeneralAdviceVC *generalVC = [[GeneralAdviceVC alloc] initWithNibName:@"GeneralAdviceVC" bundle:nil];
                            generalVC.get_Selected_Value = @"1";
                            generalVC.title = @"Daily Card";
                            generalVC.spread_type = @"Daily";
                            if (self.get_Data == 0) {
                                generalVC.get_cardData = self.get_cardID;
                            } else {
                                joinedString = [self.get_Data objectForKey:@"card_number"];
                                NSString *timeCreated = joinedString;
                                NSArray *timeArray = [timeCreated componentsSeparatedByString:@","];
                                NSMutableArray *data = [[NSMutableArray alloc] init];
                                [data addObjectsFromArray:timeArray];
                                generalVC.get_cardData = data;
                            }
                            generalVC.getid = readingid;
                            [self.navigationController pushViewController:generalVC animated:YES];
                        }
                        else {
                            GeneralAdviceVC *generalVC = [[GeneralAdviceVC alloc] initWithNibName:@"GeneralAdviceVC_IPad" bundle:nil];
                            generalVC.get_Selected_Value = @"1";
                            generalVC.title = @"Daily Card";
                            generalVC.spread_type = @"Daily";
                            if (self.get_Data == 0) {
                                generalVC.get_cardData = self.get_cardID;
                            } else {
                                joinedString = [self.get_Data objectForKey:@"card_number"];
                                NSString *timeCreated = joinedString;
                                NSArray *timeArray = [timeCreated componentsSeparatedByString:@","];
                                NSMutableArray *data = [[NSMutableArray alloc] init];
                                [data addObjectsFromArray:timeArray];
                                generalVC.get_cardData = data;
                            }
                            generalVC.getid = readingid;
                            [self.navigationController pushViewController:generalVC animated:YES];
                        }
                    }];
                
            } else {
                [CommanMethods displayAlertMessage:@"UOC" message:[dict objectForKey:@"message"] currentRef:self];
            }
        });
    } Error:^(NSError *error, NSURLResponse *response) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [AppDelegate removeVwLoading];
        });
    }];
}

-(void)callForEditReading:(NSString*)title  {
    NSMutableDictionary *dcitParams = [[NSMutableDictionary alloc] init];
    [dcitParams setValue:@"" forKey:@"custom_spread_id"];
    [dcitParams setValue:@"Daily" forKey:@"spread_type"];
    [dcitParams setValue:title forKey:@"save_name"];
    [dcitParams setValue:joinedString forKey:@"card_number"];
    [dcitParams setValue:@"1" forKey:@"no_of_cards"];
    [dcitParams setValue:[self.get_Data objectForKey:@"id"] forKey:@"id"];
    
    
    NSLog(@" dcitParams :-%@", dcitParams);
    
    [AppDelegate addVwLoading];
    [CommunicationHandler callForSaveReading:dcitParams Sucess:^(NSDictionary *dict, NSURLResponse *response) {
        NSLog(@"%@", dict);
        dispatch_async(dispatch_get_main_queue(), ^{
            [AppDelegate removeVwLoading];
            if ([[dict objectForKey:@"error"] boolValue] == false) {
                if ([self.getvalue isEqualToString:@"1"]) {
                    [CommanMethods displayAlertMessageWithCallBack:nil message:[dict objectForKey:@"message"] refrence:self Success:^{
                        readingid = [dict objectForKey:@"save_reading_id"];
                        if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
                            [self.navigationController popViewControllerAnimated:YES];
                        } else {
                            [self.navigationController popViewControllerAnimated:YES];
                        }
                    }];
                } else {
                    [CommanMethods displayAlertMessage:@"UOC" message:[dict objectForKey:@"message"] currentRef:self];
                    readingid = [dict objectForKey:@"save_reading_id"];
                }
            } else {
                [CommanMethods displayAlertMessage:@"UOC" message:[dict objectForKey:@"message"] currentRef:self];
            }
        });
    } Error:^(NSError *error, NSURLResponse *response) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [AppDelegate removeVwLoading];
        });
    }];
}



@end
