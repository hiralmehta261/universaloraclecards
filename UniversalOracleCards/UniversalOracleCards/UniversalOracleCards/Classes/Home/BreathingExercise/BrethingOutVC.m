//
//  BrethingOutVC.m
//  UniversalOracleCards
//
//  Created by whiznic on 08/03/19.
//  Copyright © 2019 whiznic. All rights reserved.
//

#import "BrethingOutVC.h"
#import "CardSelectionVC.h"
#import "HomeVC.h"
#import "DailyCardVC.h"
#import <AudioToolbox/AudioServices.h>
#import "SharkfoodMuteSwitchDetector.h"
#import <MediaPlayer/MediaPlayer.h>
@import MediaPlayer;

@interface BrethingOutVC ()
{
    NSTimer *timerCounter;
    BOOL muted;
}
@property (nonatomic,strong) SharkfoodMuteSwitchDetector* detector;

@end

@implementation BrethingOutVC


- (void)viewDidLoad {
    [super viewDidLoad];
    
    /*self.detector = [SharkfoodMuteSwitchDetector shared];
    __weak BrethingOutVC* sself = self;
    self.detector.silentNotify = ^(BOOL silent){
        if (silent) {
            [sself.playerViewController setVolume:0.0];
            sself.btnMute.hidden = FALSE;
            sself.btnVolume.hidden = TRUE;
            NSLog(@"device is silent");
        } else {
            [sself.playerViewController setVolume:1.0];
            sself.btnMute.hidden = TRUE;
            sself.btnVolume.hidden = FALSE;
            NSLog(@"device is in ringing mode");
        }
    };*/
    
    
   
    
    
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    NSString *stringVideoName = @"stars_animated.mp4";
    NSString *filepath = [[NSBundle mainBundle] pathForResource:stringVideoName ofType:nil];
    NSLog(@" filepath :- %@", filepath);
    NSURL *fileURL = [NSURL fileURLWithPath:filepath];
    self.playerViewController = [AVPlayer playerWithURL:fileURL];
    self.playerViewController.actionAtItemEnd = AVPlayerActionAtItemEndNone;
    AVPlayerLayer *videoLayer = [AVPlayerLayer playerLayerWithPlayer:self.playerViewController];
    videoLayer.frame = self.view.bounds;
    videoLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
    [self.view.layer addSublayer:videoLayer];
    
    
    [self.playerViewController play];
    
    NSString *savedValue = [[NSUserDefaults standardUserDefaults]
                            stringForKey:@"audio"];
    if([savedValue isEqualToString:@"off"]) {
        self.btnMute.hidden = FALSE;
        self.btnVolume.hidden = TRUE;
        [self.playerViewController setVolume:0.0];
    } else {
        self.btnMute.hidden = TRUE;
        self.btnVolume.hidden = FALSE;
        [self.playerViewController setVolume:1.0];
    }
    
    NSLog(@" savedValue :- %@", savedValue);
    
    
    self.viewDisply.layer.zPosition = 1;
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(itemDidFinishPlaying:) name:AVPlayerItemDidPlayToEndTimeNotification object:[self.playerViewController currentItem]];
    self.imgBreatheIn.hidden = TRUE;
    
    
    timerCounter = [NSTimer scheduledTimerWithTimeInterval:2.0 target:self selector:@selector(doMyWork) userInfo:nil repeats: YES];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationDidBecomeActive:) name:UIApplicationDidEnterBackgroundNotification object:nil];
    
    
    [[UIApplication sharedApplication] beginReceivingRemoteControlEvents];
    [self becomeFirstResponder];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [self.playerViewController pause];
    [super viewWillDisappear:animated];
    [[UIApplication sharedApplication] endReceivingRemoteControlEvents];
    [self resignFirstResponder];
}


- (void)remoteControlReceivedWithEvent:(UIEvent *)event {
    switch (event.subtype) {
        case UIEventSubtypeRemoteControlTogglePlayPause:
            if([self.playerViewController rate] == 0){
                [self.playerViewController play];
            } else {
                [self.playerViewController pause];
            }
            break;
        case UIEventSubtypeRemoteControlPlay:
            [self.playerViewController play];
            break;
        case UIEventSubtypeRemoteControlPause:
            [self.playerViewController pause];
            break;
        default:
            break;
    }
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    self.detector = [SharkfoodMuteSwitchDetector shared];
    __weak BrethingOutVC* sself = self;
    self.detector.silentNotify = ^(BOOL silent){
        if (silent) {
            NSLog(@"device is silent");
        } else {
            
            NSLog(@"device is in ringing mode");
            [self.playerViewController performSelector:@selector(play) withObject:nil afterDelay:0.01];
        }
    };
}



-(void)viewWillAppear:(BOOL)animated {
    

}

-(void)doMyWork
{

    [UIView animateWithDuration:4.0 animations:^{
        self.imgBreatheOut.transform = CGAffineTransformMakeScale(1, 1);
        //self.lblText.transform = CGAffineTransformMakeScale(1, 1);
        self.lblText.text = @"Breathe In...";
    }
                     completion:^(BOOL finished){
                         [UIView animateWithDuration:4.0 animations:^{
                             self.imgBreatheOut.transform = CGAffineTransformMakeScale(0.5, 0.5);
                            // self.lblText.transform = CGAffineTransformMakeScale(0.5, 0.5);
                             self.lblText.text = @"Breathe Out...";
                         }];
                     }];
    
 
  
    
    /*timerCounter = [NSTimer scheduledTimerWithTimeInterval:2.0 target:self selector:@selector(doMyData) userInfo:nil repeats: YES];
    self.lblText.text = @"Breathe In...";*/
    
}

-(void)doMyData
{
  
   
    /*timerCounter = [NSTimer scheduledTimerWithTimeInterval:2.0 target:self selector:@selector(doMyWork) userInfo:nil repeats: YES];
    self.lblText.text = @"Breathe Out...";*/
    
    
}


- (void)itemDidFinishPlaying:(NSNotification *)notification {
    AVPlayerItem *player = [notification object];
    [player seekToTime:kCMTimeZero];
}



- (IBAction)btnBack:(id)sender {
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
        HomeVC *homeVC = [[HomeVC alloc] initWithNibName:@"HomeVC" bundle:nil];
        [self.navigationController pushViewController:homeVC animated:YES];
    } else {
        HomeVC *homeVC = [[HomeVC alloc] initWithNibName:@"HomeVC_IPad" bundle:nil];
        [self.navigationController pushViewController:homeVC animated:YES];
    }
}

- (IBAction)btnContinue:(id)sender {
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
        CardSelectionVC *CardselectionVC = [[CardSelectionVC alloc] initWithNibName:@"CardSelectionVC" bundle:nil];
        CardselectionVC.get_Selected_Value = self.get_Value;
        [self.navigationController pushViewController:CardselectionVC animated:YES];
    } else {
        CardSelectionVC *CardselectionVC = [[CardSelectionVC alloc] initWithNibName:@"CardSelectionVC_IPad" bundle:nil];
        CardselectionVC.get_Selected_Value = self.get_Value;
        [self.navigationController pushViewController:CardselectionVC animated:YES];
    }
}
- (IBAction)btnVolume:(id)sender {
    
    UIButton *buttonPressed = (UIButton *)sender;
    
    
    if ([self.btnVolume.accessibilityHint isEqualToString:@"1"]) {
        self.btnVolume.accessibilityHint = @"0";
        self.btnVolume.hidden = TRUE;
        self.btnMute.hidden = FALSE;
        [self.playerViewController setVolume:0.0];
        NSUserDefaults *audiocheck = [NSUserDefaults standardUserDefaults];
        [audiocheck setValue:@"off" forKey:@"audio"];
        NSLog(@"audiocheck :- %@", audiocheck);
        [[NSUserDefaults standardUserDefaults] synchronize];
        
    } else {
        self.btnVolume.accessibilityHint = @"1";
        self.btnVolume.hidden = FALSE;
        self.btnMute.hidden = TRUE;
        [self.playerViewController setVolume:1.0];
        NSUserDefaults *audiocheck = [NSUserDefaults standardUserDefaults];
        [audiocheck setValue:@"on" forKey:@"audio"];
        NSLog(@"audiocheck :- %@", audiocheck);
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    
}
@end
