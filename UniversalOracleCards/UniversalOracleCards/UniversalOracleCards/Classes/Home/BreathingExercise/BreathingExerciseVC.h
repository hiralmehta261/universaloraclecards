//
//  BreathingExerciseVC.h
//  UniversalOracleCards
//
//  Created by whiznic on 05/03/19.
//  Copyright © 2019 whiznic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <AVKit/AVKit.h>


@interface BreathingExerciseVC : UIViewController
@property (strong, nonatomic) IBOutlet UILabel *lblBreathe;
@property (strong, nonatomic) IBOutlet UIButton *btnVolume;
@property (nonatomic) AVPlayer *playerViewController;
@property (strong, nonatomic) IBOutlet UIView *viewDisply;
@property (strong, nonatomic) NSString *get_Value;
@property (weak, nonatomic) IBOutlet UIButton *btnMute;
@end


