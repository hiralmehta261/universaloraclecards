//
//  BrethingOutVC.h
//  UniversalOracleCards
//
//  Created by whiznic on 08/03/19.
//  Copyright © 2019 whiznic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <AVKit/AVKit.h>

@interface BrethingOutVC : UIViewController
@property (nonatomic) AVPlayer *playerViewController;
@property (strong, nonatomic) IBOutlet UIView *viewDisply;
@property (strong, nonatomic) NSString *get_Value;
@property (weak, nonatomic) IBOutlet UIButton *btnMute;
@property (weak, nonatomic) IBOutlet UILabel *lblText;
@property (weak, nonatomic) IBOutlet UIImageView *imgBreatheIn;
@property (weak, nonatomic) IBOutlet UIImageView *imgBreatheOut;
@property (weak, nonatomic) IBOutlet UIButton *btnVolume;
@end


