//
//  BreathingExerciseVC.m
//  UniversalOracleCards
//
//  Created by whiznic on 05/03/19.
//  Copyright © 2019 whiznic. All rights reserved.
//

#import "BreathingExerciseVC.h"
#import "HomeVC.h"
#import "DailyCardVC.h"
#import "SharkfoodMuteSwitchDetector.h"
@import MediaPlayer;

@interface BreathingExerciseVC ()
@property (nonatomic,strong) SharkfoodMuteSwitchDetector* detector;
@end

@implementation BreathingExerciseVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.detector = [SharkfoodMuteSwitchDetector shared];
    __weak BreathingExerciseVC* sself = self;
    self.detector.silentNotify = ^(BOOL silent){
        if (silent) {
            sself.btnMute.hidden = FALSE;
            sself.btnVolume.hidden = TRUE;
            NSLog(@"device is silent");
        } else {
            sself.btnMute.hidden = TRUE;
            sself.btnVolume.hidden = FALSE;
            NSLog(@"device is in ringing mode");
        }
    };
    
    NSString *stringVideoName = @"stars_animated.mp4";
    NSString *filepath = [[NSBundle mainBundle] pathForResource:stringVideoName ofType:nil];
    NSLog(@" filepath :- %@", filepath);
    NSURL *fileURL = [NSURL fileURLWithPath:filepath];
    self.playerViewController = [AVPlayer playerWithURL:fileURL];
    self.playerViewController.actionAtItemEnd = AVPlayerActionAtItemEndNone;
    AVPlayerLayer *videoLayer = [AVPlayerLayer playerLayerWithPlayer:self.playerViewController];
    videoLayer.frame = self.view.bounds;
    videoLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
    [self.view.layer addSublayer:videoLayer];
    
    [self.playerViewController play];
    
    self.viewDisply.layer.zPosition = 1;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(itemDidFinishPlaying:) name:AVPlayerItemDidPlayToEndTimeNotification object:[self.playerViewController currentItem]];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationDidBecomeActive:) name:UIApplicationDidEnterBackgroundNotification object:nil];
    
    
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [[UIApplication sharedApplication] beginReceivingRemoteControlEvents];
    [self becomeFirstResponder];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [self.playerViewController pause];
    [super viewWillDisappear:animated];
    [[UIApplication sharedApplication] endReceivingRemoteControlEvents];
    [self resignFirstResponder];
}


- (void)remoteControlReceivedWithEvent:(UIEvent *)event {
    switch (event.subtype) {
        case UIEventSubtypeRemoteControlTogglePlayPause:
            if([self.playerViewController rate] == 0){
                [self.playerViewController play];
            } else {
                [self.playerViewController pause];
            }
            break;
        case UIEventSubtypeRemoteControlPlay:
            [self.playerViewController play];
            break;
        case UIEventSubtypeRemoteControlPause:
            [self.playerViewController pause];
            break;
        default:
            break;
    }
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    self.detector = [SharkfoodMuteSwitchDetector shared];
    __weak BreathingExerciseVC* sself = self;
    self.detector.silentNotify = ^(BOOL silent){
        if (silent) {
            
            NSLog(@"device is silent");
        } else {
            
            NSLog(@"device is in ringing mode");
            [self.playerViewController performSelector:@selector(play) withObject:nil afterDelay:0.01];
        }
    };
}


- (void)itemDidFinishPlaying:(NSNotification *)notification {
    AVPlayerItem *player = [notification object];
    [player seekToTime:kCMTimeZero];
}

#pragma Mark - Action Button

- (IBAction)btnVolume:(id)sender {
}

- (IBAction)btnContinue:(id)sender {
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
        DailyCardVC *dailycardVC = [[DailyCardVC alloc] initWithNibName:@"DailyCardVC" bundle:nil];
        [self.navigationController pushViewController:dailycardVC animated:YES];
    } else {
        DailyCardVC *dailycardVC = [[DailyCardVC alloc] initWithNibName:@"DailyCardVC_IPad" bundle:nil];
        [self.navigationController pushViewController:dailycardVC animated:YES];
    }
}

- (IBAction)btnBack:(id)sender {
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
        HomeVC *homeVC = [[HomeVC alloc] initWithNibName:@"HomeVC" bundle:nil];
        [self.navigationController pushViewController:homeVC animated:YES];
    } else {
        HomeVC *homeVC = [[HomeVC alloc] initWithNibName:@"HomeVC_IPad" bundle:nil];
        [self.navigationController pushViewController:homeVC animated:YES];
    }
}

@end
