//
//  CardDisplywithNamesVC.m
//  UniversalOracleCards
//
//  Created by whiznic on 15/03/19.
//  Copyright © 2019 whiznic. All rights reserved.
//

#import "CardDisplywithNamesVC.h"
#import "AddingTextNoteVC.h"
#import <IQKeyboardManager/IQKeyboardManager.h>
#import "CardDisplywithNamesVC.h"
#import "SharkfoodMuteSwitchDetector.h"
#import "HomeVC.h"
#import <AudioToolbox/AudioServices.h>
#import "SharkfoodMuteSwitchDetector.h"
#import "TextNotesVC.h"
#import "AppDelegate.h"
#import "CommanMethods.h"
#import "CommunicationHandler.h"
#import "AudioNotesVC.h"

@import MediaPlayer;

@interface CardDisplywithNamesVC ()<AVAudioRecorderDelegate>
{
    NSMutableArray *arrNotesList;
    int value;
    NSString *cardid,*joinedString;
    int timeSec;
    int timeMin;
    NSTimer *timer;
    BOOL isAudio;
    AVAudioRecorder *recorder;
    AVAudioPlayer *player;
    NSURL *outputFileURL;
}
@property (nonatomic, assign) BOOL wrap;
@property (nonatomic, strong) NSMutableArray *items;
@property (nonatomic,strong) SharkfoodMuteSwitchDetector* detector;
@end

@implementation CardDisplywithNamesVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
     value = -1;
    isAudio = false;
    
    timeSec = 0;
    timeMin = 0;
    
   /* self.detector = [SharkfoodMuteSwitchDetector shared];
    __weak CardDisplywithNamesVC* sself = self;
    self.detector.silentNotify = ^(BOOL silent){
        if (silent) {
            [sself.playerViewController setVolume:0.0];
            sself.btnMute.hidden = FALSE;
            sself.btnVolume.hidden = TRUE;
            NSLog(@"device is silent");
        } else {
            [sself.playerViewController setVolume:1.0];
            sself.btnMute.hidden = TRUE;
            sself.btnVolume.hidden = FALSE;
            NSLog(@"device is in ringing mode");
        }
    };*/
    
    
    
    if (self.get_cardid.count == 1) {
        self.demoiCarousel.scrollEnabled = FALSE;
        joinedString = [self.get_cardid componentsJoinedByString:@","];
        if ([CommanMethods isNetworkRechable]) {
            [self callForGetNotesList:joinedString];
        } else {
            [CommanMethods displayAlertMessage:key_Informaction message:kInternetConnectionMessage currentRef:self];
        }
    } else {
        NSString *joinedString1 = [self.get_cardid componentsJoinedByString:@","];
        NSString *timeCreated = joinedString1;
        NSArray *timeArray = [timeCreated componentsSeparatedByString:@","];
        joinedString = [timeArray objectAtIndex:0];
        if ([CommanMethods isNetworkRechable]) {
            [self callForGetNotesList:joinedString];
        } else {
            [CommanMethods displayAlertMessage:key_Informaction message:kInternetConnectionMessage currentRef:self];
        }
    }
    
  
    
    
    self.demoiCarousel.dataSource = self;
    self.demoiCarousel.delegate = self;
    
    self.demoiCarousel.type = iCarouselTypeRotary;
    
    
        UINib *cellnib = [UINib nibWithNibName:@"TextNotesVC" bundle:nil];
        [self.tblNotesDetails registerNib:cellnib forCellReuseIdentifier:TextNotesVCID];
    
    
        UINib *cellnib1 = [UINib nibWithNibName:@"AudioNotesVC" bundle:nil];
        [self.tblNotesDetails registerNib:cellnib1 forCellReuseIdentifier:AudioNotesVCID];
        
    
    
    self.tblNotesDetails.estimatedRowHeight = 160.0;
    self.tblNotesDetails.delegate = self;
    self.tblNotesDetails.dataSource = self;
    
    
   
    
    NSLog(@"get_cardid :- %@", self.get_cardid);
    
    
}

-(void)viewWillAppear:(BOOL)animated {
   // [self StartTimer];
}

-(void) StartTimer
{
    timer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(timerTick:) userInfo:nil repeats:YES];
    [[NSRunLoop currentRunLoop] addTimer:timer forMode:NSDefaultRunLoopMode];
}

//Event called every time the NSTimer ticks.
- (void)timerTick:(NSTimer *)timer
{
    timeSec++;
    if (timeSec == 60)
    {
        timeSec = 0;
        timeMin++;
    }
    //Format the string 00:00
    NSString* timeNow = [NSString stringWithFormat:@"%02d:%02d", timeMin, timeSec];
    //Display on your label
    //[timeLabel setStringValue:timeNow];
    self.lblAudioRecording.text= timeNow;
    isAudio = true;
}

//Call this to stop the timer event(could use as a 'Pause' or 'Reset')
- (void) StopTimer
{
    [timer invalidate];
    timeSec = 0;
    timeMin = 0;
    //Since we reset here, and timerTick won't update your label again, we need to refresh it again.
    //Format the string in 00:00
    NSString* timeNow = [NSString stringWithFormat:@"%02d:%02d", timeMin, timeSec];
    //Display on your label
    // [timeLabel setStringValue:timeNow];
    self.lblAudioRecording.text= timeNow;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    NSString *stringVideoName = @"stars_animated.mp4";
    NSString *filepath = [[NSBundle mainBundle] pathForResource:stringVideoName ofType:nil];
    NSLog(@" filepath :- %@", filepath);
    NSURL *fileURL = [NSURL fileURLWithPath:filepath];
    self.playerViewController = [AVPlayer playerWithURL:fileURL];
    self.playerViewController.actionAtItemEnd = AVPlayerActionAtItemEndNone;
    AVPlayerLayer *videoLayer = [AVPlayerLayer playerLayerWithPlayer:self.playerViewController];
    videoLayer.frame = self.view.bounds;
    videoLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
    [self.view.layer addSublayer:videoLayer];
    
    [self.playerViewController play];
    
    NSString *savedValue = [[NSUserDefaults standardUserDefaults]
                            stringForKey:@"audio"];
    if([savedValue isEqualToString:@"off"]) {
        self.btnMute.hidden = FALSE;
        self.btnVolume.hidden = TRUE;
        [self.playerViewController setVolume:0.0];
    } else {
        self.btnMute.hidden = TRUE;
        self.btnVolume.hidden = FALSE;
        [self.playerViewController setVolume:1.0];
    }
    
    NSLog(@" savedValue :- %@", savedValue);
    
    
    self.viewDisply.layer.zPosition = 1;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(itemDidFinishPlaying:) name:AVPlayerItemDidPlayToEndTimeNotification object:[self.playerViewController currentItem]];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationDidBecomeActive:) name:UIApplicationDidEnterBackgroundNotification object:nil];
    
    
    [[UIApplication sharedApplication] beginReceivingRemoteControlEvents];
    [self becomeFirstResponder];
    
    //Recorde file login.
    NSString *strName = [self randomStringWithLength:10];
    strName = [NSString stringWithFormat:@"%@.m4a",strName];
    
    NSArray *dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docsDir = [dirPaths objectAtIndex:0];
    outputFileURL = [NSURL fileURLWithPath:[docsDir stringByAppendingPathComponent:strName]];
    NSDictionary *recordSettings = [NSDictionary dictionaryWithObjectsAndKeys:
                                    [NSNumber numberWithInt: kAudioFormatMPEG4AAC], AVFormatIDKey,
                                    [NSNumber numberWithFloat:16000.0], AVSampleRateKey,
                                    [NSNumber numberWithInt: 1], AVNumberOfChannelsKey,
                                    nil];
    NSError *error = nil;
    recorder = [[AVAudioRecorder alloc] initWithURL:outputFileURL settings:recordSettings error:&error];
    recorder.delegate = self;
    [recorder prepareToRecord];
    
    AVAudioSession *session = [AVAudioSession sharedInstance];
    [session setCategory:AVAudioSessionCategoryRecord error:nil];
    [session setActive:YES error:nil];
    
    [recorder record];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [self.playerViewController pause];
    [super viewWillDisappear:animated];
    [[UIApplication sharedApplication] endReceivingRemoteControlEvents];
    [self resignFirstResponder];
}


- (void)remoteControlReceivedWithEvent:(UIEvent *)event {
    switch (event.subtype) {
        case UIEventSubtypeRemoteControlTogglePlayPause:
            if([self.playerViewController rate] == 0){
                [self.playerViewController play];
            } else {
                [self.playerViewController pause];
            }
            break;
        case UIEventSubtypeRemoteControlPlay:
            [self.playerViewController play];
            break;
        case UIEventSubtypeRemoteControlPause:
            [self.playerViewController pause];
            break;
        default:
            break;
    }
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    self.detector = [SharkfoodMuteSwitchDetector shared];
    __weak CardDisplywithNamesVC* sself = self;
    self.detector.silentNotify = ^(BOOL silent){
        if (silent) {
            
            NSLog(@"device is silent");
        } else {
            
            NSLog(@"device is in ringing mode");
            [self.playerViewController performSelector:@selector(play) withObject:nil afterDelay:0.01];
        }
    };
}



- (void)itemDidFinishPlaying:(NSNotification *)notification {
    AVPlayerItem *player = [notification object];
    [player seekToTime:kCMTimeZero];
}

- (NSInteger)numberOfItemsInCarousel:(__unused iCarousel *)carousel
{
    return [self.get_cardid count];
}

- (UIView *)carousel:(__unused iCarousel *)carousel viewForItemAtIndex:(NSInteger)index reusingView:(UIView *)view
{
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
        if (view == nil)
        {
            view = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 200.0, 320.0)];
            view.layer.backgroundColor = [UIColor clearColor].CGColor;
            self.imgCard.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@",[self.get_cardid objectAtIndex:index]]];
            ((UIImageView *)view).image = self.imgCard.image;
        }
        else
        {
            view = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 200.0, 320.0)];
            view.layer.backgroundColor = [UIColor clearColor].CGColor;
            self.imgCard.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@",[self.get_cardid objectAtIndex:index]]];
            ((UIImageView *)view).image = self.imgCard.image;
        }
    } else {
        if (view == nil)
        {
            view = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 400.0, 520.0)];
            view.layer.backgroundColor = [UIColor clearColor].CGColor;
            self.imgCard.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@",[self.get_cardid objectAtIndex:index]]];
            ((UIImageView *)view).image = self.imgCard.image;
        }
        else
        {
            view = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 400.0, 520.0)];
            view.layer.backgroundColor = [UIColor clearColor].CGColor;
            self.imgCard.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@",[self.get_cardid objectAtIndex:index]]];
            ((UIImageView *)view).image = self.imgCard.image;
        }
    }
    
    view.layer.borderColor = [UIColor colorWithRed:173/255.0 green:173/255.0 blue:173/255.0 alpha:1.0].CGColor;
    view.layer.borderWidth = 1;
    view.layer.cornerRadius = 20;
    return view;
    
}

- (NSInteger)numberOfPlaceholdersInCarousel:(__unused iCarousel *)carousel
{
    //note: placeholder views are only displayed on some carousels if wrapping is disabled
    return 2;
}

- (UIView *)carousel:(__unused iCarousel *)carousel placeholderViewAtIndex:(NSInteger)index reusingView:(UIView *)view
{
    UILabel *label = nil;
    
    
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
        if (view == nil)
        {
            view = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 200.0, 320.0)];
            view.layer.backgroundColor = [UIColor clearColor].CGColor;
            self.imgCard.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@",[self.get_cardid objectAtIndex:index]]];
            ((UIImageView *)view).image = self.imgCard.image;
        }
        else
        {
            view = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 200.0, 320.0)];
            view.layer.backgroundColor = [UIColor clearColor].CGColor;
            self.imgCard.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@",[self.get_cardid objectAtIndex:index]]];
            ((UIImageView *)view).image = self.imgCard.image;
        }
    } else {
        if (view == nil)
        {
            view = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 400.0, 520.0)];
            view.layer.backgroundColor = [UIColor clearColor].CGColor;
            self.imgCard.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@",[self.get_cardid objectAtIndex:index]]];
            ((UIImageView *)view).image = self.imgCard.image;
        }
        else
        {
            view = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 400.0, 520.0)];
            view.layer.backgroundColor = [UIColor clearColor].CGColor;
            self.imgCard.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@",[self.get_cardid objectAtIndex:index]]];
            ((UIImageView *)view).image = self.imgCard.image;
        }
    }
    
    
    
    label.text = (index == 0)? @"[": @"]";
    
    return view;
}

- (CATransform3D)carousel:(__unused iCarousel *)carousel itemTransformForOffset:(CGFloat)offset baseTransform:(CATransform3D)transform
{
    
    transform = CATransform3DRotate(transform, M_PI / 8.0, 0.0, 1.0, 0.0);
    return CATransform3DTranslate(transform, 0.0, 0.0, offset * self.demoiCarousel.itemWidth);
}


#pragma mark -
#pragma mark iCarousel taps

- (void)carousel:(__unused iCarousel *)carousel didSelectItemAtIndex:(NSInteger)index
{
    NSNumber *item = (self.get_cardid)[(NSUInteger)index];
    NSLog(@"Tapped view number: %@", item);
}

- (void)carouselCurrentItemIndexDidChange:(__unused iCarousel *)carousel
{
    NSLog(@"Index: %@", @(self.demoiCarousel.currentItemIndex));
    
    for (int i = 0; i<self.get_cardid.count; i++) {
        if (self.demoiCarousel.currentItemIndex == i) {
            NSLog(@"value of i :- %@", [self.get_cardid objectAtIndex:i]);
            cardid = [self.get_cardid objectAtIndex:i];
        }
    }
    if ([CommanMethods isNetworkRechable]) {
        [self callForGetNotesList:cardid];
    } else {
        [CommanMethods displayAlertMessage:key_Informaction message:kInternetConnectionMessage currentRef:self];
    }
}

- (CGFloat)carousel:(iCarousel *)carousel valueForOption:(iCarouselOption)option withDefault:(CGFloat)value
{
    CGFloat result;
    
    switch(option)
    {
        case iCarouselOptionSpacing:
            result = 1.2; // If the width of your items is 40 e.g, the spacing would be 4 px.
            break;
        case iCarouselOptionFadeMin:
            return 0;
            break;
        case iCarouselOptionFadeMax:
            return 0;
            break;
        case iCarouselOptionFadeRange:
            return 3;
            break;
        case iCarouselOptionShowBackfaces:
            return NO;
            break;
        default:
            result = value;
            break;
    }
    
    return result;
}

- (IBAction)btnBack:(id)sender {
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
        HomeVC *homeVC = [[HomeVC alloc] initWithNibName:@"HomeVC" bundle:nil];
        [self.navigationController pushViewController:homeVC animated:YES];
    } else {
        HomeVC *homeVC = [[HomeVC alloc] initWithNibName:@"HomeVC_IPad" bundle:nil];
        [self.navigationController pushViewController:homeVC animated:YES];
    }
}

- (IBAction)btnVolume:(id)sender {
    if ([self.btnVolume.accessibilityHint isEqualToString:@"1"]) {
        self.btnVolume.accessibilityHint = @"0";
        self.btnVolume.hidden = TRUE;
        self.btnMute.hidden = FALSE;
        [self.playerViewController setVolume:0.0];
        NSUserDefaults *audiocheck = [NSUserDefaults standardUserDefaults];
        [audiocheck setValue:@"off" forKey:@"audio"];
        NSLog(@"audiocheck :- %@", audiocheck);
        [[NSUserDefaults standardUserDefaults] synchronize];
        
    } else {
        self.btnVolume.accessibilityHint = @"1";
        self.btnVolume.hidden = FALSE;
        self.btnMute.hidden = TRUE;
        [self.playerViewController setVolume:1.0];
        NSUserDefaults *audiocheck = [NSUserDefaults standardUserDefaults];
        [audiocheck setValue:@"on" forKey:@"audio"];
        NSLog(@"audiocheck :- %@", audiocheck);
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}

- (IBAction)btnTextEdit:(id)sender {
    if (self.get_cardid.count == 1) {
        self.demoiCarousel.scrollEnabled = FALSE;
        joinedString = [self.get_cardid componentsJoinedByString:@","];
        
        if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
            AddingTextNoteVC *textnoteVC = [[AddingTextNoteVC alloc] initWithNibName:@"AddingTextNoteVC" bundle:nil];
            textnoteVC.get_cardID = [NSString stringWithFormat:@"%@",cardid];
            textnoteVC.getSaving_ID = joinedString;
            textnoteVC.get_cardid = self.get_cardid;
            [self.navigationController pushViewController:textnoteVC animated:NO];
        } else {
            AddingTextNoteVC *textnoteVC = [[AddingTextNoteVC alloc] initWithNibName:@"AddingTextNotesVC_IPad" bundle:nil];
            textnoteVC.get_cardID = cardid;
            textnoteVC.getSaving_ID = joinedString;
            textnoteVC.get_cardid = self.get_cardid;
            [self.navigationController pushViewController:textnoteVC animated:NO];
        }
        
    } else {
        for (int i = 0; i<self.get_cardid.count; i++) {
            if (self.demoiCarousel.currentItemIndex == i) {
                NSLog(@"value of i :- %@", [self.get_cardid objectAtIndex:i]);
                cardid = [self.get_cardid objectAtIndex:i];
            }
        }
        if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
            AddingTextNoteVC *textnoteVC = [[AddingTextNoteVC alloc] initWithNibName:@"AddingTextNoteVC" bundle:nil];
            textnoteVC.get_cardID = [NSString stringWithFormat:@"%@",cardid];
            textnoteVC.getSaving_ID = cardid;
            textnoteVC.get_cardid = self.get_cardid;
            [self.navigationController pushViewController:textnoteVC animated:NO];
        } else {
            AddingTextNoteVC *textnoteVC = [[AddingTextNoteVC alloc] initWithNibName:@"AddingTextNotesVC_IPad" bundle:nil];
            textnoteVC.get_cardID = cardid;
            textnoteVC.getSaving_ID = cardid;
            textnoteVC.get_cardid = self.get_cardid;
            [self.navigationController pushViewController:textnoteVC animated:NO];
        }
    }
}



- (IBAction)btnVoiceNote:(id)sender {
    isAudio = false;
    timeSec = 0;
    timeMin = 0;
    
    [self StartTimer];
    
    self.viewAudio.hidden = FALSE;
}

- (IBAction)btnCheckAudio:(id)sender {
    [recorder stop];
    AVAudioSession *session = [AVAudioSession sharedInstance];
    int flags = AVAudioSessionSetActiveFlags_NotifyOthersOnDeactivation;
    [session setActive:NO withFlags:flags error:nil];
}

- (IBAction)btnCheck:(id)sender {
    
}

-(void)audioRecorderDidFinishRecording:(AVAudioRecorder *)recorder successfully:(BOOL)flag {
    if (flag) {
        if ([CommanMethods isNetworkRechable]) {
            [self callForSaveAudio:outputFileURL];
        } else {
            [CommanMethods displayAlertMessage:key_Informaction message:kInternetConnectionMessage currentRef:self];
        }
    }
}

- (IBAction)btnAudio:(id)sender {
    
    
}


#pragma mark - TablViewCellAction

-(IBAction)btnCellDelete:(id)sender {
    
    NSIndexPath *indexPath;
    UITableView *tableView;
    
    
    NSDictionary *dictData = [[NSDictionary alloc] init];
    dictData = [arrNotesList objectAtIndex:[sender tag]];
    
    if ([[dictData objectForKey:@"note_type"] isEqual: @"text"] ) {
        TextNotesVC *cell = [tableView dequeueReusableCellWithIdentifier:TextNotesVCID forIndexPath:indexPath];
        cell = [tableView cellForRowAtIndexPath:indexPath];
        
        
        
        UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"UOC" message:@"Are you sure you want to Delete?" preferredStyle:UIAlertControllerStyleAlert];
        
        [actionSheet addAction:[UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
            
            [self dismissViewControllerAnimated:YES completion:^{
            }];
        }]];
        
        
        [actionSheet addAction:[UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            
            NSLog(@" [dictData objectForKey:] :- %@", [dictData objectForKey:@"id"]);
            
            if ([CommanMethods isNetworkRechable]) {
                [self callForDeleteNotes:[dictData objectForKey:@"id"]];
            } else {
                [CommanMethods displayAlertMessage:key_Informaction message:kInternetConnectionMessage currentRef:self];
            }
            
            
        }]];
        
        // Present action sheet.
        [self presentViewController:actionSheet animated:YES completion:nil];
     
    } else if ([[dictData objectForKey:@"note_type"] isEqual: @"audio"] ) {
        
        AudioNotesVC *cell = [tableView dequeueReusableCellWithIdentifier:AudioNotesVCID forIndexPath:indexPath];
        cell = [tableView cellForRowAtIndexPath:indexPath];
        
        UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"UOC" message:@"Are you sure you want to Delete?" preferredStyle:UIAlertControllerStyleAlert];
        
        [actionSheet addAction:[UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
            
            [self dismissViewControllerAnimated:YES completion:^{
            }];
        }]];
        
        
        [actionSheet addAction:[UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            
            NSLog(@" [dictData objectForKey:] :- %@", [dictData objectForKey:@"id"]);
            
           /* if ([CommanMethods isNetworkRechable]) {
                [self callForDeleteNotes:[dictData objectForKey:@"id"]];
            } else {
                [CommanMethods displayAlertMessage:key_Informaction message:kInternetConnectionMessage currentRef:self];
            }*/
            
            
        }]];
        
        // Present action sheet.
        [self presentViewController:actionSheet animated:YES completion:nil];
        
    }

    
}

-(IBAction)btnCellEdit:(id)sender {
    
    NSIndexPath *indexPath;
    UITableView *tableView;
    
    NSDictionary *dictData = [[NSDictionary alloc] init];
    dictData = [arrNotesList objectAtIndex:[sender tag]];
    
    if ([[dictData objectForKey:@"note_type"] isEqual: @"text"] ) {
        
        if (self.get_cardid.count == 1) {
            self.demoiCarousel.scrollEnabled = FALSE;
            joinedString = [self.get_cardid componentsJoinedByString:@","];
            
            if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
                AddingTextNoteVC *textnoteVC = [[AddingTextNoteVC alloc] initWithNibName:@"AddingTextNoteVC" bundle:nil];
                textnoteVC.get_cardID = [NSString stringWithFormat:@"%@",cardid];
                textnoteVC.getSaving_ID = joinedString;
                textnoteVC.get_cardid = self.get_cardid;
                textnoteVC.pass = @"1";
                textnoteVC.message = [dictData objectForKey:@"message"];
                [self.navigationController pushViewController:textnoteVC animated:YES];
            } else {
                AddingTextNoteVC *textnoteVC = [[AddingTextNoteVC alloc] initWithNibName:@"AddingTextNotesVC_IPad" bundle:nil];
                textnoteVC.get_cardID = cardid;
                textnoteVC.getSaving_ID = joinedString;
                textnoteVC.get_cardid = self.get_cardid;
                textnoteVC.pass = @"1";
                textnoteVC.message = [dictData objectForKey:@"message"];
                [self.navigationController pushViewController:textnoteVC animated:YES];
            }
            
        } else {
            for (int i = 0; i<self.get_cardid.count; i++) {
                if (self.demoiCarousel.currentItemIndex == i) {
                    NSLog(@"value of i :- %@", [self.get_cardid objectAtIndex:i]);
                    cardid = [self.get_cardid objectAtIndex:i];
                }
            }
            if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
                AddingTextNoteVC *textnoteVC = [[AddingTextNoteVC alloc] initWithNibName:@"AddingTextNoteVC" bundle:nil];
                textnoteVC.get_cardID = [NSString stringWithFormat:@"%@",cardid];
                textnoteVC.getSaving_ID = cardid;
                textnoteVC.get_cardid = self.get_cardid;
                textnoteVC.pass = @"1";
                textnoteVC.message = [dictData objectForKey:@"message"];
                [self.navigationController pushViewController:textnoteVC animated:YES];
            } else {
                AddingTextNoteVC *textnoteVC = [[AddingTextNoteVC alloc] initWithNibName:@"AddingTextNotesVC_IPad" bundle:nil];
                textnoteVC.get_cardID = cardid;
                textnoteVC.getSaving_ID = cardid;
                textnoteVC.get_cardid = self.get_cardid;
                textnoteVC.pass = @"1";
                textnoteVC.message = [dictData objectForKey:@"message"];
                [self.navigationController pushViewController:textnoteVC animated:YES];
            }
            
        }
        
        
    }
}

-(IBAction)btnCellMoreAction:(id)sender {
    NSIndexPath *indexPath;
    UITableView *tableView;
    
    NSDictionary *dictData = [[NSDictionary alloc] init];
    dictData = [arrNotesList objectAtIndex:[sender tag]];
    
    if ([[dictData objectForKey:@"note_type"] isEqual: @"text"] ) {
        
        TextNotesVC *cell = [tableView dequeueReusableCellWithIdentifier:TextNotesVCID forIndexPath:indexPath];
        cell = [tableView cellForRowAtIndexPath:indexPath];
        
        value = [sender tag];
        
        NSLog(@" value :- %d", value);
        [self.tblNotesDetails reloadData];
    }
}

-(IBAction)btnCellPlay:(id)sender {
    NSIndexPath *indexPath;
    UITableView *tableView;
    
    NSDictionary *dictData = [[NSDictionary alloc] init];
    dictData = [arrNotesList objectAtIndex:[sender tag]];
    
    AudioNotesVC *cell = [tableView dequeueReusableCellWithIdentifier:AudioNotesVCID forIndexPath:indexPath];
    cell = [tableView cellForRowAtIndexPath:indexPath];
    
    NSURL *soundFileURL = [NSURL URLWithString:[dictData objectForKey:@"message"]];
    
    NSLog(@" value :- %@", [dictData objectForKey:@"message"]);
    NSLog(@" soundFileURL :- %@", soundFileURL);
    
    player = [[AVAudioPlayer alloc] initWithContentsOfURL:soundFileURL error:nil];
    //player.numberOfLoops = 0;
    
    player.delegate = self;
    [player prepareToPlay];
    player.volume = 10.0f;
    //[player play];
    
    timer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateTime:) userInfo:nil repeats:YES];
    
    cell.btnPause.hidden = FALSE;
    cell.btnPlay.hidden = TRUE;
    
}

- (void)updateTime:(NSTimer *)timer
{
    NSIndexPath *indexPath;
    UITableView *tableView;
    AudioNotesVC *cell = [tableView dequeueReusableCellWithIdentifier:AudioNotesVCID forIndexPath:indexPath];
    cell = [tableView cellForRowAtIndexPath:indexPath];
    cell.AudioPlaying.value = floorf(player.currentTime);
    cell.lblcurrent.text = [NSString stringWithFormat:@"%f",player.currentTime];
}

-(IBAction)btnCellPause:(id)sender {
    
    NSIndexPath *indexPath;
    UITableView *tableView;
    
    NSDictionary *dictData = [[NSDictionary alloc] init];
    dictData = [arrNotesList objectAtIndex:[sender tag]];
    
    AudioNotesVC *cell = [tableView dequeueReusableCellWithIdentifier:AudioNotesVCID forIndexPath:indexPath];
    cell = [tableView cellForRowAtIndexPath:indexPath];
    
    cell.btnPause.hidden = TRUE;
    cell.btnPlay.hidden = FALSE;
    [player pause];
}




#pragma mark- TableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return arrNotesList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSDictionary *dictData = [[NSDictionary alloc] init];
    dictData = [arrNotesList objectAtIndex:indexPath.row];
    
    if ([[dictData objectForKey:@"note_type"] isEqual: @"audio"] ) {
        
        AudioNotesVC *cell = [tableView dequeueReusableCellWithIdentifier:AudioNotesVCID forIndexPath:indexPath];
        
        cell.lblDate.text = [dictData objectForKey:@"add_date"];
        
        cell.lbltotal.text = [NSString stringWithFormat:@"%f",player.duration];
        cell.lblcurrent.text = @"0.0";
        cell.AudioPlaying.value = 0.0;
       
        cell.btnPlay.tag = indexPath.row;
        cell.btnPause.tag = indexPath.row;
        cell.btnDelete.tag = indexPath.row;
        
        
        
        [cell.btnDelete addTarget:self action:@selector(btnCellDelete:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btnPlay addTarget:self action:@selector(btnCellPlay:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btnPause addTarget:self action:@selector(btnCellPause:) forControlEvents:UIControlEventTouchUpInside];
        
        return cell;
    }
    
    else
    {
        TextNotesVC *cell = [tableView dequeueReusableCellWithIdentifier:TextNotesVCID forIndexPath:indexPath];
        
        cell.viewOtion.layer.borderColor = UIColor.clearColor.CGColor;
        cell.viewOtion.layer.borderWidth = 1.0;
        cell.viewOtion.layer.cornerRadius = 8;
        
        
        cell.lblNotes.text = [dictData objectForKey:@"message"];
        
        cell.lblDate.text = [dictData objectForKey:@"add_date"];
        
        cell.btnEdit.tag = indexPath.row;
        cell.btnDelete.tag = indexPath.row;
        cell.btnMore.tag = indexPath.row;
        //cell.viewOtion.hidden = TRUE;
        
        [cell.btnDelete addTarget:self action:@selector(btnCellDelete:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btnEdit addTarget:self action:@selector(btnCellEdit:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btnMore addTarget:self action:@selector(btnCellMoreAction:) forControlEvents:UIControlEventTouchUpInside];
        
        if (indexPath.row == value )
        {
            cell.viewOtion.hidden = FALSE;
        }
        else
        {
            cell.viewOtion.hidden = TRUE;
        }
        
        return cell;
    }
    
    
    
}

#pragma mark- TableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
}


#pragma CallAPI

-(void)callForGetNotesList:(NSString*)card  {
    NSMutableDictionary *dcitParams = [[NSMutableDictionary alloc] init];
    [dcitParams setValue:card forKey:@"card_id"];
    [dcitParams setValue:self.get_readingid forKey:@"save_reading_id"];
    
    //NSLog(@" dcitParams :-%@", dcitParams);
    
    [AppDelegate addVwLoading];
    [CommunicationHandler callForGetNotesList:dcitParams Sucess:^(NSDictionary *dict, NSURLResponse *response) {
        //NSLog(@"dict :- %@", dict);
        dispatch_async(dispatch_get_main_queue(), ^{
            [AppDelegate removeVwLoading];
            if ([[dict objectForKey:@"error"] boolValue] == false) {
                NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
                [userDefaults setObject:dict forKey:@"spreaddata"];
                arrNotesList = [[NSMutableArray alloc] initWithArray:[dict valueForKey:@"data"]];
                NSLog(@"arrJournalList :- %@", arrNotesList);
                [self.tblNotesDetails reloadData];
                
            } else {
                [CommanMethods displayAlertMessage:@"UOC" message:[dict objectForKey:@"message"] currentRef:self];
            }
        });
    } Error:^(NSError *error, NSURLResponse *response) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [AppDelegate removeVwLoading];
        });
    }];
}

-(void)callForDeleteNotes:(NSString*)getid  {
    NSMutableDictionary *dcitParams = [[NSMutableDictionary alloc] init];
    [dcitParams setValue:getid forKey:@"id"];
    NSLog(@" dcitParams :-%@", dcitParams);
    
    [AppDelegate addVwLoading];
    [CommunicationHandler callForDeleteNotes:dcitParams Sucess:^(NSDictionary *dict, NSURLResponse *response) {
        //NSLog(@"dict :- %@", dict);
        dispatch_async(dispatch_get_main_queue(), ^{
            [AppDelegate removeVwLoading];
            if ([[dict objectForKey:@"error"] boolValue] == false) {
                [CommanMethods displayAlertMessageWithCallBack:nil message:[dict objectForKey:@"message"] refrence:self Success:^{
                    value = -1;
                    if (self.get_cardid.count == 1) {
                        self.demoiCarousel.scrollEnabled = FALSE;
                        joinedString = [self.get_cardid componentsJoinedByString:@","];
                        if ([CommanMethods isNetworkRechable]) {
                            [self callForGetNotesList:joinedString];
                        } else {
                            [CommanMethods displayAlertMessage:key_Informaction message:kInternetConnectionMessage currentRef:self];
                        }
                    } else {
                        for (int i = 0; i<self.get_cardid.count; i++) {
                            if (self.demoiCarousel.currentItemIndex == i) {
                                NSLog(@"value of i :- %@", [self.get_cardid objectAtIndex:i]);
                                cardid = [self.get_cardid objectAtIndex:i];
                            }
                        }
                        if ([CommanMethods isNetworkRechable]) {
                            [self callForGetNotesList:cardid];
                        } else {
                            [CommanMethods displayAlertMessage:key_Informaction message:kInternetConnectionMessage currentRef:self];
                        }
                    }
                }];
                
            } else {
                [CommanMethods displayAlertMessage:@"UOC" message:[dict objectForKey:@"message"] currentRef:self];
            }
        });
    } Error:^(NSError *error, NSURLResponse *response) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [AppDelegate removeVwLoading];
        });
    }];
}


-(void)callForSaveAudio:(NSURL *)fileUrl  {
    
    NSData *fileData = [[NSData alloc] initWithContentsOfURL:fileUrl];
    
    joinedString = [self.get_cardid componentsJoinedByString:@","];
    NSMutableDictionary *dcitParams = [[NSMutableDictionary alloc] init];
    [dcitParams setValue:self.get_readingid forKey:@"save_reading_id"];
    [dcitParams setValue:joinedString forKey:@"card_id"];
    [dcitParams setValue:@"audio" forKey:@"note_type"];
    [dcitParams setValue:@"" forKey:@"message"];
    [dcitParams setValue:@"test" forKey:@"save_name"];
    [dcitParams setValue:fileData forKey:@"recording"];
    /*if (fileData) {
     [dcitParams setValue:fileData forKey:@"recording"];
     }*/
    
    NSLog(@" dcitParams :-%@", dcitParams);
    
    
    [AppDelegate addVwLoading];
    [CommunicationHandler callForSaveAudio:dcitParams Sucess:^(NSDictionary *dict, NSURLResponse *response) {
        NSLog(@"%@", dict);
        dispatch_async(dispatch_get_main_queue(), ^{
            [AppDelegate removeVwLoading];
            if ([[dict objectForKey:@"error"] boolValue] == false) {
                [CommanMethods displayAlertMessageWithCallBack:nil message:[dict objectForKey:@"message"] refrence:self Success:^{
                    value = -1;
                     self.viewAudio.hidden = TRUE;
                    if (self.get_cardid.count == 1) {
                        self.demoiCarousel.scrollEnabled = FALSE;
                        joinedString = [self.get_cardid componentsJoinedByString:@","];
                        if ([CommanMethods isNetworkRechable]) {
                            [self callForGetNotesList:joinedString];
                        } else {
                            [CommanMethods displayAlertMessage:key_Informaction message:kInternetConnectionMessage currentRef:self];
                        }
                    } else {
                        for (int i = 0; i<self.get_cardid.count; i++) {
                            if (self.demoiCarousel.currentItemIndex == i) {
                                NSLog(@"value of i :- %@", [self.get_cardid objectAtIndex:i]);
                                cardid = [self.get_cardid objectAtIndex:i];
                            }
                        }
                        if ([CommanMethods isNetworkRechable]) {
                            [self callForGetNotesList:cardid];
                        } else {
                            [CommanMethods displayAlertMessage:key_Informaction message:kInternetConnectionMessage currentRef:self];
                        }
                    }
                }];
            } else {
                [CommanMethods displayAlertMessage:@"UOC" message:[dict objectForKey:@"message"] currentRef:self];
            }
        });
    } Error:^(NSError *error, NSURLResponse *response) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [AppDelegate removeVwLoading];
        });
    }];
}

-(NSString *) randomStringWithLength: (int) len {
    NSString *letters = @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    
    NSMutableString *randomString = [NSMutableString stringWithCapacity: len];
    
    for (int i=0; i<len; i++) {
        [randomString appendFormat: @"%C", [letters characterAtIndex: arc4random_uniform([letters length])]];
    }
    
    return randomString;
}

@end
