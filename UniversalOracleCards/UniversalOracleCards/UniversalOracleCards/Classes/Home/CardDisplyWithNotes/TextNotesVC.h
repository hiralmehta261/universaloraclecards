//
//  TextNotesVC.h
//  UniversalOracleCards
//
//  Created by whiznic on 08/04/19.
//  Copyright © 2019 whiznic. All rights reserved.
//

#import <UIKit/UIKit.h>
#define TextNotesVCID @"TextNotesVCID"


@interface TextNotesVC : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblNotes;
@property (weak, nonatomic) IBOutlet UIView *viewOtion;
@property (weak, nonatomic) IBOutlet UIButton *btnEdit;
@property (weak, nonatomic) IBOutlet UIButton *btnMore;
@property (weak, nonatomic) IBOutlet UILabel *lblDate;
@property (weak, nonatomic) IBOutlet UIButton *btnDelete;
@end


