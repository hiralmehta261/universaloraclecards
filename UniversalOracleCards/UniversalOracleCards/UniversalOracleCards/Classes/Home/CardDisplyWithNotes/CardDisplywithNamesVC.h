//
//  CardDisplywithNamesVC.h
//  UniversalOracleCards
//
//  Created by whiznic on 15/03/19.
//  Copyright © 2019 whiznic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <AVKit/AVKit.h>
#import "iCarousel.h"

@interface CardDisplywithNamesVC : UIViewController <iCarouselDataSource, iCarouselDelegate, UITextFieldDelegate, UITableViewDelegate,UITableViewDataSource>

@property (strong, nonatomic) IBOutlet UIImageView *imgCard;
@property (strong, nonatomic) IBOutlet iCarousel *demoiCarousel;
@property (nonatomic) AVPlayer *playerViewController;
@property (strong, nonatomic) IBOutlet UIView *viewDisply;
@property (strong, nonatomic) IBOutlet UITableView *tblNotesDetails;
@property (strong, nonatomic) IBOutlet UIView *viewAudio;
@property (strong, nonatomic) IBOutlet UILabel *lblAudioRecording;
@property (strong, nonatomic) IBOutlet UIView *viewVideo;
@property (weak, nonatomic) IBOutlet UIButton *btnMute;
@property (weak, nonatomic) IBOutlet UIButton *btnVolume;
@property (strong, nonatomic) NSMutableArray *get_cardid;
@property (strong, nonatomic) NSString *get_readingid,*get_Selected_Value;
@end

