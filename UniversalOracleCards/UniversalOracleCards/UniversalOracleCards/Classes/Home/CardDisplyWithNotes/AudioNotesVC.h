//
//  AudioNotesVC.h
//  UniversalOracleCards
//
//  Created by whiznic on 09/04/19.
//  Copyright © 2019 whiznic. All rights reserved.
//

#import <UIKit/UIKit.h>

#define AudioNotesVCID @"AudioNotesVCID"

@interface AudioNotesVC : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *btnPlay;
@property (weak, nonatomic) IBOutlet UIButton *btnPause;
@property (weak, nonatomic) IBOutlet UILabel *lblDate;
@property (weak, nonatomic) IBOutlet UIButton *btnDelete;
@property (weak, nonatomic) IBOutlet UILabel *lblcurrent;
@property (weak, nonatomic) IBOutlet UISlider *AudioPlaying;
@property (weak, nonatomic) IBOutlet UILabel *lbltotal;

@end


