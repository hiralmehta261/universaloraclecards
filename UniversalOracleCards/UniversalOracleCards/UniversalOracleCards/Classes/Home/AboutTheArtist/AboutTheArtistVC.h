//
//  AboutTheArtistVC.h
//  UniversalOracleCards
//
//  Created by whiznic on 07/03/19.
//  Copyright © 2019 whiznic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <AVKit/AVKit.h>


@interface AboutTheArtistVC : UIViewController
@property (strong, nonatomic) IBOutlet UILabel *lblline;
@property (nonatomic) AVPlayer *playerViewController;
@property (nonatomic) AVPlayerLayer *player;
@property (strong, nonatomic) IBOutlet UIView *viewDisply;
@end


