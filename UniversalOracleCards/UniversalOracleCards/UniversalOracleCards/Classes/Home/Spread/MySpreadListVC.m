//
//  MySpreadListVC.m
//  UniversalOracleCards
//
//  Created by whiznic on 13/03/19.
//  Copyright © 2019 whiznic. All rights reserved.
//

#import "MySpreadListVC.h"
#import "CreateCustomSpreadVC.h"
#import "HomeVC.h"
#import "MySpreadListVCCell.h"
#import "CardSelectionVC.h"
#import <AudioToolbox/AudioServices.h>
#import "SharkfoodMuteSwitchDetector.h"
#import "AppDelegate.h"
#import "CommanMethods.h"
#import "CommunicationHandler.h"
@import MediaPlayer;

@interface MySpreadListVC ()
{
     NSMutableArray *arrSpreadList;
     int value;
}
@property (nonatomic,strong) SharkfoodMuteSwitchDetector* detector;
@end

@implementation MySpreadListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
   /* self.detector = [SharkfoodMuteSwitchDetector shared];
    __weak MySpreadListVC* sself = self;
    self.detector.silentNotify = ^(BOOL silent){
        if (silent) {
            [sself.playerViewController setVolume:0.0];
            sself.btnMute.hidden = FALSE;
            sself.btnVolume.hidden = TRUE;
            NSLog(@"device is silent");
        } else {
            [sself.playerViewController setVolume:1.0];
            sself.btnMute.hidden = TRUE;
            sself.btnVolume.hidden = FALSE;
            NSLog(@"device is in ringing mode");
        }
    };*/


    
    self.tblSpreadList.dataSource = self;
    self.tblSpreadList.delegate = self;
    
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
        UINib *cellnib = [UINib nibWithNibName:@"MySpreadListVCCell" bundle:nil];
        [self.tblSpreadList registerNib:cellnib forCellReuseIdentifier:MySpreadListVCCellID];
    } else {
        UINib *cellnib = [UINib nibWithNibName:@"MySpreadListVCCell_iPad" bundle:nil];
        [self.tblSpreadList registerNib:cellnib forCellReuseIdentifier:MySpreadListVCCellID];
        
    }
    
    value = -1;
    
    if ([CommanMethods isNetworkRechable]) {
        [self callForGetMySpreadList];
    } else {
        [CommanMethods displayAlertMessage:key_Informaction message:kInternetConnectionMessage currentRef:self];
    }
   
    
    
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    NSString *stringVideoName = @"stars_animated.mp4";
    NSString *filepath = [[NSBundle mainBundle] pathForResource:stringVideoName ofType:nil];
    NSLog(@" filepath :- %@", filepath);
    NSURL *fileURL = [NSURL fileURLWithPath:filepath];
    self.playerViewController = [AVPlayer playerWithURL:fileURL];
    self.playerViewController.actionAtItemEnd = AVPlayerActionAtItemEndNone;
    AVPlayerLayer *videoLayer = [AVPlayerLayer playerLayerWithPlayer:self.playerViewController];
    videoLayer.frame = self.view.bounds;
    videoLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
    [self.view.layer addSublayer:videoLayer];
    
    [self.playerViewController play];
    
    NSString *savedValue = [[NSUserDefaults standardUserDefaults]
                            stringForKey:@"audio"];
    if([savedValue isEqualToString:@"off"]) {
        self.btnMute.hidden = FALSE;
        self.btnVolume.hidden = TRUE;
        [self.playerViewController setVolume:0.0];
    } else {
        self.btnMute.hidden = TRUE;
        self.btnVolume.hidden = FALSE;
        [self.playerViewController setVolume:1.0];
    }
    
    NSLog(@" savedValue :- %@", savedValue);
    
    self.viewDisply.layer.zPosition = 1;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(itemDidFinishPlaying:) name:AVPlayerItemDidPlayToEndTimeNotification object:[self.playerViewController currentItem]];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationDidBecomeActive:) name:UIApplicationDidEnterBackgroundNotification object:nil];
    
    [[UIApplication sharedApplication] beginReceivingRemoteControlEvents];
    [self becomeFirstResponder];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [self.playerViewController pause];
    [super viewWillDisappear:animated];
    [[UIApplication sharedApplication] endReceivingRemoteControlEvents];
    [self resignFirstResponder];
}


- (void)remoteControlReceivedWithEvent:(UIEvent *)event {
    switch (event.subtype) {
        case UIEventSubtypeRemoteControlTogglePlayPause:
            if([self.playerViewController rate] == 0){
                [self.playerViewController play];
            } else {
                [self.playerViewController pause];
            }
            break;
        case UIEventSubtypeRemoteControlPlay:
            [self.playerViewController play];
            break;
        case UIEventSubtypeRemoteControlPause:
            [self.playerViewController pause];
            break;
        default:
            break;
    }
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    self.detector = [SharkfoodMuteSwitchDetector shared];
    __weak MySpreadListVC* sself = self;
    self.detector.silentNotify = ^(BOOL silent){
        if (silent) {
            
            NSLog(@"device is silent");
        } else {
            
            NSLog(@"device is in ringing mode");
            [self.playerViewController performSelector:@selector(play) withObject:nil afterDelay:0.01];
        }
    };
}


- (void)itemDidFinishPlaying:(NSNotification *)notification {
    AVPlayerItem *player = [notification object];
    [player seekToTime:kCMTimeZero];
}

- (IBAction)btnBack:(id)sender {
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
        HomeVC *homeVC = [[HomeVC alloc] initWithNibName:@"HomeVC" bundle:nil];
        [self.navigationController pushViewController:homeVC animated:YES];
    } else {
        HomeVC *homeVC = [[HomeVC alloc] initWithNibName:@"HomeVC_IPad" bundle:nil];
        [self.navigationController pushViewController:homeVC animated:YES];
    }
}

- (IBAction)btnCreateNewSpread:(id)sender {
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
        CreateCustomSpreadVC *createspreadVC = [[CreateCustomSpreadVC alloc] initWithNibName:@"CreateCustomSpreadVC" bundle:nil];
        [self.navigationController pushViewController:createspreadVC animated:YES];
    } else {
        CreateCustomSpreadVC *createspreadVC = [[CreateCustomSpreadVC alloc] initWithNibName:@"CreateCustomSpreadVC_IPad" bundle:nil];
        [self.navigationController pushViewController:createspreadVC animated:YES];
    }
}

- (IBAction)btnVolume:(id)sender {
    
    
    if ([self.btnVolume.accessibilityHint isEqualToString:@"1"]) {
        self.btnVolume.accessibilityHint = @"0";
        self.btnVolume.hidden = TRUE;
        self.btnMute.hidden = FALSE;
        [self.playerViewController setVolume:0.0];
        NSUserDefaults *audiocheck = [NSUserDefaults standardUserDefaults];
        [audiocheck setValue:@"off" forKey:@"audio"];
        NSLog(@"audiocheck :- %@", audiocheck);
        [[NSUserDefaults standardUserDefaults] synchronize];
        
    } else {
        self.btnVolume.accessibilityHint = @"1";
        self.btnVolume.hidden = FALSE;
        self.btnMute.hidden = TRUE;
        [self.playerViewController setVolume:1.0];
        NSUserDefaults *audiocheck = [NSUserDefaults standardUserDefaults];
        [audiocheck setValue:@"on" forKey:@"audio"];
        NSLog(@"audiocheck :- %@", audiocheck);
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}

#pragma mark - TablViewCellAction

-(IBAction)btnCellDelete:(id)sender {
    
    NSIndexPath *indexPath;
    UITableView *tableView;
    
    MySpreadListVCCell *cell = [tableView dequeueReusableCellWithIdentifier:MySpreadListVCCellID forIndexPath:indexPath];
    cell = [tableView cellForRowAtIndexPath:indexPath];
    
  

    NSDictionary *dictData = [[NSDictionary alloc] init];
    dictData = [arrSpreadList objectAtIndex:[sender tag]];
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"UOC" message:@"Are you sure you want to Delete?" preferredStyle:UIAlertControllerStyleAlert];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
        [self dismissViewControllerAnimated:YES completion:^{
        }];
    }]];
    
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        if ([CommanMethods isNetworkRechable]) {
            [self callForDeleteSpread:[dictData objectForKey:@"id"]];
        } else {
            [CommanMethods displayAlertMessage:key_Informaction message:kInternetConnectionMessage currentRef:self];
        }
        
        
    }]];
    
    // Present action sheet.
    [self presentViewController:actionSheet animated:YES completion:nil];

    
    NSLog(@" Delete row :- %@", [arrSpreadList objectAtIndex:[sender tag]]);
}

-(IBAction)btnCellEdit:(id)sender {
    
    NSIndexPath *indexPath;
    UITableView *tableView;
    
    MySpreadListVCCell *cell = [tableView dequeueReusableCellWithIdentifier:MySpreadListVCCellID forIndexPath:indexPath];
    cell = [tableView cellForRowAtIndexPath:indexPath];
    
    NSDictionary *dictData = [[NSDictionary alloc] init];
    dictData = [arrSpreadList objectAtIndex:[sender tag]];
    
    if ([[dictData objectForKey:@"spread_type"] isEqual: @"2x2"]) {
        if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
            CreateCustomSpreadVC *dailyVC = [[CreateCustomSpreadVC alloc] initWithNibName:@"CreateCustomSpreadVC" bundle:nil];
            dailyVC.gridnumber = @"2";
            dailyVC.singleGridDate = dictData;
            dailyVC.value1 = @"1";
            [self.navigationController pushViewController:dailyVC animated:YES];
        } else {
            CreateCustomSpreadVC *dailyVC = [[CreateCustomSpreadVC alloc] initWithNibName:@"CreateCustomSpreadVC_IPad" bundle:nil];
            dailyVC.gridnumber = @"2";
            dailyVC.singleGridDate = dictData;
            dailyVC.value1 = @"1";
            [self.navigationController pushViewController:dailyVC animated:YES];
        }
    } else if ([[dictData objectForKey:@"spread_type"] isEqual: @"3x3"]) {
        if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
            CreateCustomSpreadVC *dailyVC = [[CreateCustomSpreadVC alloc] initWithNibName:@"CreateCustomSpreadVC" bundle:nil];
            dailyVC.gridnumber = @"3";
            dailyVC.singleGridDate = dictData;
            dailyVC.value1 = @"1";
            [self.navigationController pushViewController:dailyVC animated:YES];
        } else {
            CreateCustomSpreadVC *dailyVC = [[CreateCustomSpreadVC alloc] initWithNibName:@"CreateCustomSpreadVC_IPad" bundle:nil];
            dailyVC.gridnumber = @"3";
            dailyVC.singleGridDate = dictData;
            dailyVC.value1 = @"1";
            [self.navigationController pushViewController:dailyVC animated:YES];
        }
    } else if ([[dictData objectForKey:@"spread_type"] isEqual: @"4x4"]) {
        if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
            CreateCustomSpreadVC *dailyVC = [[CreateCustomSpreadVC alloc] initWithNibName:@"CreateCustomSpreadVC" bundle:nil];
            dailyVC.gridnumber = @"4";
            dailyVC.singleGridDate = dictData;
            dailyVC.value1 = @"1";
            [self.navigationController pushViewController:dailyVC animated:YES];
        } else {
            CreateCustomSpreadVC *dailyVC = [[CreateCustomSpreadVC alloc] initWithNibName:@"CreateCustomSpreadVC_IPad" bundle:nil];
            dailyVC.gridnumber = @"4";
            dailyVC.singleGridDate = dictData;
            dailyVC.value1 = @"1";
            [self.navigationController pushViewController:dailyVC animated:YES];
        }
    } else if ([[dictData objectForKey:@"spread_type"] isEqual: @"5x5"]) {
        if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
            CreateCustomSpreadVC *dailyVC = [[CreateCustomSpreadVC alloc] initWithNibName:@"CreateCustomSpreadVC" bundle:nil];
            dailyVC.gridnumber = @"5";
            dailyVC.singleGridDate = dictData;
            dailyVC.value1 = @"1";
            [self.navigationController pushViewController:dailyVC animated:YES];
        } else {
            CreateCustomSpreadVC *dailyVC = [[CreateCustomSpreadVC alloc] initWithNibName:@"CreateCustomSpreadVC_IPad" bundle:nil];
            dailyVC.gridnumber = @"5";
            dailyVC.singleGridDate = dictData;
            dailyVC.value1 = @"1";
            [self.navigationController pushViewController:dailyVC animated:YES];
        }
    } else if ([[dictData objectForKey:@"spread_type"] isEqual: @"6x6"]) {
        if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
            CreateCustomSpreadVC *dailyVC = [[CreateCustomSpreadVC alloc] initWithNibName:@"CreateCustomSpreadVC" bundle:nil];
            dailyVC.gridnumber = @"6";
            dailyVC.singleGridDate = dictData;
            dailyVC.value1 = @"1";
            [self.navigationController pushViewController:dailyVC animated:YES];
        } else {
            CreateCustomSpreadVC *dailyVC = [[CreateCustomSpreadVC alloc] initWithNibName:@"CreateCustomSpreadVC_IPad" bundle:nil];
            dailyVC.gridnumber = @"6";
            dailyVC.singleGridDate = dictData;
            dailyVC.value1 = @"1";
            [self.navigationController pushViewController:dailyVC animated:YES];
        }
    }
    
    //NSDictionary *dictData = [[NSDictionary alloc] init];
    //dictData = [arrOnlineList objectAtIndex:[sender tag]];
    
    NSLog(@" Edit row :- %@", [arrSpreadList objectAtIndex:[sender tag]]);
}

-(IBAction)btnCellMoreAction:(id)sender {
    NSIndexPath *indexPath;
    UITableView *tableView;
    
    MySpreadListVCCell *cell = [tableView dequeueReusableCellWithIdentifier:MySpreadListVCCellID forIndexPath:indexPath];
    cell = [tableView cellForRowAtIndexPath:indexPath];
    
    value = [sender tag];
    NSLog(@" value :- %d", value);
    [self.tblSpreadList reloadData];
}


#pragma mark- TableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return arrSpreadList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    MySpreadListVCCell *cell = [tableView dequeueReusableCellWithIdentifier:MySpreadListVCCellID forIndexPath:indexPath];
    
    cell.ViewMoreAction.layer.borderColor = UIColor.clearColor.CGColor;
    cell.ViewMoreAction.layer.borderWidth = 1.0;
    cell.ViewMoreAction.layer.cornerRadius = 8;
    
    cell.btnMore.tag = indexPath.row;
    cell.btnEdit.tag = indexPath.row;
    cell.btnDelete.tag = indexPath.row;
    //cell.ViewMoreAction.hidden = FALSE;
    
    NSDictionary *dictData = [[NSDictionary alloc] init];
    dictData = [arrSpreadList objectAtIndex:indexPath.row];
    
     cell.lblSpreadName.text = [dictData objectForKey:@"spread_name"];
    NSMutableArray *totalcards = [[NSMutableArray alloc]init];
    totalcards = [dictData objectForKey:@"card_position"];
    int total = totalcards.count;
    NSString *str_name = [NSString stringWithFormat:@"%i", total];
    NSString *str1 = [[str_name stringByAppendingString:@" "] stringByAppendingString:@"Cards "];
    cell.lblTotalCards.text = str1;
    [cell.btnDelete addTarget:self action:@selector(btnCellDelete:) forControlEvents:UIControlEventTouchUpInside];
    [cell.btnEdit addTarget:self action:@selector(btnCellEdit:) forControlEvents:UIControlEventTouchUpInside];
    [cell.btnMore addTarget:self action:@selector(btnCellMoreAction:) forControlEvents:UIControlEventTouchUpInside];
   
   
    if (indexPath.row == value )
    {
        cell.ViewMoreAction.hidden = FALSE;
    }
    else
    {
        cell.ViewMoreAction.hidden = TRUE;
    }

    
    return cell;
}

#pragma mark- TableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
        return 140;
    } else {
        return 175;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    value = -1;
    [self.tblSpreadList reloadData];
    NSDictionary *dictData = [[NSDictionary alloc] init];
    dictData = [arrSpreadList objectAtIndex:[indexPath row]];
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
        CardSelectionVC *CardselectionVC = [[CardSelectionVC alloc] initWithNibName:@"CardSelectionVC" bundle:nil];
        CardselectionVC.get_Selected_Value = [dictData objectForKey:@"spread_type"];
        CardselectionVC.getToatalCards = [dictData objectForKey:@"card_position"];
        CardselectionVC.spread_id = [dictData objectForKey:@"id"];
        CardselectionVC.spread_Name = [dictData objectForKey:@"spread_name"];
        CardselectionVC.getCard_Title = [dictData objectForKey:@"card_name"];
        [self.navigationController pushViewController:CardselectionVC animated:YES];
    } else {
        CardSelectionVC *CardselectionVC = [[CardSelectionVC alloc] initWithNibName:@"CardSelectionVC_IPad" bundle:nil];
        CardselectionVC.get_Selected_Value = [dictData objectForKey:@"spread_type"];
        CardselectionVC.getToatalCards = [dictData objectForKey:@"card_position"];
        CardselectionVC.spread_id = [dictData objectForKey:@"id"];
        CardselectionVC.spread_Name = [dictData objectForKey:@"spread_name"];
        CardselectionVC.getCard_Title = [dictData objectForKey:@"card_name"];
        [self.navigationController pushViewController:CardselectionVC animated:YES];
    }
}


#pragma CallAPI

-(void)callForGetMySpreadList  {
    NSMutableDictionary *dcitParams = [[NSMutableDictionary alloc] init];
    
    //NSLog(@" dcitParams :-%@", dcitParams);
    
    [AppDelegate addVwLoading];
    [CommunicationHandler callForGetMySpreadList:dcitParams Sucess:^(NSDictionary *dict, NSURLResponse *response) {
        //NSLog(@"dict :- %@", dict);
        dispatch_async(dispatch_get_main_queue(), ^{
            [AppDelegate removeVwLoading];
            if ([[dict objectForKey:@"error"] boolValue] == false) {
                NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
                [userDefaults setObject:dict forKey:@"spreaddata"];
                arrSpreadList = [[NSMutableArray alloc] initWithArray:[dict valueForKey:@"data"]];
                NSLog(@"arrJournalList :- %@", arrSpreadList);
                [self.tblSpreadList reloadData];
                value = -1;
                
            } else {
                [CommanMethods displayAlertMessage:@"UOC" message:[dict objectForKey:@"message"] currentRef:self];
            }
        });
    } Error:^(NSError *error, NSURLResponse *response) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [AppDelegate removeVwLoading];
        });
    }];
}

-(void)callForDeleteSpread:(NSString*)getid  {
    NSMutableDictionary *dcitParams = [[NSMutableDictionary alloc] init];
    [dcitParams setValue:getid forKey:@"id"];
    //NSLog(@" dcitParams :-%@", dcitParams);
    
    [AppDelegate addVwLoading];
    [CommunicationHandler callForDeleteSpread:dcitParams Sucess:^(NSDictionary *dict, NSURLResponse *response) {
        //NSLog(@"dict :- %@", dict);
        dispatch_async(dispatch_get_main_queue(), ^{
            [AppDelegate removeVwLoading];
            if ([[dict objectForKey:@"error"] boolValue] == false) {
                [CommanMethods displayAlertMessageWithCallBack:nil message:[dict objectForKey:@"message"] refrence:self Success:^{
                    [self callForGetMySpreadList];
                }];
                
            } else {
                [CommanMethods displayAlertMessage:@"UOC" message:[dict objectForKey:@"message"] currentRef:self];
            }
        });
    } Error:^(NSError *error, NSURLResponse *response) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [AppDelegate removeVwLoading];
        });
    }];
}

@end
