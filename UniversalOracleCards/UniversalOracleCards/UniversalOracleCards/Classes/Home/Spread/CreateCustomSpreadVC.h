//
//  CreateCustomSpreadVC.h
//  UniversalOracleCards
//
//  Created by whiznic on 06/03/19.
//  Copyright © 2019 whiznic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <AVKit/AVKit.h>


@interface CreateCustomSpreadVC : UIViewController <UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UICollectionViewDelegate, UITextFieldDelegate>
@property (strong, nonatomic) IBOutlet UICollectionView *colSpread;
@property (strong, nonatomic) IBOutlet UICollectionView *clvMini;
@property (strong, nonatomic) IBOutlet UIView *ViewSpread;
@property (strong, nonatomic) IBOutlet UIView *viewText;
@property (strong, nonatomic) IBOutlet UIButton *btnOk;
@property (strong, nonatomic) IBOutlet UITextField *txtSpread;
@property (nonatomic) AVPlayer *playerViewController;
@property (strong, nonatomic) IBOutlet UIView *viewDisply;
@property (strong, nonatomic) IBOutlet UILabel *lbl2x2;
@property (strong, nonatomic) IBOutlet UILabel *lbl3x3;
@property (strong, nonatomic) IBOutlet UILabel *lbl4x4;
@property (strong, nonatomic) IBOutlet UILabel *lbl5x5;
@property (strong, nonatomic) IBOutlet UILabel *lbl6x6;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (strong, nonatomic) NSString *gridnumber,*value1,*back;
@property (strong, nonatomic) NSDictionary *singleGridDate;
@end


