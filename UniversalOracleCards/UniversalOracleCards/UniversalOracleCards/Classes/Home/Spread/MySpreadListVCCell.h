//
//  MySpreadListVCCell.h
//  UniversalOracleCards
//
//  Created by whiznic on 14/03/19.
//  Copyright © 2019 whiznic. All rights reserved.
//

#import <UIKit/UIKit.h>
#define MySpreadListVCCellID @"MySpreadListVCCellID"


@interface MySpreadListVCCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lblSpreadName;
@property (strong, nonatomic) IBOutlet UILabel *lblTotalCards;
@property (strong, nonatomic) IBOutlet UIButton *btnMore;
@property (strong, nonatomic) IBOutlet UIView *ViewMoreAction;
@property (strong, nonatomic) IBOutlet UIButton *btnEdit;
@property (strong, nonatomic) IBOutlet UIButton *btnDelete;

@end


