//
//  MySpreadListVC.h
//  UniversalOracleCards
//
//  Created by whiznic on 13/03/19.
//  Copyright © 2019 whiznic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <AVKit/AVKit.h>

@interface MySpreadListVC : UIViewController <UITableViewDelegate, UITableViewDataSource>
@property (nonatomic) AVPlayer *playerViewController;
@property (strong, nonatomic) IBOutlet UITableView *tblSpreadList;
@property (strong, nonatomic) IBOutlet UIView *viewDisply;
@property (weak, nonatomic) IBOutlet UIButton *btnMute;
@property (weak, nonatomic) IBOutlet UIButton *btnVolume;
@end


