//
//  DragClvCell.h
//  UniversalOracleCards
//
//  Created by Ankit Vadalia on 29/03/19.
//  Copyright © 2019 whiznic. All rights reserved.
//

#import <UIKit/UIKit.h>

#define DragClvCellID @"DragClvCellID"

@interface DragClvCell : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UIImageView *img;
@property (weak, nonatomic) IBOutlet UILabel *lbltitle;

@end
