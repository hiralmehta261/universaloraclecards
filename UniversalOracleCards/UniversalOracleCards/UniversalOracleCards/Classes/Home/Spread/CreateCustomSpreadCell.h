//
//  CreateCustomSpreadCell.h
//  UniversalOracleCards
//
//  Created by whiznic on 06/03/19.
//  Copyright © 2019 whiznic. All rights reserved.
//

#import <UIKit/UIKit.h>

#define CreateCustomSpreadCellID @"CreateCustomSpreadCellID"

@interface CreateCustomSpreadCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UIView *ViewFrame;
@property (strong, nonatomic) IBOutlet UIView *ViewBorder;
@property (strong, nonatomic) IBOutlet UIButton *btnEdit;
@property (strong, nonatomic) IBOutlet UIButton *btnDelete;
@property (strong, nonatomic) IBOutlet UIButton *btnMove;
@property (strong, nonatomic) IBOutlet UIImageView *imgCell;

@end


