//
//  CreateCustomSpreadVC.m
//  UniversalOracleCards
//
//  Created by whiznic on 06/03/19.
//  Copyright © 2019 whiznic. All rights reserved.
//

#import "CreateCustomSpreadVC.h"
#import "CreateCustomSpreadCell.h"
#import "CreateSpreadVC.h"
#import <IQKeyboardManager/IQKeyboardManager.h>
#import "MySpreadListVC.h"
#import "AppDelegate.h"
#import "CommanMethods.h"
#import "CommunicationHandler.h"
#import "DragClvCell.h"
#import <AudioToolbox/AudioServices.h>
#import "SharkfoodMuteSwitchDetector.h"
@import MediaPlayer;

@interface CreateCustomSpreadVC ()
{
    NSMutableArray *arrNewGrid2,*arrNewGrid3,*arrNewGrid4,*arrNewGrid5,*arrNewGrid6,*arrDragGrid,*arrCardTitle,*CardTitle;
    NSMutableArray *arrDragStatusNewGrid2,*arrDragStatusNewGrid3,*arrDragStatusNewGrid4,*arrDragStatusNewGrid5,*arrDragStatusNewGrid6;
    NSMutableArray *arrCreateSpread, *carddisplay;
    float grid;
    int cardposition, passname,pass;
    NSString *grid_Type;
}
@property (nonatomic,strong) SharkfoodMuteSwitchDetector* detector;
@end

@implementation CreateCustomSpreadVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.colSpread.delegate = self;
    self.colSpread.dataSource = self;
    
    self.detector = [SharkfoodMuteSwitchDetector shared];
    __weak CreateCustomSpreadVC* sself = self;
    self.detector.silentNotify = ^(BOOL silent){
        if (silent) {
            
            NSLog(@"device is silent");
        } else {
            
            NSLog(@"device is in ringing mode");
            
        }
    };
    
    //UINib *cellnib = [UINib nibWithNibName:NSStringFromClass(CreateCustomSpreadCell.class) bundle:nil];
    //[self.colSpread registerNib:cellnib forCellWithReuseIdentifier:CreateCustomSpreadCellID];
    
    NSLog(@" _singleGridDate :- %@", _singleGridDate);
    
    NSLog(@"enter");
    arrNewGrid2 = [[NSMutableArray alloc] init];
    arrNewGrid3 = [[NSMutableArray alloc] init];
    arrNewGrid4 = [[NSMutableArray alloc] init];
    arrNewGrid5 = [[NSMutableArray alloc] init];
    arrNewGrid6 = [[NSMutableArray alloc] init];
    
    arrCardTitle = [[NSMutableArray alloc] init];
    
    arrDragGrid = [[NSMutableArray alloc] initWithObjects:@"0", @"0", @"0", @"0", nil];
    
    arrDragStatusNewGrid2 = [[NSMutableArray alloc] init];
    arrDragStatusNewGrid3 = [[NSMutableArray alloc] init];
    arrDragStatusNewGrid4 = [[NSMutableArray alloc] init];
    arrDragStatusNewGrid5 = [[NSMutableArray alloc] init];
    arrDragStatusNewGrid6 = [[NSMutableArray alloc] init];
    
    self.ViewSpread.layer.borderColor = [UIColor clearColor].CGColor;
    self.ViewSpread.layer.borderWidth = 1;
    self.ViewSpread.layer.cornerRadius = 10;
    
    self.btnOk.layer.borderColor = [UIColor clearColor].CGColor;
    self.btnOk.layer.borderWidth = 1;
    self.btnOk.layer.cornerRadius = 10;
    
    self.viewText.layer.borderColor = [UIColor colorWithRed:173/255.0 green:173/255.0 blue:173/255.0 alpha:1.0].CGColor;
    self.viewText.layer.borderWidth = 1;
    self.viewText.layer.cornerRadius = 10;
    
    self.txtSpread.delegate = self;
    

    
    self.colSpread.dropDelegate = self;
    self.clvMini.dragDelegate = self;
    self.clvMini.dragInteractionEnabled = true;
    
    cardposition = -1;
    passname = 0;
    
   
    
    
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    NSString *stringVideoName = @"stars_animated.mp4";
    NSString *filepath = [[NSBundle mainBundle] pathForResource:stringVideoName ofType:nil];
    NSLog(@" filepath :- %@", filepath);
    NSURL *fileURL = [NSURL fileURLWithPath:filepath];
    self.playerViewController = [AVPlayer playerWithURL:fileURL];
    self.playerViewController.actionAtItemEnd = AVPlayerActionAtItemEndNone;
    AVPlayerLayer *videoLayer = [AVPlayerLayer playerLayerWithPlayer:self.playerViewController];
    videoLayer.frame = self.view.bounds;
    videoLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
    [self.view.layer addSublayer:videoLayer];
    
    [self.playerViewController play];
    
    NSString *savedValue = [[NSUserDefaults standardUserDefaults]
                            stringForKey:@"audio"];
    if([savedValue isEqualToString:@"off"]) {
        
        [self.playerViewController setVolume:0.0];
    } else {
        
        [self.playerViewController setVolume:1.0];
    }
    
    NSLog(@" savedValue :- %@", savedValue);
    
    self.viewDisply.layer.zPosition = 1;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(itemDidFinishPlaying:) name:AVPlayerItemDidPlayToEndTimeNotification object:[self.playerViewController currentItem]];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationDidBecomeActive:) name:UIApplicationDidEnterBackgroundNotification object:nil];
    
    [[UIApplication sharedApplication] beginReceivingRemoteControlEvents];
    [self becomeFirstResponder];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [self.playerViewController pause];
    [super viewWillDisappear:animated];
    [[UIApplication sharedApplication] endReceivingRemoteControlEvents];
    [self resignFirstResponder];
}


- (void)remoteControlReceivedWithEvent:(UIEvent *)event {
    switch (event.subtype) {
        case UIEventSubtypeRemoteControlTogglePlayPause:
            if([self.playerViewController rate] == 0){
                [self.playerViewController play];
            } else {
                [self.playerViewController pause];
            }
            break;
        case UIEventSubtypeRemoteControlPlay:
            [self.playerViewController play];
            break;
        case UIEventSubtypeRemoteControlPause:
            [self.playerViewController pause];
            break;
        default:
            break;
    }
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    self.detector = [SharkfoodMuteSwitchDetector shared];
    __weak CreateCustomSpreadVC* sself = self;
    self.detector.silentNotify = ^(BOOL silent){
        if (silent) {
            
            NSLog(@"device is silent");
        } else {
            
            NSLog(@"device is in ringing mode");
            [self.playerViewController performSelector:@selector(play) withObject:nil afterDelay:0.01];
        }
    };
}


- (void)itemDidFinishPlaying:(NSNotification *)notification {
    AVPlayerItem *player = [notification object];
    [player seekToTime:kCMTimeZero];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if ([self.gridnumber isEqual: @"2"]) {
        [self btn2x2:nil];
        self.lbl2x2.hidden = FALSE;
        self.lbl3x3.hidden = TRUE;
        self.lbl4x4.hidden = TRUE;
        self.lbl5x5.hidden = TRUE;
        self.lbl6x6.hidden = TRUE;
        //NSLog(@" self.singleGridDate :- %@", self.singleGridDate);
    } else if ([self.gridnumber isEqual: @"3"]) {
        [self btn3x3:nil];
        self.lbl2x2.hidden = TRUE;
        self.lbl3x3.hidden = FALSE;
        self.lbl4x4.hidden = TRUE;
        self.lbl5x5.hidden = TRUE;
        self.lbl6x6.hidden = TRUE;
        //NSLog(@" self.singleGridDate :- %@", self.singleGridDate);
    } else if ([self.gridnumber isEqual: @"4"]) {
        [self btn4x4:nil];
        self.lbl2x2.hidden = TRUE;
        self.lbl3x3.hidden = TRUE;
        self.lbl4x4.hidden = FALSE;
        self.lbl5x5.hidden = TRUE;
        self.lbl6x6.hidden = TRUE;
        //NSLog(@" self.singleGridDate :- %@", self.singleGridDate);
    } else if ([self.gridnumber isEqual: @"5"]) {
        [self btn5x5:nil];
        self.lbl2x2.hidden = TRUE;
        self.lbl3x3.hidden = TRUE;
        self.lbl4x4.hidden = TRUE;
        self.lbl5x5.hidden = FALSE;
        self.lbl6x6.hidden = TRUE;
        //NSLog(@" self.singleGridDate :- %@", self.singleGridDate);
    } else if ([self.gridnumber isEqual: @"6"]) {
        [self btn6x6:nil];
        self.lbl2x2.hidden = TRUE;
        self.lbl3x3.hidden = TRUE;
        self.lbl4x4.hidden = TRUE;
        self.lbl5x5.hidden = TRUE;
        self.lbl6x6.hidden = FALSE;
       // NSLog(@" self.singleGridDate :- %@", self.singleGridDate);
    } else {
        [self btn4x4:nil];
        self.lbl2x2.hidden = TRUE;
        self.lbl3x3.hidden = TRUE;
        self.lbl4x4.hidden = FALSE;
        self.lbl5x5.hidden = TRUE;
        self.lbl6x6.hidden = TRUE;
    }
}

#pragma TEXTFIELD-DELEGATE

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

#pragma Mark Action

- (IBAction)btn2x2:(id)sender {
    arrNewGrid2 = [[NSMutableArray alloc] init];
    arrDragStatusNewGrid2 = [[NSMutableArray alloc] init];
    arrCardTitle = [[NSMutableArray alloc] init];
    for (int i = 1; i < 5; i++) {
        [arrNewGrid2 addObject:[NSDecimalNumber numberWithInt:i]];
        [arrDragStatusNewGrid2 addObject:@"0"];
        [arrCardTitle addObject:@"0"];
    }
    if ([self.value1 isEqualToString:@"1"] || [self.value1 isEqualToString:@"2"]) {
         CardTitle = [self.singleGridDate objectForKey:@"card_name"];
    }
    grid = 2.0;
    grid_Type = @"2x2";
    self.lbl2x2.hidden = FALSE;
    self.lbl3x3.hidden = TRUE;
    self.lbl4x4.hidden = TRUE;
    self.lbl5x5.hidden = TRUE;
    self.lbl6x6.hidden = TRUE;
    [self.colSpread reloadData];
    [self.clvMini reloadData];
}

- (IBAction)btn3x3:(id)sender {
    arrNewGrid3 = [[NSMutableArray alloc] init];
    arrDragStatusNewGrid3 = [[NSMutableArray alloc] init];
    arrCardTitle = [[NSMutableArray alloc] init];
    for (int i = 1; i < 10; i++) {
        [arrNewGrid3 addObject:[NSDecimalNumber numberWithInt:i]];
        [arrDragStatusNewGrid3 addObject:@"0"];
        [arrCardTitle addObject:@"0"];
    }
    grid = 3.0;
    grid_Type = @"3x3";
    self.lbl2x2.hidden = TRUE;
    self.lbl3x3.hidden = FALSE;
    self.lbl4x4.hidden = TRUE;
    self.lbl5x5.hidden = TRUE;
    self.lbl6x6.hidden = TRUE;
    [self.colSpread reloadData];
    [self.clvMini reloadData];
}

- (IBAction)btn4x4:(id)sender {
    arrNewGrid4 = [[NSMutableArray alloc] init];
    arrDragStatusNewGrid4 = [[NSMutableArray alloc] init];
    arrCardTitle = [[NSMutableArray alloc] init];
    for (int i = 1; i < 17; i++) {
        [arrNewGrid4 addObject:[NSDecimalNumber numberWithInt:i]];
        [arrDragStatusNewGrid4 addObject:@"0"];
        [arrCardTitle addObject:@"0"];
    }
    grid = 4.0;
    grid_Type = @"4x4";
    self.lbl2x2.hidden = TRUE;
    self.lbl3x3.hidden = TRUE;
    self.lbl4x4.hidden = FALSE;
    self.lbl5x5.hidden = TRUE;
    self.lbl6x6.hidden = TRUE;
    [self.colSpread reloadData];
    [self.clvMini reloadData];
    
}

- (IBAction)btn5x5:(id)sender {
    arrNewGrid5 = [[NSMutableArray alloc] init];
    arrDragStatusNewGrid5 = [[NSMutableArray alloc] init];
    arrCardTitle = [[NSMutableArray alloc] init];
    for (int i = 1; i < 26; i++) {
        [arrNewGrid5 addObject:[NSDecimalNumber numberWithInt:i]];
        [arrDragStatusNewGrid5 addObject:@"0"];
        [arrCardTitle addObject:@"0"];
    }
    grid = 5.0;
    grid_Type = @"5x5";
    self.lbl2x2.hidden = TRUE;
    self.lbl3x3.hidden = TRUE;
    self.lbl4x4.hidden = TRUE;
    self.lbl5x5.hidden = FALSE;
    self.lbl6x6.hidden = TRUE;
    [self.colSpread reloadData];
    [self.clvMini reloadData];
}

- (IBAction)btn6x6:(id)sender {
    arrNewGrid6 = [[NSMutableArray alloc] init];
    arrDragStatusNewGrid6 = [[NSMutableArray alloc] init];
    arrCardTitle = [[NSMutableArray alloc] init];
    for (int i = 1; i < 37; i++) {
        [arrNewGrid6 addObject:[NSDecimalNumber numberWithInt:i]];
        [arrDragStatusNewGrid6 addObject:@"0"];
        [arrCardTitle addObject:@"0"];
    }
    grid = 6.0;
    grid_Type = @"6x6";
    self.lbl2x2.hidden = TRUE;
    self.lbl3x3.hidden = TRUE;
    self.lbl4x4.hidden = TRUE;
    self.lbl5x5.hidden = TRUE;
    self.lbl6x6.hidden = FALSE;
    [self.colSpread reloadData];
    [self.clvMini reloadData];
}



- (IBAction)btnBack:(id)sender {
    if ([self.back isEqualToString:@"1"]) {
        if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
            CreateSpreadVC *createspreadVC = [[CreateSpreadVC alloc] initWithNibName:@"CreateSpreadVC" bundle:nil];
            [self.navigationController pushViewController:createspreadVC animated:YES];
        } else {
            CreateSpreadVC *createspreadVC = [[CreateSpreadVC alloc] initWithNibName:@"CreateSpreadVC_IPad" bundle:nil];
            [self.navigationController pushViewController:createspreadVC animated:YES];
        }
    } else {
        if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
            MySpreadListVC *spreadlistVC = [[MySpreadListVC alloc] initWithNibName:@"MySpreadListVC" bundle:nil];
            [self.navigationController pushViewController:spreadlistVC animated:YES];
        } else {
            MySpreadListVC *spreadlistVC = [[MySpreadListVC alloc] initWithNibName:@"MySpreadListVC_IPad" bundle:nil];
            [self.navigationController pushViewController:spreadlistVC animated:YES];
        }
    }
    
}

- (IBAction)btnSaveSpread:(id)sender {
    if ([self.value1 isEqualToString:@"1"] || [self.value1 isEqualToString:@"2"]) {
        
        NSDictionary *dictData = [[NSDictionary alloc] init];
        dictData = [arrCardTitle objectAtIndex:[sender tag]];
        
        
        CardTitle = [self.singleGridDate objectForKey:@"card_name"];
        
        
        for (int i = 0; i<arrDragStatusNewGrid2.count; i++) {
            if ([[arrDragStatusNewGrid2 objectAtIndex:i] isEqualToString:@"0"]) {
                [CardTitle replaceObjectAtIndex:cardposition withObject:@"10"];
            }
        }
        
        NSLog(@" arrCardTitle :- %@", arrCardTitle);
        NSLog(@" arrDragStatusNewGrid2 :- %@", arrDragStatusNewGrid2);
        
       
        
        /*if ([CommanMethods isNetworkRechable]) {
            [self callForEditSpread];
        } else {
            [CommanMethods displayAlertMessage:key_Informaction message:kInternetConnectionMessage currentRef:self];
        }*/
    } else {
        [self.txtSpread becomeFirstResponder];
        self.ViewSpread.hidden = FALSE;
        self.lblTitle.text = @"Name Your Spread";
        self.txtSpread.text = @"";
        self.txtSpread.placeholder = @"Custom Spread 1";
        passname = 1;
    }
   
}


-(IBAction)btnCellDelete:(id)sender {
    
    NSIndexPath *indexPath;
    UICollectionView *collectionView;
    
    static int counter = 0;
    NSString *identifier = [NSString stringWithFormat:@"Identifier_%d", counter];
    CreateCustomSpreadCell *cell = (CreateCustomSpreadCell *)[collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    counter++;
    cell = [collectionView cellForItemAtIndexPath:indexPath];
    
    //NSDictionary *dictData = [[NSDictionary alloc] init];
    //dictData = [arrOnlineList objectAtIndex:[sender tag]];
    
    
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"UOC" message:@"Are you sure you want to Delete Card?" preferredStyle:UIAlertControllerStyleAlert];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
        [self dismissViewControllerAnimated:YES completion:^{
            
        }];
    }]];
    
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        if ([self.value1 isEqualToString:@"1"]) {
            if ([self.gridnumber isEqualToString:@"2"]) {
                [arrDragStatusNewGrid2 replaceObjectAtIndex:cardposition withObject:@"0"];
                cell.imgCell.hidden = TRUE;
                cell.btnEdit.hidden = TRUE;
                cell.btnMove.hidden = TRUE;
                cell.btnDelete.hidden = TRUE;
                self.value1 = @"2";
                [self.colSpread reloadData];
                NSLog(@" arrDragStatusNewGrid2 :- %@", arrDragStatusNewGrid2);
                [CommanMethods displayAlertMessage:@"UOC" message:@"Card Delete Successfully" currentRef:self];
            
            } else if ([self.gridnumber isEqualToString:@"3"]) {
                [arrDragStatusNewGrid3 replaceObjectAtIndex:cardposition withObject:@"0"];
                cell.imgCell.hidden = TRUE;
                cell.btnEdit.hidden = TRUE;
                cell.btnMove.hidden = TRUE;
                cell.btnDelete.hidden = TRUE;
                self.value1 = @"2";
                [self.colSpread reloadData];
                NSLog(@" arrDragStatusNewGrid3 :- %@", arrDragStatusNewGrid3);
                [CommanMethods displayAlertMessage:@"UOC" message:@"Card Delete Successfully" currentRef:self];
           
            } else if ([self.gridnumber isEqualToString:@"4"]) {
                [arrDragStatusNewGrid4 replaceObjectAtIndex:cardposition withObject:@"0"];
                cell.imgCell.hidden = TRUE;
                cell.btnEdit.hidden = TRUE;
                cell.btnMove.hidden = TRUE;
                cell.btnDelete.hidden = TRUE;
                self.value1 = @"2";
                [self.colSpread reloadData];
                NSLog(@" arrDragStatusNewGrid4 :- %@", arrDragStatusNewGrid4);
                [CommanMethods displayAlertMessage:@"UOC" message:@"Card Delete Successfully" currentRef:self];
            }
        
        } else {
            if (grid == 2) {
                [arrDragStatusNewGrid2 replaceObjectAtIndex:cardposition withObject:@"0"];
                cell.imgCell.hidden = TRUE;
                cell.btnEdit.hidden = TRUE;
                cell.btnMove.hidden = TRUE;
                cell.btnDelete.hidden = TRUE;
                [self.colSpread reloadData];
                NSLog(@" arrDragStatusNewGrid2 :- %@", arrDragStatusNewGrid2);
                [CommanMethods displayAlertMessage:@"UOC" message:@"Card Delete Successfully" currentRef:self];
            } else if (grid == 3) {
                [arrDragStatusNewGrid3 replaceObjectAtIndex:cardposition withObject:@"0"];
                cell.imgCell.hidden = TRUE;
                cell.btnEdit.hidden = TRUE;
                cell.btnMove.hidden = TRUE;
                cell.btnDelete.hidden = TRUE;
                
                [self.colSpread reloadData];
                
                NSLog(@" arrDragStatusNewGrid3 :- %@", arrDragStatusNewGrid3);
                
                [CommanMethods displayAlertMessage:@"UOC" message:@"Card Delete Successfully" currentRef:self];
            } else if (grid == 4) {
                [arrDragStatusNewGrid4 replaceObjectAtIndex:cardposition withObject:@"0"];
                cell.imgCell.hidden = TRUE;
                cell.btnEdit.hidden = TRUE;
                cell.btnMove.hidden = TRUE;
                cell.btnDelete.hidden = TRUE;
                
                [self.colSpread reloadData];
                
                NSLog(@" arrDragStatusNewGrid4 :- %@", arrDragStatusNewGrid4);
                
                [CommanMethods displayAlertMessage:@"UOC" message:@"Card Delete Successfully" currentRef:self];
            }
        }
        
        
    }]];
    
    // Present action sheet.
    [self presentViewController:actionSheet animated:YES completion:nil];
    NSLog(@" delete row :- %ld", (long)[indexPath row]);
}

-(IBAction)btnCellName:(id)sender {
    
    NSIndexPath *indexPath;
    UICollectionView *collectionView;
    
    static int counter = 0;
    NSString *identifier = [NSString stringWithFormat:@"Identifier_%d", counter];
    CreateCustomSpreadCell *cell = (CreateCustomSpreadCell *)[collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    counter++;
    cell = [collectionView cellForItemAtIndexPath:indexPath];
    [self.txtSpread becomeFirstResponder];
    self.ViewSpread.hidden = FALSE;
    self.lblTitle.text = @"Card Title";
    self.txtSpread.text = @"";
    self.txtSpread.placeholder = @"Name the Card";
    passname = 2;
    
    //NSDictionary *dictData = [[NSDictionary alloc] init];
    //dictData = [arrOnlineList objectAtIndex:[sender tag]];
    
    NSLog(@" Edit row :- %ld", (long)[indexPath row]);
}


- (IBAction)btnOk:(id)sender {
   
    if (passname == 1) {
        if ([CommanMethods isNetworkRechable]) {
            [self callForCreateSpread:self.txtSpread.text];
        } else {
            [CommanMethods displayAlertMessage:key_Informaction message:kInternetConnectionMessage currentRef:self];
        }
    } else {
        
        [arrCardTitle replaceObjectAtIndex:cardposition withObject:self.txtSpread.text];
        NSLog(@" arrCardTitle :- %@", arrCardTitle);
        [CommanMethods displayAlertMessage:@"UOC" message:@"Card Title Add Successfully" currentRef:self];
        self.ViewSpread.hidden = TRUE;
        self.txtSpread.text = @"";
    }
    
    
   
    /*if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
        MySpreadListVC *myspreadVC = [[MySpreadListVC alloc] initWithNibName:@"MySpreadListVC" bundle:nil];
        [self.navigationController pushViewController:myspreadVC animated:YES];
    } else {
        MySpreadListVC *myspreadVC = [[MySpreadListVC alloc] initWithNibName:@"MySpreadListVC_IPad" bundle:nil];
        [self.navigationController pushViewController:myspreadVC animated:YES];
    }*/
}

#pragma CollectionView Method

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (collectionView == self.colSpread) {
        if (grid == 0) {
            return 16;
        } else if (grid == 2){
            return arrNewGrid2.count;
        } else if (grid == 3){
            return arrNewGrid3.count;
        } else if (grid == 4){
            return arrNewGrid4.count;
        } else if (grid == 5){
            return arrNewGrid5.count;
        } else {
            return arrNewGrid6.count;
        }
    } else {
        return 3;
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (collectionView == self.colSpread) {
        static int counter = 0;
        NSString *identifier = [NSString stringWithFormat:@"Identifier_%d", counter];
        //[collectionView registerClass:[CreateCustomSpreadCell class] forCellWithReuseIdentifier:identifier];
        
        UINib *cellnib = [UINib nibWithNibName:NSStringFromClass(CreateCustomSpreadCell.class) bundle:nil];
        [collectionView registerNib:cellnib forCellWithReuseIdentifier:identifier];
        
        CreateCustomSpreadCell *cell = (CreateCustomSpreadCell *)[collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
        
        //animated image
        cell.btnEdit.hidden = TRUE;
        cell.btnDelete.hidden = TRUE;
        cell.btnMove.hidden = TRUE;
        
        counter++;
        
        CAShapeLayer *yourViewBorder = [CAShapeLayer layer];
        yourViewBorder.strokeColor = [UIColor colorWithRed:173/255.0 green:173/255.0 blue:173/255.0 alpha:1.0].CGColor;
        yourViewBorder.fillColor = nil;
        yourViewBorder.lineDashPattern = @[@1, @1];
        
       
           if ([self.value1 isEqualToString:@"1"]) {
                carddisplay = [self.singleGridDate objectForKey:@"card_position"];
               
               
               
               
                if ([self.gridnumber isEqualToString:@"2"]) {
                    
                    cell.btnEdit.tag = indexPath.row;
                    cell.btnDelete.tag = indexPath.row;
                    
                    [cell.btnDelete addTarget:self action:@selector(btnCellDelete:) forControlEvents:UIControlEventTouchUpInside];
                    [cell.btnEdit addTarget:self action:@selector(btnCellName:) forControlEvents:UIControlEventTouchUpInside];
                    
                    if ([carddisplay containsObject:[NSString stringWithFormat:@"%d",indexPath.item]]) {
                        for (int i = 0; i<carddisplay.count; i++) {
                            if ([[carddisplay objectAtIndex:i] isEqualToString:[NSString stringWithFormat:@"%d",indexPath.item]]) {
                                cell.imgCell.image = [UIImage imageNamed:@"animated image"];
                                 cell.btnMove.hidden = FALSE;
                                cell.ViewBorder.layer.borderColor = [UIColor colorWithRed:173/255.0 green:173/255.0 blue:173/255.0 alpha:1.0].CGColor;
                                cell.ViewBorder.layer.borderWidth = 1;
                                cell.ViewBorder.layer.cornerRadius = 10;
                                [arrDragStatusNewGrid2 replaceObjectAtIndex:indexPath.item withObject:@"1"];
                               
                                if ([indexPath row] == cardposition) {
                                    cell.btnDelete.hidden = FALSE;
                                    cell.btnEdit.hidden = FALSE;
                                }
                            }
                        }
                    
                    } else {
                        cell.imgCell.image = nil;
                        cell.btnDelete.hidden = TRUE;
                        cell.btnEdit.hidden = TRUE;
                        
                    }
                    
                } else if ([self.gridnumber isEqualToString:@"3"]) {
                    
               
                    
                    cell.btnEdit.tag = indexPath.row;
                    cell.btnDelete.tag = indexPath.row;
                    
                    [cell.btnDelete addTarget:self action:@selector(btnCellDelete:) forControlEvents:UIControlEventTouchUpInside];
                    [cell.btnEdit addTarget:self action:@selector(btnCellName:) forControlEvents:UIControlEventTouchUpInside];
                    
                    if ([carddisplay containsObject:[NSString stringWithFormat:@"%d",indexPath.item]]) {
                        for (int i = 0; i<carddisplay.count; i++) {
                            if ([[carddisplay objectAtIndex:i] isEqualToString:[NSString stringWithFormat:@"%d",indexPath.item]]) {
                                cell.imgCell.image = [UIImage imageNamed:@"animated image"];
                                cell.btnMove.hidden = FALSE;
                                cell.ViewBorder.layer.borderColor = [UIColor colorWithRed:173/255.0 green:173/255.0 blue:173/255.0 alpha:1.0].CGColor;
                                cell.ViewBorder.layer.borderWidth = 1;
                                cell.ViewBorder.layer.cornerRadius = 10;
                                [arrDragStatusNewGrid3 replaceObjectAtIndex:indexPath.item withObject:@"1"];
                                if ([indexPath row] == cardposition) {
                                    cell.btnDelete.hidden = FALSE;
                                    cell.btnEdit.hidden = FALSE;
                                }
                            }
                        }
                        
                    } else {
                        cell.imgCell.image = nil;
                        cell.btnDelete.hidden = TRUE;
                        cell.btnEdit.hidden = TRUE;
                        
                    }
                } else if ([self.gridnumber isEqualToString:@"4"]) {
                    
                 
                    cell.btnEdit.tag = indexPath.row;
                    cell.btnDelete.tag = indexPath.row;
                    
                    [cell.btnDelete addTarget:self action:@selector(btnCellDelete:) forControlEvents:UIControlEventTouchUpInside];
                    [cell.btnEdit addTarget:self action:@selector(btnCellName:) forControlEvents:UIControlEventTouchUpInside];
                    
                    if ([carddisplay containsObject:[NSString stringWithFormat:@"%d",indexPath.item]]) {
                        for (int i = 0; i<carddisplay.count; i++) {
                            if ([[carddisplay objectAtIndex:i] isEqualToString:[NSString stringWithFormat:@"%d",indexPath.item]]) {
                                cell.imgCell.image = [UIImage imageNamed:@"animated image"];
                                cell.btnMove.hidden = FALSE;
                                cell.ViewBorder.layer.borderColor = [UIColor colorWithRed:173/255.0 green:173/255.0 blue:173/255.0 alpha:1.0].CGColor;
                                cell.ViewBorder.layer.borderWidth = 1;
                                cell.ViewBorder.layer.cornerRadius = 5;
                                [arrDragStatusNewGrid4 replaceObjectAtIndex:indexPath.item withObject:@"1"];
                                if ([indexPath row] == cardposition) {
                                    cell.btnDelete.hidden = FALSE;
                                    cell.btnEdit.hidden = FALSE;
                                }
                            }
                        }
                        
                    } else {
                        cell.imgCell.image = nil;
                        cell.btnDelete.hidden = TRUE;
                        cell.btnEdit.hidden = TRUE;
                        
                    }
                } else if ([self.gridnumber isEqualToString:@"5"]) {
                    
                  
                    
                    cell.btnEdit.tag = indexPath.row;
                    cell.btnDelete.tag = indexPath.row;
                    
                    [cell.btnDelete addTarget:self action:@selector(btnCellDelete:) forControlEvents:UIControlEventTouchUpInside];
                    [cell.btnEdit addTarget:self action:@selector(btnCellName:) forControlEvents:UIControlEventTouchUpInside];
                    
                    if ([carddisplay containsObject:[NSString stringWithFormat:@"%d",indexPath.item]]) {
                        for (int i = 0; i<carddisplay.count; i++) {
                            if ([[carddisplay objectAtIndex:i] isEqualToString:[NSString stringWithFormat:@"%d",indexPath.item]]) {
                                cell.imgCell.image = [UIImage imageNamed:@"animated image"];
                                cell.btnMove.hidden = FALSE;
                                cell.ViewBorder.layer.borderColor = [UIColor colorWithRed:173/255.0 green:173/255.0 blue:173/255.0 alpha:1.0].CGColor;
                                cell.ViewBorder.layer.borderWidth = 1;
                                cell.ViewBorder.layer.cornerRadius = 4;
                                [arrDragStatusNewGrid5 replaceObjectAtIndex:indexPath.item withObject:@"1"];
                                if ([indexPath row] == cardposition) {
                                    cell.btnDelete.hidden = FALSE;
                                    cell.btnEdit.hidden = FALSE;
                                }
                            }
                        }
                        
                    } else {
                        cell.imgCell.image = nil;
                        
                        
                    }
                } else if ([self.gridnumber isEqualToString:@"6"]) {
                    
                    
                    cell.btnEdit.tag = indexPath.row;
                    cell.btnDelete.tag = indexPath.row;
                    
                    [cell.btnDelete addTarget:self action:@selector(btnCellDelete:) forControlEvents:UIControlEventTouchUpInside];
                    [cell.btnEdit addTarget:self action:@selector(btnCellName:) forControlEvents:UIControlEventTouchUpInside];
                    
                    if ([carddisplay containsObject:[NSString stringWithFormat:@"%d",indexPath.item]]) {
                        for (int i = 0; i<carddisplay.count; i++) {
                            if ([[carddisplay objectAtIndex:i] isEqualToString:[NSString stringWithFormat:@"%d",indexPath.item]]) {
                                cell.imgCell.image = [UIImage imageNamed:@"animated image"];
                                cell.btnMove.hidden = FALSE;
                                cell.ViewBorder.layer.borderColor = [UIColor colorWithRed:173/255.0 green:173/255.0 blue:173/255.0 alpha:1.0].CGColor;
                                cell.ViewBorder.layer.borderWidth = 1;
                                cell.ViewBorder.layer.cornerRadius = 3;
                                [arrDragStatusNewGrid6 replaceObjectAtIndex:indexPath.item withObject:@"1"];
                                if ([indexPath row] == cardposition) {
                                    cell.btnDelete.hidden = FALSE;
                                    cell.btnEdit.hidden = FALSE;
                                }
                            }
                        }
                        
                    } else {
                        cell.imgCell.image = nil;
                        
                        
                    }
                }

            } else {
        
               
                if (grid == 2) {
                
                 cell.btnEdit.tag = indexPath.row;
                 cell.btnDelete.tag = indexPath.row;
                 
                 [cell.btnDelete addTarget:self action:@selector(btnCellDelete:) forControlEvents:UIControlEventTouchUpInside];
                 [cell.btnEdit addTarget:self action:@selector(btnCellName:) forControlEvents:UIControlEventTouchUpInside];
                 
                 
                 if ([[arrDragStatusNewGrid2 objectAtIndex:indexPath.item] isEqualToString:@"1"]) {
                     cell.imgCell.image = [UIImage imageNamed:@"animated image"];
                     cell.btnMove.hidden = FALSE;
                     cell.ViewBorder.layer.borderColor = [UIColor colorWithRed:173/255.0 green:173/255.0 blue:173/255.0 alpha:1.0].CGColor;
                     cell.ViewBorder.layer.borderWidth = 1;
                     cell.ViewBorder.layer.cornerRadius = 10;
                     if ([indexPath row] == cardposition) {
                         cell.btnDelete.hidden = FALSE;
                         cell.btnEdit.hidden = FALSE;
                        
                     }
                 } else {
                         cell.imgCell.image = nil;
                         cell.btnDelete.hidden = TRUE;
                         cell.btnEdit.hidden = TRUE;
                     }
                 
                 } else if (grid == 3) {
                     
                     cell.btnEdit.tag = indexPath.row;
                     cell.btnDelete.tag = indexPath.row;
                     
                     [cell.btnDelete addTarget:self action:@selector(btnCellDelete:) forControlEvents:UIControlEventTouchUpInside];
                     [cell.btnEdit addTarget:self action:@selector(btnCellName:) forControlEvents:UIControlEventTouchUpInside];
                     
                     
                     if ([[arrDragStatusNewGrid3 objectAtIndex:indexPath.item] isEqualToString:@"1"]) {
                         cell.btnMove.hidden = FALSE;
                         cell.ViewBorder.layer.borderColor = [UIColor colorWithRed:173/255.0 green:173/255.0 blue:173/255.0 alpha:1.0].CGColor;
                         cell.ViewBorder.layer.borderWidth = 1;
                         cell.ViewBorder.layer.cornerRadius = 10;
                         cell.imgCell.image = [UIImage imageNamed:@"animated image"];
                         if ([indexPath row] == cardposition) {
                             cell.btnDelete.hidden = FALSE;
                             cell.btnEdit.hidden = FALSE;
                             
                         }
                     } else {
                         cell.imgCell.image = nil;
                         cell.btnDelete.hidden = TRUE;
                         cell.btnEdit.hidden = TRUE;
                         
                     }
                 
                 } else if (grid == 4) {
                     
                     cell.btnEdit.tag = indexPath.row;
                     cell.btnDelete.tag = indexPath.row;
                     
                     [cell.btnDelete addTarget:self action:@selector(btnCellDelete:) forControlEvents:UIControlEventTouchUpInside];
                     [cell.btnEdit addTarget:self action:@selector(btnCellName:) forControlEvents:UIControlEventTouchUpInside];
                     
                     if ([[arrDragStatusNewGrid4 objectAtIndex:indexPath.item] isEqualToString:@"1"]) {
                         cell.btnMove.hidden = FALSE;
                         cell.ViewBorder.layer.borderColor = [UIColor colorWithRed:173/255.0 green:173/255.0 blue:173/255.0 alpha:1.0].CGColor;
                         cell.ViewBorder.layer.borderWidth = 1;
                         cell.ViewBorder.layer.cornerRadius = 5;
                         cell.imgCell.image = [UIImage imageNamed:@"animated image"];
                         if ([indexPath row] == cardposition) {
                             cell.btnDelete.hidden = FALSE;
                             cell.btnEdit.hidden = FALSE;
                             
                         }
                     } else {
                     cell.imgCell.image = nil;
                     cell.btnDelete.hidden = TRUE;
                     cell.btnEdit.hidden = TRUE;
                         
                     }
                 
                 } else if (grid == 5) {
                     
                     
                     if ([[arrDragStatusNewGrid5 objectAtIndex:indexPath.item] isEqualToString:@"1"]) {
                         cell.btnMove.hidden = FALSE;
                         cell.ViewBorder.layer.borderColor = [UIColor colorWithRed:173/255.0 green:173/255.0 blue:173/255.0 alpha:1.0].CGColor;
                         cell.ViewBorder.layer.borderWidth = 1;
                         cell.ViewBorder.layer.cornerRadius = 4;
                         cell.imgCell.image = [UIImage imageNamed:@"animated image"];
                 } else {
                     cell.imgCell.image = nil;
                 }
                 
                 } else if (grid == 6) {
                     
                     
                     if ([[arrDragStatusNewGrid6 objectAtIndex:indexPath.item] isEqualToString:@"1"]) {
                         cell.btnMove.hidden = FALSE;
                         cell.ViewBorder.layer.borderColor = [UIColor colorWithRed:173/255.0 green:173/255.0 blue:173/255.0 alpha:1.0].CGColor;
                         cell.ViewBorder.layer.borderWidth = 1;
                         cell.ViewBorder.layer.cornerRadius = 3;
                         cell.imgCell.image = [UIImage imageNamed:@"animated image"];
                     } else {
                     cell.imgCell.image = nil;
                    }
                 
                 }
            }
        
        
        //yourViewBorder.frame = cell.ViewFrame.bounds;
        CGRect screenRect = [[UIScreen mainScreen] bounds];
        CGFloat screenWidth = screenRect.size.width;
        if (grid == 0.0) {
            float cellWidth = (screenWidth) / 4;
            yourViewBorder.frame = CGRectMake(0, 0, cellWidth, cellWidth);
            yourViewBorder.path = [UIBezierPath bezierPathWithRect:yourViewBorder.bounds].CGPath;
            [cell.ViewFrame.layer addSublayer:yourViewBorder];
        } else {
            float cellWidth = (screenWidth) / grid;
            yourViewBorder.frame = CGRectMake(0, 0, cellWidth, cellWidth);
            yourViewBorder.path = [UIBezierPath bezierPathWithRect:yourViewBorder.bounds].CGPath;
            [cell.ViewFrame.layer addSublayer:yourViewBorder];
        }
        return cell;
    } else {
        NSString *identifier = [NSString stringWithFormat:@"cellMini"];
        UINib *cellnib = [UINib nibWithNibName:NSStringFromClass(DragClvCell.class) bundle:nil];
        [collectionView registerNib:cellnib forCellWithReuseIdentifier:identifier];
        
        DragClvCell *cell = (DragClvCell *)[collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
        
       
        
        if (indexPath.item == 0) {
            cell.img.layer.borderColor = [UIColor colorWithRed:173/255.0 green:173/255.0 blue:173/255.0 alpha:0.4].CGColor;
            cell.img.layer.borderWidth = 1;
            cell.img.layer.cornerRadius = 10;
            
            cell.img.image = [UIImage imageNamed:@"Group 221"];
            cell.lbltitle.hidden = FALSE;
            
            
            
        } else {
            cell.img.layer.borderColor = UIColor.clearColor.CGColor;
            cell.img.layer.borderWidth = 0;
            cell.img.layer.cornerRadius = 0;
            cell.img.image = [UIImage imageNamed:@"second"];
            cell.lbltitle.hidden = TRUE;
        }
        return cell;
    }
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
     if (collectionView == self.colSpread) {
         static int counter = 0;
         NSString *identifier = [NSString stringWithFormat:@"Identifier_%d", counter];
         CreateCustomSpreadCell *cell = (CreateCustomSpreadCell *)[collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
         counter++;
         
          if ([self.value1 isEqualToString:@"1"]) {
              if ([self.gridnumber isEqualToString:@"2"]) {
                  cardposition = [indexPath item];
                  NSLog(@" cardposition :- %d", cardposition);
                  [self.colSpread reloadData];
              } else if ([self.gridnumber isEqualToString:@"3"]) {
                  cardposition = [indexPath item];
                  NSLog(@" cardposition :- %d", cardposition);
                  [self.colSpread reloadData];
              } else if ([self.gridnumber isEqualToString:@"4"]) {
                  cardposition = [indexPath item];
                  NSLog(@" cardposition :- %d", cardposition);
                  [self.colSpread reloadData];
              } else if ([self.gridnumber isEqualToString:@"5"]) {
                  cell.btnEdit.hidden = TRUE;
                  cell.btnDelete.hidden = TRUE;
                  
                  if ([[arrDragStatusNewGrid5 objectAtIndex:indexPath.item] isEqualToString:@"1"]) {
                      UIAlertController *alert = [UIAlertController
                                                  alertControllerWithTitle:@"Select Options"
                                                  message:nil
                                                  preferredStyle:UIAlertControllerStyleActionSheet];
                      alert.view.tag = indexPath.row;
                      
                      cardposition = alert.view.tag;
                      
                      UIAlertAction *name = [UIAlertAction
                                             actionWithTitle:@"Add Card Title"
                                             style:UIAlertActionStyleDefault
                                             handler:^(UIAlertAction * action) {
                                                 NSLog(@" value :- %ld", cardposition);
                                                 
                                                 [self.colSpread reloadData];
                                             }];
                      
                      UIAlertAction *delete = [UIAlertAction
                                               actionWithTitle:@"Delete Card"
                                               style:UIAlertActionStyleDefault
                                               handler:^(UIAlertAction * action) {
                                                   NSLog(@" value :- %ld", cardposition);
                                                   [arrDragStatusNewGrid5 replaceObjectAtIndex:cardposition withObject:@"0"];
                                                   cell.imgCell.hidden = TRUE;
                                                   cell.btnEdit.hidden = TRUE;
                                                   cell.btnMove.hidden = TRUE;
                                                   cell.btnDelete.hidden = TRUE;
                                                   self.value1 = @"2";
                                                   [CommanMethods displayAlertMessage:@"UOC" message:@"Card Delete Successfully" currentRef:self];
                                                   [self.colSpread reloadData];
                                                   NSLog(@" arrDragStatusNewGrid5 :- %@", arrDragStatusNewGrid5);
                                               }];
                      
                      
                      UIAlertAction *cancel = [UIAlertAction
                                               actionWithTitle:@"Cancel"
                                               style:UIAlertActionStyleCancel
                                               handler:^(UIAlertAction * action) {
                                                   
                                               }];
                      
                      [alert addAction:name];
                      [alert addAction:delete];
                      [alert addAction:cancel];
                      
                      [self presentViewController:alert animated:YES completion:nil];
                  }
              } else if ([self.gridnumber isEqualToString:@"6"]) {
                  cell.btnEdit.hidden = TRUE;
                  cell.btnDelete.hidden = TRUE;
                  
                  if ([[arrDragStatusNewGrid5 objectAtIndex:indexPath.item] isEqualToString:@"1"]) {
                      UIAlertController *alert = [UIAlertController
                                                  alertControllerWithTitle:@"Select Options"
                                                  message:nil
                                                  preferredStyle:UIAlertControllerStyleActionSheet];
                      alert.view.tag = indexPath.row;
                      
                      cardposition = alert.view.tag;
                      
                      UIAlertAction *name = [UIAlertAction
                                             actionWithTitle:@"Add Card Title"
                                             style:UIAlertActionStyleDefault
                                             handler:^(UIAlertAction * action) {
                                                 NSLog(@" value :- %ld", cardposition);
                                                 [self.colSpread reloadData];
                                             }];
                      
                      UIAlertAction *delete = [UIAlertAction
                                               actionWithTitle:@"Delete Card"
                                               style:UIAlertActionStyleDefault
                                               handler:^(UIAlertAction * action) {
                                                   NSLog(@" value :- %ld", cardposition);
                                                   [arrDragStatusNewGrid6 replaceObjectAtIndex:cardposition withObject:@"0"];
                                                   cell.imgCell.hidden = TRUE;
                                                   cell.btnEdit.hidden = TRUE;
                                                   cell.btnMove.hidden = TRUE;
                                                   cell.btnDelete.hidden = TRUE;
                                                   self.value1 = @"2";
                                                   [CommanMethods displayAlertMessage:@"UOC" message:@"Card Delete Successfully" currentRef:self];
                                                   [self.colSpread reloadData];
                                                   
                                                   NSLog(@" arrDragStatusNewGrid6 :- %@", arrDragStatusNewGrid6);
                                               }];
                      
                      
                      UIAlertAction *cancel = [UIAlertAction
                                               actionWithTitle:@"Cancel"
                                               style:UIAlertActionStyleCancel
                                               handler:^(UIAlertAction * action) {
                                                   
                                               }];
                      
                      [alert addAction:name];
                      [alert addAction:delete];
                      [alert addAction:cancel];
                      
                      [self presentViewController:alert animated:YES completion:nil];
                  }
              }
          }
         
          else {
              
              if (grid == 2) {
                  cardposition = [indexPath item];
                  NSLog(@" cardposition :- %d", cardposition);
                  [self.colSpread reloadData];
              } else if (grid == 3) {
                  cardposition = [indexPath item];
                  NSLog(@" cardposition :- %d", cardposition);
                  [self.colSpread reloadData];
              } else if (grid == 4) {
                  cardposition = [indexPath item];
                  NSLog(@" value :- %d", cardposition);
                  [self.colSpread reloadData];
              } else if (grid == 5) {
                  cell.btnEdit.hidden = TRUE;
                  cell.btnDelete.hidden = TRUE;
                  
                  if ([[arrDragStatusNewGrid5 objectAtIndex:indexPath.item] isEqualToString:@"1"]) {
                      UIAlertController *alert = [UIAlertController
                                                  alertControllerWithTitle:@"Select Options"
                                                  message:nil
                                                  preferredStyle:UIAlertControllerStyleActionSheet];
                      alert.view.tag = indexPath.row;
                      cardposition = alert.view.tag;
                      
                      UIAlertAction *name = [UIAlertAction
                                             actionWithTitle:@"Add Card Title"
                                             style:UIAlertActionStyleDefault
                                             handler:^(UIAlertAction * action) {
                                                 NSLog(@" value :- %ld", cardposition);
                                                 [self.txtSpread becomeFirstResponder];
                                                 self.ViewSpread.hidden = FALSE;
                                                 self.lblTitle.text = @"Card Title";
                                                 self.txtSpread.text = @"";
                                                 self.txtSpread.placeholder = @"Name the Card";
                                                 [self.colSpread reloadData];
                                             }];
                      
                      UIAlertAction *delete = [UIAlertAction
                                               actionWithTitle:@"Delete Card"
                                               style:UIAlertActionStyleDefault
                                               handler:^(UIAlertAction * action) {
                                                   NSLog(@" value :- %ld", cardposition);
                                                   [arrDragStatusNewGrid5 replaceObjectAtIndex:cardposition withObject:@"0"];
                                                   cell.imgCell.hidden = TRUE;
                                                   cell.btnEdit.hidden = TRUE;
                                                   cell.btnMove.hidden = TRUE;
                                                   cell.btnDelete.hidden = TRUE;
                                                   
                                                   [self.colSpread reloadData];
                                                   
                                                   NSLog(@" arrDragStatusNewGrid5 :- %@", arrDragStatusNewGrid5);
                                                   
                                                   [CommanMethods displayAlertMessage:@"UOC" message:@"Card Delete Successfully" currentRef:self];
                                               }];
                      
                      
                      UIAlertAction *cancel = [UIAlertAction
                                               actionWithTitle:@"Cancel"
                                               style:UIAlertActionStyleCancel
                                               handler:^(UIAlertAction * action) {
                                                   
                                               }];
                      
                      [alert addAction:name];
                      [alert addAction:delete];
                      [alert addAction:cancel];
                      
                      [self presentViewController:alert animated:YES completion:nil];
                  }
                  
                  
              }
              
              else if (grid == 6) {
                  cell.btnEdit.hidden = TRUE;
                  cell.btnDelete.hidden = TRUE;
                  
                  if ([[arrDragStatusNewGrid6 objectAtIndex:indexPath.item] isEqualToString:@"1"]) {
                      UIAlertController *alert = [UIAlertController
                                                  alertControllerWithTitle:@"Select Options"
                                                  message:nil
                                                  preferredStyle:UIAlertControllerStyleActionSheet];
                      
                      alert.view.tag = indexPath.row;
                      cardposition = alert.view.tag;
                      
                      UIAlertAction *name = [UIAlertAction
                                             actionWithTitle:@"Add Card Title"
                                             style:UIAlertActionStyleDefault
                                             handler:^(UIAlertAction * action) {
                                                 NSLog(@" value :- %ld", cardposition);
                                                 [self.txtSpread becomeFirstResponder];
                                                 self.ViewSpread.hidden = FALSE;
                                                 self.lblTitle.text = @"Card Title";
                                                 self.txtSpread.text = @"";
                                                 self.txtSpread.placeholder = @"Name the Card";
                                                 [self.colSpread reloadData];
                                             }];
                      
                      UIAlertAction *delete = [UIAlertAction
                                               actionWithTitle:@"Delete Card"
                                               style:UIAlertActionStyleDefault
                                               handler:^(UIAlertAction * action) {
                                                   NSLog(@" value :- %ld", cardposition);
                                                   [arrDragStatusNewGrid6 replaceObjectAtIndex:cardposition withObject:@"0"];
                                                   cell.imgCell.hidden = TRUE;
                                                   cell.btnEdit.hidden = TRUE;
                                                   cell.btnMove.hidden = TRUE;
                                                   cell.btnDelete.hidden = TRUE;
                                                   
                                                   [self.colSpread reloadData];
                                                   
                                                   NSLog(@" arrDragStatusNewGrid6 :- %@", arrDragStatusNewGrid6);
                                                   
                                                   [CommanMethods displayAlertMessage:@"UOC" message:@"Card Delete Successfully" currentRef:self];
                                               }];
                      
                      
                      UIAlertAction *cancel = [UIAlertAction
                                               actionWithTitle:@"Cancel"
                                               style:UIAlertActionStyleCancel
                                               handler:^(UIAlertAction * action) {
                                                   
                                               }];
                      
                      [alert addAction:name];
                      [alert addAction:delete];
                      [alert addAction:cancel];
                      
                      [self presentViewController:alert animated:YES completion:nil];
                  }
              }
          }
        
        
         
     }
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (collectionView == self.colSpread) {
        CGRect screenRect = [[UIScreen mainScreen] bounds];
        CGFloat screenWidth = screenRect.size.width;
        if (grid == 0.0) {
            float cellWidth = (screenWidth) / 4.0;
            CGFloat screenHeight = screenRect.size.height;
            float cellHeight = (screenWidth) / 4.0;
            CGSize size = CGSizeMake(cellWidth, cellHeight);
            return size;
        } else {
            float cellWidth = (screenWidth-1) / grid;
            CGFloat screenHeight = screenRect.size.height;
            float cellHeight = (screenWidth-1) / grid;
            CGSize size = CGSizeMake(cellWidth, cellHeight);
            return size;
        }
    } else {
        if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
            if (indexPath.item == 0) {
                return CGSizeMake(70, 90);
                
            } else {
                return CGSizeMake(30, 90);
            }
        } else {
            if (indexPath.item == 0) {
                return CGSizeMake(100, 120);
                
            } else {
                return CGSizeMake(50, 120);
            }
        }
        
        
    }
}

-(NSArray<UIDragItem *> *)collectionView:(UICollectionView *)collectionView itemsForBeginningDragSession:(id<UIDragSession>)session atIndexPath:(NSIndexPath *)indexPath {
    NSString *item = @"none";
    NSItemProvider *itemProvider = [[NSItemProvider alloc] initWithObject:item];
    UIDragItem *dragItem = [[UIDragItem alloc] initWithItemProvider:itemProvider];
    dragItem.localObject = item;
    return [[NSArray alloc] initWithObjects:dragItem, nil];
}

-(NSArray<UIDragItem *> *)collectionView:(UICollectionView *)collectionView itemsForAddingToDragSession:(id<UIDragSession>)session atIndexPath:(NSIndexPath *)indexPath point:(CGPoint)point {
    NSString *item = @"none";
    NSItemProvider *itemProvider = [[NSItemProvider alloc] initWithObject:item];
    UIDragItem *dragItem = [[UIDragItem alloc] initWithItemProvider:itemProvider];
    dragItem.localObject = item;
    return [[NSArray alloc] initWithObjects:dragItem, nil];
}

-(UIDragPreviewParameters *)collectionView:(UICollectionView *)collectionView dragPreviewParametersForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if (collectionView == self.clvMini) {
        UIDragPreviewParameters *previewParameters = [[UIDragPreviewParameters alloc] init];
        //UIBezierPath *ovalPath = [UIBezierPath bezierPathWithOvalInRect: CGRectMake(25,25,120,120)];
        if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
            UIBezierPath *squarePath = [UIBezierPath bezierPathWithRect:CGRectMake(0,0,70,90)];
            previewParameters.visiblePath = squarePath;
            return previewParameters;
        } else {
            UIBezierPath *squarePath = [UIBezierPath bezierPathWithRect:CGRectMake(0,0,100,120)];
            previewParameters.visiblePath = squarePath;
            return previewParameters;
        }
    }
    return nil;
}

-(BOOL)collectionView:(UICollectionView *)collectionView canHandleDropSession:(id<UIDropSession>)session {
    return true;
}

-(UICollectionViewDropProposal *)collectionView:(UICollectionView *)collectionView dropSessionDidUpdate:(id<UIDropSession>)session withDestinationIndexPath:(NSIndexPath *)destinationIndexPath {
    
    if (collectionView.hasActiveDrag) {
        return [[UICollectionViewDropProposal alloc] initWithDropOperation:UIDropOperationCopy intent:UICollectionViewDropIntentInsertIntoDestinationIndexPath];
    } else {
        return [[UICollectionViewDropProposal alloc] initWithDropOperation:UIDropOperationCopy intent:UICollectionViewDropIntentInsertIntoDestinationIndexPath];
    }
}

-(void)collectionView:(UICollectionView *)collectionView performDropWithCoordinator:(id<UICollectionViewDropCoordinator>)coordinator {
    
    NSIndexPath *destinationIndexPath = [[NSIndexPath alloc] init];
    if (coordinator.destinationIndexPath) {
        destinationIndexPath = coordinator.destinationIndexPath;
    } else {
        destinationIndexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    }
    
    switch (coordinator.proposal.operation) {
        case UIDropOperationMove:
            NSLog(@"");
            [self copyItem:coordinator destinationIndexPath:destinationIndexPath collectionView:collectionView];
            break;
        case UIDropOperationCopy:
            NSLog(@"");
            [self copyItem:coordinator destinationIndexPath:destinationIndexPath collectionView:collectionView];
            break;
        default:
            break;
    }
}

-(void)copyItem:(id<UICollectionViewDropCoordinator>)coordinator destinationIndexPath:(NSIndexPath *)destinationIndexPath collectionView:(UICollectionView *)collectionView {
    
    NSLog(@"Destination IndexPath : %@",destinationIndexPath);
    NSLog(@"Destination IndexPath item : %d",destinationIndexPath.item);
    
    if (grid == 2) {
        [arrDragStatusNewGrid2 replaceObjectAtIndex:destinationIndexPath.item withObject:@"1"];
       // [arrDragGrid removeLastObject];
    } else if (grid == 3) {
        [arrDragStatusNewGrid3 replaceObjectAtIndex:destinationIndexPath.item withObject:@"1"];
       // [arrDragGrid removeLastObject];
    } else if (grid == 4) {
        [arrDragStatusNewGrid4 replaceObjectAtIndex:destinationIndexPath.item withObject:@"1"];
        //[arrDragGrid removeLastObject];
    } else if (grid == 5) {
        [arrDragStatusNewGrid5 replaceObjectAtIndex:destinationIndexPath.item withObject:@"1"];
        //[arrDragGrid removeLastObject];
    } else if (grid == 6) {
        [arrDragStatusNewGrid6 replaceObjectAtIndex:destinationIndexPath.item withObject:@"1"];
       // [arrDragGrid removeLastObject];
    }
    
    [self.colSpread reloadData];
    [self.clvMini reloadData];
}

-(void)callForCreateSpread:(NSString*)title  {
    NSMutableDictionary *dcitParams = [[NSMutableDictionary alloc] init];
    [dcitParams setValue:title forKey:@"spread_name"];
    [dcitParams setValue:grid_Type forKey:@"spread_type"];
    //[dcitParams setValue:@"" forKey:@"card_name"];
    //[dcitParams setValue:@"" forKey:@"card_position"];
 
    if (grid == 2) {
        for (int i=0; i<arrDragStatusNewGrid2.count; i++) {
            if ([[arrDragStatusNewGrid2 objectAtIndex:i] isEqualToString:@"1"])  {
                [dcitParams setValue:[NSString stringWithFormat:@"%d",i] forKey:[NSString stringWithFormat:@"card_position[%d]",i]];
                [dcitParams setValue:[arrCardTitle objectAtIndex:i] forKey:[NSString stringWithFormat:@"card_name[%d]",i]];
            }
        }
    } else if (grid == 3) {
        for (int i=0; i<arrDragStatusNewGrid3.count; i++) {
            if ([[arrDragStatusNewGrid3 objectAtIndex:i] isEqualToString:@"1"])  {
                [dcitParams setValue:[NSString stringWithFormat:@"%d",i] forKey:[NSString stringWithFormat:@"card_position[%d]",i]];
                [dcitParams setValue:[arrCardTitle objectAtIndex:i] forKey:[NSString stringWithFormat:@"card_name[%d]",i]];
            }
        }
    } else if (grid == 4) {
        for (int i=0; i<arrDragStatusNewGrid4.count; i++) {
            if ([[arrDragStatusNewGrid4 objectAtIndex:i] isEqualToString:@"1"])  {
                [dcitParams setValue:[NSString stringWithFormat:@"%d",i] forKey:[NSString stringWithFormat:@"card_position[%d]",i]];
                [dcitParams setValue:[arrCardTitle objectAtIndex:i] forKey:[NSString stringWithFormat:@"card_name[%d]",i]];
            }
        }
    } else if (grid == 5) {
        for (int i=0; i<arrDragStatusNewGrid5.count; i++) {
            if ([[arrDragStatusNewGrid5 objectAtIndex:i] isEqualToString:@"1"])  {
                [dcitParams setValue:[NSString stringWithFormat:@"%d",i] forKey:[NSString stringWithFormat:@"card_position[%d]",i]];
                [dcitParams setValue:[arrCardTitle objectAtIndex:i] forKey:[NSString stringWithFormat:@"card_name[%d]",i]];
            }
        }
    } else if (grid == 6) {
        for (int i=0; i<arrDragStatusNewGrid6.count; i++) {
            if ([[arrDragStatusNewGrid6 objectAtIndex:i] isEqualToString:@"1"])  {
                [dcitParams setValue:[NSString stringWithFormat:@"%d",i] forKey:[NSString stringWithFormat:@"card_position[%d]",i]];
                [dcitParams setValue:[arrCardTitle objectAtIndex:i] forKey:[NSString stringWithFormat:@"card_name[%d]",i]];
            }
        }
    }
    
    NSLog(@" dcitParams :-%@", dcitParams);
    
    [AppDelegate addVwLoading];
    [CommunicationHandler callForCreateSpread:dcitParams Sucess:^(NSDictionary *dict, NSURLResponse *response) {
       
        dispatch_async(dispatch_get_main_queue(), ^{
            [AppDelegate removeVwLoading];
            if ([[dict objectForKey:@"error"] boolValue] == false) {
                [CommanMethods displayAlertMessageWithCallBack:nil message:[dict objectForKey:@"message"] refrence:self Success:^{
                    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
                        MySpreadListVC *myspreadVC = [[MySpreadListVC alloc] initWithNibName:@"MySpreadListVC" bundle:nil];
                        [self.navigationController pushViewController:myspreadVC animated:YES];
                    } else {
                        MySpreadListVC *myspreadVC = [[MySpreadListVC alloc] initWithNibName:@"MySpreadListVC_IPad" bundle:nil];
                        [self.navigationController pushViewController:myspreadVC animated:YES];
                    }
                }];
                
            } else {
                [CommanMethods displayAlertMessage:@"UOC" message:[dict objectForKey:@"message"] currentRef:self];
            }
        });
    } Error:^(NSError *error, NSURLResponse *response) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [AppDelegate removeVwLoading];
        });
    }];
}


-(void)callForEditSpread  {
    NSMutableDictionary *dcitParams = [[NSMutableDictionary alloc] init];
    [dcitParams setValue:[self.singleGridDate objectForKey:@"spread_name"] forKey:@"spread_name"];
    [dcitParams setValue:[self.singleGridDate objectForKey:@"spread_type"] forKey:@"spread_type"];
    [dcitParams setValue:[self.singleGridDate objectForKey:@"id"] forKey:@"id"];
    //[dcitParams setValue:@"" forKey:@"card_position"];
    
    if ([self.gridnumber isEqualToString:@"2"]) {
        for (int i=0; i<arrDragStatusNewGrid2.count; i++) {
            if ([[arrDragStatusNewGrid2 objectAtIndex:i] isEqualToString:@"1"])  {
                [dcitParams setValue:[NSString stringWithFormat:@"%d",i] forKey:[NSString stringWithFormat:@"card_position[%d]",i]];
                [dcitParams setValue:[arrCardTitle objectAtIndex:i] forKey:[NSString stringWithFormat:@"card_name[%d]",i]];
            }
        }
    } else if ([self.gridnumber isEqualToString:@"3"]) {
        for (int i=0; i<arrDragStatusNewGrid3.count; i++) {
            if ([[arrDragStatusNewGrid3 objectAtIndex:i] isEqualToString:@"1"])  {
                [dcitParams setValue:[NSString stringWithFormat:@"%d",i] forKey:[NSString stringWithFormat:@"card_position[%d]",i]];
                [dcitParams setValue:[arrCardTitle objectAtIndex:i] forKey:[NSString stringWithFormat:@"card_name[%d]",i]];
            }
        }
    } else if ([self.gridnumber isEqualToString:@"4"]) {
        for (int i=0; i<arrDragStatusNewGrid4.count; i++) {
            if ([[arrDragStatusNewGrid4 objectAtIndex:i] isEqualToString:@"1"])  {
                [dcitParams setValue:[NSString stringWithFormat:@"%d",i] forKey:[NSString stringWithFormat:@"card_position[%d]",i]];
                [dcitParams setValue:[arrCardTitle objectAtIndex:i] forKey:[NSString stringWithFormat:@"card_name[%d]",i]];
            }
        }
    } else if ([self.gridnumber isEqualToString:@"5"]) {
        for (int i=0; i<arrDragStatusNewGrid5.count; i++) {
            if ([[arrDragStatusNewGrid5 objectAtIndex:i] isEqualToString:@"1"])  {
                [dcitParams setValue:[NSString stringWithFormat:@"%d",i] forKey:[NSString stringWithFormat:@"card_position[%d]",i]];
                [dcitParams setValue:[arrCardTitle objectAtIndex:i] forKey:[NSString stringWithFormat:@"card_name[%d]",i]];
            }
        }
    } else if ([self.gridnumber isEqualToString:@"6"]) {
        for (int i=0; i<arrDragStatusNewGrid6.count; i++) {
            if ([[arrDragStatusNewGrid6 objectAtIndex:i] isEqualToString:@"1"])  {
                [dcitParams setValue:[NSString stringWithFormat:@"%d",i] forKey:[NSString stringWithFormat:@"card_position[%d]",i]];
                [dcitParams setValue:[arrCardTitle objectAtIndex:i] forKey:[NSString stringWithFormat:@"card_name[%d]",i]];
            }
        }
    }
    
    NSLog(@" dcitParams :-%@", dcitParams);
    
    [AppDelegate addVwLoading];
    [CommunicationHandler callForCreateSpread:dcitParams Sucess:^(NSDictionary *dict, NSURLResponse *response) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [AppDelegate removeVwLoading];
            if ([[dict objectForKey:@"error"] boolValue] == false) {
                [CommanMethods displayAlertMessageWithCallBack:nil message:@"Spread Update Successfully" refrence:self Success:^{
                    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
                        MySpreadListVC *myspreadVC = [[MySpreadListVC alloc] initWithNibName:@"MySpreadListVC" bundle:nil];
                        [self.navigationController pushViewController:myspreadVC animated:YES];
                    } else {
                        MySpreadListVC *myspreadVC = [[MySpreadListVC alloc] initWithNibName:@"MySpreadListVC_IPad" bundle:nil];
                        [self.navigationController pushViewController:myspreadVC animated:YES];
                    }
                }];
                
            } else {
                [CommanMethods displayAlertMessage:@"UOC" message:[dict objectForKey:@"message"] currentRef:self];
            }
        });
    } Error:^(NSError *error, NSURLResponse *response) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [AppDelegate removeVwLoading];
        });
    }];
}



@end
