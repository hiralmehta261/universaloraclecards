//
//  GeneralAdviceVC.m
//  UniversalOracleCards
//
//  Created by whiznic on 12/03/19.
//  Copyright © 2019 whiznic. All rights reserved.
//

#import "GeneralAdviceVC.h"
#import "AddingTextNoteVC.h"
#import <IQKeyboardManager/IQKeyboardManager.h>
#import "CardDisplywithNamesVC.h"
#import "SharkfoodMuteSwitchDetector.h"
#import "CommanMethods.h"
#import "AppDelegate.h"
#import "CommunicationHandler.h"
#import "PaymentVC.h"
#import "CardDisplywithNamesVC.h"
#import "TextNotesVC.h"
#import "AudioNotesVC.h"

#define DOCUMENTS_FOLDER [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"]
@import MediaPlayer;
@import AVFoundation;

@interface GeneralAdviceVC ()<AVAudioRecorderDelegate>
{
    AVAudioRecorder *recorder;
    //AVAudioPlayer *player;
    NSString *joinedString,*set_id,*s1,*s2,*s3,*s4,*s5,*s6,*cardid;
    NSNumber *item;
    int timeSec;
    int timeMin;
    NSTimer *timer;
    BOOL isAudio;
    NSMutableArray *arrNotesList;
    int value;
    NSURL *outputFileURL;
    
    AVURLAsset *audioAssetCell;
    CMTime audioDurationCell;
    
}
@property (nonatomic, assign) BOOL wrap;
@property (nonatomic,strong) SharkfoodMuteSwitchDetector* detector;
@property (nonatomic,strong) AVAudioPlayer *player;
@end

@implementation GeneralAdviceVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    /*self.detector = [SharkfoodMuteSwitchDetector shared];
    __weak GeneralAdviceVC* sself = self;
    self.detector.silentNotify = ^(BOOL silent){
        if (silent) {
            [sself.playerViewController setVolume:0.0];
            sself.btnMute.hidden = FALSE;
            sself.btnVolume.hidden = TRUE;
            NSLog(@"device is silent");
        } else {
            [sself.playerViewController setVolume:1.0];
            sself.btnMute.hidden = TRUE;
            sself.btnVolume.hidden = FALSE;
            NSLog(@"device is in ringing mode");
        }
    };*/
    
   
    self.demoiCarousel.dataSource = self;
    self.demoiCarousel.delegate = self;
    
    self.demoiCarousel.type = iCarouselTypeRotary;
    
    self.ViewReading.layer.borderColor = [UIColor clearColor].CGColor;
    self.ViewReading.layer.borderWidth = 1;
    self.ViewReading.layer.cornerRadius = 20;
    
    self.btnCheck.layer.borderColor = [UIColor clearColor].CGColor;
    self.btnCheck.layer.borderWidth = 1;
    self.btnCheck.layer.cornerRadius = 10;
    
    self.ViewText.layer.borderColor = [UIColor colorWithRed:173/255.0 green:173/255.0 blue:173/255.0 alpha:1.0].CGColor;
    self.ViewText.layer.borderWidth = 1;
    self.ViewText.layer.cornerRadius = 10;
    
    self.txtReading.delegate = self;
    
    if ([self.value isEqualToString:@"1"]) {
        self.ViewReading.hidden = FALSE;
    } else {
        self.ViewReading.hidden = TRUE;
    }
    

    
    self.tblNotesDetails.estimatedRowHeight = 160.0;
    self.tblNotesDetails.delegate = self;
    self.tblNotesDetails.dataSource = self;
    
    
    self.lblTitle.text = self.title;
   
    
    NSLog(@"getid :- %@", self.getid);
    NSLog(@"Current Item Index: %ld",(long)_demoiCarousel.currentItemIndex);
    
    isAudio = false;
    
    timeSec = 0;
    timeMin = 0;
    
   value = -1;
    
    
    if ([self.get_Selected_Value isEqualToString:@"1"]) {
        self.demoiCarousel.scrollEnabled = FALSE;
    }
    
    if (self.get_cardDataCustomSpread.count == 1) {
        self.demoiCarousel.scrollEnabled = FALSE;
    }
    
    
    NSLog(@" get_cardData :- %@", self.get_cardData);
    
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
        UINib *cellnib = [UINib nibWithNibName:@"TextNotesVC" bundle:nil];
        [self.tblNotesDetails registerNib:cellnib forCellReuseIdentifier:TextNotesVCID];
        
        
        UINib *cellnib1 = [UINib nibWithNibName:@"AudioNotesVC" bundle:nil];
        [self.tblNotesDetails registerNib:cellnib1 forCellReuseIdentifier:AudioNotesVCID];
    } else
    {
        UINib *cellnib = [UINib nibWithNibName:@"TextNotesVC_IPad" bundle:nil];
        [self.tblNotesDetails registerNib:cellnib forCellReuseIdentifier:TextNotesVCID];
        
        
        UINib *cellnib1 = [UINib nibWithNibName:@"AudioNotesVC_IPad" bundle:nil];
        [self.tblNotesDetails registerNib:cellnib1 forCellReuseIdentifier:AudioNotesVCID];
    }
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    if (self.get_cardData.count == 1) {
        self.demoiCarousel.scrollEnabled = FALSE;
        joinedString = [self.get_cardData componentsJoinedByString:@","];
        if ([CommanMethods isNetworkRechable]) {
            [self callForGetNotesList];
        } else {
            [CommanMethods displayAlertMessage:key_Informaction message:kInternetConnectionMessage currentRef:self];
        }
    } else {
        NSString *joinedString1 = [self.get_cardData componentsJoinedByString:@","];
        NSString *timeCreated = joinedString1;
        NSArray *timeArray = [timeCreated componentsSeparatedByString:@","];
        joinedString = [timeArray objectAtIndex:0];
        NSLog(@" joinedString :- %@", joinedString);
        if ([CommanMethods isNetworkRechable]) {
            [self callForGetNotesList];
        } else {
            [CommanMethods displayAlertMessage:key_Informaction message:kInternetConnectionMessage currentRef:self];
        }
    }
    
    NSString *stringVideoName = @"stars_animated.mp4";
    NSString *filepath = [[NSBundle mainBundle] pathForResource:stringVideoName ofType:nil];

    NSURL *fileURL = [NSURL fileURLWithPath:filepath];
    self.playerViewController = [AVPlayer playerWithURL:fileURL];
    self.playerViewController.actionAtItemEnd = AVPlayerActionAtItemEndNone;
    AVPlayerLayer *videoLayer = [AVPlayerLayer playerLayerWithPlayer:self.playerViewController];
    videoLayer.frame = self.view.bounds;
    videoLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
    [self.view.layer addSublayer:videoLayer];

    [self.playerViewController play];

    NSString *savedValue = [[NSUserDefaults standardUserDefaults]
                            stringForKey:@"audio"];
    if([savedValue isEqualToString:@"off"]) {
        self.btnMute.hidden = FALSE;
        self.btnVolume.hidden = TRUE;
        [self.playerViewController setVolume:0.0];
    } else {
        self.btnMute.hidden = TRUE;
        self.btnVolume.hidden = FALSE;
        [self.playerViewController setVolume:1.0];
    }

    NSLog(@" savedValue :- %@", savedValue);

    self.viewDisply.layer.zPosition = 1;

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(itemDidFinishPlaying:) name:AVPlayerItemDidPlayToEndTimeNotification object:[self.playerViewController currentItem]];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationDidBecomeActive:) name:UIApplicationDidEnterBackgroundNotification object:nil];

    [[UIApplication sharedApplication] beginReceivingRemoteControlEvents];
    [self becomeFirstResponder];
    //[self btnCheck:nil];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [self.playerViewController pause];
    [super viewWillDisappear:animated];
    [[UIApplication sharedApplication] endReceivingRemoteControlEvents];
    [self resignFirstResponder];
}


- (void)remoteControlReceivedWithEvent:(UIEvent *)event {
    switch (event.subtype) {
        case UIEventSubtypeRemoteControlTogglePlayPause:
            if([self.playerViewController rate] == 0){
                [self.playerViewController play];
            } else {
                [self.playerViewController pause];
            }
            break;
        case UIEventSubtypeRemoteControlPlay:
            [self.playerViewController play];
            break;
        case UIEventSubtypeRemoteControlPause:
            [self.playerViewController pause];
            break;
        default:
            break;
    }
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    self.detector = [SharkfoodMuteSwitchDetector shared];
    __weak GeneralAdviceVC* sself = self;
    self.detector.silentNotify = ^(BOOL silent){
        if (silent) {
            
            NSLog(@"device is silent");
        } else {
            
            NSLog(@"device is in ringing mode");
            [self.playerViewController performSelector:@selector(play) withObject:nil afterDelay:0.01];
        }
    };
}


- (void)itemDidFinishPlaying:(NSNotification *)notification {
    AVPlayerItem *player = [notification object];
    [player seekToTime:kCMTimeZero];
}

-(void)viewWillAppear:(BOOL)animated {
    //[self StartTimer];
}

-(void) StartTimer
{
    timer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(timerTick:) userInfo:nil repeats:YES];
    [[NSRunLoop currentRunLoop] addTimer:timer forMode:NSDefaultRunLoopMode];
}

//Event called every time the NSTimer ticks.
- (void)timerTick:(NSTimer *)timer
{
    timeSec++;
    if (timeSec == 60)
    {
        timeSec = 0;
        timeMin++;
    }
    //Format the string 00:00
    NSString* timeNow = [NSString stringWithFormat:@"%02d:%02d", timeMin, timeSec];
    //Display on your label
    //[timeLabel setStringValue:timeNow];
    self.lblAudioRecording.text= timeNow;
    isAudio = true;
}

//Call this to stop the timer event(could use as a 'Pause' or 'Reset')
- (void) StopTimer
{
    [timer invalidate];
    timeSec = 0;
    timeMin = 0;
    //Since we reset here, and timerTick won't update your label again, we need to refresh it again.
    //Format the string in 00:00
    NSString* timeNow = [NSString stringWithFormat:@"%02d:%02d", timeMin, timeSec];
    //Display on your label
    // [timeLabel setStringValue:timeNow];
    self.lblAudioRecording.text= timeNow;
}



#pragma TEXTFIELD-DELEGATE

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}


- (NSInteger)numberOfItemsInCarousel:(__unused iCarousel *)carousel
{
    if ([self.passid isEqualToString:@"1"]) {
        return self.get_cardDataCustomSpread.count;
    } else {
        if ([self.get_Selected_Value isEqualToString:@"1"]) {
            return 1;
        } else if ([self.get_Selected_Value isEqualToString:@"3"]) {
            return 3;
        } else if ([self.get_Selected_Value isEqualToString:@"6"]) {
            return 6;
        } else if ([self.get_Selected_Value isEqualToString:@"5"]) {
            return 5;
        } else {
            return 0;
        }
    }
    
}

- (UIView *)carousel:(__unused iCarousel *)carousel viewForItemAtIndex:(NSInteger)index reusingView:(UIView *)view
{
    if ([self.passid isEqualToString:@"1"]) {
        if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
            if (view == nil)
            {
               
                view = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 270.0, 400.0)];
                view.layer.backgroundColor = [UIColor clearColor].CGColor;
                self.imgCard.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@",[self.get_cardDataCustomSpread objectAtIndex:index]]];
                
                ((UIImageView *)view).image = self.imgCard.image;
            }
            else
            {
                view = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 270.0, 400.0)];
                view.layer.backgroundColor = [UIColor clearColor].CGColor;
                self.imgCard.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@",[self.get_cardDataCustomSpread objectAtIndex:index]]];
                ((UIImageView *)view).image = self.imgCard.image;
            }
        } else {
            if (view == nil)
            {
                view = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 270.0, 400.0)];
                view.layer.backgroundColor = [UIColor clearColor].CGColor;
                self.imgCard.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@",[self.get_cardDataCustomSpread objectAtIndex:index]]];
                ((UIImageView *)view).image = self.imgCard.image;
            }
            else
            {
                view = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 270.0, 400.0)];
                view.layer.backgroundColor = [UIColor clearColor].CGColor;
                self.imgCard.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@",[self.get_cardDataCustomSpread objectAtIndex:index]]];
                ((UIImageView *)view).image = self.imgCard.image;
            }
        }
        
        view.layer.borderColor = [UIColor colorWithRed:173/255.0 green:173/255.0 blue:173/255.0 alpha:1.0].CGColor;
        view.layer.borderWidth = 1;
        view.layer.cornerRadius = 20;
        return view;
    } else {
        if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
            if (view == nil)
            {
                view = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 270.0, 400.0)];
                view.layer.backgroundColor = [UIColor clearColor].CGColor;
                self.imgCard.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@",[self.get_cardData objectAtIndex:index]]];
                
                ((UIImageView *)view).image = self.imgCard.image;
            }
            else
            {
                view = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 270.0, 400.0)];
                view.layer.backgroundColor = [UIColor clearColor].CGColor;
                self.imgCard.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@",[self.get_cardData objectAtIndex:index]]];
                ((UIImageView *)view).image = self.imgCard.image;
            }
        } else {
            if (view == nil)
            {
                view = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 270.0, 400.0)];
                view.layer.backgroundColor = [UIColor clearColor].CGColor;
                self.imgCard.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@",[self.get_cardData objectAtIndex:index]]];
                ((UIImageView *)view).image = self.imgCard.image;
            }
            else
            {
                view = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 270.0, 400.0)];
                view.layer.backgroundColor = [UIColor clearColor].CGColor;
                self.imgCard.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@",[self.get_cardData objectAtIndex:index]]];
                ((UIImageView *)view).image = self.imgCard.image;
            }
        }
        
        view.layer.borderColor = [UIColor colorWithRed:173/255.0 green:173/255.0 blue:173/255.0 alpha:1.0].CGColor;
        view.layer.borderWidth = 1;
        view.layer.cornerRadius = 20;
        return view;
    }
    
    
}

- (NSInteger)numberOfPlaceholdersInCarousel:(__unused iCarousel *)carousel
{
    //note: placeholder views are only displayed on some carousels if wrapping is disabled
    return 2;
}

- (UIView *)carousel:(__unused iCarousel *)carousel placeholderViewAtIndex:(NSInteger)index reusingView:(UIView *)view
{
    UILabel *label = nil;
    
    if ([self.passid isEqualToString:@"1"]) {
        if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
            if (view == nil)
            {
                view = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 270.0, 400.0)];
                view.layer.backgroundColor = [UIColor clearColor].CGColor;
                self.imgCard.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@",[self.get_cardDataCustomSpread objectAtIndex:index]]];
                ((UIImageView *)view).image = self.imgCard.image;
            }
            else
            {
                view = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 270.0, 400.0)];
                view.layer.backgroundColor = [UIColor clearColor].CGColor;
                self.imgCard.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@",[self.get_cardDataCustomSpread objectAtIndex:index]]];
                ((UIImageView *)view).image = self.imgCard.image;
            }
        } else {
            if (view == nil)
            {
                view = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 270.0, 400.0)];
                view.layer.backgroundColor = [UIColor clearColor].CGColor;
                self.imgCard.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@",[self.get_cardDataCustomSpread objectAtIndex:index]]];
                ((UIImageView *)view).image = self.imgCard.image;
            }
            else
            {
                view = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 270.0, 400.0)];
                view.layer.backgroundColor = [UIColor clearColor].CGColor;
                self.imgCard.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@",[self.get_cardDataCustomSpread objectAtIndex:index]]];
                ((UIImageView *)view).image = self.imgCard.image;
            }
        }
        
        
        label.text = (index == 0)? @"[": @"]";
        
        return view;
    } else{
        if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
            if (view == nil)
            {
                view = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 270.0, 400.0)];
                view.layer.backgroundColor = [UIColor clearColor].CGColor;
                self.imgCard.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@",[self.get_cardData objectAtIndex:index]]];
                ((UIImageView *)view).image = self.imgCard.image;
            }
            else
            {
                view = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 270.0, 400.0)];
                view.layer.backgroundColor = [UIColor clearColor].CGColor;
                self.imgCard.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@",[self.get_cardData objectAtIndex:index]]];
                ((UIImageView *)view).image = self.imgCard.image;
            }
        } else {
            if (view == nil)
            {
                view = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 270.0, 400.0)];
                view.layer.backgroundColor = [UIColor clearColor].CGColor;
                self.imgCard.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@",[self.get_cardData objectAtIndex:index]]];
                ((UIImageView *)view).image = self.imgCard.image;
            }
            else
            {
                view = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 270.0, 400.0)];
                view.layer.backgroundColor = [UIColor clearColor].CGColor;
                self.imgCard.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@",[self.get_cardData objectAtIndex:index]]];
                ((UIImageView *)view).image = self.imgCard.image;
            }
        }
        
        
        label.text = (index == 0)? @"[": @"]";
        
        return view;
    }
    
}

- (CATransform3D)carousel:(__unused iCarousel *)carousel itemTransformForOffset:(CGFloat)offset baseTransform:(CATransform3D)transform
{
    
    transform = CATransform3DRotate(transform, M_PI / 8.0, 0.0, 1.0, 0.0);
    return CATransform3DTranslate(transform, 0.0, 0.0, offset * self.demoiCarousel.itemWidth);
}


#pragma mark -
#pragma mark iCarousel taps

- (void)carousel:(__unused iCarousel *)carousel didSelectItemAtIndex:(NSInteger)index
{
    if ([self.passid isEqualToString:@"1"]) {
        item = (self.get_cardDataCustomSpread)[(NSUInteger)index];
    } else {
        item = (self.get_cardData)[(NSUInteger)index];
    }
   // NSLog(@"Tapped view number: %@", item);
}

- (void)carouselCurrentItemIndexDidChange:(__unused iCarousel *)carousel
{
    NSLog(@"Index: %@", @(self.demoiCarousel.currentItemIndex));
    
    for (int i = 0; i<self.get_cardData.count; i++) {
        if (self.demoiCarousel.currentItemIndex == i) {
            NSLog(@"value of i :- %@", [self.get_cardData objectAtIndex:i]);
            cardid = [self.get_cardData objectAtIndex:i];
        }
    }
    if ([CommanMethods isNetworkRechable]) {
        [self callForGetNotesList];
    } else {
        [CommanMethods displayAlertMessage:key_Informaction message:kInternetConnectionMessage currentRef:self];
    }
    
}

- (CGFloat)carousel:(iCarousel *)carousel valueForOption:(iCarouselOption)option withDefault:(CGFloat)value
{
    CGFloat result;
    
    switch(option)
    {
        case iCarouselOptionSpacing:
            result = 1.2; // If the width of your items is 40 e.g, the spacing would be 4 px.
            break;
        case iCarouselOptionFadeMin:
            return 0;
            break;
        case iCarouselOptionFadeMax:
            return 0;
            break;
        case iCarouselOptionFadeRange:
            return 3;
            break;
        case iCarouselOptionShowBackfaces:
            return NO;
            break;
        default:
            result = value;
            break;
    }
    
    return result;
}

- (IBAction)btnBack:(id)sender {
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
        [self.navigationController popViewControllerAnimated:YES];
    } else {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (IBAction)btnVolume:(id)sender {
    if ([self.btnVolume.accessibilityHint isEqualToString:@"1"]) {
        self.btnVolume.accessibilityHint = @"0";
        self.btnVolume.hidden = TRUE;
        self.btnMute.hidden = FALSE;
        [self.playerViewController setVolume:0.0];
        NSUserDefaults *audiocheck = [NSUserDefaults standardUserDefaults];
        [audiocheck setValue:@"off" forKey:@"audio"];
        NSLog(@"audiocheck :- %@", audiocheck);
        [[NSUserDefaults standardUserDefaults] synchronize];
        
    } else {
        self.btnVolume.accessibilityHint = @"1";
        self.btnVolume.hidden = FALSE;
        self.btnMute.hidden = TRUE;
        [self.playerViewController setVolume:1.0];
        NSUserDefaults *audiocheck = [NSUserDefaults standardUserDefaults];
        [audiocheck setValue:@"on" forKey:@"audio"];
        NSLog(@"audiocheck :- %@", audiocheck);
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    
}

- (IBAction)btnVoiceNote:(id)sender {
    /*if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
        PaymentVC *vc = [[PaymentVC alloc] initWithNibName:@"PaymentVC" bundle:nil];
        [self addChildViewController:vc];
        self.viewDisply.layer.zPosition = 0;
        vc.view.frame = self.view.frame;
        [self.view addSubview:vc.view];
        [vc didMoveToParentViewController:self];
    }
    
    else {
        PaymentVC *vc = [[PaymentVC alloc] initWithNibName:@"PaymentVC_IPad" bundle:nil];
        [self addChildViewController:vc];
        self.viewDisply.layer.zPosition = 0;
        vc.view.frame = self.view.frame;
        [self.view addSubview:vc.view];
        [vc didMoveToParentViewController:self];
    }*/
    //Recorde file login.
    NSString *strName = [self randomStringWithLength:10];
    strName = [NSString stringWithFormat:@"%@.m4a",strName];
    
    NSArray *dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docsDir = [dirPaths objectAtIndex:0];
    outputFileURL = [NSURL fileURLWithPath:[docsDir stringByAppendingPathComponent:strName]];
    NSDictionary *recordSettings = [NSDictionary dictionaryWithObjectsAndKeys:
                                    [NSNumber numberWithInt: kAudioFormatMPEG4AAC], AVFormatIDKey,
                                    [NSNumber numberWithFloat:16000.0], AVSampleRateKey,
                                    [NSNumber numberWithInt: 1], AVNumberOfChannelsKey,
                                    nil];
    NSError *error = nil;
    recorder = [[AVAudioRecorder alloc] initWithURL:outputFileURL settings:recordSettings error:&error];
    recorder.delegate = self;
    [recorder prepareToRecord];
    
    AVAudioSession *session = [AVAudioSession sharedInstance];
    [session setCategory:AVAudioSessionCategoryPlayAndRecord error:nil];
    [session setActive:YES withOptions:kAudioSessionProperty_OverrideCategoryMixWithOthers error:nil];
    [session setActive:YES error:nil];
    
    [recorder record];
    [self StartTimer];
    
    self.viewAudio.hidden = FALSE;
}


- (IBAction)btnCheck:(id)sender {
    [recorder stop];
    //AVAudioSession *session = [AVAudioSession sharedInstance];
    //int flags = AVAudioSessionSetActiveFlags_NotifyOthersOnDeactivation;
    //[session setActive:NO withFlags:flags error:nil];
}

-(void)audioRecorderDidFinishRecording:(AVAudioRecorder *)recorder successfully:(BOOL)flag {
    if (flag) {
        if ([CommanMethods isNetworkRechable]) {
            [timer invalidate];
            timer = nil;
            timeSec = 0;
            timeMin = 0;
            NSString* timeNow = [NSString stringWithFormat:@"%02d:%02d", timeMin, timeSec];
            self.lblAudioRecording.text= timeNow;
            
            [self callForSaveAudio:outputFileURL];
            self.viewAudio.hidden = TRUE;
            
            dispatch_time_t restartTime = dispatch_time(DISPATCH_TIME_NOW,
                                                        1.5 * NSEC_PER_SEC);
            
            dispatch_after(restartTime, dispatch_get_global_queue(0, 0), ^{
                
                [[AVAudioSession sharedInstance] setActive:YES error:nil];
                [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:nil];
                
                NSString *stringVideoName = @"stars_animated.mp4";
                NSString *filepath = [[NSBundle mainBundle] pathForResource:stringVideoName ofType:nil];
                
                NSURL *fileURL = [NSURL fileURLWithPath:filepath];
                self.playerViewController = [AVPlayer playerWithURL:fileURL];
                self.playerViewController.actionAtItemEnd = AVPlayerActionAtItemEndNone;
                AVPlayerLayer *videoLayer = [AVPlayerLayer playerLayerWithPlayer:self.playerViewController];
                videoLayer.frame = self.view.bounds;
                videoLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
                [self.view.layer addSublayer:videoLayer];
                
                [self.playerViewController play];
                
                NSString *savedValue = [[NSUserDefaults standardUserDefaults]
                                        stringForKey:@"audio"];
                if([savedValue isEqualToString:@"off"]) {
                    self.btnMute.hidden = FALSE;
                    self.btnVolume.hidden = TRUE;
                    [self.playerViewController setVolume:0.0];
                } else {
                    self.btnMute.hidden = TRUE;
                    self.btnVolume.hidden = FALSE;
                    [self.playerViewController setVolume:1.0];
                }
                
                NSLog(@" savedValue :- %@", savedValue);
                
                self.viewDisply.layer.zPosition = 1;
                
                [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(itemDidFinishPlaying:) name:AVPlayerItemDidPlayToEndTimeNotification object:[self.playerViewController currentItem]];
                
            });
            
        } else {
            [CommanMethods displayAlertMessage:key_Informaction message:kInternetConnectionMessage currentRef:self];
        }
    }
}

- (void) audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag{
}

- (IBAction)btnTextEdit:(id)sender {
    if ([self.passid isEqualToString:@"1"]) {
        
        for (int i = 0; i<self.get_cardDataCustomSpread.count; i++) {
             if (self.demoiCarousel.currentItemIndex == i) {
                 NSLog(@"value of i :- %@", [self.get_cardDataCustomSpread objectAtIndex:i]);
                 cardid = [self.get_cardDataCustomSpread objectAtIndex:i];
             }
        }
        NSLog(@"cardid :- %@", cardid);
        if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
            AddingTextNoteVC *textnoteVC = [[AddingTextNoteVC alloc] initWithNibName:@"AddingTextNoteVC" bundle:nil];
            textnoteVC.get_cardID = [NSString stringWithFormat:@"%@",cardid];
            textnoteVC.getSaving_ID = self.getid;
            textnoteVC.get_cardid = self.get_cardDataCustomSpread;
            [self.navigationController pushViewController:textnoteVC animated:YES];
        } else {
            AddingTextNoteVC *textnoteVC = [[AddingTextNoteVC alloc] initWithNibName:@"AddingTextNotesVC_IPad" bundle:nil];
            textnoteVC.get_cardID = cardid;
            textnoteVC.getSaving_ID = self.getid;
            textnoteVC.get_cardid = self.get_cardDataCustomSpread;
            [self.navigationController pushViewController:textnoteVC animated:YES];
        }
   
    } else {
        if ([self.get_Selected_Value isEqualToString:@"1"]) {
            joinedString = [self.get_cardData componentsJoinedByString:@","];
            if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
                AddingTextNoteVC *textnoteVC = [[AddingTextNoteVC alloc] initWithNibName:@"AddingTextNoteVC" bundle:nil];
                textnoteVC.get_cardID = joinedString;
                textnoteVC.getSaving_ID = self.getid;
                textnoteVC.get_cardid = self.get_cardData;
                NSLog(@" textnoteVC.get_cardid :- %@", textnoteVC.get_cardid);
                [self.navigationController pushViewController:textnoteVC animated:YES];
            } else {
                AddingTextNoteVC *textnoteVC = [[AddingTextNoteVC alloc] initWithNibName:@"AddingTextNotesVC_IPad" bundle:nil];
                textnoteVC.get_cardID = joinedString;
                textnoteVC.getSaving_ID = self.getid;
                textnoteVC.get_cardid = self.get_cardData;
                [self.navigationController pushViewController:textnoteVC animated:YES];
            }
        }
        
        else if ([self.get_Selected_Value isEqualToString:@"3"]) {
            s1 = [self.get_cardData componentsJoinedByString:@","];
            NSString *timeCreated = s1;
            NSArray *timeArray = [timeCreated componentsSeparatedByString:@","];
            if (self.demoiCarousel.currentItemIndex == 0) {
                joinedString = [timeArray objectAtIndex:0];
                if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
                    AddingTextNoteVC *textnoteVC = [[AddingTextNoteVC alloc] initWithNibName:@"AddingTextNoteVC" bundle:nil];
                    textnoteVC.get_cardID = joinedString;
                    textnoteVC.getSaving_ID = self.getid;
                    textnoteVC.get_cardid = self.get_cardData;
                    [self.navigationController pushViewController:textnoteVC animated:YES];
                } else {
                    AddingTextNoteVC *textnoteVC = [[AddingTextNoteVC alloc] initWithNibName:@"AddingTextNotesVC_IPad" bundle:nil];
                    textnoteVC.get_cardID = joinedString;
                    textnoteVC.getSaving_ID = self.getid;
                    textnoteVC.get_cardid = self.get_cardData;
                    [self.navigationController pushViewController:textnoteVC animated:YES];
                }
            } else if (self.demoiCarousel.currentItemIndex == 1) {
                joinedString = [timeArray objectAtIndex:1];
                if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
                    AddingTextNoteVC *textnoteVC = [[AddingTextNoteVC alloc] initWithNibName:@"AddingTextNoteVC" bundle:nil];
                    textnoteVC.get_cardID = joinedString;
                    textnoteVC.getSaving_ID = self.getid;
                    textnoteVC.get_cardid = self.get_cardData;
                    [self.navigationController pushViewController:textnoteVC animated:YES];
                } else {
                    AddingTextNoteVC *textnoteVC = [[AddingTextNoteVC alloc] initWithNibName:@"AddingTextNotesVC_IPad" bundle:nil];
                    textnoteVC.get_cardID = joinedString;
                    textnoteVC.getSaving_ID = self.getid;
                    textnoteVC.get_cardid = self.get_cardData;
                    [self.navigationController pushViewController:textnoteVC animated:YES];
                }
            } else {
                joinedString = [timeArray objectAtIndex:2];
                if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
                    AddingTextNoteVC *textnoteVC = [[AddingTextNoteVC alloc] initWithNibName:@"AddingTextNoteVC" bundle:nil];
                    textnoteVC.get_cardID = joinedString;
                    textnoteVC.getSaving_ID = self.getid;
                    textnoteVC.get_cardid = self.get_cardData;
                    [self.navigationController pushViewController:textnoteVC animated:YES];
                } else {
                    AddingTextNoteVC *textnoteVC = [[AddingTextNoteVC alloc] initWithNibName:@"AddingTextNotesVC_IPad" bundle:nil];
                    textnoteVC.get_cardID = joinedString;
                    textnoteVC.getSaving_ID = self.getid;
                    textnoteVC.get_cardid = self.get_cardData;
                    [self.navigationController pushViewController:textnoteVC animated:YES];
                }
            }
        }
        
        else if ([self.get_Selected_Value isEqualToString:@"6"]) {
            s1 = [self.get_cardData componentsJoinedByString:@","];
            NSString *timeCreated = s1;
            NSArray *timeArray = [timeCreated componentsSeparatedByString:@","];
            if (self.demoiCarousel.currentItemIndex == 0) {
                joinedString = [timeArray objectAtIndex:0];
                if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
                    AddingTextNoteVC *textnoteVC = [[AddingTextNoteVC alloc] initWithNibName:@"AddingTextNoteVC" bundle:nil];
                    textnoteVC.get_cardID = joinedString;
                    textnoteVC.getSaving_ID = self.getid;
                    textnoteVC.get_cardid = self.get_cardData;
                    [self.navigationController pushViewController:textnoteVC animated:YES];
                } else {
                    AddingTextNoteVC *textnoteVC = [[AddingTextNoteVC alloc] initWithNibName:@"AddingTextNotesVC_IPad" bundle:nil];
                    textnoteVC.get_cardID = joinedString;
                    textnoteVC.getSaving_ID = self.getid;
                    textnoteVC.get_cardid = self.get_cardData;
                    [self.navigationController pushViewController:textnoteVC animated:YES];
                }
            } else if (self.demoiCarousel.currentItemIndex == 1) {
                joinedString = [timeArray objectAtIndex:1];
                if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
                    AddingTextNoteVC *textnoteVC = [[AddingTextNoteVC alloc] initWithNibName:@"AddingTextNoteVC" bundle:nil];
                    textnoteVC.get_cardID = joinedString;
                    textnoteVC.getSaving_ID = self.getid;
                    textnoteVC.get_cardid = self.get_cardData;
                    [self.navigationController pushViewController:textnoteVC animated:YES];
                } else {
                    AddingTextNoteVC *textnoteVC = [[AddingTextNoteVC alloc] initWithNibName:@"AddingTextNotesVC_IPad" bundle:nil];
                    textnoteVC.get_cardID = joinedString;
                    textnoteVC.getSaving_ID = self.getid;
                    textnoteVC.get_cardid = self.get_cardData;
                    [self.navigationController pushViewController:textnoteVC animated:YES];
                }
            } else if (self.demoiCarousel.currentItemIndex == 2) {
                joinedString = [timeArray objectAtIndex:2];
                if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
                    AddingTextNoteVC *textnoteVC = [[AddingTextNoteVC alloc] initWithNibName:@"AddingTextNoteVC" bundle:nil];
                    textnoteVC.get_cardID = joinedString;
                    textnoteVC.getSaving_ID = self.getid;
                    textnoteVC.get_cardid = self.get_cardData;
                    [self.navigationController pushViewController:textnoteVC animated:YES];
                } else {
                    AddingTextNoteVC *textnoteVC = [[AddingTextNoteVC alloc] initWithNibName:@"AddingTextNotesVC_IPad" bundle:nil];
                    textnoteVC.get_cardID = joinedString;
                    textnoteVC.getSaving_ID = self.getid;
                    textnoteVC.get_cardid = self.get_cardData;
                    [self.navigationController pushViewController:textnoteVC animated:YES];
                }
            } else if (self.demoiCarousel.currentItemIndex == 3) {
                joinedString = [timeArray objectAtIndex:3];
                if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
                    AddingTextNoteVC *textnoteVC = [[AddingTextNoteVC alloc] initWithNibName:@"AddingTextNoteVC" bundle:nil];
                    textnoteVC.get_cardID = joinedString;
                    textnoteVC.getSaving_ID = self.getid;
                    textnoteVC.get_cardid = self.get_cardData;
                    [self.navigationController pushViewController:textnoteVC animated:YES];
                } else {
                    AddingTextNoteVC *textnoteVC = [[AddingTextNoteVC alloc] initWithNibName:@"AddingTextNotesVC_IPad" bundle:nil];
                    textnoteVC.get_cardID = joinedString;
                    textnoteVC.getSaving_ID = self.getid;
                    textnoteVC.get_cardid = self.get_cardData;
                    [self.navigationController pushViewController:textnoteVC animated:YES];
                }
            } else if (self.demoiCarousel.currentItemIndex == 4) {
                joinedString = [timeArray objectAtIndex:4];
                if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
                    AddingTextNoteVC *textnoteVC = [[AddingTextNoteVC alloc] initWithNibName:@"AddingTextNoteVC" bundle:nil];
                    textnoteVC.get_cardID = joinedString;
                    textnoteVC.getSaving_ID = self.getid;
                    textnoteVC.get_cardid = self.get_cardData;
                    [self.navigationController pushViewController:textnoteVC animated:YES];
                } else {
                    AddingTextNoteVC *textnoteVC = [[AddingTextNoteVC alloc] initWithNibName:@"AddingTextNotesVC_IPad" bundle:nil];
                    textnoteVC.get_cardID = joinedString;
                    textnoteVC.getSaving_ID = self.getid;
                    textnoteVC.get_cardid = self.get_cardData;
                    [self.navigationController pushViewController:textnoteVC animated:YES];
                }
            } else {
                joinedString = [timeArray objectAtIndex:5];
                if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
                    AddingTextNoteVC *textnoteVC = [[AddingTextNoteVC alloc] initWithNibName:@"AddingTextNoteVC" bundle:nil];
                    textnoteVC.get_cardID = joinedString;
                    textnoteVC.getSaving_ID = self.getid;
                    textnoteVC.get_cardid = self.get_cardData;
                    [self.navigationController pushViewController:textnoteVC animated:YES];
                } else {
                    AddingTextNoteVC *textnoteVC = [[AddingTextNoteVC alloc] initWithNibName:@"AddingTextNotesVC_IPad" bundle:nil];
                    textnoteVC.get_cardID = joinedString;
                    textnoteVC.getSaving_ID = self.getid;
                    textnoteVC.get_cardid = self.get_cardData;
                    [self.navigationController pushViewController:textnoteVC animated:YES];
                }
            }
        }
        
        else if ([self.get_Selected_Value isEqualToString:@"5"]) {
            s1 = [self.get_cardData componentsJoinedByString:@","];
            NSString *timeCreated = s1;
            NSArray *timeArray = [timeCreated componentsSeparatedByString:@","];
            if (self.demoiCarousel.currentItemIndex == 0) {
                joinedString = [timeArray objectAtIndex:0];
                if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
                    AddingTextNoteVC *textnoteVC = [[AddingTextNoteVC alloc] initWithNibName:@"AddingTextNoteVC" bundle:nil];
                    textnoteVC.get_cardID = joinedString;
                    textnoteVC.getSaving_ID = self.getid;
                    textnoteVC.get_cardid = self.get_cardData;
                    [self.navigationController pushViewController:textnoteVC animated:YES];
                } else {
                    AddingTextNoteVC *textnoteVC = [[AddingTextNoteVC alloc] initWithNibName:@"AddingTextNotesVC_IPad" bundle:nil];
                    textnoteVC.get_cardID = joinedString;
                    textnoteVC.getSaving_ID = self.getid;
                    textnoteVC.get_cardid = self.get_cardData;
                    [self.navigationController pushViewController:textnoteVC animated:YES];
                }
            } else if (self.demoiCarousel.currentItemIndex == 1) {
                joinedString = [timeArray objectAtIndex:1];
                if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
                    AddingTextNoteVC *textnoteVC = [[AddingTextNoteVC alloc] initWithNibName:@"AddingTextNoteVC" bundle:nil];
                    textnoteVC.get_cardID = joinedString;
                    textnoteVC.getSaving_ID = self.getid;
                    textnoteVC.get_cardid = self.get_cardData;
                    [self.navigationController pushViewController:textnoteVC animated:YES];
                } else {
                    AddingTextNoteVC *textnoteVC = [[AddingTextNoteVC alloc] initWithNibName:@"AddingTextNotesVC_IPad" bundle:nil];
                    textnoteVC.get_cardID = joinedString;
                    textnoteVC.getSaving_ID = self.getid;
                    textnoteVC.get_cardid = self.get_cardData;
                    [self.navigationController pushViewController:textnoteVC animated:YES];
                }
            } else if (self.demoiCarousel.currentItemIndex == 2) {
                joinedString = [timeArray objectAtIndex:2];
                if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
                    AddingTextNoteVC *textnoteVC = [[AddingTextNoteVC alloc] initWithNibName:@"AddingTextNoteVC" bundle:nil];
                    textnoteVC.get_cardID = joinedString;
                    textnoteVC.getSaving_ID = self.getid;
                    textnoteVC.get_cardid = self.get_cardData;
                    [self.navigationController pushViewController:textnoteVC animated:YES];
                } else {
                    AddingTextNoteVC *textnoteVC = [[AddingTextNoteVC alloc] initWithNibName:@"AddingTextNotesVC_IPad" bundle:nil];
                    textnoteVC.get_cardID = joinedString;
                    textnoteVC.getSaving_ID = self.getid;
                    textnoteVC.get_cardid = self.get_cardData;
                    [self.navigationController pushViewController:textnoteVC animated:YES];
                }
            } else if (self.demoiCarousel.currentItemIndex == 3) {
                joinedString = [timeArray objectAtIndex:3];
                if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
                    AddingTextNoteVC *textnoteVC = [[AddingTextNoteVC alloc] initWithNibName:@"AddingTextNoteVC" bundle:nil];
                    textnoteVC.get_cardID = joinedString;
                    textnoteVC.getSaving_ID = self.getid;
                    textnoteVC.get_cardid = self.get_cardData;
                    [self.navigationController pushViewController:textnoteVC animated:YES];
                } else {
                    AddingTextNoteVC *textnoteVC = [[AddingTextNoteVC alloc] initWithNibName:@"AddingTextNotesVC_IPad" bundle:nil];
                    textnoteVC.get_cardID = joinedString;
                    textnoteVC.getSaving_ID = self.getid;
                    textnoteVC.get_cardid = self.get_cardData;
                    [self.navigationController pushViewController:textnoteVC animated:YES];
                }
            } else {
                joinedString = [timeArray objectAtIndex:4];
                if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
                    AddingTextNoteVC *textnoteVC = [[AddingTextNoteVC alloc] initWithNibName:@"AddingTextNoteVC" bundle:nil];
                    textnoteVC.get_cardID = joinedString;
                    textnoteVC.getSaving_ID = self.getid;
                    textnoteVC.get_cardid = self.get_cardData;
                    [self.navigationController pushViewController:textnoteVC animated:YES];
                } else {
                    AddingTextNoteVC *textnoteVC = [[AddingTextNoteVC alloc] initWithNibName:@"AddingTextNotesVC_IPad" bundle:nil];
                    textnoteVC.get_cardID = joinedString;
                    textnoteVC.getSaving_ID = self.getid;
                    textnoteVC.get_cardid = self.get_cardData;
                    [self.navigationController pushViewController:textnoteVC animated:YES];
                }
            }
        }
    
    }
    
}

#pragma mark - TablViewCellAction

-(IBAction)btnCellDelete:(id)sender {
    
    NSIndexPath *indexPath;
    UITableView *tableView;
    
    
    NSDictionary *dictData = [[NSDictionary alloc] init];
    dictData = [arrNotesList objectAtIndex:[sender tag]];
    
    if ([[dictData objectForKey:@"note_type"] isEqual: @"text"] ) {
        TextNotesVC *cell = [tableView dequeueReusableCellWithIdentifier:TextNotesVCID forIndexPath:indexPath];
        cell = [tableView cellForRowAtIndexPath:indexPath];
        
        
        
        UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"UOC" message:@"Are you sure you want to Delete?" preferredStyle:UIAlertControllerStyleAlert];
        
        [actionSheet addAction:[UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
            
            [self dismissViewControllerAnimated:YES completion:^{
            }];
        }]];
        
        
        [actionSheet addAction:[UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            
            NSLog(@" [dictData objectForKey:] :- %@", [dictData objectForKey:@"id"]);
            
            if ([CommanMethods isNetworkRechable]) {
                [self callForDeleteNotes:[dictData objectForKey:@"id"]];
            } else {
                [CommanMethods displayAlertMessage:key_Informaction message:kInternetConnectionMessage currentRef:self];
            }
            
            
        }]];
        
        // Present action sheet.
        [self presentViewController:actionSheet animated:YES completion:nil];
        
    } else if ([[dictData objectForKey:@"note_type"] isEqual: @"audio"] ) {
        
        AudioNotesVC *cell = [tableView dequeueReusableCellWithIdentifier:AudioNotesVCID forIndexPath:indexPath];
        cell = [tableView cellForRowAtIndexPath:indexPath];
        
        UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"UOC" message:@"Are you sure you want to Delete?" preferredStyle:UIAlertControllerStyleAlert];
        
        [actionSheet addAction:[UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
            
            [self dismissViewControllerAnimated:YES completion:^{
            }];
        }]];
        
        
        [actionSheet addAction:[UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            
            NSLog(@" [dictData objectForKey:] :- %@", [dictData objectForKey:@"id"]);
            
             if ([CommanMethods isNetworkRechable]) {
                 [self callForDeleteNotes:[dictData objectForKey:@"id"]];
             } else {
                 [CommanMethods displayAlertMessage:key_Informaction message:kInternetConnectionMessage currentRef:self];
             }
            
            
        }]];
        
        // Present action sheet.
        [self presentViewController:actionSheet animated:YES completion:nil];
        
    }
    
    
}

-(IBAction)btnCellEdit:(id)sender {
    
    NSIndexPath *indexPath;
    UITableView *tableView;
    
    NSDictionary *dictData = [[NSDictionary alloc] init];
    dictData = [arrNotesList objectAtIndex:[sender tag]];
    
    if ([[dictData objectForKey:@"note_type"] isEqual: @"text"] ) {
        
        if (self.get_cardData.count == 1) {
            self.demoiCarousel.scrollEnabled = FALSE;
            joinedString = [self.get_cardData componentsJoinedByString:@","];
            
            if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
                AddingTextNoteVC *textnoteVC = [[AddingTextNoteVC alloc] initWithNibName:@"AddingTextNoteVC" bundle:nil];
                textnoteVC.get_cardID = [NSString stringWithFormat:@"%@",cardid];
                textnoteVC.getSaving_ID = joinedString;
                textnoteVC.get_cardid = self.get_cardData;
                textnoteVC.pass = @"1";
                textnoteVC.message = [dictData objectForKey:@"message"];
                textnoteVC.getid = [dictData objectForKey:@"id"];
                [self.navigationController pushViewController:textnoteVC animated:YES];
            } else {
                AddingTextNoteVC *textnoteVC = [[AddingTextNoteVC alloc] initWithNibName:@"AddingTextNotesVC_IPad" bundle:nil];
                textnoteVC.get_cardID = cardid;
                textnoteVC.getSaving_ID = joinedString;
                textnoteVC.get_cardid = self.get_cardData;
                textnoteVC.pass = @"1";
                textnoteVC.message = [dictData objectForKey:@"message"];
                textnoteVC.getid = [dictData objectForKey:@"id"];
                [self.navigationController pushViewController:textnoteVC animated:YES];
            }
            
        } else {
            for (int i = 0; i<self.get_cardData.count; i++) {
                if (self.demoiCarousel.currentItemIndex == i) {
                    NSLog(@"value of i :- %@", [self.get_cardData objectAtIndex:i]);
                    cardid = [self.get_cardData objectAtIndex:i];
                }
            }
            if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
                AddingTextNoteVC *textnoteVC = [[AddingTextNoteVC alloc] initWithNibName:@"AddingTextNoteVC" bundle:nil];
                textnoteVC.get_cardID = [NSString stringWithFormat:@"%@",cardid];
                textnoteVC.getSaving_ID = cardid;
                textnoteVC.get_cardid = self.get_cardData;
                textnoteVC.pass = @"1";
                textnoteVC.message = [dictData objectForKey:@"message"];
                textnoteVC.getid = [dictData objectForKey:@"id"];
                [self.navigationController pushViewController:textnoteVC animated:YES];
            } else {
                AddingTextNoteVC *textnoteVC = [[AddingTextNoteVC alloc] initWithNibName:@"AddingTextNotesVC_IPad" bundle:nil];
                textnoteVC.get_cardID = cardid;
                textnoteVC.getSaving_ID = cardid;
                textnoteVC.get_cardid = self.get_cardData;
                textnoteVC.pass = @"1";
                textnoteVC.message = [dictData objectForKey:@"message"];
                textnoteVC.getid = [dictData objectForKey:@"id"];
                [self.navigationController pushViewController:textnoteVC animated:YES];
            }
            
        }
        
        
    }
}

-(IBAction)btnCellMoreAction:(id)sender {
    NSIndexPath *indexPath;
    UITableView *tableView;
    
    NSDictionary *dictData = [[NSDictionary alloc] init];
    dictData = [arrNotesList objectAtIndex:[sender tag]];
    
    if ([[dictData objectForKey:@"note_type"] isEqual: @"text"] ) {
        
        TextNotesVC *cell = [tableView dequeueReusableCellWithIdentifier:TextNotesVCID forIndexPath:indexPath];
        cell = [tableView cellForRowAtIndexPath:indexPath];
        
        value = [sender tag];
        
        NSLog(@" value :- %d", value);
        [self.tblNotesDetails reloadData];
    }
}

-(IBAction)btnCellPlay:(id)sender {
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[sender tag] inSection:0];
    NSDictionary *dictData = [[NSDictionary alloc] init];
    dictData = [arrNotesList objectAtIndex:[sender tag]];
    
    AudioNotesVC *cell = [self.tblNotesDetails dequeueReusableCellWithIdentifier:AudioNotesVCID forIndexPath:indexPath];
    
    NSURL *soundFileURL = [NSURL URLWithString:[dictData objectForKey:@"message"]];
    
    NSLog(@" value :- %@", [dictData objectForKey:@"message"]);
    NSLog(@" soundFileURL :- %@", soundFileURL);
    
    NSURL *url = [NSURL URLWithString:[dictData objectForKey:@"message"]];
    self.avAsset = [AVURLAsset URLAssetWithURL:url options:nil];
    self.playerItem = [AVPlayerItem playerItemWithAsset:self.avAsset];
    self.audioPlayer = [[AVPlayer alloc] initWithPlayerItem:self.playerItem];
    //self.audioPlayer = [AVPlayer playerWithPlayerItem:self.playerItem];
    [self.audioPlayer play];
    
    timer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateTime:) userInfo:nil repeats:YES];
    
    cell.btnPause.hidden = FALSE;
    cell.btnPlay.hidden = TRUE;
}

- (void)updateTime:(NSTimer *)timer
{
    NSIndexPath *indexPath;
    UITableView *tableView;
    AudioNotesVC *cell = [tableView dequeueReusableCellWithIdentifier:AudioNotesVCID forIndexPath:indexPath];
    cell = [tableView cellForRowAtIndexPath:indexPath];
    //cell.AudioPlaying.value = floorf(player.currentTime);
    //cell.lblcurrent.text = [NSString stringWithFormat:@"%f",player.currentTime];
}

-(IBAction)btnCellPause:(id)sender {
    
    NSIndexPath *indexPath;
    UITableView *tableView;
    
    NSDictionary *dictData = [[NSDictionary alloc] init];
    dictData = [arrNotesList objectAtIndex:[sender tag]];
    
    AudioNotesVC *cell = [tableView dequeueReusableCellWithIdentifier:AudioNotesVCID forIndexPath:indexPath];
    cell = [tableView cellForRowAtIndexPath:indexPath];
    
    cell.btnPause.hidden = TRUE;
    cell.btnPlay.hidden = FALSE;
    [self.player pause];
}




#pragma mark- TableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return arrNotesList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSDictionary *dictData = [[NSDictionary alloc] init];
    dictData = [arrNotesList objectAtIndex:indexPath.row];
    
    if ([[dictData objectForKey:@"note_type"] isEqual: @"audio"] ) {
        
        AudioNotesVC *cell = [tableView dequeueReusableCellWithIdentifier:AudioNotesVCID forIndexPath:indexPath];
        
        cell.lblDate.text = [dictData objectForKey:@"add_date"];
        
        cell.lbltotal.text = [NSString stringWithFormat:@"%f",self.player.duration];
        cell.lblcurrent.text = @"0.0";
        cell.AudioPlaying.value = 0.0;
        
        cell.btnPlay.tag = indexPath.row;
        cell.btnPause.tag = indexPath.row;
        cell.btnDelete.tag = indexPath.row;
        
        [cell.btnDelete addTarget:self action:@selector(btnCellDelete:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btnPlay addTarget:self action:@selector(btnCellPlay:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btnPause addTarget:self action:@selector(btnCellPause:) forControlEvents:UIControlEventTouchUpInside];
        
        audioAssetCell = [AVURLAsset URLAssetWithURL:[NSURL URLWithString:[dictData objectForKey:@"message"]] options:nil];
        audioDurationCell = audioAssetCell.duration;
        cell.lbltotal.text = [NSString stringWithFormat:@"%.02f",CMTimeGetSeconds(audioDurationCell)];
        
        return cell;
    } else {
        TextNotesVC *cell = [tableView dequeueReusableCellWithIdentifier:TextNotesVCID forIndexPath:indexPath];
        
        cell.viewOtion.layer.borderColor = UIColor.clearColor.CGColor;
        cell.viewOtion.layer.borderWidth = 1.0;
        cell.viewOtion.layer.cornerRadius = 8;
        
        
        cell.lblNotes.text = [dictData objectForKey:@"message"];
        
        cell.lblDate.text = [dictData objectForKey:@"add_date"];
        
        cell.btnEdit.tag = indexPath.row;
        cell.btnDelete.tag = indexPath.row;
        cell.btnMore.tag = indexPath.row;
        //cell.viewOtion.hidden = TRUE;
        
        [cell.btnDelete addTarget:self action:@selector(btnCellDelete:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btnEdit addTarget:self action:@selector(btnCellEdit:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btnMore addTarget:self action:@selector(btnCellMoreAction:) forControlEvents:UIControlEventTouchUpInside];
        
        if (indexPath.row == value )
        {
            cell.viewOtion.hidden = FALSE;
        } else {
            cell.viewOtion.hidden = TRUE;
        }
        
        return cell;
    }
}

#pragma mark- TableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
}


#pragma CallAPI

-(void)callForGetNotesList {
    NSMutableDictionary *dcitParams = [[NSMutableDictionary alloc] init];
    [dcitParams setValue:self.spread_type forKey:@"spread_type"];
    
    NSLog(@" dcitParams :-%@", dcitParams);
    
    [AppDelegate addVwLoading];
    [CommunicationHandler callForGetNotesList:dcitParams Sucess:^(NSDictionary *dict, NSURLResponse *response) {
        //NSLog(@"dict :- %@", dict);
        dispatch_async(dispatch_get_main_queue(), ^{
            [AppDelegate removeVwLoading];
            if ([[dict objectForKey:@"error"] boolValue] == false) {
                arrNotesList = [[NSMutableArray alloc] initWithArray:[dict valueForKey:@"data"]];
                NSLog(@"arrNotesList :- %@", arrNotesList);
                [self.tblNotesDetails reloadData];
                
            } else {
                [CommanMethods displayAlertMessage:@"UOC" message:[dict objectForKey:@"message"] currentRef:self];
            }
        });
    } Error:^(NSError *error, NSURLResponse *response) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [AppDelegate removeVwLoading];
        });
    }];
}

-(void)callForDeleteNotes:(NSString*)getid  {
    NSMutableDictionary *dcitParams = [[NSMutableDictionary alloc] init];
    [dcitParams setValue:getid forKey:@"id"];
    NSLog(@" dcitParams :-%@", dcitParams);
    
    [AppDelegate addVwLoading];
    [CommunicationHandler callForDeleteNotes:dcitParams Sucess:^(NSDictionary *dict, NSURLResponse *response) {
        //NSLog(@"dict :- %@", dict);
        dispatch_async(dispatch_get_main_queue(), ^{
            [AppDelegate removeVwLoading];
            if ([[dict objectForKey:@"error"] boolValue] == false) {
                [CommanMethods displayAlertMessageWithCallBack:nil message:[dict objectForKey:@"message"] refrence:self Success:^{
                    value = -1;
                    if (self.get_cardData.count == 1) {
                        self.demoiCarousel.scrollEnabled = FALSE;
                        joinedString = [self.get_cardData componentsJoinedByString:@","];
                        if ([CommanMethods isNetworkRechable]) {
                            [self callForGetNotesList];
                        } else {
                            [CommanMethods displayAlertMessage:key_Informaction message:kInternetConnectionMessage currentRef:self];
                        }
                    } else {
                        for (int i = 0; i<self.get_cardData.count; i++) {
                            if (self.demoiCarousel.currentItemIndex == i) {
                                NSLog(@"value of i :- %@", [self.get_cardData objectAtIndex:i]);
                                cardid = [self.get_cardData objectAtIndex:i];
                            }
                        }
                        if ([CommanMethods isNetworkRechable]) {
                            [self callForGetNotesList];
                        } else {
                            [CommanMethods displayAlertMessage:key_Informaction message:kInternetConnectionMessage currentRef:self];
                        }
                    }
                }];
                
            } else {
                [CommanMethods displayAlertMessage:@"UOC" message:[dict objectForKey:@"message"] currentRef:self];
            }
        });
    } Error:^(NSError *error, NSURLResponse *response) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [AppDelegate removeVwLoading];
        });
    }];
}


#pragma CallAPI

-(void)callForSaveAudio:(NSURL *)fileUrl  {
    
    NSData *fileData = [[NSData alloc] initWithContentsOfURL:fileUrl];
    
    joinedString = [self.get_cardData componentsJoinedByString:@","];
    NSMutableDictionary *dcitParams = [[NSMutableDictionary alloc] init];
    [dcitParams setValue:self.getid forKey:@"save_reading_id"];
    [dcitParams setValue:joinedString forKey:@"card_id"];
    [dcitParams setValue:@"audio" forKey:@"note_type"];
    [dcitParams setValue:@"" forKey:@"message"];
    [dcitParams setValue:@"test" forKey:@"save_name"];
    [dcitParams setValue:fileData forKey:@"recording"];
    /*if (fileData) {
        [dcitParams setValue:fileData forKey:@"recording"];
    }*/
    
    NSLog(@" dcitParams :-%@", dcitParams);
    
    
    [AppDelegate addVwLoading];
    [CommunicationHandler callForSaveAudio:dcitParams Sucess:^(NSDictionary *dict, NSURLResponse *response) {
        NSLog(@"%@", dict);
        dispatch_async(dispatch_get_main_queue(), ^{
            [AppDelegate removeVwLoading];
            if ([[dict objectForKey:@"error"] boolValue] == false) {
                [CommanMethods displayAlertMessageWithCallBack:nil message:[dict objectForKey:@"message"] refrence:self Success:^{
                    value = -1;
                    if (self.get_cardData.count == 1) {
                        self.demoiCarousel.scrollEnabled = FALSE;
                        joinedString = [self.get_cardData componentsJoinedByString:@","];
                        if ([CommanMethods isNetworkRechable]) {
                            [self callForGetNotesList];
                            [self viewDidAppear:true];
                        } else {
                            [CommanMethods displayAlertMessage:key_Informaction message:kInternetConnectionMessage currentRef:self];
                        }
                    } else {
                        for (int i = 0; i<self.get_cardData.count; i++) {
                            if (self.demoiCarousel.currentItemIndex == i) {
                                NSLog(@"value of i :- %@", [self.get_cardData objectAtIndex:i]);
                                cardid = [self.get_cardData objectAtIndex:i];
                            }
                        }
                        if ([CommanMethods isNetworkRechable]) {
                            [self callForGetNotesList];
                        } else {
                            [CommanMethods displayAlertMessage:key_Informaction message:kInternetConnectionMessage currentRef:self];
                        }
                    }
                }];
            } else {
                [CommanMethods displayAlertMessage:@"UOC" message:[dict objectForKey:@"message"] currentRef:self];
            }
        });
    } Error:^(NSError *error, NSURLResponse *response) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [AppDelegate removeVwLoading];
        });
    }];
}

-(NSString *) randomStringWithLength: (int) len {
    NSString *letters = @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    
    NSMutableString *randomString = [NSMutableString stringWithCapacity: len];
    
    for (int i=0; i<len; i++) {
        [randomString appendFormat: @"%C", [letters characterAtIndex: arc4random_uniform([letters length])]];
    }
    
    return randomString;
}

@end
