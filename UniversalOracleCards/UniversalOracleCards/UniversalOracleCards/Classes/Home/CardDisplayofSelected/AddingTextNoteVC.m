//
//  AddingTextNoteVC.m
//  UniversalOracleCards
//
//  Created by whiznic on 12/03/19.
//  Copyright © 2019 whiznic. All rights reserved.
//

#import "AddingTextNoteVC.h"
#import <IQKeyboardManager/IQKeyboardManager.h>
#import "CardDisplywithNamesVC.h"
#import <AudioToolbox/AudioServices.h>
#import "SharkfoodMuteSwitchDetector.h"
#import "CommanMethods.h"
#import "AppDelegate.h"
#import "CommunicationHandler.h"
#import "CardDisplywithNamesVC.h"
#import "GeneralAdviceVC.h"
@import MediaPlayer;

@interface AddingTextNoteVC ()
@property (nonatomic,strong) SharkfoodMuteSwitchDetector* detector;
@end

@implementation AddingTextNoteVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    /*self.detector = [SharkfoodMuteSwitchDetector shared];
    __weak AddingTextNoteVC* sself = self;
    self.detector.silentNotify = ^(BOOL silent){
        if (silent) {
            [sself.playerViewController setVolume:0.0];
            sself.btnMute.hidden = FALSE;
            sself.btnVolume.hidden = TRUE;
            NSLog(@"device is silent");
        } else {
            [sself.playerViewController setVolume:1.0];
            sself.btnMute.hidden = TRUE;
            sself.btnVolume.hidden = FALSE;
            NSLog(@"device is in ringing mode");
        }
    };*/
    
    self.txtNotes.delegate = self;
    
    CALayer *border = [CALayer layer];
    CGFloat borderWidth = 1;
    border.borderColor = [UIColor colorWithRed:173/255.0 green:173/255.0 blue:173/255.0 alpha:1.0].CGColor;
    border.frame = CGRectMake(0, self.txtNotes.frame.size.height - borderWidth, self.txtNotes.frame.size.width, self.txtNotes.frame.size.height);
    border.borderWidth = borderWidth;
    [self.txtNotes.layer addSublayer:border];
    self.txtNotes.layer.masksToBounds = YES;
    
    
    self.txtNotes.textColor = [UIColor colorWithRed:173/255.0 green:173/255.0 blue:173/255.0 alpha:1.0];
    self.txtNotes.delegate = self;
    
    self.imgBorder.layer.borderColor = [UIColor colorWithRed:173/255.0 green:173/255.0 blue:173/255.0 alpha:1.0].CGColor;
    self.imgBorder.layer.borderWidth = 1;
    self.imgBorder.layer.cornerRadius = 10;
    
 
    
    self.imgCard.image = [UIImage imageNamed:self.get_cardID];
    NSLog(@" _getSaving_ID :- %@", _getSaving_ID);
    
    
    if ([self.pass isEqualToString:@"1"]) {
        
        self.txtNotes.text = self.message;
    } else {
         
         self.txtNotes.text = @"Start Typing...";
    }
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    NSString *stringVideoName = @"stars_animated.mp4";
    NSString *filepath = [[NSBundle mainBundle] pathForResource:stringVideoName ofType:nil];
    NSLog(@" get_cardid :- %@", self.get_cardid);
    NSURL *fileURL = [NSURL fileURLWithPath:filepath];
    self.playerViewController = [AVPlayer playerWithURL:fileURL];
    self.playerViewController.actionAtItemEnd = AVPlayerActionAtItemEndNone;
    AVPlayerLayer *videoLayer = [AVPlayerLayer playerLayerWithPlayer:self.playerViewController];
    videoLayer.frame = self.view.bounds;
    videoLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
    [self.view.layer addSublayer:videoLayer];
    
    [self.playerViewController play];
    
    NSString *savedValue = [[NSUserDefaults standardUserDefaults]
                            stringForKey:@"audio"];
    if([savedValue isEqualToString:@"off"]) {
        self.btnMute.hidden = FALSE;
        self.btnVolume.hidden = TRUE;
        [self.playerViewController setVolume:0.0];
    } else {
        self.btnMute.hidden = TRUE;
        self.btnVolume.hidden = FALSE;
        [self.playerViewController setVolume:1.0];
    }
    
    
    
    self.viewDisply.layer.zPosition = 1;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(itemDidFinishPlaying:) name:AVPlayerItemDidPlayToEndTimeNotification object:[self.playerViewController currentItem]];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationDidBecomeActive:) name:UIApplicationDidEnterBackgroundNotification object:nil];
    
    [[UIApplication sharedApplication] beginReceivingRemoteControlEvents];
    [self becomeFirstResponder];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [self.playerViewController pause];
    [super viewWillDisappear:animated];
    [[UIApplication sharedApplication] endReceivingRemoteControlEvents];
    [self resignFirstResponder];
}


- (void)remoteControlReceivedWithEvent:(UIEvent *)event {
    switch (event.subtype) {
        case UIEventSubtypeRemoteControlTogglePlayPause:
            if([self.playerViewController rate] == 0){
                [self.playerViewController play];
            } else {
                [self.playerViewController pause];
            }
            break;
        case UIEventSubtypeRemoteControlPlay:
            [self.playerViewController play];
            break;
        case UIEventSubtypeRemoteControlPause:
            [self.playerViewController pause];
            break;
        default:
            break;
    }
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    self.detector = [SharkfoodMuteSwitchDetector shared];
    __weak AddingTextNoteVC* sself = self;
    self.detector.silentNotify = ^(BOOL silent){
        if (silent) {
            
            NSLog(@"device is silent");
        } else {
            
            NSLog(@"device is in ringing mode");
            [self.playerViewController performSelector:@selector(play) withObject:nil afterDelay:0.01];
        }
    };
}


- (void)itemDidFinishPlaying:(NSNotification *)notification {
    AVPlayerItem *player = [notification object];
    [player seekToTime:kCMTimeZero];
}

#pragma textview
#pragma TextViewDelegate Method
- (BOOL) textViewShouldBeginEditing:(UITextView *)textView {
    if ([textView.text isEqualToString:@"Start Typing..."]) {
        self.txtNotes.text = @"";
    }
    self.txtNotes.textColor = [UIColor colorWithRed:173/255.0 green:173/255.0 blue:173/255.0 alpha:1.0];
    return YES;
}

-(void) textViewDidChange:(UITextView *)textView {
    
    if(self.txtNotes.text.length == 0) {
        self.txtNotes.textColor = [UIColor colorWithRed:173/255.0 green:173/255.0 blue:173/255.0 alpha:1.0];
        self.txtNotes.text = @"Start Typing...";
        [self.txtNotes resignFirstResponder];
    }
}

-(void) textViewShouldEndEditing:(UITextView *)textView {
    
    if(self.txtNotes.text.length == 0) {
        self.txtNotes.textColor = [UIColor colorWithRed:173/255.0 green:173/255.0 blue:173/255.0 alpha:1.0];
        self.txtNotes.text = @"Start Typing...";
        [self.txtNotes resignFirstResponder];
    }
}



#pragma Action Button
- (IBAction)btnSaveNotes:(id)sender {
    
    if ([self.pass isEqualToString:@"1"]) {
        
        if ([CommanMethods isNetworkRechable]) {
            [self callForUpdateNotes:self.txtNotes.text];
        } else {
            [CommanMethods displayAlertMessage:key_Informaction message:kInternetConnectionMessage currentRef:self];
        }
    } else {
        
        if ([CommanMethods isNetworkRechable]) {
            [self callForSaveNotes:self.txtNotes.text];
        } else {
            [CommanMethods displayAlertMessage:key_Informaction message:kInternetConnectionMessage currentRef:self];
        }
    }
    
    
    /*if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
        CardDisplywithNamesVC *carddisplyVC = [[CardDisplywithNamesVC alloc] initWithNibName:@"CardDisplywithNamesVC" bundle:nil];
        carddisplyVC.get_cardid = self.get_cardid;
        [self.navigationController pushViewController:carddisplyVC animated:YES];
    } else {
        CardDisplywithNamesVC *carddisplyVC = [[CardDisplywithNamesVC alloc] initWithNibName:@"CardDisplywithNamesVC_IPad" bundle:nil];
        carddisplyVC.get_cardid = self.get_cardid;
        [self.navigationController pushViewController:carddisplyVC animated:YES];
    }*/
    
}

- (IBAction)btnClose:(id)sender {
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
        [self.navigationController popViewControllerAnimated:YES];
    } else {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (IBAction)btnVolume:(id)sender {
    if ([self.btnVolume.accessibilityHint isEqualToString:@"1"]) {
        self.btnVolume.accessibilityHint = @"0";
        self.btnVolume.hidden = TRUE;
        self.btnMute.hidden = FALSE;
        [self.playerViewController setVolume:0.0];
        NSUserDefaults *audiocheck = [NSUserDefaults standardUserDefaults];
        [audiocheck setValue:@"off" forKey:@"audio"];
        NSLog(@"audiocheck :- %@", audiocheck);
        [[NSUserDefaults standardUserDefaults] synchronize];
        
    } else {
        self.btnVolume.accessibilityHint = @"1";
        self.btnVolume.hidden = FALSE;
        self.btnMute.hidden = TRUE;
        [self.playerViewController setVolume:1.0];
        NSUserDefaults *audiocheck = [NSUserDefaults standardUserDefaults];
        [audiocheck setValue:@"on" forKey:@"audio"];
        NSLog(@"audiocheck :- %@", audiocheck);
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}

- (IBAction)btnBack:(id)sender {
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
        [self.navigationController popViewControllerAnimated:YES];
    } else {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

#pragma CallAPI

-(void)callForSaveNotes:(NSString*)title  {
    NSMutableDictionary *dcitParams = [[NSMutableDictionary alloc] init];
    [dcitParams setValue:self.getSaving_ID forKey:@"save_reading_id"];
    [dcitParams setValue:self.get_cardID forKey:@"card_id"];
    [dcitParams setValue:@"text" forKey:@"note_type"];
    [dcitParams setValue:title forKey:@"message"];
    [dcitParams setValue:@"test" forKey:@"save_name"];
    [dcitParams setValue:@"" forKey:@"recording"];
    
    NSLog(@" dcitParams :-%@", dcitParams);
    
    [AppDelegate addVwLoading];
    [CommunicationHandler callForSaveNotes:dcitParams Sucess:^(NSDictionary *dict, NSURLResponse *response) {
        NSLog(@"%@", dict);
        dispatch_async(dispatch_get_main_queue(), ^{
            [AppDelegate removeVwLoading];
            if ([[dict objectForKey:@"error"] boolValue] == false) {
                [CommanMethods displayAlertMessageWithCallBack:nil message:[dict objectForKey:@"message"] refrence:self Success:^{
                    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
                        [self.navigationController popViewControllerAnimated:YES];
                    } else {
                        [self.navigationController popViewControllerAnimated:YES];
                    }
                }];
               
            } else {
                [CommanMethods displayAlertMessage:@"UOC" message:[dict objectForKey:@"message"] currentRef:self];
            }
        });
    } Error:^(NSError *error, NSURLResponse *response) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [AppDelegate removeVwLoading];
        });
    }];
}

-(void)callForUpdateNotes:(NSString*)title  {
    NSMutableDictionary *dcitParams = [[NSMutableDictionary alloc] init];
    [dcitParams setValue:title forKey:@"message"];
    [dcitParams setValue:self.getid forKey:@"id"];
    
    NSLog(@" dcitParams :-%@", dcitParams);
    
    [AppDelegate addVwLoading];
    [CommunicationHandler callForUpdateNotes:dcitParams Sucess:^(NSDictionary *dict, NSURLResponse *response) {
        NSLog(@"%@", dict);
        dispatch_async(dispatch_get_main_queue(), ^{
            [AppDelegate removeVwLoading];
            if ([[dict objectForKey:@"error"] boolValue] == false) {
                [CommanMethods displayAlertMessageWithCallBack:nil message:[dict objectForKey:@"message"] refrence:self Success:^{
                    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
                        [self.navigationController popViewControllerAnimated:YES];
                    } else {
                        [self.navigationController popViewControllerAnimated:YES];
                    }
                }];
                
            } else {
                [CommanMethods displayAlertMessage:@"UOC" message:[dict objectForKey:@"message"] currentRef:self];
            }
        });
    } Error:^(NSError *error, NSURLResponse *response) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [AppDelegate removeVwLoading];
        });
    }];
}


@end
