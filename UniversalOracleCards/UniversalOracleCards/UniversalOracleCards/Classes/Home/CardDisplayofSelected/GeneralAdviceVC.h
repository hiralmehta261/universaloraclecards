//
//  GeneralAdviceVC.h
//  UniversalOracleCards
//
//  Created by whiznic on 12/03/19.
//  Copyright © 2019 whiznic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "iCarousel.h"
#import <AVFoundation/AVFoundation.h>
#import <AudioToolbox/AudioToolbox.h>
#import <AVKit/AVKit.h>


@interface GeneralAdviceVC : UIViewController <iCarouselDataSource, iCarouselDelegate, UITextFieldDelegate, UITableViewDelegate,UITableViewDataSource>
@property (strong, nonatomic) IBOutlet UIImageView *imgCard;
@property (strong, nonatomic) IBOutlet iCarousel *demoiCarousel;
@property (strong, nonatomic) NSString *value;
@property (strong, nonatomic) IBOutlet UIView *ViewReading;
@property (strong, nonatomic) IBOutlet UIView *ViewText;
@property (strong, nonatomic) IBOutlet UIButton *btnCheck;
@property (strong, nonatomic) IBOutlet UITextField *txtReading;
@property (nonatomic) AVPlayer *playerViewController;
@property (strong, nonatomic) IBOutlet UIView *viewDisply;
@property (strong, nonatomic) IBOutlet UIView *viewAudio;
@property (strong, nonatomic) IBOutlet UILabel *lblAudioRecording;
@property (weak, nonatomic) IBOutlet UIButton *btnMute;
@property (weak, nonatomic) IBOutlet UIButton *btnVolume;
@property (strong, nonatomic) NSString *get_Selected_Value,*getid,*passid,*title,*spread_type;
@property (strong, nonatomic) NSMutableArray *get_cardData,*get_cardDataCustomSpread;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (strong, nonatomic) IBOutlet UITableView *tblNotesDetails;
@property (strong, nonatomic) AVURLAsset *avAsset;
@property (strong, nonatomic) AVPlayerItem *playerItem;
@property (strong, nonatomic) AVPlayer *audioPlayer;

@end

