//
//  SettingVC.h
//  UniversalOracleCards
//
//  Created by whiznic on 07/03/19.
//  Copyright © 2019 whiznic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <AVKit/AVKit.h>


@interface SettingVC : UIViewController <UITextFieldDelegate>
@property (strong, nonatomic) IBOutlet UIButton *btnShowCurrentPassword;
@property (strong, nonatomic) IBOutlet UITextField *CurrentPasswordTextField;
@property (strong, nonatomic) IBOutlet UITextField *NewPasswordTextField;
@property (strong, nonatomic) IBOutlet UIButton *btnNewPassword;
@property (strong, nonatomic) IBOutlet UILabel *lblEmail;
@property (strong, nonatomic) IBOutlet UILabel *lblFacebookName;
@property (nonatomic) AVPlayer *playerViewController;
@property (strong, nonatomic) IBOutlet UIView *viewDisply;
@property (weak, nonatomic) IBOutlet UITextField *lblName;
@property (weak, nonatomic) IBOutlet UIButton *btnRemoveFacebook;
@property (weak, nonatomic) IBOutlet UILabel *lblGmailName;
@property (weak, nonatomic) IBOutlet UIButton *btnRemoveGmail;
@property (nonatomic, strong) UINavigationController *navi1;
@property (nonatomic, strong) IBOutlet UIWindow* window;
@end


