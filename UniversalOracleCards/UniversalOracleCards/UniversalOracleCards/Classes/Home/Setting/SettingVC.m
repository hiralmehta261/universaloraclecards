//
//  SettingVC.m
//  UniversalOracleCards
//
//  Created by whiznic on 07/03/19.
//  Copyright © 2019 whiznic. All rights reserved.
//

#import "SettingVC.h"
#import "AppDelegate.h"
#import "SideMenuVC.h"
#import "CommanMethods.h"
#import <IQKeyboardManager/IQKeyboardManager.h>
#import "CommunicationHandler.h"
#import "MyJournalListVC.h"
#import <AudioToolbox/AudioServices.h>
#import "SharkfoodMuteSwitchDetector.h"
#import "SignInVC.h"
@import MediaPlayer;

@interface SettingVC ()
{
    NSDictionary *dcitData;
}
@property (nonatomic,strong) SharkfoodMuteSwitchDetector* detector;
@end

@implementation SettingVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[IQKeyboardManager sharedManager] setEnable:true];
    
    [self SetBorder:self.CurrentPasswordTextField];
    [self SetBorder:self.NewPasswordTextField];
    
    self.CurrentPasswordTextField.delegate = self;
    self.NewPasswordTextField.delegate = self;
    

    
    if ([CommanMethods isNetworkRechable]) {
        [self callForGetDetails];
    } else {
        [CommanMethods displayAlertMessage:key_Informaction message:kInternetConnectionMessage currentRef:self];
    }
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *dictData = [userDefaults objectForKey:@"getdata"];
    NSLog(@" dictData :- %@", dictData);
    self.lblName.text = [dictData objectForKey:@"full_name"];
    self.lblEmail.text = [dictData objectForKey:@"email"];
    
    
    if ([[dictData objectForKey:@"facebook_id"]  isEqual: @""]) {
        self.lblFacebookName.hidden = TRUE;
        self.btnRemoveFacebook.hidden = TRUE;
    } else {
        self.btnRemoveFacebook.hidden = FALSE;
        self.lblFacebookName.hidden = FALSE;
        self.lblFacebookName.text = [dictData objectForKey:@"full_name"];
    }
    
    
    if ([[dictData objectForKey:@"google_plus"]  isEqual: @""]) {
        self.lblGmailName.hidden = TRUE;
        self.btnRemoveGmail.hidden = TRUE;
    } else {
        self.btnRemoveGmail.hidden = FALSE;
        self.lblGmailName.hidden = FALSE;
        self.lblGmailName.text = [dictData objectForKey:@"full_name"];
    }
    
    self.lblName.text = [dictData objectForKey:@"full_name"];
    self.lblName.delegate = self;
    [[IQKeyboardManager sharedManager] setEnable:true];
    
    self.detector = [SharkfoodMuteSwitchDetector shared];
    __weak SettingVC* sself = self;
    self.detector.silentNotify = ^(BOOL silent){
        if (silent) {
            
            NSLog(@"device is silent");
        } else {
            
            NSLog(@"device is in ringing mode");
            
        }
    };
    
  
    
    
}




- (void)remoteControlReceivedWithEvent:(UIEvent *)event {
    switch (event.subtype) {
        case UIEventSubtypeRemoteControlTogglePlayPause:
            if([self.playerViewController rate] == 0){
                [self.playerViewController play];
            } else {
                [self.playerViewController pause];
            }
            break;
        case UIEventSubtypeRemoteControlPlay:
            [self.playerViewController play];
            break;
        case UIEventSubtypeRemoteControlPause:
            [self.playerViewController pause];
            break;
        default:
            break;
    }
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    self.detector = [SharkfoodMuteSwitchDetector shared];
    __weak SettingVC* sself = self;
    self.detector.silentNotify = ^(BOOL silent){
        if (silent) {
            
            NSLog(@"device is silent");
        } else {
            
            NSLog(@"device is in ringing mode");
            [self.playerViewController performSelector:@selector(play) withObject:nil afterDelay:0.01];
        }
    };
}



- (void)itemDidFinishPlaying:(NSNotification *)notification {
    AVPlayerItem *player = [notification object];
    [player seekToTime:kCMTimeZero];
}

#pragma TEXTFIELD-DELEGATE

- (BOOL) textFieldShouldBeginEditing:(UITextField *)textField {
    self.lblName.text = @"";
    
    return YES;
}

-(void) textFieldDidChange:(UITextField *)textField {
    
    if(self.lblName.text.length == 0) {
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        NSDictionary *dictData = [userDefaults objectForKey:@"getdata"];
        self.lblName.text = [dictData objectForKey:@"full_name"];
        
    }
}

-(BOOL) textFieldShouldEndEditing:(UITextField *)textField {
    
    if(self.lblName.text.length == 0) {
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        NSDictionary *dictData = [userDefaults objectForKey:@"getdata"];
        self.lblName.text = [dictData objectForKey:@"full_name"];
        
    }
    return YES;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    NSString *stringVideoName = @"stars_animated.mp4";
    NSString *filepath = [[NSBundle mainBundle] pathForResource:stringVideoName ofType:nil];
    NSLog(@" filepath :- %@", filepath);
    NSURL *fileURL = [NSURL fileURLWithPath:filepath];
    self.playerViewController = [AVPlayer playerWithURL:fileURL];
    self.playerViewController.actionAtItemEnd = AVPlayerActionAtItemEndNone;
    AVPlayerLayer *videoLayer = [AVPlayerLayer playerLayerWithPlayer:self.playerViewController];
    videoLayer.frame = self.view.bounds;
    videoLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
    [self.view.layer addSublayer:videoLayer];
    
    [self.playerViewController play];
    
    NSString *savedValue = [[NSUserDefaults standardUserDefaults]
                            stringForKey:@"audio"];
    if([savedValue isEqualToString:@"off"]) {
        
        [self.playerViewController setVolume:0.0];
    } else {
        
        [self.playerViewController setVolume:1.0];
    }
    
    NSLog(@" savedValue :- %@", savedValue);
    
    self.viewDisply.layer.zPosition = 1;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(itemDidFinishPlaying:) name:AVPlayerItemDidPlayToEndTimeNotification object:[self.playerViewController currentItem]];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationDidBecomeActive:) name:UIApplicationDidEnterBackgroundNotification object:nil];
    
    [SideMenuVC addToViewController:self];
    [SideMenuVC defaultVwFrame:self.view.frame];
    [[SideMenuVC sharedSideMenu] reloadSideMenu];
    
    [[UIApplication sharedApplication] beginReceivingRemoteControlEvents];
    [self becomeFirstResponder];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [SideMenuVC remove];
}

#pragma mark -ButtonAction

-(IBAction)sideMenuTap:(id)sender {
    [SideMenuVC.sharedSideMenu show];
}

- (IBAction)btnShowCurrentPassword:(id)sender {
    if ([self.btnShowCurrentPassword.accessibilityHint isEqualToString:@"1"]) {
        self.btnShowCurrentPassword.accessibilityHint = @"0";
        self.CurrentPasswordTextField.secureTextEntry = false;
    } else {
        self.btnShowCurrentPassword.accessibilityHint = @"1";
        self.CurrentPasswordTextField.secureTextEntry = true;
    }
}

- (IBAction)btnShowNewPassword:(id)sender {
    if ([self.btnNewPassword.accessibilityHint isEqualToString:@"1"]) {
        self.btnNewPassword.accessibilityHint = @"0";
        self.NewPasswordTextField.secureTextEntry = false;
    } else {
        self.btnNewPassword.accessibilityHint = @"1";
        self.NewPasswordTextField.secureTextEntry = true;
    }
}

- (IBAction)btnEdit:(id)sender {
    [self.lblName becomeFirstResponder];
}

- (IBAction)btnUpdateProfile:(id)sender {
    if ([CommanMethods isNetworkRechable]) {
        [self callForUpdateProfile:self.lblName.text];
    } else {
        [CommanMethods displayAlertMessage:key_Informaction message:kInternetConnectionMessage currentRef:self];
    }
}

- (IBAction)btnRemoveFacebook:(id)sender {
    if ([CommanMethods isNetworkRechable]) {
        [self callForRemoveFacebook];
    } else {
        [CommanMethods displayAlertMessage:key_Informaction message:kInternetConnectionMessage currentRef:self];
    }
}


- (IBAction)btnRemoveGmail:(id)sender {
    if ([CommanMethods isNetworkRechable]) {
        [self callForRemoveGoogle];
    } else {
        [CommanMethods displayAlertMessage:key_Informaction message:kInternetConnectionMessage currentRef:self];
    }
}


- (IBAction)btnChangePassword:(id)sender {
    if ([CommanMethods isNetworkRechable]) {
        [self callForChangePassword];
    } else {
        [CommanMethods displayAlertMessage:key_Informaction message:kInternetConnectionMessage currentRef:self];
    }
}

#pragma TEXTFIELD-DELEGATE

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

#pragma Function
-(void) SetBorder:(UITextField *)textfield {
    CALayer *border = [CALayer layer];
    CGFloat borderWidth = 1;
    border.borderColor = [UIColor colorWithRed:173/255.0 green:173/255.0 blue:173/255.0 alpha:1.0].CGColor;
    border.frame = CGRectMake(0, textfield.frame.size.height - borderWidth, textfield.frame.size.width, textfield.frame.size.height);
    border.borderWidth = borderWidth;
    [textfield.layer addSublayer:border];
    textfield.layer.masksToBounds = YES;
}

#pragma CallAPI

-(void)callForGetDetails  {
    NSMutableDictionary *dcitParams = [[NSMutableDictionary alloc] init];
    
    //NSLog(@" dcitParams :-%@", dcitParams);
    
    [AppDelegate addVwLoading];
    [CommunicationHandler callForGetDetails:dcitParams Sucess:^(NSDictionary *dict, NSURLResponse *response) {
        //NSLog(@"dict :- %@", dict);
        dispatch_async(dispatch_get_main_queue(), ^{
            [AppDelegate removeVwLoading];
            if ([[dict objectForKey:@"error"] boolValue] == false) {
                dcitData = [[NSDictionary alloc] initWithDictionary:[dict objectForKey:@"data"] copyItems:YES];
                NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
                [userDefaults setObject:dcitData forKey:@"getdata"];
                [userDefaults synchronize];
                
            } else {
                [CommanMethods displayAlertMessage:@"UOC" message:[dict objectForKey:@"message"] currentRef:self];
            }
        });
    } Error:^(NSError *error, NSURLResponse *response) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [AppDelegate removeVwLoading];
        });
    }];
}

-(void)callForRemoveFacebook  {
    NSMutableDictionary *dcitParams = [[NSMutableDictionary alloc] init];
    
    //NSLog(@" dcitParams :-%@", dcitParams);
    
    [AppDelegate addVwLoading];
    [CommunicationHandler callForRemoveFacebook:dcitParams Sucess:^(NSDictionary *dict, NSURLResponse *response) {
        //NSLog(@"dict :- %@", dict);
        dispatch_async(dispatch_get_main_queue(), ^{
            [AppDelegate removeVwLoading];
            if ([[dict objectForKey:@"error"] boolValue] == false) {
                [CommanMethods displayAlertMessageWithCallBack:nil message:[dict objectForKey:@"message"] refrence:self Success:^{
                    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
                        [self ClearAllUserDetaults];
                        SignInVC *objcLogin = [[SignInVC alloc] initWithNibName:@"SignInVC" bundle:nil];
                        self.navi1 = [[UINavigationController alloc] initWithRootViewController:objcLogin];
                        self.navi1.navigationBarHidden = TRUE;
                        AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
                        appDelegate.window.rootViewController = self.navi1;
                    } else {
                        [self ClearAllUserDetaults];
                        SignInVC *objcLogin = [[SignInVC alloc] initWithNibName:@"SignInVC_IPad" bundle:nil];
                        self.navi1 = [[UINavigationController alloc] initWithRootViewController:objcLogin];
                        self.navi1.navigationBarHidden = TRUE;
                        AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
                        appDelegate.window.rootViewController = self.navi1;
                    }
                }];
                
            } else {
                [CommanMethods displayAlertMessage:@"UOC" message:[dict objectForKey:@"message"] currentRef:self];
            }
        });
    } Error:^(NSError *error, NSURLResponse *response) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [AppDelegate removeVwLoading];
        });
    }];
}

-(void)callForRemoveGoogle  {
    NSMutableDictionary *dcitParams = [[NSMutableDictionary alloc] init];
    
    //NSLog(@" dcitParams :-%@", dcitParams);
    
    [AppDelegate addVwLoading];
    [CommunicationHandler callForRemoveGoogle:dcitParams Sucess:^(NSDictionary *dict, NSURLResponse *response) {
        //NSLog(@"dict :- %@", dict);
        dispatch_async(dispatch_get_main_queue(), ^{
            [AppDelegate removeVwLoading];
            if ([[dict objectForKey:@"error"] boolValue] == false) {
                [CommanMethods displayAlertMessageWithCallBack:nil message:[dict objectForKey:@"message"] refrence:self Success:^{
                    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
                        [self ClearAllUserDetaults];
                        SignInVC *objcLogin = [[SignInVC alloc] initWithNibName:@"SignInVC" bundle:nil];
                        self.navi1 = [[UINavigationController alloc] initWithRootViewController:objcLogin];
                        self.navi1.navigationBarHidden = TRUE;
                        AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
                        appDelegate.window.rootViewController = self.navi1;
                    } else {
                        [self ClearAllUserDetaults];
                        SignInVC *objcLogin = [[SignInVC alloc] initWithNibName:@"SignInVC_IPad" bundle:nil];
                        self.navi1 = [[UINavigationController alloc] initWithRootViewController:objcLogin];
                        self.navi1.navigationBarHidden = TRUE;
                        AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
                        appDelegate.window.rootViewController = self.navi1;
                    }
                }];
            } else {
                [CommanMethods displayAlertMessage:@"UOC" message:[dict objectForKey:@"message"] currentRef:self];
            }
        });
    } Error:^(NSError *error, NSURLResponse *response) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [AppDelegate removeVwLoading];
        });
    }];
}

-(void)callForUpdateProfile:(NSString*)name  {
    
    NSMutableDictionary *dcitParams = [[NSMutableDictionary alloc] init];
    [dcitParams setValue:self.lblEmail.text forKey:@"email"];
    [dcitParams setValue:name forKey:@"full_name"];
   
    NSLog(@" dcitParams :-%@", dcitParams);
    
    [AppDelegate addVwLoading];
    [CommunicationHandler callForUpdateProfile:dcitParams Sucess:^(NSDictionary *dict, NSURLResponse *response) {
        //NSLog(@"dict :- %@", dict);
        dispatch_async(dispatch_get_main_queue(), ^{
            [AppDelegate removeVwLoading];
            if ([[dict objectForKey:@"error"] boolValue] == false) {
                [CommanMethods displayAlertMessage:@"UOC" message:[dict objectForKey:@"message"] currentRef:self];
                
            } else {
                [CommanMethods displayAlertMessage:@"UOC" message:[dict objectForKey:@"message"] currentRef:self];
            }
        });
    } Error:^(NSError *error, NSURLResponse *response) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [AppDelegate removeVwLoading];
        });
    }];
}

-(void)callForChangePassword  {
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *dictData = [userDefaults objectForKey:@"getdata"];
    NSMutableDictionary *dcitParams = [[NSMutableDictionary alloc] init];
    [dcitParams setValue:[dictData objectForKey:@"user_id"] forKey:@"user_id"];
    [dcitParams setValue:self.CurrentPasswordTextField.text forKey:@"current_password"];
    [dcitParams setValue:self.NewPasswordTextField.text forKey:@"new_password"];
    
    NSLog(@" dcitParams :-%@", dcitParams);
    
    [AppDelegate addVwLoading];
    [CommunicationHandler callForChangePassword:dcitParams Sucess:^(NSDictionary *dict, NSURLResponse *response) {
        //NSLog(@"dict :- %@", dict);
        dispatch_async(dispatch_get_main_queue(), ^{
            [AppDelegate removeVwLoading];
            if ([[dict objectForKey:@"error"] boolValue] == false) {
                [CommanMethods displayAlertMessage:@"UOC" message:[dict objectForKey:@"message"] currentRef:self];
                
            } else {
                [CommanMethods displayAlertMessage:@"UOC" message:[dict objectForKey:@"message"] currentRef:self];
            }
        });
    } Error:^(NSError *error, NSURLResponse *response) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [AppDelegate removeVwLoading];
        });
    }];
}

-(void)ClearAllUserDetaults {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults removeObjectForKey:@"username"];
    [userDefaults removeObjectForKey:@"user_data"];
    [userDefaults removeObjectForKey:@"HomeScreen"];
    [userDefaults removeObjectForKey:@"CheckboxDailyCard"];
    [userDefaults removeObjectForKey:@"CheckboxGeneral"];
    [userDefaults removeObjectForKey:@"CheckboxInner"];
    [userDefaults removeObjectForKey:@"CheckboxConnection"];
    [userDefaults removeObjectForKey:@"CheckboxLife"];
    [userDefaults removeObjectForKey:@"audio"];
    [userDefaults removeObjectForKey:@"journaldata"];
    [userDefaults removeObjectForKey:@"spreaddata"];
}


@end
