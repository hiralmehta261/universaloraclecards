//
//  ContactUsVC.m
//  UniversalOracleCards
//
//  Created by whiznic on 16/04/19.
//  Copyright © 2019 whiznic. All rights reserved.
//

#import "ContactUsVC.h"
#import "AppDelegate.h"
#import "SideMenuVC.h"
#import "CommanMethods.h"
#import <AudioToolbox/AudioServices.h>
#import <AudioToolbox/AudioServices.h>
#import "SharkfoodMuteSwitchDetector.h"

@interface ContactUsVC ()
@property (nonatomic,strong) SharkfoodMuteSwitchDetector* detector;
@end

@implementation ContactUsVC

- (void)viewDidLoad {
    [super viewDidLoad];
   
}

- (void)itemDidFinishPlaying:(NSNotification *)notification {
    AVPlayerItem *player = [notification object];
    [player seekToTime:kCMTimeZero];
}


- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    NSString *stringVideoName = @"stars_animated.mp4";
    NSString *filepath = [[NSBundle mainBundle] pathForResource:stringVideoName ofType:nil];
    NSURL *fileURL = [NSURL fileURLWithPath:filepath];
    self.playerViewController = [AVPlayer playerWithURL:fileURL];
    self.playerViewController.actionAtItemEnd = AVPlayerActionAtItemEndNone;
    AVPlayerLayer *videoLayer = [AVPlayerLayer playerLayerWithPlayer:self.playerViewController];
    videoLayer.frame = self.view.bounds;
    videoLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
    [self.view.layer addSublayer:videoLayer];
    
    [self.playerViewController play];
    
    
    NSString *savedValue = [[NSUserDefaults standardUserDefaults]
                            stringForKey:@"audio"];
    if([savedValue isEqualToString:@"off"]) {
        
        [self.playerViewController setVolume:0.0];
    } else {
        
        [self.playerViewController setVolume:1.0];
    }
    
    NSLog(@" savedValue :- %@", savedValue);
    
    self.viewDisply.layer.zPosition = 1;
    
    
    /*self.detector = [SharkfoodMuteSwitchDetector shared];
     __weak AboutTheArtistVC* sself = self;
     self.detector.silentNotify = ^(BOOL silent){
     if (silent) {
     
     NSLog(@"device is silent");
     } else {
     
     NSLog(@"device is in ringing mode");
     [self.playerViewController performSelector:@selector(play) withObject:nil afterDelay:0.01];
     }
     };*/
    
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(itemDidFinishPlaying:) name:AVPlayerItemDidPlayToEndTimeNotification object:[self.playerViewController currentItem]];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationDidBecomeActive:) name:UIApplicationDidEnterBackgroundNotification object:nil];
    
    [[UIApplication sharedApplication] beginReceivingRemoteControlEvents];
    [self becomeFirstResponder];
    [SideMenuVC addToViewController:self];
    [SideMenuVC defaultVwFrame:self.view.frame];
    [[SideMenuVC sharedSideMenu] reloadSideMenu];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [self.playerViewController pause];
    [super viewWillDisappear:animated];
    [[UIApplication sharedApplication] endReceivingRemoteControlEvents];
    [self resignFirstResponder];
    
    [SideMenuVC addToViewController:self];
    [SideMenuVC defaultVwFrame:self.view.frame];
    [[SideMenuVC sharedSideMenu] reloadSideMenu];
}


- (void)remoteControlReceivedWithEvent:(UIEvent *)event {
    switch (event.subtype) {
        case UIEventSubtypeRemoteControlTogglePlayPause:
            if([self.playerViewController rate] == 0){
                [self.playerViewController play];
            } else {
                [self.playerViewController pause];
            }
            break;
        case UIEventSubtypeRemoteControlPlay:
            [self.playerViewController play];
            break;
        case UIEventSubtypeRemoteControlPause:
            [self.playerViewController pause];
            break;
        default:
            break;
    }
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    self.detector = [SharkfoodMuteSwitchDetector shared];
    __weak ContactUsVC* sself = self;
    self.detector.silentNotify = ^(BOOL silent){
        if (silent) {
            
            NSLog(@"device is silent");
        } else {
            
            NSLog(@"device is in ringing mode");
            [self.playerViewController performSelector:@selector(play) withObject:nil afterDelay:0.01];
        }
    };
}




- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [SideMenuVC remove];
}

#pragma mark -ButtonAction

-(IBAction)sideMenuTap:(id)sender {
    [SideMenuVC.sharedSideMenu show];
}



@end
