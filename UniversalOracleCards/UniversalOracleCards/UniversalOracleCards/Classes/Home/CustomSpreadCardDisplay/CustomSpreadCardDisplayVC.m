//
//  CustomSpreadCardDisplayVC.m
//  UniversalOracleCards
//
//  Created by whiznic on 01/04/19.
//  Copyright © 2019 whiznic. All rights reserved.
//

#import "CustomSpreadCardDisplayVC.h"
#import "HomeVC.h"
#import "GeneralAdviceVC.h"
#import <IQKeyboardManager/IQKeyboardManager.h>
#import "CommanMethods.h"
#import "AppDelegate.h"
#import "CommunicationHandler.h"
#import "CustomSpreadCardDisplayCell.h"
#import "GeneralAdviceCardVC.h"
#import <AudioToolbox/AudioServices.h>
#import "SharkfoodMuteSwitchDetector.h"
@import MediaPlayer;

@interface CustomSpreadCardDisplayVC ()
{
    NSLocale* currentLocale;
    NSDateFormatter *dateFormatter;
    NSString *joinedString;
    NSString *readingid;
    NSMutableArray *arrNewGrid2,*arrNewGrid3,*arrNewGrid4,*arrNewGrid5,*arrNewGrid6,*arrImage,*cardPosition;
    int value, passname;
    NSString *grid_Type;
    int total,pass,cards;
    NSString *str_name,*str_card_position, *str_card_id;
    NSString *str1;
}
@property (nonatomic,strong) SharkfoodMuteSwitchDetector* detector;
@end

@implementation CustomSpreadCardDisplayVC

- (void)viewDidLoad {
    [super viewDidLoad];

    CALayer *border1 = [CALayer layer];
    CGFloat borderWidth1 = 1;
    border1.borderColor = [UIColor colorWithRed:173/255.0 green:173/255.0 blue:173/255.0 alpha:1.0].CGColor;
    border1.frame = CGRectMake(0, self.txtTitle.frame.size.height - borderWidth1, self.txtTitle.frame.size.width, self.txtTitle.frame.size.height);
    border1.borderWidth = borderWidth1;
    [self.txtTitle.layer addSublayer:border1];
    self.txtTitle.layer.masksToBounds = YES;
    
    self.colSpread.dataSource = self;
    self.colSpread.delegate = self;
    
    currentLocale = [NSLocale currentLocale];
    [[NSDate date] descriptionWithLocale:currentLocale];
    
    dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    NSLog(@"get_Data :- %@", self.get_Data);
    
    if (self.get_Data == 0) {
        if (self.txtTitle.userInteractionEnabled == TRUE) {
            [self.txtTitle resignFirstResponder];
            self.txtTitle.text = @"";
        } else {
            self.txtTitle.text = [dateFormatter stringFromDate:[NSDate date]];
        }
    } else {
        self.txtTitle.text = [self.get_Data objectForKey:@"save_name"];
        
        
    }
    
    arrImage = [[NSMutableArray alloc] init];
    self.txtTitle.delegate = self;
    [[IQKeyboardManager sharedManager] setEnable:true];
    
    arrNewGrid2 = [[NSMutableArray alloc] init];
    arrNewGrid3 = [[NSMutableArray alloc] init];
    arrNewGrid4 = [[NSMutableArray alloc] init];
    arrNewGrid5 = [[NSMutableArray alloc] init];
    arrNewGrid6 = [[NSMutableArray alloc] init];

    NSLog(@" get_cardID :- %@", self.get_cardID);
    NSLog(@" get_CardPosition :- %@", self.get_CardPosition);
    NSLog(@" get_spread_id :- %@", self.get_spread_id);
 
    if (self.get_Data == 0) {
        if (self.grid == 2) {
            for (int i = 1; i < 5; i++) {
                [arrNewGrid2 addObject:[NSDecimalNumber numberWithInt:i]];
            }
        } else if (self.grid == 3) {
            for (int i = 1; i < 10; i++) {
                [arrNewGrid3 addObject:[NSDecimalNumber numberWithInt:i]];
            }
        } else if (self.grid == 4) {
            for (int i = 1; i < 17; i++) {
                [arrNewGrid4 addObject:[NSDecimalNumber numberWithInt:i]];
            }
        } else if (self.grid == 5) {
            for (int i = 1; i < 26; i++) {
                [arrNewGrid5 addObject:[NSDecimalNumber numberWithInt:i]];
            }
        } else if (self.grid == 6) {
            for (int i = 1; i < 37; i++) {
                [arrNewGrid6 addObject:[NSDecimalNumber numberWithInt:i]];
            }
        }
    } else {
        if ([[self.get_Data objectForKey:@"c_spread_type"] isEqual: @"2x2"]){
            for (int i = 1; i < 5; i++) {
                [arrNewGrid2 addObject:[NSDecimalNumber numberWithInt:i]];
            }
        } else if ([[self.get_Data objectForKey:@"c_spread_type"] isEqual: @"3x3"]){
            for (int i = 1; i < 10; i++) {
                [arrNewGrid3 addObject:[NSDecimalNumber numberWithInt:i]];
            }
        } else if ([[self.get_Data objectForKey:@"c_spread_type"] isEqual: @"4x4"]){
            for (int i = 1; i < 17; i++) {
                [arrNewGrid4 addObject:[NSDecimalNumber numberWithInt:i]];
            }
        } else if ([[self.get_Data objectForKey:@"c_spread_type"] isEqual: @"5x5"]){
            for (int i = 1; i < 26; i++) {
                [arrNewGrid5 addObject:[NSDecimalNumber numberWithInt:i]];
            }
        } else if ([[self.get_Data objectForKey:@"c_spread_type"] isEqual: @"6x6"]) {
            for (int i = 1; i < 37; i++) {
                [arrNewGrid6 addObject:[NSDecimalNumber numberWithInt:i]];
            }
        }
    }
    
    if (self.get_Data == 0) {
        self.lblTitle.text = self.Title;
    } else {
        self.lblTitle.text = [self.get_Data objectForKey:@"spread_type"];
    }
    
    
    
    /*self.detector = [SharkfoodMuteSwitchDetector shared];
    __weak CustomSpreadCardDisplayVC* sself = self;
    self.detector.silentNotify = ^(BOOL silent){
        if (silent) {
            
            NSLog(@"device is silent");
        } else {
            
            NSLog(@"device is in ringing mode");
            
        }
    };*/
    
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    NSString *stringVideoName = @"stars_animated.mp4";
    NSString *filepath = [[NSBundle mainBundle] pathForResource:stringVideoName ofType:nil];
   
    NSURL *fileURL = [NSURL fileURLWithPath:filepath];
    self.playerViewController = [AVPlayer playerWithURL:fileURL];
    self.playerViewController.actionAtItemEnd = AVPlayerActionAtItemEndNone;
    AVPlayerLayer *videoLayer = [AVPlayerLayer playerLayerWithPlayer:self.playerViewController];
    videoLayer.frame = self.view.bounds;
    videoLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
    [self.view.layer addSublayer:videoLayer];
    
    [self.playerViewController play];
    
    NSString *savedValue = [[NSUserDefaults standardUserDefaults]
                            stringForKey:@"audio"];
    if([savedValue isEqualToString:@"off"]) {
        
        [self.playerViewController setVolume:0.0];
    } else {
        
        [self.playerViewController setVolume:1.0];
    }
    
   // NSLog(@" savedValue :- %@", savedValue);
    
    
    
    self.viewDisply.layer.zPosition = 1;
    
 
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(itemDidFinishPlaying:) name:AVPlayerItemDidPlayToEndTimeNotification object:[self.playerViewController currentItem]];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationDidBecomeActive:) name:UIApplicationDidEnterBackgroundNotification object:nil];
    
    
    [[UIApplication sharedApplication] beginReceivingRemoteControlEvents];
    [self becomeFirstResponder];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [self.playerViewController pause];
    [super viewWillDisappear:animated];
    [[UIApplication sharedApplication] endReceivingRemoteControlEvents];
    [self resignFirstResponder];
}


- (void)remoteControlReceivedWithEvent:(UIEvent *)event {
    switch (event.subtype) {
        case UIEventSubtypeRemoteControlTogglePlayPause:
            if([self.playerViewController rate] == 0){
                [self.playerViewController play];
            } else {
                [self.playerViewController pause];
            }
            break;
        case UIEventSubtypeRemoteControlPlay:
            [self.playerViewController play];
            break;
        case UIEventSubtypeRemoteControlPause:
            [self.playerViewController pause];
            break;
        default:
            break;
    }
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    self.detector = [SharkfoodMuteSwitchDetector shared];
    __weak CustomSpreadCardDisplayVC* sself = self;
    self.detector.silentNotify = ^(BOOL silent){
        if (silent) {
            
            NSLog(@"device is silent");
        } else {
            
            NSLog(@"device is in ringing mode");
            [self.playerViewController performSelector:@selector(play) withObject:nil afterDelay:0.01];
        }
    };
}





- (void)itemDidFinishPlaying:(NSNotification *)notification {
    AVPlayerItem *player = [notification object];
    [player seekToTime:kCMTimeZero];
}

#pragma TEXTFIELD-DELEGATE

- (BOOL) textFieldShouldBeginEditing:(UITextField *)textField {
    self.txtTitle.text = @"";
    [textField resignFirstResponder];
    return YES;
}

-(void) textFieldDidChange:(UITextField *)textField {
    
    if(self.txtTitle.text.length == 0) {
        self.txtTitle.text = [dateFormatter stringFromDate:[NSDate date]];
        [self.txtTitle resignFirstResponder];
    }
}

-(BOOL) textFieldShouldEndEditing:(UITextField *)textField {
    
    if(self.txtTitle.text.length == 0) {
        self.txtTitle.text = [dateFormatter stringFromDate:[NSDate date]];
        [self.txtTitle resignFirstResponder];
    }
    return YES;
}

#pragma Mark - Button Action

- (IBAction)btnMyJournal:(id)sender {
   /* if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
        if (pass == 1) {
            GeneralAdviceVC *generalVC = [[GeneralAdviceVC alloc] initWithNibName:@"GeneralAdviceVC" bundle:nil];
            generalVC.get_Selected_Value = [NSString stringWithFormat:@"%d",self.get_cardID.count];
            generalVC.get_cardDataCustomSpread = self.get_cardID;
            generalVC.getid = readingid;
            generalVC.passid = @"1";
            generalVC.title = self.lblTitle.text;
            generalVC.spread_type = @"custom";
            [self.navigationController pushViewController:generalVC animated:YES];
        }else {
            if ([CommanMethods isNetworkRechable]) {
                    [self callForSaveReading:self.txtTitle.text];
                } else {
                    [CommanMethods displayAlertMessage:key_Informaction message:kInternetConnectionMessage currentRef:self];
            }
            GeneralAdviceVC *generalVC = [[GeneralAdviceVC alloc] initWithNibName:@"GeneralAdviceVC" bundle:nil];
            generalVC.get_Selected_Value = [NSString stringWithFormat:@"%d",self.get_cardID.count];
            generalVC.get_cardDataCustomSpread = self.get_cardID;
            generalVC.getid = readingid;
            generalVC.passid = @"1";
            generalVC.title = self.lblTitle.text;
            generalVC.spread_type = @"custom";
            [self.navigationController pushViewController:generalVC animated:YES];

        }

    } else {
        if (pass == 1) {
            GeneralAdviceVC *generalVC = [[GeneralAdviceVC alloc] initWithNibName:@"GeneralAdviceVC_IPad" bundle:nil];
            generalVC.get_Selected_Value = [NSString stringWithFormat:@"%d",self.get_cardID.count];
            generalVC.get_cardDataCustomSpread = self.get_cardID;
            generalVC.getid = readingid;
            generalVC.passid = @"1";
             generalVC.title = self.lblTitle.text;
            generalVC.spread_type = @"custom";
            [self.navigationController pushViewController:generalVC animated:YES];
        }else {
            if ([CommanMethods isNetworkRechable]) {
                [self callForSaveReading:self.txtTitle.text];
            } else {
                [CommanMethods displayAlertMessage:key_Informaction message:kInternetConnectionMessage currentRef:self];
            }
            GeneralAdviceVC *generalVC = [[GeneralAdviceVC alloc] initWithNibName:@"GeneralAdviceVC_IPad" bundle:nil];
            generalVC.get_Selected_Value = [NSString stringWithFormat:@"%d",self.get_cardID.count];
            generalVC.get_cardDataCustomSpread = self.get_cardID;
            generalVC.getid = readingid;
            generalVC.passid = @"1";
             generalVC.title = self.lblTitle.text;
            generalVC.spread_type = @"custom";
            [self.navigationController pushViewController:generalVC animated:YES];
        }
    }*/
    
    
    
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
        GeneralAdviceVC *generalVC = [[GeneralAdviceVC alloc] initWithNibName:@"GeneralAdviceVC" bundle:nil];
        generalVC.get_Selected_Value = [NSString stringWithFormat:@"%d",self.get_cardID.count];
        generalVC.get_cardDataCustomSpread = self.get_cardID;
        generalVC.getid = readingid;
        generalVC.passid = @"1";
        generalVC.title = self.lblTitle.text;
        generalVC.spread_type = @"custom";
       /* if (self.get_Data == 0) {
            generalVC.get_cardData = self.get_cardID;
        } else {
            joinedString = [self.get_Data objectForKey:@"card_number"];
            NSString *timeCreated = joinedString;
            NSArray *timeArray = [timeCreated componentsSeparatedByString:@","];
            NSMutableArray *data = [[NSMutableArray alloc] init];
            [data addObjectsFromArray:timeArray];
            generalVC.get_cardData = data;
        }*/
        if (readingid.length == 0) {
            if ([CommanMethods isNetworkRechable]) {
                [self callForSaveReading:self.txtTitle.text];
                generalVC.getid = readingid;
            } else {
                [CommanMethods displayAlertMessage:key_Informaction message:kInternetConnectionMessage currentRef:self];
            }
        } else {
            generalVC.getid = readingid;
            [self.navigationController pushViewController:generalVC animated:YES];
        }
        
    } else {
        GeneralAdviceVC *generalVC = [[GeneralAdviceVC alloc] initWithNibName:@"GeneralAdviceVC_IPad" bundle:nil];
        generalVC.get_Selected_Value = [NSString stringWithFormat:@"%d",self.get_cardID.count];
        generalVC.get_cardDataCustomSpread = self.get_cardID;
        generalVC.getid = readingid;
        generalVC.passid = @"1";
        generalVC.title = self.lblTitle.text;
        generalVC.spread_type = @"custom";
        /* if (self.get_Data == 0) {
         generalVC.get_cardData = self.get_cardID;
         } else {
         joinedString = [self.get_Data objectForKey:@"card_number"];
         NSString *timeCreated = joinedString;
         NSArray *timeArray = [timeCreated componentsSeparatedByString:@","];
         NSMutableArray *data = [[NSMutableArray alloc] init];
         [data addObjectsFromArray:timeArray];
         generalVC.get_cardData = data;
         }*/
        if (readingid.length == 0) {
            if ([CommanMethods isNetworkRechable]) {
                [self callForSaveReading:self.txtTitle.text];
                generalVC.getid = readingid;
            } else {
                [CommanMethods displayAlertMessage:key_Informaction message:kInternetConnectionMessage currentRef:self];
            }
        } else {
            generalVC.getid = readingid;
            [self.navigationController pushViewController:generalVC animated:YES];
        }
    }
    
    
}

- (IBAction)btnSaveReading:(id)sender {
    pass = 1;
    
    if (self.get_Data == 0) {
        if ([CommanMethods isNetworkRechable]) {
            [self callForSaveReading:self.txtTitle.text];
        } else {
            [CommanMethods displayAlertMessage:key_Informaction message:kInternetConnectionMessage currentRef:self];
        }
    } else {
        if ([CommanMethods isNetworkRechable]) {
            [self callForEditReading:self.txtTitle.text];
        } else {
            [CommanMethods displayAlertMessage:key_Informaction message:kInternetConnectionMessage currentRef:self];
        }
    }
   /* if ([CommanMethods isNetworkRechable]) {
        [self callForSaveReading:self.txtTitle.text];
    } else {
        [CommanMethods displayAlertMessage:key_Informaction message:kInternetConnectionMessage currentRef:self];
    }*/
}

- (IBAction)btnEdit:(id)sender {
    [self.txtTitle resignFirstResponder];
    self.txtTitle.userInteractionEnabled = TRUE;
    self.txtTitle.text = @"";
}

- (IBAction)btnShare:(id)sender {
    NSMutableArray *activityItems= arrImage;
    UIActivityViewController *activityViewController = [[UIActivityViewController alloc] initWithActivityItems:activityItems applicationActivities:nil];
    activityViewController.excludedActivityTypes = @[UIActivityTypePostToWeibo, UIActivityTypePrint, UIActivityTypeCopyToPasteboard, UIActivityTypeAssignToContact, UIActivityTypeSaveToCameraRoll, UIActivityTypeAddToReadingList, UIActivityTypePostToFlickr, UIActivityTypePostToVimeo, UIActivityTypePostToTencentWeibo, UIActivityTypeAirDrop];
    [self presentViewController:activityViewController animated:YES completion:nil];
}

- (IBAction)btnBack:(id)sender {
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
        [self.navigationController popViewControllerAnimated:YES];
    } else {
        [self.navigationController popViewControllerAnimated:YES];
    }
}


#pragma mark - TablViewCellAction

-(IBAction)btnCellClick:(id)sender {
    

    NSIndexPath *indexPath;
    UICollectionView *collectionView;
    
    static int counter = 0;
    NSString *identifier = [NSString stringWithFormat:@"Identifier_%d", counter];
    CustomSpreadCardDisplayCell *cell = (CustomSpreadCardDisplayCell *)[collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    counter++;
    cell = [collectionView cellForItemAtIndexPath:indexPath];
    
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
        GeneralAdviceVC *generalVC = [[GeneralAdviceVC alloc] initWithNibName:@"GeneralAdviceVC" bundle:nil];
        generalVC.get_Selected_Value = [NSString stringWithFormat:@"%d",self.get_cardID.count];
        generalVC.get_cardDataCustomSpread = self.get_cardID;
        generalVC.getid = readingid;
        generalVC.passid = @"1";
        generalVC.title = self.lblTitle.text;
        generalVC.spread_type = @"custom";
        /* if (self.get_Data == 0) {
         generalVC.get_cardData = self.get_cardID;
         } else {
         joinedString = [self.get_Data objectForKey:@"card_number"];
         NSString *timeCreated = joinedString;
         NSArray *timeArray = [timeCreated componentsSeparatedByString:@","];
         NSMutableArray *data = [[NSMutableArray alloc] init];
         [data addObjectsFromArray:timeArray];
         generalVC.get_cardData = data;
         }*/
        if (readingid.length == 0) {
            if ([CommanMethods isNetworkRechable]) {
                [self callForSaveReading:self.txtTitle.text];
                generalVC.getid = readingid;
            } else {
                [CommanMethods displayAlertMessage:key_Informaction message:kInternetConnectionMessage currentRef:self];
            }
        } else {
            generalVC.getid = readingid;
            [self.navigationController pushViewController:generalVC animated:YES];
        }
        
    } else {
        GeneralAdviceVC *generalVC = [[GeneralAdviceVC alloc] initWithNibName:@"GeneralAdviceVC_IPad" bundle:nil];
        generalVC.get_Selected_Value = [NSString stringWithFormat:@"%d",self.get_cardID.count];
        generalVC.get_cardDataCustomSpread = self.get_cardID;
        generalVC.getid = readingid;
        generalVC.passid = @"1";
        generalVC.title = self.lblTitle.text;
        generalVC.spread_type = @"custom";
        /* if (self.get_Data == 0) {
         generalVC.get_cardData = self.get_cardID;
         } else {
         joinedString = [self.get_Data objectForKey:@"card_number"];
         NSString *timeCreated = joinedString;
         NSArray *timeArray = [timeCreated componentsSeparatedByString:@","];
         NSMutableArray *data = [[NSMutableArray alloc] init];
         [data addObjectsFromArray:timeArray];
         generalVC.get_cardData = data;
         }*/
        if (readingid.length == 0) {
            if ([CommanMethods isNetworkRechable]) {
                [self callForSaveReading:self.txtTitle.text];
                generalVC.getid = readingid;
            } else {
                [CommanMethods displayAlertMessage:key_Informaction message:kInternetConnectionMessage currentRef:self];
            }
        } else {
            generalVC.getid = readingid;
            [self.navigationController pushViewController:generalVC animated:YES];
        }
    }
    
    
}



#pragma CollectionView Method

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (self.get_Data == 0) {
        if (self.grid == 2){
            return arrNewGrid2.count;
        } else if (self.grid== 3){
            return arrNewGrid3.count;
        } else if (self.grid == 4){
            return arrNewGrid4.count;
        } else if (self.grid == 5){
            return arrNewGrid5.count;
        } else {
            return arrNewGrid6.count;
        }
    } else {
        if ([[self.get_Data objectForKey:@"c_spread_type"] isEqual: @"2x2"]){
            return arrNewGrid2.count;
        } else if ([[self.get_Data objectForKey:@"c_spread_type"] isEqual: @"3x3"]){
            return arrNewGrid3.count;
        } else if ([[self.get_Data objectForKey:@"c_spread_type"] isEqual: @"4x4"]){
            return arrNewGrid4.count;
        } else if ([[self.get_Data objectForKey:@"c_spread_type"] isEqual: @"5x5"]){
            return arrNewGrid5.count;
        } else {
            return arrNewGrid6.count;
        }
    }
    
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
        static int counter = 0;
        NSString *identifier = [NSString stringWithFormat:@"Identifier_%d", counter];
    
        UINib *cellnib = [UINib nibWithNibName:NSStringFromClass(CustomSpreadCardDisplayCell.class) bundle:nil];
        [collectionView registerNib:cellnib forCellWithReuseIdentifier:identifier];
        
        CustomSpreadCardDisplayCell *cell = (CustomSpreadCardDisplayCell *)[collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
        
    
        
        /*CAShapeLayer *yourViewBorder = [CAShapeLayer layer];
        yourViewBorder.strokeColor = [UIColor colorWithRed:173/255.0 green:173/255.0 blue:173/255.0 alpha:1.0].CGColor;
        yourViewBorder.fillColor = nil;
        yourViewBorder.lineDashPattern = @[@1, @1];*/
    
       NSLog(@"get_CardPosition :- %@", _get_CardPosition);
    
        joinedString = [self.get_cardID componentsJoinedByString:@","];
        total = self.get_cardID.count;
        str_name = [NSString stringWithFormat:@"%i", total];
        str1 = [[str_name stringByAppendingString:@""] stringByAppendingString:@""];
    
        if (self.get_Data == 0) {
            if (self.grid == 2) {
                if ([self.get_CardPosition containsObject:[NSString stringWithFormat:@"%d",indexPath.item]])  {
                    for (int i = 0; i<self.get_CardPosition.count; i++) {
                        if ([[self.get_CardPosition objectAtIndex:i] isEqualToString:[NSString stringWithFormat:@"%d",indexPath.item]]) {
                            cell.imgCell.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@",[self.get_cardID objectAtIndex:i]]];
                            [arrImage addObject:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[self.get_cardID objectAtIndex:i]]]];
                            cell.ViewBorder.layer.borderColor = [UIColor colorWithRed:173/255.0 green:173/255.0 blue:173/255.0 alpha:1.0].CGColor;
                            cell.ViewBorder.layer.borderWidth = 1;
                            cell.ViewBorder.layer.cornerRadius = 10;
                            cell.btnMyjournal.tag = indexPath.row;
                             [cell.btnMyjournal addTarget:self action:@selector(btnCellClick:) forControlEvents:UIControlEventTouchUpInside];
                        }
                    }
                } else  {
                    cell.imgCell.image = nil;
                }
                
            } else if (self.grid == 3) {
               
                
                if ([self.get_CardPosition containsObject:[NSString stringWithFormat:@"%d",indexPath.item]])  {
                    for (int i = 0; i<self.get_CardPosition.count; i++) {
                        if ([[self.get_CardPosition objectAtIndex:i] isEqualToString:[NSString stringWithFormat:@"%d",indexPath.item]]) {
                            cell.imgCell.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@",[self.get_cardID objectAtIndex:i]]];
                            cell.ViewBorder.layer.borderColor = [UIColor colorWithRed:173/255.0 green:173/255.0 blue:173/255.0 alpha:1.0].CGColor;
                            cell.ViewBorder.layer.borderWidth = 1;
                            cell.ViewBorder.layer.cornerRadius = 10;
                            cell.btnMyjournal.tag = indexPath.row;
                             [cell.btnMyjournal addTarget:self action:@selector(btnCellClick:) forControlEvents:UIControlEventTouchUpInside];
                        }
                        
                    }
                } else  {
                    cell.imgCell.image = nil;
                }
                
            } else if (self.grid == 4) {
                
                
                if ([self.get_CardPosition containsObject:[NSString stringWithFormat:@"%d",indexPath.item]])  {
                    for (int i = 0; i<self.get_CardPosition.count; i++) {
                        if ([[self.get_CardPosition objectAtIndex:i] isEqualToString:[NSString stringWithFormat:@"%d",indexPath.item]]) {
                            cell.imgCell.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@",[self.get_cardID objectAtIndex:i]]];
                            cell.ViewBorder.layer.borderColor = [UIColor colorWithRed:173/255.0 green:173/255.0 blue:173/255.0 alpha:1.0].CGColor;
                            cell.ViewBorder.layer.borderWidth = 1;
                            cell.ViewBorder.layer.cornerRadius = 5;
                            cell.btnMyjournal.tag = indexPath.row;
                             [cell.btnMyjournal addTarget:self action:@selector(btnCellClick:) forControlEvents:UIControlEventTouchUpInside];
                        }
                        
                    }
                } else  {
                    cell.imgCell.image = nil;
                }
                
            } else if (self.grid == 5) {
                
                
                if ([self.get_CardPosition containsObject:[NSString stringWithFormat:@"%d",indexPath.item]])  {
                    for (int i = 0; i<self.get_CardPosition.count; i++) {
                        if ([[self.get_CardPosition objectAtIndex:i] isEqualToString:[NSString stringWithFormat:@"%d",indexPath.item]]) {
                            cell.imgCell.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@",[self.get_cardID objectAtIndex:i]]];
                            cell.ViewBorder.layer.borderColor = [UIColor colorWithRed:173/255.0 green:173/255.0 blue:173/255.0 alpha:1.0].CGColor;
                            cell.ViewBorder.layer.borderWidth = 1;
                            cell.ViewBorder.layer.cornerRadius = 4;
                            cell.btnMyjournal.tag = indexPath.row;
                             [cell.btnMyjournal addTarget:self action:@selector(btnCellClick:) forControlEvents:UIControlEventTouchUpInside];
                        }
                        
                        
                    }
                } else  {
                    cell.imgCell.image = nil;
                }
                
            } else if (self.grid == 6) {
               
                
                if ([self.get_CardPosition containsObject:[NSString stringWithFormat:@"%d",indexPath.item]])  {
                    for (int i = 0; i<self.get_CardPosition.count; i++) {
                        if ([[self.get_CardPosition objectAtIndex:i] isEqualToString:[NSString stringWithFormat:@"%d",indexPath.item]]) {
                            cell.imgCell.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@",[self.get_cardID objectAtIndex:i]]];
                            cell.ViewBorder.layer.borderColor = [UIColor colorWithRed:173/255.0 green:173/255.0 blue:173/255.0 alpha:1.0].CGColor;
                            cell.ViewBorder.layer.borderWidth = 1;
                            cell.ViewBorder.layer.cornerRadius = 3;
                            cell.btnMyjournal.tag = indexPath.row;
                             [cell.btnMyjournal addTarget:self action:@selector(btnCellClick:) forControlEvents:UIControlEventTouchUpInside];
                        }
                        
                    }
                } else  {
                    cell.imgCell.image = nil;
                }
                
            }
        } else {
        
            str_card_position = [self.get_Data objectForKey:@"card_position"];
            
            NSArray *words = [str_card_position componentsSeparatedByString: @","];
            self.get_CardPosition = [NSMutableArray arrayWithCapacity:[words count]];
            [self.get_CardPosition addObjectsFromArray:words];
            
            
            str_card_id = [self.get_Data objectForKey:@"card_number"];
            NSArray *words1 = [str_card_id componentsSeparatedByString: @","];
            self.get_cardID = [NSMutableArray arrayWithCapacity:[words1 count]];
            [self.get_cardID addObjectsFromArray:words1];
            
            
            NSLog(@"get_cardID :- %@", self.get_cardID);
            NSLog(@"get_CardPosition :- %@", self.get_CardPosition);
            
          
            if ([[self.get_Data objectForKey:@"c_spread_type"] isEqual: @"2x2"]) {
               
                
                if ([self.get_CardPosition containsObject:[NSString stringWithFormat:@"%d",indexPath.item]])  {
                    for (int i = 0; i<self.get_CardPosition.count; i++) {
                        if ([[self.get_CardPosition objectAtIndex:i] isEqualToString:[NSString stringWithFormat:@"%d",indexPath.item]]) {
                              cell.imgCell.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@",[self.get_cardID objectAtIndex:i]]];
                            [arrImage addObject:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[self.get_cardID objectAtIndex:i]]]];
                            cell.ViewBorder.layer.borderColor = [UIColor colorWithRed:173/255.0 green:173/255.0 blue:173/255.0 alpha:1.0].CGColor;
                            cell.ViewBorder.layer.borderWidth = 1;
                            cell.ViewBorder.layer.cornerRadius = 10;
                            cell.btnMyjournal.tag = indexPath.row;
                             [cell.btnMyjournal addTarget:self action:@selector(btnCellClick:) forControlEvents:UIControlEventTouchUpInside];
                        }
                    }
                } else  {
                    cell.imgCell.image = nil;
                }
                
            } else if ([[self.get_Data objectForKey:@"c_spread_type"] isEqual: @"3x3"]) {
                
                
                if ([self.get_CardPosition containsObject:[NSString stringWithFormat:@"%d",indexPath.item]])  {
                    for (int i = 0; i<self.get_CardPosition.count; i++) {
                        if ([[self.get_CardPosition objectAtIndex:i] isEqualToString:[NSString stringWithFormat:@"%d",indexPath.item]]) {
                            cell.imgCell.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@",[self.get_cardID objectAtIndex:i]]];
                            [arrImage addObject:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[self.get_cardID objectAtIndex:i]]]];
                            cell.ViewBorder.layer.borderColor = [UIColor colorWithRed:173/255.0 green:173/255.0 blue:173/255.0 alpha:1.0].CGColor;
                            cell.ViewBorder.layer.borderWidth = 1;
                            cell.ViewBorder.layer.cornerRadius = 10;
                            cell.btnMyjournal.tag = indexPath.row;
                              [cell.btnMyjournal addTarget:self action:@selector(btnCellClick:) forControlEvents:UIControlEventTouchUpInside];
                        }
                    }
                } else  {
                    cell.imgCell.image = nil;
                }
                
            } else if ([[self.get_Data objectForKey:@"c_spread_type"] isEqual: @"4x4"]) {
                
                
                if ([self.get_CardPosition containsObject:[NSString stringWithFormat:@"%d",indexPath.item]])  {
                    for (int i = 0; i<self.get_CardPosition.count; i++) {
                        if ([[self.get_CardPosition objectAtIndex:i] isEqualToString:[NSString stringWithFormat:@"%d",indexPath.item]]) {
                            cell.imgCell.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@",[self.get_cardID objectAtIndex:i]]];
                            [arrImage addObject:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[self.get_cardID objectAtIndex:i]]]];
                            cell.ViewBorder.layer.borderColor = [UIColor colorWithRed:173/255.0 green:173/255.0 blue:173/255.0 alpha:1.0].CGColor;
                            cell.ViewBorder.layer.borderWidth = 1;
                            cell.ViewBorder.layer.cornerRadius = 10;
                            cell.btnMyjournal.tag = indexPath.row;
                             [cell.btnMyjournal addTarget:self action:@selector(btnCellClick:) forControlEvents:UIControlEventTouchUpInside];
                        }
                    }
                } else  {
                    cell.imgCell.image = nil;
                }
                
            } else if ([[self.get_Data objectForKey:@"c_spread_type"] isEqual: @"5x5"]) {
               
                
                if ([self.get_CardPosition containsObject:[NSString stringWithFormat:@"%d",indexPath.item]])  {
                    for (int i = 0; i<self.get_CardPosition.count; i++) {
                        if ([[self.get_CardPosition objectAtIndex:i] isEqualToString:[NSString stringWithFormat:@"%d",indexPath.item]]) {
                            cell.imgCell.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@",[self.get_cardID objectAtIndex:i]]];
                            [arrImage addObject:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[self.get_cardID objectAtIndex:i]]]];
                            cell.ViewBorder.layer.borderColor = [UIColor colorWithRed:173/255.0 green:173/255.0 blue:173/255.0 alpha:1.0].CGColor;
                            cell.ViewBorder.layer.borderWidth = 1;
                            cell.ViewBorder.layer.cornerRadius = 10;
                            cell.btnMyjournal.tag = indexPath.row;
                            [cell.btnMyjournal addTarget:self action:@selector(btnCellClick:) forControlEvents:UIControlEventTouchUpInside];
                        }
                    }
                } else  {
                    cell.imgCell.image = nil;
                }
                
            } else if ([[self.get_Data objectForKey:@"c_spread_type"] isEqual: @"6x6"]) {
               
                
                if ([self.get_CardPosition containsObject:[NSString stringWithFormat:@"%d",indexPath.item]])  {
                    for (int i = 0; i<self.get_CardPosition.count; i++) {
                        if ([[self.get_CardPosition objectAtIndex:i] isEqualToString:[NSString stringWithFormat:@"%d",indexPath.item]]) {
                            cell.imgCell.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@",[self.get_cardID objectAtIndex:i]]];
                            [arrImage addObject:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[self.get_cardID objectAtIndex:i]]]];
                            cell.ViewBorder.layer.borderColor = [UIColor colorWithRed:173/255.0 green:173/255.0 blue:173/255.0 alpha:1.0].CGColor;
                            cell.ViewBorder.layer.borderWidth = 1;
                            cell.ViewBorder.layer.cornerRadius = 10;
                            cell.btnMyjournal.tag = indexPath.row;
                            [cell.btnMyjournal addTarget:self action:@selector(btnCellClick:) forControlEvents:UIControlEventTouchUpInside];
                        }
                    }
                } else  {
                    cell.imgCell.image = nil;
                }
                
            }
        }
    
    
        //yourViewBorder.frame = cell.ViewFrame.bounds;
        CGRect screenRect = [[UIScreen mainScreen] bounds];
        CGFloat screenWidth = screenRect.size.width;
    
       /* float cellWidth = (screenWidth) / self.grid;
        yourViewBorder.frame = CGRectMake(0, 0, cellWidth, cellWidth);
        yourViewBorder.path = [UIBezierPath bezierPathWithRect:yourViewBorder.bounds].CGPath;
        [cell.ViewFrame.layer addSublayer:yourViewBorder];*/
    
        return cell;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
        CGRect screenRect = [[UIScreen mainScreen] bounds];
        CGFloat screenWidth = screenRect.size.width;
        float cellWidth = (screenWidth-1) / self.grid;
        CGFloat screenHeight = screenRect.size.height;
        float cellHeight = (screenWidth-1) / self.grid;
        CGSize size = CGSizeMake(cellWidth, cellHeight);
        return size;
}


#pragma CallAPI

-(void)callForSaveReading:(NSString*)title  {
    NSMutableDictionary *dcitParams = [[NSMutableDictionary alloc] init];
    [dcitParams setValue:self.get_spread_id forKey:@"custom_spread_id"];
    [dcitParams setValue:@"custom" forKey:@"spread_type"];
    [dcitParams setValue:title forKey:@"save_name"];
    [dcitParams setValue:joinedString forKey:@"card_number"];
    [dcitParams setValue:str1 forKey:@"no_of_cards"];
    
    
    
    NSLog(@" dcitParams :-%@", dcitParams);
    
    [AppDelegate addVwLoading];
    [CommunicationHandler callForSaveReading:dcitParams Sucess:^(NSDictionary *dict, NSURLResponse *response) {
        NSLog(@"%@", dict);
        dispatch_async(dispatch_get_main_queue(), ^{
            [AppDelegate removeVwLoading];
            if ([[dict objectForKey:@"error"] boolValue] == false) {
                [CommanMethods displayAlertMessageWithCallBack:nil message:[dict objectForKey:@"message"] refrence:self Success:^{
                    
                readingid = [dict objectForKey:@"save_reading_id"];
                NSLog(@"readingid :- %@", readingid);
                
                
                if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
                    GeneralAdviceVC *generalVC = [[GeneralAdviceVC alloc] initWithNibName:@"GeneralAdviceVC" bundle:nil];
                    generalVC.get_Selected_Value = [NSString stringWithFormat:@"%d",self.get_cardID.count];
                    generalVC.get_cardDataCustomSpread = self.get_cardID;
                    generalVC.getid = readingid;
                    generalVC.passid = @"1";
                    generalVC.title = self.lblTitle.text;
                    generalVC.spread_type = @"custom";
                    [self.navigationController pushViewController:generalVC animated:YES];
                }
                else {
                    GeneralAdviceVC *generalVC = [[GeneralAdviceVC alloc] initWithNibName:@"GeneralAdviceVC_IPad" bundle:nil];
                    generalVC.get_Selected_Value = [NSString stringWithFormat:@"%d",self.get_cardID.count];
                    generalVC.get_cardDataCustomSpread = self.get_cardID;
                    generalVC.getid = readingid;
                    generalVC.passid = @"1";
                    generalVC.title = self.lblTitle.text;
                    generalVC.spread_type = @"custom";
                    [self.navigationController pushViewController:generalVC animated:YES];
                }
                
               }];
            } else {
                [CommanMethods displayAlertMessage:@"UOC" message:[dict objectForKey:@"message"] currentRef:self];
            }
        });
    } Error:^(NSError *error, NSURLResponse *response) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [AppDelegate removeVwLoading];
        });
    }];
}


-(void)callForEditReading:(NSString*)title  {
    NSMutableDictionary *dcitParams = [[NSMutableDictionary alloc] init];
    [dcitParams setValue:self.get_spread_id forKey:@"custom_spread_id"];
    [dcitParams setValue:@"custom" forKey:@"spread_type"];
    [dcitParams setValue:title forKey:@"save_name"];
    [dcitParams setValue:joinedString forKey:@"card_number"];
    [dcitParams setValue:str1 forKey:@"no_of_cards"];
    [dcitParams setValue:[self.get_Data objectForKey:@"id"] forKey:@"id"];
    
    
    NSLog(@" dcitParams :-%@", dcitParams);
    
    [AppDelegate addVwLoading];
    [CommunicationHandler callForSaveReading:dcitParams Sucess:^(NSDictionary *dict, NSURLResponse *response) {
        NSLog(@"%@", dict);
        dispatch_async(dispatch_get_main_queue(), ^{
            [AppDelegate removeVwLoading];
            if ([[dict objectForKey:@"error"] boolValue] == false) {
                if ([self.getvalue isEqualToString:@"1"]) {
                    [CommanMethods displayAlertMessageWithCallBack:nil message:[dict objectForKey:@"message"] refrence:self Success:^{
                        readingid = [dict objectForKey:@"save_reading_id"];
                        if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
                            [self.navigationController popViewControllerAnimated:YES];
                        } else {
                            [self.navigationController popViewControllerAnimated:YES];
                        }
                    }];
                } else {
                    [CommanMethods displayAlertMessage:@"UOC" message:[dict objectForKey:@"message"] currentRef:self];
                    readingid = [dict objectForKey:@"save_reading_id"];
                }
            } else {
                [CommanMethods displayAlertMessage:@"UOC" message:[dict objectForKey:@"message"] currentRef:self];
            }
        });
    } Error:^(NSError *error, NSURLResponse *response) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [AppDelegate removeVwLoading];
        });
    }];
}


@end
