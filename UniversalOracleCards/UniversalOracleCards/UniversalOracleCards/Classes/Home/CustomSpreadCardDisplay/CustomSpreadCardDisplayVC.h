//
//  CustomSpreadCardDisplayVC.h
//  UniversalOracleCards
//
//  Created by whiznic on 01/04/19.
//  Copyright © 2019 whiznic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <AVKit/AVKit.h>


@interface CustomSpreadCardDisplayVC : UIViewController <UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UICollectionViewDelegate, UITextFieldDelegate,UITextFieldDelegate>
@property (strong, nonatomic) IBOutlet UICollectionView *colSpread;
@property (nonatomic) AVPlayer *playerViewController;
@property (strong, nonatomic) IBOutlet UIView *viewDisply;
@property (strong, nonatomic) IBOutlet UITextField *txtTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (strong, nonatomic) NSMutableArray *get_cardID,*get_CardPosition;
@property (strong, nonatomic) NSDictionary *get_Data;
@property (strong, nonatomic) NSString *Title,*get_spread_id,*getvalue;
@property (nonatomic) float grid;
@end


