//
//  CustomSpreadCardDisplayCell.h
//  UniversalOracleCards
//
//  Created by whiznic on 01/04/19.
//  Copyright © 2019 whiznic. All rights reserved.
//

#import <UIKit/UIKit.h>
#define CustomSpreadCardDisplayVCID @"CustomSpreadCardDisplayVCID"


@interface CustomSpreadCardDisplayCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UIView *ViewFrame;
@property (strong, nonatomic) IBOutlet UIView *ViewBorder;
@property (weak, nonatomic) IBOutlet UIButton *btnMyjournal;
@property (strong, nonatomic) IBOutlet UIImageView *imgCell;
@end


