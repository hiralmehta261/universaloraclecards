//
//  ConnectionCardVC.m
//  UniversalOracleCards
//
//  Created by whiznic on 14/03/19.
//  Copyright © 2019 whiznic. All rights reserved.
//

#import "ConnectionCardVC.h"
#import "HomeVC.h"
#import "GeneralAdviceVC.h"
@import MediaPlayer;
#import <IQKeyboardManager/IQKeyboardManager.h>
#import "CommanMethods.h"
#import "AppDelegate.h"
#import "CommunicationHandler.h"
#import <AudioToolbox/AudioServices.h>
#import "SharkfoodMuteSwitchDetector.h"

@interface ConnectionCardVC ()
{
    NSLocale* currentLocale;
    NSDateFormatter *dateFormatter;
    NSMutableArray *arrImage;
    NSString *joinedString;
    NSString *readingid;
    int pass;
}
@property (nonatomic,strong) SharkfoodMuteSwitchDetector* detector;
@end

@implementation ConnectionCardVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self SetBorder:self.ViewCard1];
    [self SetBorder:self.ViewCard2];
    [self SetBorder:self.ViewCard3];
    [self SetBorder:self.ViewCard4];
    [self SetBorder:self.ViewCard5];
    
    
    
    
    currentLocale = [NSLocale currentLocale];
    [[NSDate date] descriptionWithLocale:currentLocale];
    dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    self.txtTitle.delegate = self;
    [[IQKeyboardManager sharedManager] setEnable:true];
    
    if (self.get_Data == 0) {
        self.txtTitle.text = [dateFormatter stringFromDate:[NSDate date]];
    } else {
        self.txtTitle.text = [self.get_Data objectForKey:@"save_name"];
    }
    
    if (self.get_Data == 0) {
        joinedString = [self.get_cardID componentsJoinedByString:@","];
        
        NSString *timeCreated = joinedString;
        NSArray *timeArray = [timeCreated componentsSeparatedByString:@","];
        NSString *t = [timeArray objectAtIndex:0];
        NSString *t1 = [timeArray objectAtIndex:1];
        NSString *t2 = [timeArray objectAtIndex:2];
        NSString *t3 = [timeArray objectAtIndex:3];
        NSString *t4 = [timeArray objectAtIndex:4];
        
        
        self.imgCard1.image = [UIImage imageNamed:t];
        self.imgCard2.image = [UIImage imageNamed:t1];
        self.imgCard3.image = [UIImage imageNamed:t2];
        self.imgCard4.image = [UIImage imageNamed:t3];
        self.imgCard5.image = [UIImage imageNamed:t4];
        
        
        
        
    } else {
        joinedString = [self.get_Data objectForKey:@"card_number"];
        
        NSString *timeCreated = joinedString;
        NSArray *timeArray = [timeCreated componentsSeparatedByString:@","];
        NSString *t = [timeArray objectAtIndex:0];
        NSString *t1 = [timeArray objectAtIndex:1];
        NSString *t2 = [timeArray objectAtIndex:2];
        NSString *t3 = [timeArray objectAtIndex:3];
        NSString *t4 = [timeArray objectAtIndex:4];
        
        
        self.imgCard1.image = [UIImage imageNamed:t];
        self.imgCard2.image = [UIImage imageNamed:t1];
        self.imgCard3.image = [UIImage imageNamed:t2];
        self.imgCard4.image = [UIImage imageNamed:t3];
        self.imgCard5.image = [UIImage imageNamed:t4];
        
    }
    
    arrImage = [[NSMutableArray alloc]init];
    [arrImage addObject:self.imgCard1.image];
    [arrImage addObject:self.imgCard2.image];
    [arrImage addObject:self.imgCard3.image];
    [arrImage addObject:self.imgCard4.image];
    [arrImage addObject:self.imgCard5.image];
    
    arrImage = [[NSMutableArray alloc]init];
    [arrImage addObject:self.imgCard1.image];
    [arrImage addObject:self.imgCard2.image];
    [arrImage addObject:self.imgCard3.image];
    [arrImage addObject:self.imgCard4.image];
    [arrImage addObject:self.imgCard5.image];
    
    
        self.detector = [SharkfoodMuteSwitchDetector shared];
        __weak ConnectionCardVC* sself = self;
        self.detector.silentNotify = ^(BOOL silent){
            if (silent) {
                
                NSLog(@"device is silent");
            } else {
                
                NSLog(@"device is in ringing mode");
                [self.playerViewController performSelector:@selector(play) withObject:nil afterDelay:0.01];
            }
        };
    

    
 
    
    
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    NSString *stringVideoName = @"stars_animated.mp4";
    NSString *filepath = [[NSBundle mainBundle] pathForResource:stringVideoName ofType:nil];
    NSLog(@" filepath :- %@", filepath);
    NSURL *fileURL = [NSURL fileURLWithPath:filepath];
    self.playerViewController = [AVPlayer playerWithURL:fileURL];
    self.playerViewController.actionAtItemEnd = AVPlayerActionAtItemEndNone;
    AVPlayerLayer *videoLayer = [AVPlayerLayer playerLayerWithPlayer:self.playerViewController];
    videoLayer.frame = self.view.bounds;
    videoLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
    [self.view.layer addSublayer:videoLayer];
    
    [self.playerViewController play];
    
    NSString *savedValue = [[NSUserDefaults standardUserDefaults]
                            stringForKey:@"audio"];
    if([savedValue isEqualToString:@"off"]) {
        
        [self.playerViewController setVolume:0.0];
    } else {
        
        [self.playerViewController setVolume:1.0];
    }
    
    NSLog(@" savedValue :- %@", savedValue);
    
    CALayer *border1 = [CALayer layer];
    CGFloat borderWidth1 = 1;
    border1.borderColor = [UIColor colorWithRed:173/255.0 green:173/255.0 blue:173/255.0 alpha:1.0].CGColor;
    border1.frame = CGRectMake(0, self.txtTitle.frame.size.height - borderWidth1, self.txtTitle.frame.size.width, self.txtTitle.frame.size.height);
    border1.borderWidth = borderWidth1;
    [self.txtTitle.layer addSublayer:border1];
    self.txtTitle.layer.masksToBounds = YES;
    
    self.viewDisply.layer.zPosition = 1;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(itemDidFinishPlaying:) name:AVPlayerItemDidPlayToEndTimeNotification object:[self.playerViewController currentItem]];
    
       [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationDidBecomeActive:) name:UIApplicationDidEnterBackgroundNotification object:nil];
    
    [[UIApplication sharedApplication] beginReceivingRemoteControlEvents];
    [self becomeFirstResponder];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [self.playerViewController pause];
    [super viewWillDisappear:animated];
    [[UIApplication sharedApplication] endReceivingRemoteControlEvents];
    [self resignFirstResponder];
}


- (void)remoteControlReceivedWithEvent:(UIEvent *)event {
    switch (event.subtype) {
        case UIEventSubtypeRemoteControlTogglePlayPause:
            if([self.playerViewController rate] == 0){
                [self.playerViewController play];
            } else {
                [self.playerViewController pause];
            }
            break;
        case UIEventSubtypeRemoteControlPlay:
            [self.playerViewController play];
            break;
        case UIEventSubtypeRemoteControlPause:
            [self.playerViewController pause];
            break;
        default:
            break;
    }
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    self.detector = [SharkfoodMuteSwitchDetector shared];
    __weak ConnectionCardVC* sself = self;
    self.detector.silentNotify = ^(BOOL silent){
        if (silent) {
            
            NSLog(@"device is silent");
        } else {
            
            NSLog(@"device is in ringing mode");
            [self.playerViewController performSelector:@selector(play) withObject:nil afterDelay:0.01];
        }
    };
}


- (void)itemDidFinishPlaying:(NSNotification *)notification {
    AVPlayerItem *player = [notification object];
    [player seekToTime:kCMTimeZero];
}

#pragma Function
-(void) SetBorder:(UIView *)viewborder {
    viewborder.layer.borderColor = [UIColor colorWithRed:173/255.0 green:173/255.0 blue:173/255.0 alpha:1.0].CGColor;
    viewborder.layer.borderWidth = 1;
    viewborder.layer.cornerRadius = 10;
}

#pragma TEXTFIELD-DELEGATE

- (BOOL) textFieldShouldBeginEditing:(UITextField *)textField {
    self.txtTitle.text = @"";
   
    return YES;
}

-(void) textFieldDidChange:(UITextField *)textField {
    
    if(self.txtTitle.text.length == 0) {
        self.txtTitle.text = [dateFormatter stringFromDate:[NSDate date]];
        
    }
}

-(BOOL) textFieldShouldEndEditing:(UITextField *)textField {
    
    if(self.txtTitle.text.length == 0) {
        self.txtTitle.text = [dateFormatter stringFromDate:[NSDate date]];
        
    }
    return YES;
}


#pragma Mark - Button Action

- (IBAction)btnMyJournal:(id)sender {
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
        GeneralAdviceVC *generalVC = [[GeneralAdviceVC alloc] initWithNibName:@"GeneralAdviceVC" bundle:nil];
        generalVC.get_Selected_Value = @"1";
        generalVC.title = @"Connection";
         generalVC.spread_type = @"Connection";
        if (self.get_Data == 0) {
            generalVC.get_cardData = self.get_cardID;
        } else {
            joinedString = [self.get_Data objectForKey:@"card_number"];
            NSString *timeCreated = joinedString;
            NSArray *timeArray = [timeCreated componentsSeparatedByString:@","];
            NSMutableArray *data = [[NSMutableArray alloc] init];
            [data addObjectsFromArray:timeArray];
            generalVC.get_cardData = data;
        }
        if (readingid.length == 0) {
            if ([CommanMethods isNetworkRechable]) {
                [self callForSaveReading:self.txtTitle.text];
                generalVC.getid = readingid;
            } else {
                [CommanMethods displayAlertMessage:key_Informaction message:kInternetConnectionMessage currentRef:self];
            }
        } else {
            generalVC.getid = readingid;
            [self.navigationController pushViewController:generalVC animated:YES];
        }
        
    } else {
        GeneralAdviceVC *generalVC = [[GeneralAdviceVC alloc] initWithNibName:@"GeneralAdviceVC_IPad" bundle:nil];
        generalVC.get_Selected_Value = @"5";
        generalVC.title = @"Connection";
         generalVC.spread_type = @"Connection";
        if (self.get_Data == 0) {
            generalVC.get_cardData = self.get_cardID;
        } else {
            joinedString = [self.get_Data objectForKey:@"card_number"];
            NSString *timeCreated = joinedString;
            NSArray *timeArray = [timeCreated componentsSeparatedByString:@","];
            NSMutableArray *data = [[NSMutableArray alloc] init];
            [data addObjectsFromArray:timeArray];
            generalVC.get_cardData = data;
        }
        if (readingid.length == 0) {
            if ([CommanMethods isNetworkRechable]) {
                [self callForSaveReading:self.txtTitle.text];
                generalVC.getid = readingid;
            } else {
                [CommanMethods displayAlertMessage:key_Informaction message:kInternetConnectionMessage currentRef:self];
            }
        } else {
            generalVC.getid = readingid;
            [self.navigationController pushViewController:generalVC animated:YES];
        }
        
    }
}

- (IBAction)btnSaveReading:(id)sender {
    pass = 1;
    if (self.get_Data == 0) {
        if ([CommanMethods isNetworkRechable]) {
            [self callForSaveReading:self.txtTitle.text];
        } else {
            [CommanMethods displayAlertMessage:key_Informaction message:kInternetConnectionMessage currentRef:self];
        }
    } else {
        if ([CommanMethods isNetworkRechable]) {
            [self callForEditReading:self.txtTitle.text];
        } else {
            [CommanMethods displayAlertMessage:key_Informaction message:kInternetConnectionMessage currentRef:self];
        }
    }
}

- (IBAction)btnEdit:(id)sender {
   
    [self.txtTitle becomeFirstResponder];
}

- (IBAction)btnShare:(id)sender {
    NSMutableArray *activityItems= arrImage;
    UIActivityViewController *activityViewController = [[UIActivityViewController alloc] initWithActivityItems:activityItems applicationActivities:nil];
    activityViewController.excludedActivityTypes = @[UIActivityTypePostToWeibo, UIActivityTypePrint, UIActivityTypeCopyToPasteboard, UIActivityTypeAssignToContact, UIActivityTypeSaveToCameraRoll, UIActivityTypeAddToReadingList, UIActivityTypePostToFlickr, UIActivityTypePostToVimeo, UIActivityTypePostToTencentWeibo, UIActivityTypeAirDrop];
    [self presentViewController:activityViewController animated:YES completion:nil];
}

- (IBAction)btnBack:(id)sender {
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
        [self.navigationController popViewControllerAnimated:YES];
    } else {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

#pragma CallAPI

-(void)callForSaveReading:(NSString*)title  {
    NSMutableDictionary *dcitParams = [[NSMutableDictionary alloc] init];
    [dcitParams setValue:@"" forKey:@"custom_spread_id"];
    [dcitParams setValue:@"Connection" forKey:@"spread_type"];
    [dcitParams setValue:title forKey:@"save_name"];
    [dcitParams setValue:joinedString forKey:@"card_number"];
    [dcitParams setValue:@"5" forKey:@"no_of_cards"];
    
    
    
    NSLog(@" dcitParams :-%@", dcitParams);
    
    [AppDelegate addVwLoading];
    [CommunicationHandler callForSaveReading:dcitParams Sucess:^(NSDictionary *dict, NSURLResponse *response) {
        NSLog(@"%@", dict);
        dispatch_async(dispatch_get_main_queue(), ^{
            [AppDelegate removeVwLoading];
            if ([[dict objectForKey:@"error"] boolValue] == false) {
               
                    [CommanMethods displayAlertMessageWithCallBack:nil message:[dict objectForKey:@"message"] refrence:self Success:^{
                        readingid = [dict objectForKey:@"save_reading_id"];
                        
                        if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
                            GeneralAdviceVC *generalVC = [[GeneralAdviceVC alloc] initWithNibName:@"GeneralAdviceVC" bundle:nil];
                            generalVC.get_Selected_Value = @"5";
                            generalVC.title = @"Connection";
                            generalVC.spread_type = @"Connection";
                            if (self.get_Data == 0) {
                                generalVC.get_cardData = self.get_cardID;
                            } else {
                                joinedString = [self.get_Data objectForKey:@"card_number"];
                                NSString *timeCreated = joinedString;
                                NSArray *timeArray = [timeCreated componentsSeparatedByString:@","];
                                NSMutableArray *data = [[NSMutableArray alloc] init];
                                [data addObjectsFromArray:timeArray];
                                generalVC.get_cardData = data;
                            }
                            generalVC.getid = readingid;
                            [self.navigationController pushViewController:generalVC animated:YES];
                        }
                        else {
                            GeneralAdviceVC *generalVC = [[GeneralAdviceVC alloc] initWithNibName:@"GeneralAdviceVC_IPad" bundle:nil];
                            generalVC.get_Selected_Value = @"5";
                            generalVC.title = @"Connection";
                            generalVC.spread_type = @"Connection";
                            if (self.get_Data == 0) {
                                generalVC.get_cardData = self.get_cardID;
                            } else {
                                joinedString = [self.get_Data objectForKey:@"card_number"];
                                NSString *timeCreated = joinedString;
                                NSArray *timeArray = [timeCreated componentsSeparatedByString:@","];
                                NSMutableArray *data = [[NSMutableArray alloc] init];
                                [data addObjectsFromArray:timeArray];
                                generalVC.get_cardData = data;
                            }
                            generalVC.getid = readingid;
                            [self.navigationController pushViewController:generalVC animated:YES];
                        }
                        
                    }];
               
            } else {
                [CommanMethods displayAlertMessage:@"UOC" message:[dict objectForKey:@"message"] currentRef:self];
            }
        });
    } Error:^(NSError *error, NSURLResponse *response) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [AppDelegate removeVwLoading];
        });
    }];
}


-(void)callForEditReading:(NSString*)title  {
    NSMutableDictionary *dcitParams = [[NSMutableDictionary alloc] init];
    [dcitParams setValue:@"" forKey:@"custom_spread_id"];
    [dcitParams setValue:@"Connection" forKey:@"spread_type"];
    [dcitParams setValue:title forKey:@"save_name"];
    [dcitParams setValue:joinedString forKey:@"card_number"];
    [dcitParams setValue:@"5" forKey:@"no_of_cards"];
    [dcitParams setValue:[self.get_Data objectForKey:@"id"] forKey:@"id"];
    
    
    NSLog(@" dcitParams :-%@", dcitParams);
    
    [AppDelegate addVwLoading];
    [CommunicationHandler callForSaveReading:dcitParams Sucess:^(NSDictionary *dict, NSURLResponse *response) {
        NSLog(@"%@", dict);
        dispatch_async(dispatch_get_main_queue(), ^{
            [AppDelegate removeVwLoading];
            if ([[dict objectForKey:@"error"] boolValue] == false) {
                if ([self.getvalue isEqualToString:@"1"]) {
                    [CommanMethods displayAlertMessageWithCallBack:nil message:[dict objectForKey:@"message"] refrence:self Success:^{
                        readingid = [dict objectForKey:@"save_reading_id"];
                        if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
                            [self.navigationController popViewControllerAnimated:YES];
                        } else {
                            [self.navigationController popViewControllerAnimated:YES];
                        }
                    }];
                } else {
                    [CommanMethods displayAlertMessage:@"UOC" message:[dict objectForKey:@"message"] currentRef:self];
                    readingid = [dict objectForKey:@"save_reading_id"];
                }
            } else {
                [CommanMethods displayAlertMessage:@"UOC" message:[dict objectForKey:@"message"] currentRef:self];
            }
        });
    } Error:^(NSError *error, NSURLResponse *response) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [AppDelegate removeVwLoading];
        });
    }];
}

@end
