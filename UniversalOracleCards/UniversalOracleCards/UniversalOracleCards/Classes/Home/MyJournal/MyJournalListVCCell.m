//
//  MyJournalListVCCell.m
//  UniversalOracleCards
//
//  Created by whiznic on 14/03/19.
//  Copyright © 2019 whiznic. All rights reserved.
//

#import "MyJournalListVCCell.h"

@implementation MyJournalListVCCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
