//
//  MyJournalListVC.m
//  UniversalOracleCards
//
//  Created by whiznic on 14/03/19.
//  Copyright © 2019 whiznic. All rights reserved.
//

#import "MyJournalListVC.h"
#import "HomeVC.h"
#import "MyJournalListVCCell.h"
#import "AppDelegate.h"
#import "SideMenuVC.h"
#import "CommanMethods.h"
#import "CommunicationHandler.h"
#import "DailyCardVC.h"
#import "GeneralAdviceCardVC.h"
#import "InnerWorkCardVC.h"
#import "LifePurposeCardVC.h"
#import "ConnectionCardVC.h"
#import <AudioToolbox/AudioServices.h>
#import "SharkfoodMuteSwitchDetector.h"
#import "CustomSpreadCardDisplayVC.h"

@import MediaPlayer;

@interface MyJournalListVC ()
{
    NSMutableArray *arrJournalList;
    int value;
}
@property (nonatomic,strong) SharkfoodMuteSwitchDetector* detector;
@end

@implementation MyJournalListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    
    self.tblJournalList.dataSource = self;
    self.tblJournalList.delegate = self;
    
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
        UINib *cellnib = [UINib nibWithNibName:@"MyJournalListVCCell" bundle:nil];
        [self.tblJournalList registerNib:cellnib forCellReuseIdentifier:MyJournalListVCCellID];
    } else {
        UINib *cellnib = [UINib nibWithNibName:@"MyJournalListVCCell_iPad" bundle:nil];
         [self.tblJournalList registerNib:cellnib forCellReuseIdentifier:MyJournalListVCCellID];
        
    }
    
    
    
    value = -1;
    
    if ([CommanMethods isNetworkRechable]) {
        [self callForGetMyJournal];
    } else {
        [CommanMethods displayAlertMessage:key_Informaction message:kInternetConnectionMessage currentRef:self];
    }
    
    NSUserDefaults *userDefaults1 = [NSUserDefaults standardUserDefaults];
    NSDictionary *dictData1 = [userDefaults1 objectForKey:@"journaldata"];
    NSLog(@" dictData1 :- %@", dictData1);
    if ([[dictData1 objectForKey:@"data"] count] > 0) {
        self.lblMessage.hidden = TRUE;
        self.lblMessage.text = @"";
    } else {
        self.lblMessage.text = @"No Journal here";
        self.lblMessage.hidden = FALSE;
    }
    
    self.detector = [SharkfoodMuteSwitchDetector shared];
    __weak MyJournalListVC* sself = self;
    self.detector.silentNotify = ^(BOOL silent){
        if (silent) {
            
            NSLog(@"device is silent");
        } else {
            
            NSLog(@"device is in ringing mode");
            
        }
    };
    
}

- (void)viewDidAppear:(BOOL)animated {
    
    
    [super viewDidAppear:animated];

    NSString *stringVideoName = @"stars_animated.mp4";
    NSString *filepath = [[NSBundle mainBundle] pathForResource:stringVideoName ofType:nil];
    NSLog(@" filepath :- %@", filepath);
    NSURL *fileURL = [NSURL fileURLWithPath:filepath];
    self.playerViewController = [AVPlayer playerWithURL:fileURL];
    self.playerViewController.actionAtItemEnd = AVPlayerActionAtItemEndNone;
    AVPlayerLayer *videoLayer = [AVPlayerLayer playerLayerWithPlayer:self.playerViewController];
    videoLayer.frame = self.view.bounds;
    videoLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
    [self.view.layer addSublayer:videoLayer];
    
    [self.playerViewController play];
    
    NSString *savedValue = [[NSUserDefaults standardUserDefaults]
                            stringForKey:@"audio"];
    if([savedValue isEqualToString:@"off"]) {
        
        [self.playerViewController setVolume:0.0];
    } else {
        
        [self.playerViewController setVolume:1.0];
    }
    
    NSLog(@" savedValue :- %@", savedValue);
    
    self.viewDisply.layer.zPosition = 1;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(itemDidFinishPlaying:) name:AVPlayerItemDidPlayToEndTimeNotification object:[self.playerViewController currentItem]];
    
      [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationDidBecomeActive:) name:UIApplicationDidEnterBackgroundNotification object:nil];
    
    [[UIApplication sharedApplication] beginReceivingRemoteControlEvents];
    [self becomeFirstResponder];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [self.playerViewController pause];
    [super viewWillDisappear:animated];
    [[UIApplication sharedApplication] endReceivingRemoteControlEvents];
    [self resignFirstResponder];
}


- (void)remoteControlReceivedWithEvent:(UIEvent *)event {
    switch (event.subtype) {
        case UIEventSubtypeRemoteControlTogglePlayPause:
            if([self.playerViewController rate] == 0){
                [self.playerViewController play];
            } else {
                [self.playerViewController pause];
            }
            break;
        case UIEventSubtypeRemoteControlPlay:
            [self.playerViewController play];
            break;
        case UIEventSubtypeRemoteControlPause:
            [self.playerViewController pause];
            break;
        default:
            break;
    }
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    self.detector = [SharkfoodMuteSwitchDetector shared];
    __weak MyJournalListVC* sself = self;
    self.detector.silentNotify = ^(BOOL silent){
        if (silent) {
            
            NSLog(@"device is silent");
        } else {
            
            NSLog(@"device is in ringing mode");
            [self.playerViewController performSelector:@selector(play) withObject:nil afterDelay:0.01];
        }
    };
}


- (void)itemDidFinishPlaying:(NSNotification *)notification {
    AVPlayerItem *player = [notification object];
    [player seekToTime:kCMTimeZero];
}

- (IBAction)btnBack:(id)sender {
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
        HomeVC *homeVC = [[HomeVC alloc] initWithNibName:@"HomeVC" bundle:nil];
        [self.navigationController pushViewController:homeVC animated:YES];
    } else {
        HomeVC *homeVC = [[HomeVC alloc] initWithNibName:@"HomeVC_IPad" bundle:nil];
        [self.navigationController pushViewController:homeVC animated:YES];
    }
}

#pragma mark - TablViewCellAction

-(IBAction)btnCellDelete:(id)sender {
    
    NSIndexPath *indexPath;
    UITableView *tableView;
    
    MyJournalListVCCell *cell = [tableView dequeueReusableCellWithIdentifier:MyJournalListVCCellID forIndexPath:indexPath];
    cell = [tableView cellForRowAtIndexPath:indexPath];
    
    NSDictionary *dictData = [[NSDictionary alloc] init];
    dictData = [arrJournalList objectAtIndex:[sender tag]];
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"UOC" message:@"Are you sure you want to Delete?" preferredStyle:UIAlertControllerStyleAlert];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
        [self dismissViewControllerAnimated:YES completion:^{
        }];
    }]];
    
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        if ([CommanMethods isNetworkRechable]) {
            [self callForDeleteJournal:[dictData objectForKey:@"id"]];
        } else {
            [CommanMethods displayAlertMessage:key_Informaction message:kInternetConnectionMessage currentRef:self];
        }
        
        
    }]];
    
    // Present action sheet.
    [self presentViewController:actionSheet animated:YES completion:nil];
    
    
}

-(IBAction)btnCellEdit:(id)sender {
    
    NSIndexPath *indexPath;
    UITableView *tableView;
    
    MyJournalListVCCell *cell = [tableView dequeueReusableCellWithIdentifier:MyJournalListVCCellID forIndexPath:indexPath];
    cell = [tableView cellForRowAtIndexPath:indexPath];
    
    NSDictionary *dictData = [[NSDictionary alloc] init];
    dictData = [arrJournalList objectAtIndex:[sender tag]];
  
    if ([[dictData objectForKey:@"spread_type"] isEqual: @"Daily"] && [[dictData objectForKey:@"c_spread_type"] isEqualToString:@""]) {
        if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
            DailyCardVC *dailyVC = [[DailyCardVC alloc] initWithNibName:@"DailyCardVC" bundle:nil];
            dailyVC.get_Data = dictData;
            dailyVC.getvalue = @"1";
            [self.navigationController pushViewController:dailyVC animated:YES];
        } else {
            DailyCardVC *dailyVC = [[DailyCardVC alloc] initWithNibName:@"DailyCardVC_IPad" bundle:nil];
            dailyVC.get_Data = dictData;
            dailyVC.getvalue = @"1";
            [self.navigationController pushViewController:dailyVC animated:YES];
        }
    }
    else if ([[dictData objectForKey:@"spread_type"] isEqual: @"GeneralAdvice"] && [[dictData objectForKey:@"c_spread_type"] isEqualToString:@""]) {
        if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
            GeneralAdviceCardVC *dailyVC = [[GeneralAdviceCardVC alloc] initWithNibName:@"GeneralAdviceCardVC" bundle:nil];
            dailyVC.get_Data = dictData;
            dailyVC.getvalue = @"1";
            [self.navigationController pushViewController:dailyVC animated:YES];
        } else {
            GeneralAdviceCardVC *dailyVC = [[GeneralAdviceCardVC alloc] initWithNibName:@"GeneralAdviceCardVC_IPad" bundle:nil];
            dailyVC.get_Data = dictData;
            dailyVC.getvalue = @"1";
            [self.navigationController pushViewController:dailyVC animated:YES];
        }
    }
    else if ([[dictData objectForKey:@"spread_type"] isEqual: @"InnerWork"] && [[dictData objectForKey:@"c_spread_type"] isEqualToString:@""]) {
        if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
            InnerWorkCardVC *dailyVC = [[InnerWorkCardVC alloc] initWithNibName:@"InnerWorkCardVC" bundle:nil];
            dailyVC.get_Data = dictData;
            dailyVC.getvalue = @"1";
            [self.navigationController pushViewController:dailyVC animated:YES];
        } else {
            InnerWorkCardVC *dailyVC = [[InnerWorkCardVC alloc] initWithNibName:@"InnerWorkCardVC_IPad" bundle:nil];
            dailyVC.get_Data = dictData;
            dailyVC.getvalue = @"1";
            [self.navigationController pushViewController:dailyVC animated:YES];
        }
    }
    else if ([[dictData objectForKey:@"spread_type"] isEqual: @"LifePurpose"] && [[dictData objectForKey:@"c_spread_type"] isEqualToString:@""]) {
        if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
            LifePurposeCardVC *dailyVC = [[LifePurposeCardVC alloc] initWithNibName:@"LifePurposeCardVC" bundle:nil];
            dailyVC.get_Data = dictData;
            dailyVC.getvalue = @"1";
            [self.navigationController pushViewController:dailyVC animated:YES];
        } else {
            LifePurposeCardVC *dailyVC = [[LifePurposeCardVC alloc] initWithNibName:@"LifePurposeCardVC_IPad" bundle:nil];
            dailyVC.get_Data = dictData;
            dailyVC.getvalue = @"1";
            [self.navigationController pushViewController:dailyVC animated:YES];
        }
    }
    else if ([[dictData objectForKey:@"spread_type"] isEqual: @"Connection"] && [[dictData objectForKey:@"c_spread_type"] isEqualToString:@""]) {
        if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
            ConnectionCardVC *dailyVC = [[ConnectionCardVC alloc] initWithNibName:@"ConnectionCardVC" bundle:nil];
            dailyVC.get_Data = dictData;
            dailyVC.getvalue = @"1";
            [self.navigationController pushViewController:dailyVC animated:YES];
        } else {
            ConnectionCardVC *dailyVC = [[ConnectionCardVC alloc] initWithNibName:@"ConnectionCardVC_IPad" bundle:nil];
            dailyVC.get_Data = dictData;
            dailyVC.getvalue = @"1";
            [self.navigationController pushViewController:dailyVC animated:YES];
        }
    }
    else if ([[dictData objectForKey:@"spread_type"] isEqual: @"custom"] && [[dictData objectForKey:@"c_spread_type"] isEqualToString:@"2x2"]) {
        if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
            CustomSpreadCardDisplayVC *dailyVC = [[CustomSpreadCardDisplayVC alloc] initWithNibName:@"CustomSpreadCardDisplayVC" bundle:nil];
            dailyVC.get_Data = dictData;
            dailyVC.grid = 2.0;
            dailyVC.getvalue = @"1";
            [self.navigationController pushViewController:dailyVC animated:YES];
        } else {
            CustomSpreadCardDisplayVC *dailyVC = [[CustomSpreadCardDisplayVC alloc] initWithNibName:@"CustomSpreadCardDisplayVC_IPad" bundle:nil];
            dailyVC.get_Data = dictData;
            dailyVC.grid = 2.0;
            dailyVC.getvalue = @"1";
            [self.navigationController pushViewController:dailyVC animated:YES];
        }
    }
    else if ([[dictData objectForKey:@"spread_type"] isEqual: @"custom"] && [[dictData objectForKey:@"c_spread_type"] isEqualToString:@"3x3"]) {
        if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
            CustomSpreadCardDisplayVC *dailyVC = [[CustomSpreadCardDisplayVC alloc] initWithNibName:@"CustomSpreadCardDisplayVC" bundle:nil];
            dailyVC.get_Data = dictData;
            dailyVC.grid = 3.0;
            dailyVC.getvalue = @"1";
            [self.navigationController pushViewController:dailyVC animated:YES];
        } else {
            CustomSpreadCardDisplayVC *dailyVC = [[CustomSpreadCardDisplayVC alloc] initWithNibName:@"CustomSpreadCardDisplayVC_IPad" bundle:nil];
            dailyVC.get_Data = dictData;
            dailyVC.grid = 3.0;
            dailyVC.getvalue = @"1";
            [self.navigationController pushViewController:dailyVC animated:YES];
        }
    }
    else if ([[dictData objectForKey:@"spread_type"] isEqual: @"custom"] && [[dictData objectForKey:@"c_spread_type"] isEqualToString:@"4x4"]) {
        if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
            CustomSpreadCardDisplayVC *dailyVC = [[CustomSpreadCardDisplayVC alloc] initWithNibName:@"CustomSpreadCardDisplayVC" bundle:nil];
            dailyVC.get_Data = dictData;
            dailyVC.grid = 4.0;
            dailyVC.getvalue = @"1";
            [self.navigationController pushViewController:dailyVC animated:YES];
        } else {
            CustomSpreadCardDisplayVC *dailyVC = [[CustomSpreadCardDisplayVC alloc] initWithNibName:@"CustomSpreadCardDisplayVC_IPad" bundle:nil];
            dailyVC.get_Data = dictData;
            dailyVC.grid = 4.0;
            dailyVC.getvalue = @"1";
            [self.navigationController pushViewController:dailyVC animated:YES];
        }
    }
    else if ([[dictData objectForKey:@"spread_type"] isEqual: @"custom"] && [[dictData objectForKey:@"c_spread_type"] isEqualToString:@"5x5"]) {
        if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
            CustomSpreadCardDisplayVC *dailyVC = [[CustomSpreadCardDisplayVC alloc] initWithNibName:@"CustomSpreadCardDisplayVC" bundle:nil];
            dailyVC.get_Data = dictData;
            dailyVC.grid = 5.0;
            dailyVC.getvalue = @"1";
            [self.navigationController pushViewController:dailyVC animated:YES];
        } else {
            CustomSpreadCardDisplayVC *dailyVC = [[CustomSpreadCardDisplayVC alloc] initWithNibName:@"CustomSpreadCardDisplayVC_IPad" bundle:nil];
            dailyVC.get_Data = dictData;
            dailyVC.grid = 5.0;
            dailyVC.getvalue = @"1";
            [self.navigationController pushViewController:dailyVC animated:YES];
        }
    }
    else if ([[dictData objectForKey:@"spread_type"] isEqual: @"custom"] && [[dictData objectForKey:@"c_spread_type"] isEqualToString:@"6x6"]) {
        if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
            CustomSpreadCardDisplayVC *dailyVC = [[CustomSpreadCardDisplayVC alloc] initWithNibName:@"CustomSpreadCardDisplayVC" bundle:nil];
            dailyVC.get_Data = dictData;
            dailyVC.grid = 6.0;
            dailyVC.getvalue = @"1";
            [self.navigationController pushViewController:dailyVC animated:YES];
        } else {
            CustomSpreadCardDisplayVC *dailyVC = [[CustomSpreadCardDisplayVC alloc] initWithNibName:@"CustomSpreadCardDisplayVC_IPad" bundle:nil];
            dailyVC.get_Data = dictData;
            dailyVC.grid = 6.0;
            dailyVC.getvalue = @"1";
            [self.navigationController pushViewController:dailyVC animated:YES];
        }
    }

    NSLog(@" Edit row :- %@", [arrJournalList objectAtIndex:[sender tag]]);


}

-(IBAction)btnCellMoreAction:(id)sender {
    NSIndexPath *indexPath;
    UITableView *tableView;
    
    MyJournalListVCCell *cell = [tableView dequeueReusableCellWithIdentifier:MyJournalListVCCellID forIndexPath:indexPath];
    cell = [tableView cellForRowAtIndexPath:indexPath];
    
   value = [sender tag];
    
    NSLog(@" value :- %d", value);
    [self.tblJournalList reloadData];
}

#pragma mark- TableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return arrJournalList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
   MyJournalListVCCell *cell = [tableView dequeueReusableCellWithIdentifier:MyJournalListVCCellID forIndexPath:indexPath];
    
    cell.btnMoreAction.layer.borderColor = UIColor.clearColor.CGColor;
    cell.btnMoreAction.layer.borderWidth = 1.0;
    cell.btnMoreAction.layer.cornerRadius = 8;
    cell.btnMore.tag = indexPath.row;
    cell.btnEdit.tag = indexPath.row;
    cell.btnDelete.tag = indexPath.row;
    
    NSDictionary *dictData = [[NSDictionary alloc] init];
    dictData = [arrJournalList objectAtIndex:indexPath.row];
    
    cell.lblSpreadName.text = [dictData objectForKey:@"spread_type"];
    cell.lblDate.text = [dictData objectForKey:@"save_name"];
    cell.lblTextNotes.text = [dictData objectForKey:@"num_of_note"];
    NSString *str_name = [dictData objectForKey:@"no_of_cards"];
    NSString *str1 = [[str_name stringByAppendingString:@" "] stringByAppendingString:@"Cards "];
    cell.lbltotalCards.text = str1;
    cell.lblAudio.text = [dictData objectForKey:@"num_of_audio"];
    
   
    [cell.btnDelete addTarget:self action:@selector(btnCellDelete:) forControlEvents:UIControlEventTouchUpInside];
    [cell.btnEdit addTarget:self action:@selector(btnCellEdit:) forControlEvents:UIControlEventTouchUpInside];
    [cell.btnMore addTarget:self action:@selector(btnCellMoreAction:) forControlEvents:UIControlEventTouchUpInside];
    
    
    if (indexPath.row == value )
    {
        cell.btnMoreAction.hidden = FALSE;
    }
    else
    {
        cell.btnMoreAction.hidden = TRUE;
    }
    return cell;
}

#pragma mark- TableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
        return 140;
    } else {
        return 175;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    value = -1;
    [self.tblJournalList reloadData];
    NSDictionary *dictData = [[NSDictionary alloc] init];
    dictData = [arrJournalList objectAtIndex:[indexPath row]];
    if ([[dictData objectForKey:@"spread_type"] isEqual: @"Daily"] && [[dictData objectForKey:@"c_spread_type"] isEqualToString:@""]) {
        if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
            DailyCardVC *dailyVC = [[DailyCardVC alloc] initWithNibName:@"DailyCardVC" bundle:nil];
            dailyVC.get_Data = dictData;
            dailyVC.getvalue = @"1";
            [self.navigationController pushViewController:dailyVC animated:YES];
        } else {
            DailyCardVC *dailyVC = [[DailyCardVC alloc] initWithNibName:@"DailyCardVC_IPad" bundle:nil];
            dailyVC.get_Data = dictData;
            dailyVC.getvalue = @"1";
            [self.navigationController pushViewController:dailyVC animated:YES];
        }
    }
    else if ([[dictData objectForKey:@"spread_type"] isEqual: @"GeneralAdvice"] && [[dictData objectForKey:@"c_spread_type"] isEqualToString:@""]) {
        if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
            GeneralAdviceCardVC *dailyVC = [[GeneralAdviceCardVC alloc] initWithNibName:@"GeneralAdviceCardVC" bundle:nil];
            dailyVC.get_Data = dictData;
            dailyVC.getvalue = @"1";
            [self.navigationController pushViewController:dailyVC animated:YES];
        } else {
            GeneralAdviceCardVC *dailyVC = [[GeneralAdviceCardVC alloc] initWithNibName:@"GeneralAdviceCardVC_IPad" bundle:nil];
            dailyVC.get_Data = dictData;
            dailyVC.getvalue = @"1";
            [self.navigationController pushViewController:dailyVC animated:YES];
        }
    }
    else if ([[dictData objectForKey:@"spread_type"] isEqual: @"InnerWork"] && [[dictData objectForKey:@"c_spread_type"] isEqualToString:@""])  {
        if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
            InnerWorkCardVC *dailyVC = [[InnerWorkCardVC alloc] initWithNibName:@"InnerWorkCardVC" bundle:nil];
            dailyVC.get_Data = dictData;
            dailyVC.getvalue = @"1";
            [self.navigationController pushViewController:dailyVC animated:YES];
        } else {
            InnerWorkCardVC *dailyVC = [[InnerWorkCardVC alloc] initWithNibName:@"InnerWorkCardVC_IPad" bundle:nil];
            dailyVC.get_Data = dictData;
            dailyVC.getvalue = @"1";
            [self.navigationController pushViewController:dailyVC animated:YES];
        }
    }
    else if ([[dictData objectForKey:@"spread_type"] isEqual: @"LifePurpose"] && [[dictData objectForKey:@"c_spread_type"] isEqualToString:@""]) {
        if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
            LifePurposeCardVC *dailyVC = [[LifePurposeCardVC alloc] initWithNibName:@"LifePurposeCardVC" bundle:nil];
            dailyVC.get_Data = dictData;
            dailyVC.getvalue = @"1";
            [self.navigationController pushViewController:dailyVC animated:YES];
        } else {
            LifePurposeCardVC *dailyVC = [[LifePurposeCardVC alloc] initWithNibName:@"LifePurposeCardVC_IPad" bundle:nil];
            dailyVC.get_Data = dictData;
            dailyVC.getvalue = @"1";
            [self.navigationController pushViewController:dailyVC animated:YES];
        }
    }
    else if ([[dictData objectForKey:@"spread_type"] isEqual: @"Connection"] && [[dictData objectForKey:@"c_spread_type"] isEqualToString:@""]) {
        if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
            ConnectionCardVC *dailyVC = [[ConnectionCardVC alloc] initWithNibName:@"ConnectionCardVC" bundle:nil];
            dailyVC.get_Data = dictData;
            dailyVC.getvalue = @"1";
            [self.navigationController pushViewController:dailyVC animated:YES];
        } else {
            ConnectionCardVC *dailyVC = [[ConnectionCardVC alloc] initWithNibName:@"ConnectionCardVC_IPad" bundle:nil];
            dailyVC.get_Data = dictData;
            dailyVC.getvalue = @"1";
            [self.navigationController pushViewController:dailyVC animated:YES];
        }
    }
    else if ([[dictData objectForKey:@"spread_type"] isEqual: @"custom"] && [[dictData objectForKey:@"c_spread_type"] isEqualToString:@"2x2"]) {
        if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
            CustomSpreadCardDisplayVC *dailyVC = [[CustomSpreadCardDisplayVC alloc] initWithNibName:@"CustomSpreadCardDisplayVC" bundle:nil];
            dailyVC.get_Data = dictData;
            dailyVC.grid = 2.0;
            dailyVC.getvalue = @"1";
            [self.navigationController pushViewController:dailyVC animated:YES];
        } else {
            CustomSpreadCardDisplayVC *dailyVC = [[CustomSpreadCardDisplayVC alloc] initWithNibName:@"CustomSpreadCardDisplayVC_IPad" bundle:nil];
            dailyVC.get_Data = dictData;
            dailyVC.grid = 2.0;
            dailyVC.getvalue = @"1";
            [self.navigationController pushViewController:dailyVC animated:YES];
        }
    }
    else if ([[dictData objectForKey:@"spread_type"] isEqual: @"custom"] && [[dictData objectForKey:@"c_spread_type"] isEqualToString:@"3x3"]){
        if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
            CustomSpreadCardDisplayVC *dailyVC = [[CustomSpreadCardDisplayVC alloc] initWithNibName:@"CustomSpreadCardDisplayVC" bundle:nil];
            dailyVC.get_Data = dictData;
            dailyVC.grid = 3.0;
            dailyVC.getvalue = @"1";
            [self.navigationController pushViewController:dailyVC animated:YES];
        } else {
            CustomSpreadCardDisplayVC *dailyVC = [[CustomSpreadCardDisplayVC alloc] initWithNibName:@"CustomSpreadCardDisplayVC_IPad" bundle:nil];
            dailyVC.get_Data = dictData;
            dailyVC.grid = 3.0;
            dailyVC.getvalue = @"1";
            [self.navigationController pushViewController:dailyVC animated:YES];
        }
    }
    else if ([[dictData objectForKey:@"spread_type"] isEqual: @"custom"] && [[dictData objectForKey:@"c_spread_type"] isEqualToString:@"4x4"]) {
        if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
            CustomSpreadCardDisplayVC *dailyVC = [[CustomSpreadCardDisplayVC alloc] initWithNibName:@"CustomSpreadCardDisplayVC" bundle:nil];
            dailyVC.get_Data = dictData;
            dailyVC.grid = 4.0;
            dailyVC.getvalue = @"1";
            [self.navigationController pushViewController:dailyVC animated:YES];
        } else {
            CustomSpreadCardDisplayVC *dailyVC = [[CustomSpreadCardDisplayVC alloc] initWithNibName:@"CustomSpreadCardDisplayVC_IPad" bundle:nil];
            dailyVC.get_Data = dictData;
            dailyVC.grid = 4.0;
            dailyVC.getvalue = @"1";
            [self.navigationController pushViewController:dailyVC animated:YES];
        }
    }
    else if ([[dictData objectForKey:@"spread_type"] isEqual: @"custom"] && [[dictData objectForKey:@"c_spread_type"] isEqualToString:@"5x5"]) {
        if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
            CustomSpreadCardDisplayVC *dailyVC = [[CustomSpreadCardDisplayVC alloc] initWithNibName:@"CustomSpreadCardDisplayVC" bundle:nil];
            dailyVC.get_Data = dictData;
            dailyVC.grid = 5.0;
            dailyVC.getvalue = @"1";
            [self.navigationController pushViewController:dailyVC animated:YES];
        } else {
            CustomSpreadCardDisplayVC *dailyVC = [[CustomSpreadCardDisplayVC alloc] initWithNibName:@"CustomSpreadCardDisplayVC_IPad" bundle:nil];
            dailyVC.get_Data = dictData;
            dailyVC.grid = 5.0;
            dailyVC.getvalue = @"1";
            [self.navigationController pushViewController:dailyVC animated:YES];
        }
    }
    else if ([[dictData objectForKey:@"spread_type"] isEqual: @"custom"] && [[dictData objectForKey:@"c_spread_type"] isEqualToString:@"6x6"]) {
        if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
            CustomSpreadCardDisplayVC *dailyVC = [[CustomSpreadCardDisplayVC alloc] initWithNibName:@"CustomSpreadCardDisplayVC" bundle:nil];
            dailyVC.get_Data = dictData;
            dailyVC.grid = 6.0;
            dailyVC.getvalue = @"1";
            [self.navigationController pushViewController:dailyVC animated:YES];
        } else {
            CustomSpreadCardDisplayVC *dailyVC = [[CustomSpreadCardDisplayVC alloc] initWithNibName:@"CustomSpreadCardDisplayVC_IPad" bundle:nil];
            dailyVC.get_Data = dictData;
            dailyVC.grid = 6.0;
            dailyVC.getvalue = @"1";
            [self.navigationController pushViewController:dailyVC animated:YES];
        }
    }
}

#pragma CallAPI

-(void)callForGetMyJournal  {
    NSMutableDictionary *dcitParams = [[NSMutableDictionary alloc] init];
    
    //NSLog(@" dcitParams :-%@", dcitParams);
    
    [AppDelegate addVwLoading];
    [CommunicationHandler callForGetMyJournalList:dcitParams Sucess:^(NSDictionary *dict, NSURLResponse *response) {
        //NSLog(@"dict :- %@", dict);
        dispatch_async(dispatch_get_main_queue(), ^{
            [AppDelegate removeVwLoading];
            if ([[dict objectForKey:@"error"] boolValue] == false) {
                NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
                [userDefaults setObject:dict forKey:@"journaldata"];
                arrJournalList = [[NSMutableArray alloc] initWithArray:[dict valueForKey:@"data"]];
                NSLog(@"arrJournalList :- %@", arrJournalList);
                [self.tblJournalList reloadData];
                value = -1;
              
            } else {
                [CommanMethods displayAlertMessage:@"UOC" message:[dict objectForKey:@"message"] currentRef:self];
            }
        });
    } Error:^(NSError *error, NSURLResponse *response) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [AppDelegate removeVwLoading];
        });
    }];
}

-(void)callForDeleteJournal:(NSString*)getid  {
    NSMutableDictionary *dcitParams = [[NSMutableDictionary alloc] init];
    [dcitParams setValue:getid forKey:@"id"];
    //NSLog(@" dcitParams :-%@", dcitParams);
    
    [AppDelegate addVwLoading];
    [CommunicationHandler callForDeleteJournal:dcitParams Sucess:^(NSDictionary *dict, NSURLResponse *response) {
        //NSLog(@"dict :- %@", dict);
        dispatch_async(dispatch_get_main_queue(), ^{
            [AppDelegate removeVwLoading];
            if ([[dict objectForKey:@"error"] boolValue] == false) {
                [CommanMethods displayAlertMessageWithCallBack:nil message:[dict objectForKey:@"message"] refrence:self Success:^{
                    [self callForGetMyJournal];
                    value = -1;
                }];
                
            } else {
                [CommanMethods displayAlertMessage:@"UOC" message:[dict objectForKey:@"message"] currentRef:self];
            }
        });
    } Error:^(NSError *error, NSURLResponse *response) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [AppDelegate removeVwLoading];
        });
    }];
}





@end
