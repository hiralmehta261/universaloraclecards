//
//  MyJournalListVC.h
//  UniversalOracleCards
//
//  Created by whiznic on 14/03/19.
//  Copyright © 2019 whiznic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <AVKit/AVKit.h>

@interface MyJournalListVC : UIViewController <UITableViewDelegate, UITableViewDataSource>
@property (strong, nonatomic) IBOutlet UIView *viewDisply;
@property (strong, nonatomic) IBOutlet UITableView *tblJournalList;
@property (nonatomic) AVPlayer *playerViewController;
@property (weak, nonatomic) IBOutlet UILabel *lblMessage;

@end
