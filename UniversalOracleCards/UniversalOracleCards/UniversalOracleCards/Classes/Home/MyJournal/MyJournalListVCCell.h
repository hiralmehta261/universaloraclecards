//
//  MyJournalListVCCell.h
//  UniversalOracleCards
//
//  Created by whiznic on 14/03/19.
//  Copyright © 2019 whiznic. All rights reserved.
//

#import <UIKit/UIKit.h>
#define MyJournalListVCCellID @"MyJournalListVCCellID"

@interface MyJournalListVCCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lblDate;
@property (strong, nonatomic) IBOutlet UILabel *lbltotalCards;
@property (strong, nonatomic) IBOutlet UILabel *lblAudio;
@property (strong, nonatomic) IBOutlet UILabel *lblTextNotes;
@property (strong, nonatomic) IBOutlet UIButton *btnEdit;
@property (strong, nonatomic) IBOutlet UIButton *btnDelete;
@property (strong, nonatomic) IBOutlet UIButton *btnMore;
@property (strong, nonatomic) IBOutlet UIView *btnMoreAction;
@property (weak, nonatomic) IBOutlet UILabel *lblSpreadName;

@end


