//
//  PaymentVC.m
//  UniversalOracleCards
//
//  Created by whiznic on 22/03/19.
//  Copyright © 2019 whiznic. All rights reserved.
//

#import "PaymentVC.h"
#import "HomeVC.h"
#import <StoreKit/StoreKit.h>

@interface PaymentVC () <SKProductsRequestDelegate,SKPaymentTransactionObserver>
{
    SKProductsRequest *productsRequest;
    NSArray *validProducts;
    UIActivityIndicatorView *activityIndicatorView;
    NSString *productPrice;
}
@end

@implementation PaymentVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor=[[UIColor blackColor] colorWithAlphaComponent:.7];
    self.viewDisply.layer.zPosition = 1;
    self.ViewPayment.layer.borderColor = UIColor.clearColor.CGColor;
    self.ViewPayment.layer.borderWidth = 1;
    self.ViewPayment.layer.cornerRadius = 20;
    [self showAnimate];
    
    // Adding activity indicator
    activityIndicatorView = [[UIActivityIndicatorView alloc]
                             initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activityIndicatorView.center = self.view.center;
    activityIndicatorView.backgroundColor = [UIColor grayColor];
    [activityIndicatorView hidesWhenStopped];
    [self.view addSubview:activityIndicatorView];
    [activityIndicatorView startAnimating];
    
    
    [self fetchAvailableProducts];
}


- (void)showAnimate
{
    self.view.transform = CGAffineTransformMakeScale(1.3, 1.3);
    self.view.alpha = 0;
    [UIView animateWithDuration:.25 animations:^{
        self.view.alpha = 1;
        self.view.transform = CGAffineTransformMakeScale(1, 1);
    }];
}

- (void)removeAnimate
{
    [UIView animateWithDuration:.25 animations:^{
        self.view.transform = CGAffineTransformMakeScale(1.3, 1.3);
        self.view.alpha = 0.0;
    } completion:^(BOOL finished) {
        if (finished) {
            [self.view removeFromSuperview];
        }
    }];
}

- (IBAction)btnClose:(id)sender {
    
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
        HomeVC *homeVC = [[HomeVC alloc] initWithNibName:@"HomeVC" bundle:nil];
        NSUserDefaults *imgcheck = [NSUserDefaults standardUserDefaults];
        [imgcheck setValue:@"1" forKey:@"HomeScreen"];
        NSLog(@"imgcheck :- %@", imgcheck);
        [[NSUserDefaults standardUserDefaults] synchronize];
        [self.navigationController pushViewController:homeVC animated:YES];
    } else {
        HomeVC *homeVC = [[HomeVC alloc] initWithNibName:@"HomeVC_IPad" bundle:nil];
        NSUserDefaults *imgcheck = [NSUserDefaults standardUserDefaults];
        [imgcheck setValue:@"1" forKey:@"HomeScreen"];
        NSLog(@"imgcheck :- %@", imgcheck);
        [[NSUserDefaults standardUserDefaults] synchronize];
        [self.navigationController pushViewController:homeVC animated:YES];
    }
    [self removeAnimate];
    
}
- (IBAction)btnRestore:(id)sender {
    [self purchaseMyProduct:[validProducts objectAtIndex:1]];
}

- (IBAction)btnPurchase:(id)sender {
    [self purchaseMyProduct:[validProducts objectAtIndex:1]];
    /*purchaseButton.enabled = NO;
    restoreButton.enabled = NO;*/
}


//Mark:- In-App Purchase methods

-(void)fetchAvailableProducts {
    NSSet *productIdentifiers = [NSSet
                                 setWithObjects:@"com.uoc.premium",nil];
    productsRequest = [[SKProductsRequest alloc]
                       initWithProductIdentifiers:productIdentifiers];
    productsRequest.delegate = self;
    [productsRequest start];
}

- (BOOL)canMakePurchases {
    return [SKPaymentQueue canMakePayments];
}

- (void)purchaseMyProduct:(SKProduct*)product {
    if ([self canMakePurchases]) {
        if (product) {
            SKPayment *payment = [SKPayment paymentWithProduct:product];
            [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
            [[SKPaymentQueue defaultQueue] addPayment:payment];
        } else {
            [self fetchAvailableProducts];
        }
    } else {
        UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:
                                  @"Purchases are disabled in your device" message:nil delegate:
                                  self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alertView show];
    }
}

#pragma mark StoreKit Delegate

-(void)paymentQueue:(SKPaymentQueue *)queue
updatedTransactions:(NSArray *)transactions {
    for (SKPaymentTransaction *transaction in transactions) {
        switch (transaction.transactionState) {
            case SKPaymentTransactionStatePurchasing:
                NSLog(@"Purchasing");
                break;
                
            case SKPaymentTransactionStatePurchased:
                if ([transaction.payment.productIdentifier
                     isEqualToString:@"com.uoc.premium"]) {
                    NSLog(@"Purchased");
                    NSLog(@"Transaction Date : %@",transaction.transactionDate);
                    
                    [[NSUserDefaults standardUserDefaults] setObject:transaction.transactionDate forKey:@"purchase_date"];
                    [[NSUserDefaults standardUserDefaults] setObject:@"0" forKey:@"display_add"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    
                    UIAlertController * alert = [UIAlertController
                                                 alertControllerWithTitle:@""
                                                 message:@"Purchase is completed succesfully"
                                                 preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction* yesButton = [UIAlertAction
                                                actionWithTitle:@"OK"
                                                style:UIAlertActionStyleDefault
                                                handler:^(UIAlertAction * action) {
                                                    //[self updatePurchaseStatus];
                                                }];
                    [alert addAction:yesButton];
                    [self presentViewController:alert animated:YES completion:nil];
                }
                [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
                break;
                
            case SKPaymentTransactionStateRestored:
                if ([transaction.payment.productIdentifier
                     isEqualToString:@"com.uoc.premium"]) {
                    NSLog(@"Restore");
                    NSLog(@"Transaction Date : %@",transaction.transactionDate);
                    
                    [[NSUserDefaults standardUserDefaults] setObject:transaction.transactionDate forKey:@"purchase_date"];
                    [[NSUserDefaults standardUserDefaults] setObject:@"0" forKey:@"display_add"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    
                    UIAlertController * alert = [UIAlertController
                                                 alertControllerWithTitle:@""
                                                 message:@"Restore is completed succesfully"
                                                 preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction* yesButton = [UIAlertAction
                                                actionWithTitle:@"OK"
                                                style:UIAlertActionStyleDefault
                                                handler:^(UIAlertAction * action) {
                                                    //[self updatePurchaseStatus];
                                                }];
                    [alert addAction:yesButton];
                    [self presentViewController:alert animated:YES completion:nil];
                }
                [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
                break;
                
            case SKPaymentTransactionStateFailed:
               /* purchaseButton.enabled = YES;
                restoreButton.enabled = YES;*/
                NSLog(@"Purchase failed ");
                break;
            default:
                break;
        }
    }
}

-(void)productsRequest:(SKProductsRequest *)request
    didReceiveResponse:(SKProductsResponse *)response {
    SKProduct *validProduct = nil;
    int count = [response.products count];
    
    if (count>0) {
        validProducts = response.products;
        validProduct = [response.products objectAtIndex:1];
        SKProduct *SKProduct2 = [response.products objectAtIndex:0];
        
        /*[productTitleLabel setText:[NSString stringWithFormat:
                                    @"Product Title: %@",validProduct.localizedTitle]];
        [productDescriptionLabel setText:[NSString stringWithFormat:
                                          @"Product Desc: %@",validProduct.localizedDescription]];
        [productPriceLabel setText:[NSString stringWithFormat:
                                    @"Product Price: %@",[NSString stringWithFormat:@"%@%@ %@", [validProduct.priceLocale objectForKey:NSLocaleCurrencySymbol], validProduct.price, [validProduct.priceLocale objectForKey:NSLocaleCurrencyCode]]]];*/
        
        productPrice = [NSString stringWithFormat:
                        @"Product Price: %@",validProduct.price];
        
        /*purchaseButton.hidden = NO;
        restoreButton.hidden = NO;*/
        
    } else {
        UIAlertView *tmp = [[UIAlertView alloc]
                            initWithTitle:@"Not Available"
                            message:@"No products to purchase"
                            delegate:self
                            cancelButtonTitle:nil
                            otherButtonTitles:@"Ok", nil];
        [tmp show];
    }
    [activityIndicatorView stopAnimating];
}

@end
