//
//  PaymentVC.h
//  UniversalOracleCards
//
//  Created by whiznic on 22/03/19.
//  Copyright © 2019 whiznic. All rights reserved.
//

#import <UIKit/UIKit.h>



@interface PaymentVC : UIViewController
@property (weak, nonatomic) IBOutlet UIView *ViewPayment;
@property (weak, nonatomic) IBOutlet UILabel *lblEmail;
@property (weak, nonatomic) IBOutlet UILabel *lblPrice;
@property (strong, nonatomic) IBOutlet UIView *viewDisply;
@property (weak, nonatomic) IBOutlet UIButton *btnrestore;
@property (weak, nonatomic) IBOutlet UIButton *btnpurchase;
@end


