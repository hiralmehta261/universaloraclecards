//
//  HomeVC.m
//  UniversalOracleCards
//
//  Created by whiznic on 04/03/19.
//  Copyright © 2019 whiznic. All rights reserved.
//

#import "HomeVC.h"
#import "AppDelegate.h"
#import "SideMenuVC.h"
#import "CommanMethods.h"
#import "UseofDailyCardsVC.h"
#import "CreateSpreadVC.h"
#import "IntuitionExerciseVC.h"
#import "MyJournalListVC.h"
#import "ConnectionCardVC.h"
#import "LifePurposeCardVC.h"
#import "BrethingOutVC.h"
#import "PaymentVC.h"
#import "MySpreadListVC.h"
#import <AudioToolbox/AudioServices.h>
#import "SharkfoodMuteSwitchDetector.h"
@import MediaPlayer;

@interface HomeVC ()
@property (nonatomic,strong) SharkfoodMuteSwitchDetector* detector;

@end

@implementation HomeVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSString *savedValue = [[NSUserDefaults standardUserDefaults]
                            stringForKey:@"HomeScreen"];
    NSLog(@"savedValue :- %@", savedValue);
    if([savedValue isEqualToString:@"1"]) {
        self.viewDisply.layer.zPosition = 1;
    }
    
   
    
    /*self.detector = [SharkfoodMuteSwitchDetector shared];
    __weak HomeVC* sself = self;
    self.detector.silentNotify = ^(BOOL silent){
        if (silent) {
            
            NSLog(@"device is silent");
        } else {
            
            NSLog(@"device is in ringing mode");
        }
    };*/
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    self.detector = [SharkfoodMuteSwitchDetector shared];
    __weak HomeVC* sself = self;
    self.detector.silentNotify = ^(BOOL silent){
        if (silent) {
            
            NSLog(@"device is silent");
        } else {
            
            NSLog(@"device is in ringing mode");
        }
    };
    
    [[UIApplication sharedApplication] beginReceivingRemoteControlEvents];
    [self becomeFirstResponder];
    [SideMenuVC addToViewController:self];
    [SideMenuVC defaultVwFrame:self.view.frame];
    [[SideMenuVC sharedSideMenu] reloadSideMenu];
    
    NSString *stringVideoName = @"stars_animated.mp4";
    NSString *filepath = [[NSBundle mainBundle] pathForResource:stringVideoName ofType:nil];
    
    NSURL *fileURL = [NSURL fileURLWithPath:filepath];
    self.playerViewController = [AVPlayer playerWithURL:fileURL];
    self.playerViewController.actionAtItemEnd = AVPlayerActionAtItemEndNone;
    AVPlayerLayer *videoLayer = [AVPlayerLayer playerLayerWithPlayer:self.playerViewController];
    videoLayer.frame = self.view.bounds;
    videoLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
    [self.view.layer addSublayer:videoLayer];
    
    [self.playerViewController play];
    
    
    NSString *savedValue1 = [[NSUserDefaults standardUserDefaults]
                             stringForKey:@"audio"];
    if([savedValue1 isEqualToString:@"off"]) {
        
        [self.playerViewController setVolume:0.0];
    } else {
        
        [self.playerViewController setVolume:1.0];
    }
    
    NSLog(@" savedValue :- %@", savedValue1);
    
    
    self.viewDisply.layer.zPosition = 1;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(itemDidFinishPlaying:) name:AVPlayerItemDidPlayToEndTimeNotification object:[self.playerViewController currentItem]];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationDidBecomeActive:) name:UIApplicationDidEnterBackgroundNotification object:nil];
    
    [[UIApplication sharedApplication] endReceivingRemoteControlEvents];
    [self resignFirstResponder];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}


- (void)remoteControlReceivedWithEvent:(UIEvent *)event {
    switch (event.subtype) {
        case UIEventSubtypeRemoteControlTogglePlayPause:
            if([self.playerViewController rate] == 0){
                [self.playerViewController play];
            } else {
                [self.playerViewController pause];
            }
            break;
        case UIEventSubtypeRemoteControlPlay:
            [self.playerViewController play];
            break;
        case UIEventSubtypeRemoteControlPause:
            [self.playerViewController pause];
            break;
        default:
            break;
    }
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
   /* self.detector = [SharkfoodMuteSwitchDetector shared];
    __weak HomeVC* sself = self;
    self.detector.silentNotify = ^(BOOL silent){
        if (silent) {
            
            NSLog(@"device is silent");
        } else {
            
            NSLog(@"device is in ringing mode");
            [self.playerViewController performSelector:@selector(play) withObject:nil afterDelay:0.01];
        }
    };*/
    NSString *savedValue1 = [[NSUserDefaults standardUserDefaults]
                            stringForKey:@"audio"];
    if([savedValue1 isEqualToString:@"off"]) {
       
        [self.playerViewController setVolume:0.0];
    } else {
        
        [self.playerViewController setVolume:0.0];
    }
}




- (void)itemDidFinishPlaying:(NSNotification *)notification {
    AVPlayerItem *player = [notification object];
    [player seekToTime:kCMTimeZero];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [SideMenuVC remove];
}

#pragma mark -ButtonAction

-(IBAction)sideMenuTap:(id)sender {
    [SideMenuVC.sharedSideMenu show];
}

- (IBAction)btnDailyCards:(id)sender {
    NSString *savedValue = [[NSUserDefaults standardUserDefaults]
                            stringForKey:@"CheckboxDailyCard"];
    if([savedValue isEqualToString:@"1"]) {
        if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
            BrethingOutVC *BreathingoutVC = [[BrethingOutVC alloc] initWithNibName:@"BrethingOutVC" bundle:nil];
            BreathingoutVC.get_Value = @"1";
            [self.navigationController pushViewController:BreathingoutVC animated:YES];
        } else {
            BrethingOutVC *BreathingoutVC = [[BrethingOutVC alloc] initWithNibName:@"BrethingOutVC_IPad" bundle:nil];
            BreathingoutVC.get_Value = @"1";
            [self.navigationController pushViewController:BreathingoutVC animated:YES];
        }
    }
    else {
        if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
        UseofDailyCardsVC *DailyCardsVC = [[UseofDailyCardsVC alloc] initWithNibName:@"UseofDailyCardsVC" bundle:nil];
        DailyCardsVC.Value = @"1";
        [self.navigationController pushViewController:DailyCardsVC animated:YES];
        } else {
            UseofDailyCardsVC *DailyCardsVC = [[UseofDailyCardsVC alloc] initWithNibName:@"UseofDailyCardsVC_IPad" bundle:nil];
            DailyCardsVC.Value = @"1";
            [self.navigationController pushViewController:DailyCardsVC animated:YES];
        }
    }
}

- (IBAction)btnGeneralAdvice:(id)sender {
    NSString *savedValue = [[NSUserDefaults standardUserDefaults]
                            stringForKey:@"CheckboxGeneral"];
    if([savedValue isEqualToString:@"1"]) {
        if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
            BrethingOutVC *BreathingoutVC = [[BrethingOutVC alloc] initWithNibName:@"BrethingOutVC" bundle:nil];
            BreathingoutVC.get_Value = @"3";
            [self.navigationController pushViewController:BreathingoutVC animated:YES];
        } else {
            BrethingOutVC *BreathingoutVC = [[BrethingOutVC alloc] initWithNibName:@"BrethingOutVC_IPad" bundle:nil];
            BreathingoutVC.get_Value = @"3";
            [self.navigationController pushViewController:BreathingoutVC animated:YES];
        }
    }
    else {
        if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
            UseofDailyCardsVC *DailyCardsVC = [[UseofDailyCardsVC alloc] initWithNibName:@"UseofDailyCardsVC" bundle:nil];
            DailyCardsVC.Value = @"3";
            [self.navigationController pushViewController:DailyCardsVC animated:YES];
        } else {
            UseofDailyCardsVC *DailyCardsVC = [[UseofDailyCardsVC alloc] initWithNibName:@"UseofDailyCardsVC_IPad" bundle:nil];
            DailyCardsVC.Value = @"3";
            [self.navigationController pushViewController:DailyCardsVC animated:YES];
        }
    }
}

- (IBAction)btnInnerWork:(id)sender {
    NSString *savedValue = [[NSUserDefaults standardUserDefaults]
                            stringForKey:@"CheckboxInner"];
    NSLog(@"savedValue :- %@", savedValue);
    if([savedValue isEqualToString:@"1"]) {
        if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
            BrethingOutVC *BreathingoutVC = [[BrethingOutVC alloc] initWithNibName:@"BrethingOutVC" bundle:nil];
            BreathingoutVC.get_Value = @"6";
            [self.navigationController pushViewController:BreathingoutVC animated:YES];
        } else {
            BrethingOutVC *BreathingoutVC = [[BrethingOutVC alloc] initWithNibName:@"BrethingOutVC_IPad" bundle:nil];
            BreathingoutVC.get_Value = @"6";
            [self.navigationController pushViewController:BreathingoutVC animated:YES];
        }
    }
    else {
        if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
            UseofDailyCardsVC *DailyCardsVC = [[UseofDailyCardsVC alloc] initWithNibName:@"UseofDailyCardsVC" bundle:nil];
            DailyCardsVC.Value = @"6";
            [self.navigationController pushViewController:DailyCardsVC animated:YES];
        } else {
            UseofDailyCardsVC *DailyCardsVC = [[UseofDailyCardsVC alloc] initWithNibName:@"UseofDailyCardsVC_IPad" bundle:nil];
            DailyCardsVC.Value = @"6";
            [self.navigationController pushViewController:DailyCardsVC animated:YES];
        }
    }
}

- (IBAction)btnConnection:(id)sender {
    NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"CheckboxConnection"];
    if([savedValue isEqualToString:@"1"]) {
        if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
            BrethingOutVC *BreathingoutVC = [[BrethingOutVC alloc] initWithNibName:@"BrethingOutVC" bundle:nil];
            BreathingoutVC.get_Value = @"5C";
            [self.navigationController pushViewController:BreathingoutVC animated:YES];
        } else {
            BrethingOutVC *BreathingoutVC = [[BrethingOutVC alloc] initWithNibName:@"BrethingOutVC_IPad" bundle:nil];
            BreathingoutVC.get_Value = @"5C";
            [self.navigationController pushViewController:BreathingoutVC animated:YES];
        }
    }
    else {
        if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
            UseofDailyCardsVC *connectionVC = [[UseofDailyCardsVC alloc] initWithNibName:@"UseofDailyCardsVC" bundle:nil];
            connectionVC.Value = @"5C";
            [self.navigationController pushViewController:connectionVC animated:YES];
        } else {
            UseofDailyCardsVC *connectionVC = [[UseofDailyCardsVC alloc] initWithNibName:@"UseofDailyCardsVC_IPad" bundle:nil];
            connectionVC.Value = @"5C";
            [self.navigationController pushViewController:connectionVC animated:YES];
        }
    }
    /*if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
        PaymentVC *vc = [[PaymentVC alloc] initWithNibName:@"PaymentVC" bundle:nil];
        [self addChildViewController:vc];
        self.viewDisply.layer.zPosition = 0;
        vc.view.frame = self.view.frame;
        [self.view addSubview:vc.view];
        [vc didMoveToParentViewController:self];
    }
    
    else {
        PaymentVC *vc = [[PaymentVC alloc] initWithNibName:@"PaymentVC_IPad" bundle:nil];
        [self addChildViewController:vc];
        self.viewDisply.layer.zPosition = 0;
        vc.view.frame = self.view.frame;
        [self.view addSubview:vc.view];
        [vc didMoveToParentViewController:self];
    }*/
    
}

- (IBAction)btnLockConnection:(id)sender {
  /* NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"CheckboxLife"];
    if([savedValue isEqualToString:@"1"]) {
        if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
            BrethingOutVC *BreathingoutVC = [[BrethingOutVC alloc] initWithNibName:@"BrethingOutVC" bundle:nil];
            BreathingoutVC.get_Value = @"5L";
            [self.navigationController pushViewController:BreathingoutVC animated:YES];
        } else {
            BrethingOutVC *BreathingoutVC = [[BrethingOutVC alloc] initWithNibName:@"BrethingOutVC_IPad" bundle:nil];
            BreathingoutVC.get_Value = @"5L";
            [self.navigationController pushViewController:BreathingoutVC animated:YES];
        }
    } else {
        if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
            UseofDailyCardsVC *lifepurposeVC = [[UseofDailyCardsVC alloc] initWithNibName:@"UseofDailyCardsVC" bundle:nil];
            lifepurposeVC.Value = @"5L";
            [self.navigationController pushViewController:lifepurposeVC animated:YES];
        } else {
            UseofDailyCardsVC *lifepurposeVC = [[UseofDailyCardsVC alloc] initWithNibName:@"UseofDailyCardsVC_IPad" bundle:nil];
            lifepurposeVC.Value = @"5L";
            [self.navigationController pushViewController:lifepurposeVC animated:YES];
        }
    }*/
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
        PaymentVC *vc = [[PaymentVC alloc] initWithNibName:@"PaymentVC" bundle:nil];
        [self addChildViewController:vc];
        self.viewDisply.layer.zPosition = 0;
        vc.view.frame = self.view.frame;
        [self.view addSubview:vc.view];
        [vc didMoveToParentViewController:self];
    }
    else {
        PaymentVC *vc = [[PaymentVC alloc] initWithNibName:@"PaymentVC_IPad" bundle:nil];
        [self addChildViewController:vc];
        self.viewDisply.layer.zPosition = 0;
        vc.view.frame = self.view.frame;
        [self.view addSubview:vc.view];
        [vc didMoveToParentViewController:self];
    }
    
}

- (IBAction)btnMySpreads:(id)sender {
    NSUserDefaults *userDefaults1 = [NSUserDefaults standardUserDefaults];
    NSDictionary *dictData1 = [userDefaults1 objectForKey:@"spreaddata"];
    NSLog(@" dictData1 :- %@", dictData1);
    if ([[dictData1 objectForKey:@"data"] count] == 0) {
        if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
            CreateSpreadVC *createspreadVC = [[CreateSpreadVC alloc] initWithNibName:@"CreateSpreadVC" bundle:nil];
            [self.navigationController pushViewController:createspreadVC animated:YES];
        } else {
            CreateSpreadVC *createspreadVC = [[CreateSpreadVC alloc] initWithNibName:@"CreateSpreadVC_IPad" bundle:nil];
            [self.navigationController pushViewController:createspreadVC animated:YES];
        }
    } else {
        if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
            MySpreadListVC *createspreadVC = [[MySpreadListVC alloc] initWithNibName:@"MySpreadListVC" bundle:nil];
            [self.navigationController pushViewController:createspreadVC animated:YES];
        } else {
            MySpreadListVC *createspreadVC = [[MySpreadListVC alloc] initWithNibName:@"MySpreadListVC_IPad" bundle:nil];
            [self.navigationController pushViewController:createspreadVC animated:YES];
        }
    }
    /*if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
        PaymentVC *vc = [[PaymentVC alloc] initWithNibName:@"PaymentVC" bundle:nil];
        [self addChildViewController:vc];
        self.viewDisply.layer.zPosition = 0;
        vc.view.frame = self.view.frame;
        [self.view addSubview:vc.view];
        [vc didMoveToParentViewController:self];
    }*/
    
}

- (IBAction)btnMyJournal:(id)sender {
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
        MyJournalListVC *journalVC = [[MyJournalListVC alloc] initWithNibName:@"MyJournalListVC" bundle:nil];
        [self.navigationController pushViewController:journalVC animated:YES];
    } else {
        MyJournalListVC *journalVC = [[MyJournalListVC alloc] initWithNibName:@"MyJournalListVC_IPad" bundle:nil];
        [self.navigationController pushViewController:journalVC animated:YES];
    }
}

- (IBAction)btnExercise:(id)sender {
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
        IntuitionExerciseVC *exerciseVC = [[IntuitionExerciseVC alloc] initWithNibName:@"IntuitionExerciseVC" bundle:nil];
        [self.navigationController pushViewController:exerciseVC animated:YES];
    } else {
        IntuitionExerciseVC *exerciseVC = [[IntuitionExerciseVC alloc] initWithNibName:@"IntuitionExerciseVC_IPad" bundle:nil];
        [self.navigationController pushViewController:exerciseVC animated:YES];
    }
    /*if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
        PaymentVC *vc = [[PaymentVC alloc] initWithNibName:@"PaymentVC" bundle:nil];
        [self addChildViewController:vc];
        self.viewDisply.layer.zPosition = 0;
        vc.view.frame = self.view.frame;
        [self.view addSubview:vc.view];
        [vc didMoveToParentViewController:self];
    }*/
}

- (IBAction)btnLifePurpose:(id)sender {
     NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"CheckboxLife"];
     /*if([savedValue isEqualToString:@"1"]) {
         if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
             BrethingOutVC *BreathingoutVC = [[BrethingOutVC alloc] initWithNibName:@"BrethingOutVC" bundle:nil];
             BreathingoutVC.get_Value = @"5L";
             [self.navigationController pushViewController:BreathingoutVC animated:YES];
         } else {
             BrethingOutVC *BreathingoutVC = [[BrethingOutVC alloc] initWithNibName:@"BrethingOutVC_IPad" bundle:nil];
             BreathingoutVC.get_Value = @"5L";
             [self.navigationController pushViewController:BreathingoutVC animated:YES];
         }
     } else {
         if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
             UseofDailyCardsVC *lifepurposeVC = [[UseofDailyCardsVC alloc] initWithNibName:@"UseofDailyCardsVC" bundle:nil];
             lifepurposeVC.Value = @"5L";
             [self.navigationController pushViewController:lifepurposeVC animated:YES];
         } else {
             UseofDailyCardsVC *lifepurposeVC = [[UseofDailyCardsVC alloc] initWithNibName:@"UseofDailyCardsVC_IPad" bundle:nil];
             lifepurposeVC.Value = @"5L";
             [self.navigationController pushViewController:lifepurposeVC animated:YES];
     }
     }*/
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
        PaymentVC *vc = [[PaymentVC alloc] initWithNibName:@"PaymentVC" bundle:nil];
        [self addChildViewController:vc];
        self.viewDisply.layer.zPosition = 0;
        vc.view.frame = self.view.frame;
        [self.view addSubview:vc.view];
        [vc didMoveToParentViewController:self];
    }
    else {
        PaymentVC *vc = [[PaymentVC alloc] initWithNibName:@"PaymentVC_IPad" bundle:nil];
        [self addChildViewController:vc];
        self.viewDisply.layer.zPosition = 0;
        vc.view.frame = self.view.frame;
        [self.view addSubview:vc.view];
        [vc didMoveToParentViewController:self];
    }
}

- (IBAction)btnLockLifePurpose:(id)sender {
    /*NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"CheckboxLife"];
     if([savedValue isEqualToString:@"1"]) {
         if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
             BrethingOutVC *BreathingoutVC = [[BrethingOutVC alloc] initWithNibName:@"BrethingOutVC" bundle:nil];
             BreathingoutVC.get_Value = @"5L";
             [self.navigationController pushViewController:BreathingoutVC animated:YES];
        } else {
             BrethingOutVC *BreathingoutVC = [[BrethingOutVC alloc] initWithNibName:@"BrethingOutVC_IPad" bundle:nil];
             BreathingoutVC.get_Value = @"5L";
             [self.navigationController pushViewController:BreathingoutVC animated:YES];
        }
     } else {
         if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
             UseofDailyCardsVC *lifepurposeVC = [[UseofDailyCardsVC alloc] initWithNibName:@"UseofDailyCardsVC" bundle:nil];
             lifepurposeVC.Value = @"5L";
             [self.navigationController pushViewController:lifepurposeVC animated:YES];
         } else {
             UseofDailyCardsVC *lifepurposeVC = [[UseofDailyCardsVC alloc] initWithNibName:@"UseofDailyCardsVC_IPad" bundle:nil];
             lifepurposeVC.Value = @"5L";
             [self.navigationController pushViewController:lifepurposeVC animated:YES];
     }
     }*/
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
        PaymentVC *vc = [[PaymentVC alloc] initWithNibName:@"PaymentVC" bundle:nil];
        [self addChildViewController:vc];
        self.viewDisply.layer.zPosition = 0;
        vc.view.frame = self.view.frame;
        [self.view addSubview:vc.view];
        [vc didMoveToParentViewController:self];
    }
    else {
        PaymentVC *vc = [[PaymentVC alloc] initWithNibName:@"PaymentVC_IPad" bundle:nil];
        [self addChildViewController:vc];
        self.viewDisply.layer.zPosition = 0;
        vc.view.frame = self.view.frame;
        [self.view addSubview:vc.view];
        [vc didMoveToParentViewController:self];
    }
}

- (IBAction)btnLockMySpreads:(id)sender {
}

- (IBAction)btnLockExercise:(id)sender {
}

- (IBAction)btnNext:(id)sender {
    if (self.btnDailyCard.hidden == FALSE) {
        self.btnGeneralAdvice.hidden = FALSE;
        self.btnDailyCard.hidden = TRUE;
        self.btnBack.hidden = FALSE;
        self.BackView.hidden = FALSE;
    } else if (self.btnGeneralAdvice.hidden == FALSE){
        self.btnInnerWork.hidden = FALSE;
        self.btnGeneralAdvice.hidden = TRUE;
    } else if (self.btnInnerWork.hidden == FALSE) {
        self.btnLifePurpose.hidden = FALSE;
        self.btnLockLifePurpose.hidden = FALSE;
        self.btnInnerWork.hidden = TRUE;
    } else if (self.btnLifePurpose.hidden == FALSE) {
        self.btnLifePurpose.hidden = TRUE;
        self.btnLockLifePurpose.hidden = TRUE;
        self.btnConnection.hidden = FALSE;
        self.btnLockConnection.hidden = FALSE;
        self.btnNext.hidden = TRUE;
        self.buttonNext.hidden = TRUE;
    }
}

- (IBAction)btnBack:(id)sender {
    if (self.btnConnection.hidden == FALSE) {
        self.btnLifePurpose.hidden = FALSE;
        self.btnLockLifePurpose.hidden = FALSE;
        self.btnLockConnection.hidden = TRUE;
        self.btnConnection.hidden = TRUE;
        self.btnNext.hidden = FALSE;
        self.buttonNext.hidden = FALSE;
    } else if (self.btnLifePurpose.hidden == FALSE) {
        self.btnInnerWork.hidden = FALSE;
        self.btnLockLifePurpose.hidden = TRUE;
        self.btnLifePurpose.hidden = TRUE;
    } else if (self.btnInnerWork.hidden == FALSE) {
        self.btnGeneralAdvice.hidden = FALSE;
        self.btnInnerWork.hidden = TRUE;
    } else if (self.btnGeneralAdvice.hidden == FALSE) {
        self.btnDailyCard.hidden = FALSE;
        self.btnGeneralAdvice.hidden = TRUE;
        self.btnBack.hidden = TRUE;
        self.BackView.hidden = TRUE;
    }
}


- (IBAction)btnShop:(id)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString: @"https://google.com"] options:@{} completionHandler:nil];

}



@end
