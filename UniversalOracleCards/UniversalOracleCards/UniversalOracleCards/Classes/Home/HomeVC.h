//
//  HomeVC.h
//  UniversalOracleCards
//
//  Created by whiznic on 04/03/19.
//  Copyright © 2019 whiznic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <AVKit/AVKit.h>


@interface HomeVC : UIViewController 
@property (strong, nonatomic) IBOutlet UIButton *btnDailyCard;
@property (strong, nonatomic) IBOutlet UIButton *btnMySpreads;
@property (strong, nonatomic) IBOutlet UIButton *btnMyJournal;
@property (strong, nonatomic) IBOutlet UIButton *btnExercise;
@property (strong, nonatomic) IBOutlet UIButton *btnLockMySpreads;
@property (strong, nonatomic) IBOutlet UIButton *btnLockExercise;
@property (strong, nonatomic) IBOutlet UIButton *btnNext;
@property (strong, nonatomic) IBOutlet UIButton *btnLifePurpose;
@property (strong, nonatomic) IBOutlet UIButton *btnLockLifePurpose;
@property (strong, nonatomic) IBOutlet UIButton *BackView;
@property (strong, nonatomic) IBOutlet UIButton *btnBack;
@property (nonatomic) AVPlayer *playerViewController;
@property (nonatomic) AVPlayerLayer *player;
@property (strong, nonatomic) IBOutlet UIView *viewDisply;
@property (strong, nonatomic) IBOutlet UIButton *btnGeneralAdvice;
@property (strong, nonatomic) IBOutlet UIButton *btnInnerWork;
@property (strong, nonatomic) IBOutlet UIButton *btnConnection;
@property (strong, nonatomic) IBOutlet UIButton *btnLockConnection;
@property (strong, nonatomic) IBOutlet UIButton *buttonNext;
@property (strong, nonatomic) NSString *passValue;
@end


