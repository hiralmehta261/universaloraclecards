//
//  IntuitionExerciseVC.m
//  UniversalOracleCards
//
//  Created by whiznic on 07/03/19.
//  Copyright © 2019 whiznic. All rights reserved.
//

#import "IntuitionExerciseVC.h"
#import "HomeVC.h"
#import "IntuitionExerciseCardRevealedVC.h"
#import <AudioToolbox/AudioServices.h>
#import "SharkfoodMuteSwitchDetector.h"
@import MediaPlayer;
@interface IntuitionExerciseVC ()
@property (nonatomic,strong) SharkfoodMuteSwitchDetector* detector;
@end

@implementation IntuitionExerciseVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.lblQuestion2.hidden = TRUE;
    self.lblQuestion3.hidden = TRUE;
    self.lblQuestion4.hidden = TRUE;
    self.lblQuestion5.hidden = TRUE;
    self.btnBack.hidden = TRUE;
    
    /* self.detector = [SharkfoodMuteSwitchDetector shared];
     __weak IntuitionExerciseVC* sself = self;
     self.detector.silentNotify = ^(BOOL silent){
     if (silent) {
     [sself.playerViewController setVolume:0.0];
     sself.btnMute.hidden = FALSE;
     sself.btnVolume.hidden = TRUE;
     NSLog(@"device is silent");
     } else {
     [sself.playerViewController setVolume:1.0];
     sself.btnMute.hidden = TRUE;
     sself.btnVolume.hidden = FALSE;
     NSLog(@"device is in ringing mode");
     }
     };*/
    
    self.ViewBorder.layer.borderColor = [UIColor colorWithRed:173/255.0 green:173/255.0 blue:173/255.0 alpha:1.0].CGColor;
    self.ViewBorder.layer.borderWidth = 1;
    self.ViewBorder.layer.cornerRadius = 20;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    NSString *stringVideoName = @"stars_animated.mp4";
    NSString *filepath = [[NSBundle mainBundle] pathForResource:stringVideoName ofType:nil];
    NSLog(@" filepath :- %@", filepath);
    NSURL *fileURL = [NSURL fileURLWithPath:filepath];
    self.playerViewController = [AVPlayer playerWithURL:fileURL];
    self.playerViewController.actionAtItemEnd = AVPlayerActionAtItemEndNone;
    AVPlayerLayer *videoLayer = [AVPlayerLayer playerLayerWithPlayer:self.playerViewController];
    videoLayer.frame = self.view.bounds;
    videoLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
    [self.view.layer addSublayer:videoLayer];
    
    [self.playerViewController play];
    
    NSString *savedValue = [[NSUserDefaults standardUserDefaults]
                            stringForKey:@"audio"];
    if([savedValue isEqualToString:@"off"]) {
        self.btnMute.hidden = FALSE;
        self.btnVolume.hidden = TRUE;
        [self.playerViewController setVolume:0.0];
    } else {
        self.btnMute.hidden = TRUE;
        self.btnVolume.hidden = FALSE;
        [self.playerViewController setVolume:1.0];
    }
    
    NSLog(@" savedValue :- %@", savedValue);
    
    self.viewDisply.layer.zPosition = 1;
    
    
    self.btnHome.hidden = TRUE;
    self.btnTryAgain.hidden = TRUE;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(itemDidFinishPlaying:) name:AVPlayerItemDidPlayToEndTimeNotification object:[self.playerViewController currentItem]];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationDidBecomeActive:) name:UIApplicationDidEnterBackgroundNotification object:nil];
    
    
    [[UIApplication sharedApplication] beginReceivingRemoteControlEvents];
    [self becomeFirstResponder];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [self.playerViewController pause];
    [super viewWillDisappear:animated];
    [[UIApplication sharedApplication] endReceivingRemoteControlEvents];
    [self resignFirstResponder];
}


- (void)remoteControlReceivedWithEvent:(UIEvent *)event {
    switch (event.subtype) {
        case UIEventSubtypeRemoteControlTogglePlayPause:
            if([self.playerViewController rate] == 0){
                [self.playerViewController play];
            } else {
                [self.playerViewController pause];
            }
            break;
        case UIEventSubtypeRemoteControlPlay:
            [self.playerViewController play];
            break;
        case UIEventSubtypeRemoteControlPause:
            [self.playerViewController pause];
            break;
        default:
            break;
    }
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    self.detector = [SharkfoodMuteSwitchDetector shared];
    __weak IntuitionExerciseVC* sself = self;
    self.detector.silentNotify = ^(BOOL silent){
        if (silent) {
            
            NSLog(@"device is silent");
        } else {
            
            NSLog(@"device is in ringing mode");
            [self.playerViewController performSelector:@selector(play) withObject:nil afterDelay:0.01];
        }
    };
}


- (void)itemDidFinishPlaying:(NSNotification *)notification {
    AVPlayerItem *player = [notification object];
    [player seekToTime:kCMTimeZero];
}

#pragma Mark Action

- (IBAction)btnBack:(id)sender {
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
        HomeVC *homeVC = [[HomeVC alloc] initWithNibName:@"HomeVC" bundle:nil];
        [self.navigationController pushViewController:homeVC animated:YES];
    } else {
        HomeVC *homeVC = [[HomeVC alloc] initWithNibName:@"HomeVC_IPad" bundle:nil];
        [self.navigationController pushViewController:homeVC animated:YES];
    }
}

- (IBAction)btnReveal:(id)sender {
    /*if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
     IntuitionExerciseCardRevealedVC *cardVC = [[IntuitionExerciseCardRevealedVC alloc] initWithNibName:@"IntuitionExerciseCardRevealedVC" bundle:nil];
     [self.navigationController pushViewController:cardVC animated:YES];
     } else {
     IntuitionExerciseCardRevealedVC *cardVC = [[IntuitionExerciseCardRevealedVC alloc] initWithNibName:@"IntuitionExerciseCardRevealedVC_IPad" bundle:nil];
     [self.navigationController pushViewController:cardVC animated:YES];
     }*/
    
    self.btnHome.hidden = FALSE;
    self.btnTryAgain.hidden = FALSE;
    self.btnReveal.hidden = TRUE;
    self.lblQuestion5.hidden = TRUE;
    self.btnBack.hidden = TRUE;
    self.lblQuestion1.hidden = TRUE;
    self.lblQuestion2.hidden = TRUE;
    self.lblQuestion3.hidden = TRUE;
    self.lblQuestion4.hidden = TRUE;
    self.btnNext.hidden = TRUE;
    self.lblNext.hidden = TRUE;
    
    int lowerBound = 0;
    int upperBound = 49;
    int rndValue = lowerBound + arc4random() % (upperBound - lowerBound);
    
    NSLog(@"rndValue :- %d", rndValue);
    
    [UIView animateWithDuration:0.5 delay:0.0 options:UIViewAnimationOptionTransitionFlipFromTop
                     animations:^(void) {
                         self.imgCard.transform = CGAffineTransformMakeScale(1, 1);
                         self.imgCard.image = [UIImage imageNamed:[NSString stringWithFormat:@"%d", rndValue]];
                     }
                     completion:nil];
}


- (IBAction)btnNext:(id)sender {
    if (self.lblQuestion1.hidden == FALSE) {
        self.lblQuestion2.hidden = FALSE;
        self.btnBack.hidden = FALSE;
        self.lblQuestion2.text = @"What Shape do you Perceive? ";
        self.lblQuestion1.hidden = TRUE;
    } else if (self.lblQuestion2.hidden == FALSE) {
        self.lblQuestion3.hidden = FALSE;
        self.lblQuestion3.text = @"What Energy do you Perceive? ";
        self.btnBack.hidden = FALSE;
        self.lblQuestion2.hidden = TRUE;
    } else if (self.lblQuestion3.hidden == FALSE) {
        self.lblQuestion4.hidden = FALSE;
        self.lblQuestion4.text = @"What Emotions are coming up? ";
        self.btnBack.hidden = FALSE;
        self.lblQuestion3.hidden = TRUE;
    } else if (self.lblQuestion4.hidden == FALSE) {
        self.lblQuestion5.hidden = FALSE;
        self.lblQuestion5.text = @"What Thoughts are coming up? ";
        self.lblQuestion4.hidden = TRUE;
        self.btnBack.hidden = FALSE;
        self.btnNext.hidden = TRUE;
        self.lblNext.hidden = TRUE;
        
    }
    
    
    
}
- (IBAction)btnBackques:(id)sender {
    if (self.lblQuestion5.hidden == FALSE) {
        self.lblQuestion4.hidden = FALSE;
        self.btnBack.hidden = FALSE;
        self.lblQuestion4.text = @"What Emotions are coming up? ";
        self.lblQuestion5.hidden = TRUE;
        self.btnNext.hidden = FALSE;
        self.lblNext.hidden = FALSE;
    } else if (self.lblQuestion4.hidden == FALSE) {
        self.lblQuestion3.hidden = FALSE;
        self.lblQuestion3.text = @"What Energy do you Perceive? ";
        self.btnBack.hidden = FALSE;
        self.lblQuestion4.hidden = TRUE;
    } else if (self.lblQuestion3.hidden == FALSE) {
        self.lblQuestion2.hidden = FALSE;
        self.lblQuestion2.text = @"What Shape do you Perceive? ";
        self.btnBack.hidden = FALSE;
        self.lblQuestion3.hidden = TRUE;
    } else if (self.lblQuestion2.hidden == FALSE) {
        self.lblQuestion1.hidden = FALSE;
        self.lblQuestion1.text = @"What Color do you Perceive? ";
        self.lblQuestion2.hidden = TRUE;
        self.btnBack.hidden = TRUE;
        self.btnNext.hidden = FALSE;
    }
    
}

- (IBAction)btnTryAgain:(id)sender {
    self.btnHome.hidden = TRUE;
    self.btnTryAgain.hidden = TRUE;
    self.btnReveal.hidden = FALSE;
    [UIView animateWithDuration:0.5 delay:0.0 options:UIViewAnimationOptionTransitionFlipFromTop
                     animations:^(void) {
                         self.imgCard.transform = CGAffineTransformMakeScale(1, -1);
                         self.imgCard.image = [UIImage imageNamed:@"animated image"];
                     }
                     completion:nil];
    self.lblQuestion2.hidden = TRUE;
    self.lblQuestion3.hidden = TRUE;
    self.lblQuestion4.hidden = TRUE;
    self.lblQuestion4.hidden = TRUE;
    self.btnBack.hidden = TRUE;
    self.lblQuestion1.hidden = FALSE;
    self.btnNext.hidden = FALSE;
}

- (IBAction)btnVolume:(id)sender {
    if ([self.btnVolume.accessibilityHint isEqualToString:@"1"]) {
        self.btnVolume.accessibilityHint = @"0";
        self.btnVolume.hidden = TRUE;
        self.btnMute.hidden = FALSE;
        [self.playerViewController setVolume:0.0];
        NSUserDefaults *audiocheck = [NSUserDefaults standardUserDefaults];
        [audiocheck setValue:@"off" forKey:@"audio"];
        NSLog(@"audiocheck :- %@", audiocheck);
        [[NSUserDefaults standardUserDefaults] synchronize];
        
    } else {
        self.btnVolume.accessibilityHint = @"1";
        self.btnVolume.hidden = FALSE;
        self.btnMute.hidden = TRUE;
        [self.playerViewController setVolume:1.0];
        NSUserDefaults *audiocheck = [NSUserDefaults standardUserDefaults];
        [audiocheck setValue:@"on" forKey:@"audio"];
        NSLog(@"audiocheck :- %@", audiocheck);
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}

@end
