//
//  IntuitionExerciseVC.h
//  UniversalOracleCards
//
//  Created by whiznic on 07/03/19.
//  Copyright © 2019 whiznic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <AVKit/AVKit.h>



@interface IntuitionExerciseVC : UIViewController
@property (strong, nonatomic) IBOutlet UIView *ViewBorder;
@property (strong, nonatomic) IBOutlet UILabel *lblQuestion1;
@property (nonatomic) AVPlayer *playerViewController;
@property (strong, nonatomic) IBOutlet UIView *viewDisply;
@property (weak, nonatomic) IBOutlet UIButton *btnMute;
@property (weak, nonatomic) IBOutlet UILabel *lblQuestion2;
@property (weak, nonatomic) IBOutlet UIButton *btnVolume;
@property (weak, nonatomic) IBOutlet UILabel *lblQuestion3;
@property (weak, nonatomic) IBOutlet UILabel *lblQuestion4;
@property (weak, nonatomic) IBOutlet UIButton *btnNext;
@property (weak, nonatomic) IBOutlet UIButton *btnBack;
@property (weak, nonatomic) IBOutlet UILabel *lblQuestion5;
@property (weak, nonatomic) IBOutlet UILabel *lblNext;
@property (weak, nonatomic) IBOutlet UIButton *btnTryAgain;
@property (weak, nonatomic) IBOutlet UIButton *btnHome;
@property (weak, nonatomic) IBOutlet UIImageView *imgCard;
@property (weak, nonatomic) IBOutlet UIButton *btnReveal;
@end

