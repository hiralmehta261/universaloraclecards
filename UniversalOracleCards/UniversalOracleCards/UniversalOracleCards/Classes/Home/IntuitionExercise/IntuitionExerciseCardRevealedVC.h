//
//  IntuitionExerciseCardRevealedVC.h
//  UniversalOracleCards
//
//  Created by whiznic on 07/03/19.
//  Copyright © 2019 whiznic. All rights reserved.
//

#import <UIKit/UIKit.h>


#import <AVFoundation/AVFoundation.h>
#import <AVKit/AVKit.h>


@interface IntuitionExerciseCardRevealedVC : UIViewController
@property (strong, nonatomic) IBOutlet UIView *ViewBorder;
@property (strong, nonatomic) IBOutlet UIImageView *imgCard;
@property (nonatomic) AVPlayer *playerViewController;
@property (strong, nonatomic) IBOutlet UIView *viewDisply;
@property (weak, nonatomic) IBOutlet UIButton *btnMute;
@property (weak, nonatomic) IBOutlet UIButton *btnVolume;
@end

