//
//  InnerWorkCardVC.h
//  UniversalOracleCards
//
//  Created by whiznic on 14/03/19.
//  Copyright © 2019 whiznic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <AVKit/AVKit.h>


@interface InnerWorkCardVC : UIViewController <UITextFieldDelegate>
@property (nonatomic) AVPlayer *playerViewController;
@property (strong, nonatomic) IBOutlet UIView *viewDisply;
@property (strong, nonatomic) IBOutlet UITextField *txtTitle;
@property (strong, nonatomic) IBOutlet UIView *ViewCard1;
@property (strong, nonatomic) IBOutlet UIImageView *imgCard1;
@property (strong, nonatomic) IBOutlet UIView *ViewCard2;
@property (strong, nonatomic) IBOutlet UIImageView *imgCard2;
@property (strong, nonatomic) IBOutlet UIView *ViewCard3;
@property (strong, nonatomic) IBOutlet UIImageView *imgCard3;
@property (strong, nonatomic) IBOutlet UIView *ViewCard4;
@property (strong, nonatomic) IBOutlet UIImageView *imgCard4;
@property (strong, nonatomic) IBOutlet UIView *ViewCard5;
@property (strong, nonatomic) IBOutlet UIImageView *imgCard5;
@property (strong, nonatomic) IBOutlet UIView *ViewCard6;
@property (strong, nonatomic) IBOutlet UIImageView *imgCard6;
@property (strong, nonatomic) NSMutableArray *get_cardID;
@property (strong, nonatomic) NSDictionary *get_Data;
@property (strong, nonatomic) NSString *getvalue;
@end


