//
//  VerifyOTPVC.h
//  UniversalOracleCards
//
//  Created by whiznic on 28/03/19.
//  Copyright © 2019 whiznic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <AVKit/AVKit.h>


@interface VerifyOTPVC : UIViewController <UITextFieldDelegate>
@property (strong, nonatomic) NSString *get_userID;
@property (strong, nonatomic) IBOutlet UITextField *OTPTextField;
@property (strong, nonatomic) IBOutlet UIButton *btnResetPassword;
@property (nonatomic, strong) UINavigationController *navi1;
@property (nonatomic, strong) IBOutlet UIWindow* window;
@property (nonatomic) AVPlayer *playerViewController;
@property (strong, nonatomic) IBOutlet UIView *viewDisply;
@end


