//
//  ForgotPasswordVC.m
//  UniversalOracleCards
//
//  Created by whiznic on 04/03/19.
//  Copyright © 2019 whiznic. All rights reserved.
//

#import "ForgotPasswordVC.h"
#import <IQKeyboardManager/IQKeyboardManager.h>
#import "ResetPasswordVC.h"
#import <Firebase.h>
#import "CommanMethods.h"
#import "AppDelegate.h"
#import "CommunicationHandler.h"
#import "SignInVC.h"
#import "VerifyOTPVC.h"
#import <AudioToolbox/AudioServices.h>
#import "SharkfoodMuteSwitchDetector.h"
@import MediaPlayer;


@interface ForgotPasswordVC ()
{
    NSString *userid;
}
@property (nonatomic,strong) SharkfoodMuteSwitchDetector* detector;
@end

@implementation ForgotPasswordVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    CALayer *border1 = [CALayer layer];
    CGFloat borderWidth1 = 1;
    border1.borderColor = [UIColor colorWithRed:173/255.0 green:173/255.0 blue:173/255.0 alpha:1.0].CGColor;
    border1.frame = CGRectMake(0, self.EmailTextField.frame.size.height - borderWidth1, self.EmailTextField.frame.size.width, self.EmailTextField.frame.size.height);
    border1.borderWidth = borderWidth1;
    [self.EmailTextField.layer addSublayer:border1];
    self.EmailTextField.layer.masksToBounds = YES;
    
    self.EmailTextField.delegate = self;
    
    [[IQKeyboardManager sharedManager] setEnable:true];
   
    
    
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    NSString *stringVideoName = @"stars_animated.mp4";
    NSString *filepath = [[NSBundle mainBundle] pathForResource:stringVideoName ofType:nil];
    NSLog(@" filepath :- %@", filepath);
    NSURL *fileURL = [NSURL fileURLWithPath:filepath];
    self.playerViewController = [AVPlayer playerWithURL:fileURL];
    self.playerViewController.actionAtItemEnd = AVPlayerActionAtItemEndNone;
    AVPlayerLayer *videoLayer = [AVPlayerLayer playerLayerWithPlayer:self.playerViewController];
    videoLayer.frame = self.view.bounds;
    videoLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
    [self.view.layer addSublayer:videoLayer];
    
    [self.playerViewController play];
    
    self.viewDisply.layer.zPosition = 1;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(itemDidFinishPlaying:) name:AVPlayerItemDidPlayToEndTimeNotification object:[self.playerViewController currentItem]];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationDidBecomeActive:) name:UIApplicationDidEnterBackgroundNotification object:nil];
    
    [[UIApplication sharedApplication] beginReceivingRemoteControlEvents];
    [self becomeFirstResponder];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [self.playerViewController pause];
    [super viewWillDisappear:animated];
    [[UIApplication sharedApplication] endReceivingRemoteControlEvents];
    [self resignFirstResponder];
}


- (void)remoteControlReceivedWithEvent:(UIEvent *)event {
    switch (event.subtype) {
        case UIEventSubtypeRemoteControlTogglePlayPause:
            if([self.playerViewController rate] == 0){
                [self.playerViewController play];
            } else {
                [self.playerViewController pause];
            }
            break;
        case UIEventSubtypeRemoteControlPlay:
            [self.playerViewController play];
            break;
        case UIEventSubtypeRemoteControlPause:
            [self.playerViewController pause];
            break;
        default:
            break;
    }
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    self.detector = [SharkfoodMuteSwitchDetector shared];
    __weak ForgotPasswordVC* sself = self;
    self.detector.silentNotify = ^(BOOL silent){
        if (silent) {
            
            NSLog(@"device is silent");
        } else {
            
            NSLog(@"device is in ringing mode");
            [self.playerViewController performSelector:@selector(play) withObject:nil afterDelay:0.01];
        }
    };
}


- (void)itemDidFinishPlaying:(NSNotification *)notification {
    AVPlayerItem *player = [notification object];
    [player seekToTime:kCMTimeZero];
}

#pragma TEXTFIELD-DELEGATE

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

#pragma  Mark - Action

- (IBAction)btnResetPassword:(id)sender {
    if ([CommanMethods isNetworkRechable]) {
        [self callForForgotPassword];
       
    } else {
        [CommanMethods displayAlertMessage:key_Informaction message:kInternetConnectionMessage currentRef:self];
    }
   
}

- (IBAction)btnBack:(id)sender {
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
        [self.navigationController popViewControllerAnimated:YES];
    } else {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

#pragma CallAPI

-(void)callForForgotPassword {
    NSMutableDictionary *dcitParams = [[NSMutableDictionary alloc] init];
    [dcitParams setValue:self.EmailTextField.text forKey:@"user_email"];
    
    NSLog(@" dcitParams :- %@", dcitParams);
    
    [AppDelegate addVwLoading];
    [CommunicationHandler callForForgotPassword:dcitParams Sucess:^(NSDictionary *dict, NSURLResponse *response) {
        NSLog(@" dict :- %@", dict);
        dispatch_async(dispatch_get_main_queue(), ^{
            [AppDelegate removeVwLoading];
            if ([[dict objectForKey:@"error"] boolValue] == false) {
                
                self->userid = [dict objectForKey:@"user_id"];
                [CommanMethods displayAlertMessageWithCallBack:nil message:[dict objectForKey:@"message"] refrence:self Success:^{
                    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
                        VerifyOTPVC *verifyVC = [[VerifyOTPVC alloc] initWithNibName:@"VerifyOTPVC" bundle:nil];
                        verifyVC.get_userID = self->userid;
                        [self.navigationController pushViewController:verifyVC animated:YES];
                    } else {
                        VerifyOTPVC *verifyVC = [[VerifyOTPVC alloc] initWithNibName:@"VerifyOTPVC_IPad" bundle:nil];
                        verifyVC.get_userID = self->userid;
                        [self.navigationController pushViewController:verifyVC animated:YES];
                    }
                    
                }];
                
                
 
            } else {
                [CommanMethods displayAlertMessage:@"Cards" message:[dict objectForKey:@"message"] currentRef:self];
                
            }
        });
    } Error:^(NSError *error, NSURLResponse *response) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [AppDelegate removeVwLoading];
        });
    }];
}


@end
