//
//  ForgotPasswordVC.h
//  UniversalOracleCards
//
//  Created by whiznic on 04/03/19.
//  Copyright © 2019 whiznic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <AVKit/AVKit.h>


@interface ForgotPasswordVC : UIViewController <UITextFieldDelegate>
@property (strong, nonatomic) IBOutlet UITextField *EmailTextField;
@property (strong, nonatomic) IBOutlet UIButton *btnResetPassword;
@property (nonatomic) AVPlayer *playerViewController;
@property (strong, nonatomic) IBOutlet UIView *viewDisply;
@end


