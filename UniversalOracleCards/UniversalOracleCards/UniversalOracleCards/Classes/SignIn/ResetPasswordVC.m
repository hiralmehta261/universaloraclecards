//
//  ResetPasswordVC.m
//  UniversalOracleCards
//
//  Created by whiznic on 04/03/19.
//  Copyright © 2019 whiznic. All rights reserved.
//

#import "ResetPasswordVC.h"
#import <IQKeyboardManager/IQKeyboardManager.h>
#import "CommanMethods.h"
#import "CommunicationHandler.h"
#import "AppDelegate.h"
#import "CommanMethods.h"
#import "AppDelegate.h"
#import "CommunicationHandler.h"
#import "SignInVC.h"
#import <AudioToolbox/AudioServices.h>
#import "SharkfoodMuteSwitchDetector.h"
@import MediaPlayer;


@interface ResetPasswordVC ()
@property (nonatomic,strong) SharkfoodMuteSwitchDetector* detector;
@end

@implementation ResetPasswordVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
     [[IQKeyboardManager sharedManager] setEnable:true];
    
    [self SetBorder:self.NewPasswordTextField];
    [self SetBorder:self.ConfirmNewPasswordTextField];
    
    self.NewPasswordTextField.delegate = self;
    self.ConfirmNewPasswordTextField.delegate = self;
    
    
    
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    NSString *stringVideoName = @"stars_animated.mp4";
    NSString *filepath = [[NSBundle mainBundle] pathForResource:stringVideoName ofType:nil];
    NSLog(@" filepath :- %@", filepath);
    NSURL *fileURL = [NSURL fileURLWithPath:filepath];
    self.playerViewController = [AVPlayer playerWithURL:fileURL];
    self.playerViewController.actionAtItemEnd = AVPlayerActionAtItemEndNone;
    AVPlayerLayer *videoLayer = [AVPlayerLayer playerLayerWithPlayer:self.playerViewController];
    videoLayer.frame = self.view.bounds;
    videoLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
    [self.view.layer addSublayer:videoLayer];
    
    [self.playerViewController play];
    
    self.viewDisply.layer.zPosition = 1;
    
    
    self.detector = [SharkfoodMuteSwitchDetector shared];
    __weak ResetPasswordVC* sself = self;
    self.detector.silentNotify = ^(BOOL silent){
        if (silent) {
            
            NSLog(@"device is silent");
        } else {
            
            NSLog(@"device is in ringing mode");
            [self.playerViewController performSelector:@selector(play) withObject:nil afterDelay:0.01];
        }
    };
    
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(itemDidFinishPlaying:) name:AVPlayerItemDidPlayToEndTimeNotification object:[self.playerViewController currentItem]];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationDidBecomeActive:) name:UIApplicationDidEnterBackgroundNotification object:nil];
    
    [[UIApplication sharedApplication] beginReceivingRemoteControlEvents];
    [self becomeFirstResponder];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [self.playerViewController pause];
    [super viewWillDisappear:animated];
    [[UIApplication sharedApplication] endReceivingRemoteControlEvents];
    [self resignFirstResponder];
}


- (void)remoteControlReceivedWithEvent:(UIEvent *)event {
    switch (event.subtype) {
        case UIEventSubtypeRemoteControlTogglePlayPause:
            if([self.playerViewController rate] == 0){
                [self.playerViewController play];
            } else {
                [self.playerViewController pause];
            }
            break;
        case UIEventSubtypeRemoteControlPlay:
            [self.playerViewController play];
            break;
        case UIEventSubtypeRemoteControlPause:
            [self.playerViewController pause];
            break;
        default:
            break;
    }
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    self.detector = [SharkfoodMuteSwitchDetector shared];
    __weak ResetPasswordVC* sself = self;
    self.detector.silentNotify = ^(BOOL silent){
        if (silent) {
            
            NSLog(@"device is silent");
        } else {
            
            NSLog(@"device is in ringing mode");
            [self.playerViewController performSelector:@selector(play) withObject:nil afterDelay:0.01];
        }
    };
}


- (void)itemDidFinishPlaying:(NSNotification *)notification {
    AVPlayerItem *player = [notification object];
    [player seekToTime:kCMTimeZero];
}

# pragma Mark --- Button Action

- (IBAction)btnBack:(id)sender {
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
        [self.navigationController popViewControllerAnimated:YES];
    } else {
        [self.navigationController popViewControllerAnimated:YES];
    }
}


- (IBAction)btnShowNewPassword:(id)sender {
    if ([self.btnShowNewPassword.accessibilityHint isEqualToString:@"1"]) {
        self.btnShowNewPassword.accessibilityHint = @"0";
        self.NewPasswordTextField.secureTextEntry = false;
    } else {
        self.btnShowNewPassword.accessibilityHint = @"1";
        self.NewPasswordTextField.secureTextEntry = true;
    }
}

- (IBAction)btnUpdte:(id)sender {
    if (![self.NewPasswordTextField.text isEqualToString:self.ConfirmNewPasswordTextField.text]) {
        [CommanMethods displayAlertMessageWithCallBack:@"EXIST" message:@"Your confirm pasword and new password not matched" refrence:self Success:^{
            [self.NewPasswordTextField becomeFirstResponder];
        }];
    } else {
        if ([CommanMethods isNetworkRechable]) {
            [self callForResetPassword];
        } else {
            [CommanMethods displayAlertMessage:key_Informaction message:kInternetConnectionMessage currentRef:self];
        }
    }
}



- (IBAction)btnShowConfirmNewPassword:(id)sender {
    if ([self.btnShowConfirmNewPassword.accessibilityHint isEqualToString:@"1"]) {
        self.btnShowConfirmNewPassword.accessibilityHint = @"0";
        self.ConfirmNewPasswordTextField.secureTextEntry = false;
    } else {
        self.btnShowConfirmNewPassword.accessibilityHint = @"1";
        self.ConfirmNewPasswordTextField.secureTextEntry = true;
    }
}


#pragma TEXTFIELD-DELEGATE

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

#pragma Function
-(void) SetBorder:(UITextField *)textfield {
    CALayer *border = [CALayer layer];
    CGFloat borderWidth = 1;
    border.borderColor = [UIColor colorWithRed:173/255.0 green:173/255.0 blue:173/255.0 alpha:1.0].CGColor;
    border.frame = CGRectMake(0, textfield.frame.size.height - borderWidth, textfield.frame.size.width, textfield.frame.size.height);
    border.borderWidth = borderWidth;
    [textfield.layer addSublayer:border];
    textfield.layer.masksToBounds = YES;
}

#pragma CallAPI

-(void)callForResetPassword {
    NSMutableDictionary *dcitParams = [[NSMutableDictionary alloc] init];
    [dcitParams setValue:self.get_userid forKey:@"user_id"];
    [dcitParams setValue:self.NewPasswordTextField.text forKey:@"new_password"];
    [dcitParams setValue:self.ConfirmNewPasswordTextField.text forKey:@"confirm_password"];
    
    
    [AppDelegate addVwLoading];
    [CommunicationHandler callForResetPassword:dcitParams Sucess:^(NSDictionary *dict, NSURLResponse *response) {
        NSLog(@" dict :- %@", dict);
        dispatch_async(dispatch_get_main_queue(), ^{
            [AppDelegate removeVwLoading];
            if ([[dict objectForKey:@"error"] boolValue] == false) {
                 if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
                    [CommanMethods displayAlertMessage:@"UOC" message:[dict objectForKey:@"message"] currentRef:self];
                    [self ClearAllUserDetaults];
                    SignInVC *objcLogin = [[SignInVC alloc] initWithNibName:@"SignInVC" bundle:nil];
                    self.navi1 = [[UINavigationController alloc] initWithRootViewController:objcLogin];
                    self.navi1.navigationBarHidden = TRUE;
                    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
                    appDelegate.window.rootViewController = self.navi1;
                } else {
                    [CommanMethods displayAlertMessage:@"UOC" message:[dict objectForKey:@"message"] currentRef:self];
                    [self ClearAllUserDetaults];
                    SignInVC *objcLogin = [[SignInVC alloc] initWithNibName:@"SignInVC" bundle:nil];
                    self.navi1 = [[UINavigationController alloc] initWithRootViewController:objcLogin];
                    self.navi1.navigationBarHidden = TRUE;
                    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
                    appDelegate.window.rootViewController = self.navi1;
                }
            }
        });
    } Error:^(NSError *error, NSURLResponse *response) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [AppDelegate removeVwLoading];
        });
    }];
}

-(void)ClearAllUserDetaults {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults removeObjectForKey:@"username"];
    [userDefaults removeObjectForKey:@"user_data"];
    [userDefaults removeObjectForKey:@"HomeScreen"];
    [userDefaults removeObjectForKey:@"CheckboxDailyCard"];
    [userDefaults removeObjectForKey:@"CheckboxGeneral"];
    [userDefaults removeObjectForKey:@"CheckboxInner"];
    [userDefaults removeObjectForKey:@"CheckboxConnection"];
    [userDefaults removeObjectForKey:@"CheckboxLife"];
}


@end
