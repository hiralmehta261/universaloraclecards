//
//  ResetPasswordVC.h
//  UniversalOracleCards
//
//  Created by whiznic on 04/03/19.
//  Copyright © 2019 whiznic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <AVKit/AVKit.h>

@interface ResetPasswordVC : UIViewController <UITextFieldDelegate>
@property (strong, nonatomic) IBOutlet UITextField *NewPasswordTextField;
@property (strong, nonatomic) IBOutlet UITextField *ConfirmNewPasswordTextField;
@property (strong, nonatomic) IBOutlet UIButton *btnShowNewPassword;
@property (strong, nonatomic) IBOutlet UIButton *btnShowConfirmNewPassword;
@property (nonatomic) AVPlayer *playerViewController;
@property (strong, nonatomic) IBOutlet UIView *viewDisply;
@property (strong, nonatomic) NSString *get_userid;
@property (nonatomic, strong) UINavigationController *navi1;
@property (nonatomic, strong) IBOutlet UIWindow* window;
@end

