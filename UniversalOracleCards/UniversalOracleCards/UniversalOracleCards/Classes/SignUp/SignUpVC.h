//
//  SignUpVC.h
//  UniversalOracleCards
//
//  Created by whiznic on 02/03/19.
//  Copyright © 2019 whiznic. All rights reserved.
//

#import <UIKit/UIKit.h>
@import GoogleSignIn;
@import Firebase;

#import <AVFoundation/AVFoundation.h>
#import <AVKit/AVKit.h>


@interface SignUpVC : UIViewController <UITextFieldDelegate, GIDSignInUIDelegate>
@property (strong, nonatomic) IBOutlet UIButton *btnSignin;
@property (strong, nonatomic) IBOutlet UIButton *btnSignup;
@property (strong, nonatomic) IBOutlet UITextField *EmailTextField;
@property (strong, nonatomic) IBOutlet UITextField *PasswordTextField;
@property (strong, nonatomic) IBOutlet UITextField *UsernameTextField;
@property (strong, nonatomic) IBOutlet UIButton *btnShowPassword;
@property (strong, nonatomic) IBOutlet UIButton *btnSubmit;
@property (strong, nonatomic) IBOutlet UIButton *btnGoogle;
@property (strong, nonatomic) IBOutlet UIButton *btnFacebook;
@property (strong, nonatomic) IBOutlet GIDSignInButton *BtnGoogleSignIn;
@property (nonatomic) AVPlayer *playerViewController;
@property (strong, nonatomic) IBOutlet UIView *viewDisply;
@property (nonatomic, strong) UINavigationController *navi1;
@property (nonatomic, strong) IBOutlet UIWindow* window;
@end


