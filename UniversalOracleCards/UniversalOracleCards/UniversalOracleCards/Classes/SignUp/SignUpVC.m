//
//  SignUpVC.m
//  UniversalOracleCards
//
//  Created by whiznic on 02/03/19.
//  Copyright © 2019 whiznic. All rights reserved.
//

#import "SignUpVC.h"
#import <IQKeyboardManager/IQKeyboardManager.h>
#import "SignUpVC.h"
#import "ForgotPasswordVC.h"
#import "HomeVC.h"
#import <Firebase.h>
#import "CommanMethods.h"
#import "AppDelegate.h"
#import "CommunicationHandler.h"
#import "SignInVC.h"
@import MediaPlayer;
#import <AudioToolbox/AudioServices.h>
#import "SharkfoodMuteSwitchDetector.h"


@interface SignUpVC () {
        NSString *idToken;
        GIDGoogleUser *googleUser;
}
@property (nonatomic,strong) SharkfoodMuteSwitchDetector* detector;
@end

@implementation SignUpVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
        self.detector = [SharkfoodMuteSwitchDetector shared];
        __weak SignUpVC* sself = self;
        self.detector.silentNotify = ^(BOOL silent){
            if (silent) {
                
                NSLog(@"device is silent");
            } else {
                
                NSLog(@"device is in ringing mode");
                [self.playerViewController performSelector:@selector(play) withObject:nil afterDelay:0.01];
            }
        };
    

    
    [self SetBorder:self.EmailTextField];
    [self SetBorder:self.PasswordTextField];
    [self SetBorder:self.UsernameTextField];
    
    self.EmailTextField.delegate = self;
    self.PasswordTextField.delegate = self;
    self.UsernameTextField.delegate = self;
    
    [[IQKeyboardManager sharedManager] setEnable:true];
    
   
    
}

- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    NSString *stringVideoName = @"stars_animated.mp4";
    NSString *filepath = [[NSBundle mainBundle] pathForResource:stringVideoName ofType:nil];
    NSLog(@" filepath :- %@", filepath);
    NSURL *fileURL = [NSURL fileURLWithPath:filepath];
    self.playerViewController = [AVPlayer playerWithURL:fileURL];
    self.playerViewController.actionAtItemEnd = AVPlayerActionAtItemEndNone;
    AVPlayerLayer *videoLayer = [AVPlayerLayer playerLayerWithPlayer:self.playerViewController];
    videoLayer.frame = self.view.bounds;
    videoLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
    [self.view.layer addSublayer:videoLayer];
    
    [self.playerViewController play];
    
    self.viewDisply.layer.zPosition = 1;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(itemDidFinishPlaying:) name:AVPlayerItemDidPlayToEndTimeNotification object:[self.playerViewController currentItem]];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationDidBecomeActive:) name:UIApplicationDidEnterBackgroundNotification object:nil];
    
    [[UIApplication sharedApplication] beginReceivingRemoteControlEvents];
    [self becomeFirstResponder];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [self.playerViewController pause];
    [super viewWillDisappear:animated];
    [[UIApplication sharedApplication] endReceivingRemoteControlEvents];
    [self resignFirstResponder];
}


- (void)remoteControlReceivedWithEvent:(UIEvent *)event {
    switch (event.subtype) {
        case UIEventSubtypeRemoteControlTogglePlayPause:
            if([self.playerViewController rate] == 0){
                [self.playerViewController play];
            } else {
                [self.playerViewController pause];
            }
            break;
        case UIEventSubtypeRemoteControlPlay:
            [self.playerViewController play];
            break;
        case UIEventSubtypeRemoteControlPause:
            [self.playerViewController pause];
            break;
        default:
            break;
    }
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    self.detector = [SharkfoodMuteSwitchDetector shared];
    __weak SignUpVC* sself = self;
    self.detector.silentNotify = ^(BOOL silent){
        if (silent) {
            
            NSLog(@"device is silent");
        } else {
            
            NSLog(@"device is in ringing mode");
            [self.playerViewController performSelector:@selector(play) withObject:nil afterDelay:0.01];
        }
    };
}


- (void)itemDidFinishPlaying:(NSNotification *)notification {
    AVPlayerItem *player = [notification object];
    [player seekToTime:kCMTimeZero];
}

#pragma GOOGLE-SIGNIN

- (void)signInWillDispatch:(GIDSignIn *)signIn error:(NSError *)error {
    //[myActivityIndicator stopAnimating];
}

// Present a view that prompts the user to sign in with Google
- (void)signIn:(GIDSignIn *)signIn
presentViewController:(UIViewController *)viewController {
    [self presentViewController:viewController animated:YES completion:nil];
}

// Dismiss the "Sign in with Google" view
- (void)signIn:(GIDSignIn *)signIn
dismissViewController:(UIViewController *)viewController {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)signIn:(GIDSignIn *)signIn
didSignInForUser:(GIDGoogleUser *)user
     withError:(NSError *)error {
    
    if (error == nil) {
        GIDAuthentication *authentication = user.authentication;
        FIRAuthCredential *credential =
        [FIRGoogleAuthProvider credentialWithIDToken:authentication.idToken
                                         accessToken:authentication.accessToken];
        NSString *userId = user.userID;                  // For client-side use only!
        idToken = user.authentication.idToken; // Safe to send to the server
        NSString *fullName = user.profile.name;
        NSString *givenName = user.profile.givenName;
        NSString *familyName = user.profile.familyName;
        NSString *email = user.profile.email;
        
        self.UsernameTextField.text = user.profile.name;
        self.EmailTextField.text = user.profile.email;
        
        googleUser = [[GIDGoogleUser alloc] init];
        googleUser = user;
        [self callForSocialRegister];
        
    } else {
        // ...
    }
}


# pragma Mark --- Button Action


- (IBAction)btnSubmit:(id)sender {
    if ([CommanMethods isNetworkRechable]) {
        [self callForRegister];
    } else {
        [CommanMethods displayAlertMessage:key_Informaction message:kInternetConnectionMessage currentRef:self];
    }
//    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
//        HomeVC *homeVC = [[HomeVC alloc] initWithNibName:@"HomeVC" bundle:nil];
//        [self.navigationController pushViewController:homeVC animated:YES];
//    } else {
//        HomeVC *homeVC = [[HomeVC alloc] initWithNibName:@"HomeVC_IPad" bundle:nil];
//        [self.navigationController pushViewController:homeVC animated:YES];
//    }
}

- (IBAction)btnShowPassword:(id)sender {
    if ([self.btnShowPassword.accessibilityHint isEqualToString:@"1"]) {
        self.btnShowPassword.accessibilityHint = @"0";
        self.PasswordTextField.secureTextEntry = false;
    } else {
        self.btnShowPassword.accessibilityHint = @"1";
        self.PasswordTextField.secureTextEntry = true;
    }
}

- (IBAction)btnGoogle:(id)sender {
    [GIDSignIn sharedInstance].delegate = self;
    [GIDSignIn sharedInstance].uiDelegate = self;
    [[GIDSignIn sharedInstance] signIn];
}

- (IBAction)btnFacebook:(id)sender {
}

- (IBAction)btnSignUp:(id)sender {
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone )
    {
        [self.navigationController popViewControllerAnimated:NO];
    } else {
        [self.navigationController popViewControllerAnimated:NO];
    }
}

- (IBAction)btnSignIn:(id)sender {
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone )
    {
        [self.navigationController popViewControllerAnimated:NO];
    } else {
        [self.navigationController popViewControllerAnimated:NO];
    }
}


#pragma TEXTFIELD-DELEGATE

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

#pragma Function
-(void) SetBorder:(UITextField *)textfield {
    CALayer *border = [CALayer layer];
    CGFloat borderWidth = 1;
    border.borderColor = [UIColor colorWithRed:173/255.0 green:173/255.0 blue:173/255.0 alpha:1.0].CGColor;
    border.frame = CGRectMake(0, textfield.frame.size.height - borderWidth, textfield.frame.size.width, textfield.frame.size.height);
    border.borderWidth = borderWidth;
    [textfield.layer addSublayer:border];
    textfield.layer.masksToBounds = YES;
}

-(void) setAlert:(NSString *)message {
    [CommanMethods displayAlertMessage:@"EXIST" message:message currentRef:self];
}

#pragma CallAPI

-(void)callForRegister {
    NSMutableDictionary *dcitParams = [[NSMutableDictionary alloc] init];
     [dcitParams setValue:self.EmailTextField.text forKey:@"email"];
     [dcitParams setValue:self.PasswordTextField.text forKey:@"password"];
     [dcitParams setValue:self.UsernameTextField.text forKey:@"full_name"];
     [dcitParams setValue:@"ios" forKey:@"asset_type"];
     [dcitParams setValue:[[FIRInstanceID instanceID] token] forKey:@"token_id"];
     //[dcitParams setValue:@"12345" forKey:@"facebook_id"];
     [dcitParams setValue:idToken forKey:@"google_plus"];
    
    NSLog(@" dcitParams :- %@", dcitParams);
    
   
    [AppDelegate addVwLoading];
    [CommunicationHandler callForRegister:dcitParams Sucess:^(NSDictionary *dict, NSURLResponse *response) {
        NSLog(@" dict :- %@", dict);
        dispatch_async(dispatch_get_main_queue(), ^{
            [AppDelegate removeVwLoading];
            if ([dict count] > 0) {
                if ([[dict objectForKey:@"error"] boolValue] == false) {
                    [CommanMethods displayAlertMessageWithCallBack:nil message:[dict objectForKey:@"message"] refrence:self Success:^{
                        if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
                            SignInVC *objcLogin = [[SignInVC alloc] initWithNibName:@"SignInVC" bundle:nil];
                            self.navi1 = [[UINavigationController alloc] initWithRootViewController:objcLogin];
                            self.navi1.navigationBarHidden = TRUE;
                            AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
                            appDelegate.window.rootViewController = self.navi1;
                        } else {
                            SignInVC *objcLogin = [[SignInVC alloc] initWithNibName:@"SignUpVC_IPad" bundle:nil];
                            self.navi1 = [[UINavigationController alloc] initWithRootViewController:objcLogin];
                            self.navi1.navigationBarHidden = TRUE;
                            AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
                            appDelegate.window.rootViewController = self.navi1;
                        }
                        
                    }];
                } else {
                    [CommanMethods displayAlertMessage:@"UOC" message:[dict objectForKey:@"message"] currentRef:self];
                }
            }
        });
    } Error:^(NSError *error, NSURLResponse *response) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [AppDelegate removeVwLoading];
        });
    }];
}

-(void)callForSocialRegister {
    
    NSMutableDictionary *dcitParams = [[NSMutableDictionary alloc] init];
    [dcitParams setValue:googleUser.profile.email forKey:@"email"];
    [dcitParams setValue:googleUser.profile.email forKey:@"password"];
    [dcitParams setValue:googleUser.profile.givenName forKey:@"full_name"];
    [dcitParams setValue:@"ios" forKey:@"asset_type"];
    [dcitParams setValue:[[FIRInstanceID instanceID] token] forKey:@"token_id"];
    //[dcitParams setValue:@"12345" forKey:@"facebook_id"];
    [dcitParams setValue:googleUser.authentication.idToken forKey:@"google_plus"];
    
    NSLog(@" dcitParams :- %@", dcitParams);
    
    
    [AppDelegate addVwLoading];
    [CommunicationHandler callForRegister:dcitParams Sucess:^(NSDictionary *dict, NSURLResponse *response) {
        NSLog(@" dict :- %@", dict);
        dispatch_async(dispatch_get_main_queue(), ^{
            [AppDelegate removeVwLoading];
            if ([dict count] > 0) {
                if ([[dict objectForKey:@"error"] boolValue] == false) {
                    [CommanMethods displayAlertMessageWithCallBack:nil message:[dict objectForKey:@"message"] refrence:self Success:^{
                        if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
                            SignInVC *objcLogin = [[SignInVC alloc] initWithNibName:@"SignInVC" bundle:nil];
                            self.navi1 = [[UINavigationController alloc] initWithRootViewController:objcLogin];
                            self.navi1.navigationBarHidden = TRUE;
                            AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
                            appDelegate.window.rootViewController = self.navi1;
                        } else {
                            SignInVC *objcLogin = [[SignInVC alloc] initWithNibName:@"SignInVC_IPad" bundle:nil];
                            self.navi1 = [[UINavigationController alloc] initWithRootViewController:objcLogin];
                            self.navi1.navigationBarHidden = TRUE;
                            AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
                            appDelegate.window.rootViewController = self.navi1;
                        }
                        
                    }];
                } else {
                    [CommanMethods displayAlertMessage:@"UOC" message:[dict objectForKey:@"message"] currentRef:self];
                }
            }
        });
    } Error:^(NSError *error, NSURLResponse *response) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [AppDelegate removeVwLoading];
        });
    }];
}

@end
