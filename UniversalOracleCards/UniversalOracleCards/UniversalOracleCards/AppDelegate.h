//
//  AppDelegate.h
//  UniversalOracleCards
//
//  Created by whiznic on 02/03/19.
//  Copyright © 2019 whiznic. All rights reserved.
//

#import <UIKit/UIKit.h>

@import GoogleSignIn;
@import Firebase;
static UIView *vwLoading;
static UIView *vwSideMenuBg;

@interface AppDelegate : UIResponder <UIApplicationDelegate, GIDSignInDelegate>

@property (strong, nonatomic) UIWindow *window;
+(AppDelegate*)getCurrentDelegate;
+(void)addVwLoading;
+(void)addVwLoadingIntoVw:(UIView *)vw;
+(void)removeVwLoading;
+(void)removeVwSideMenuBg;
+(void)addVwSideMenuBgToVw:(UIView *)vw;

@property (nonatomic, strong) UINavigationController *navi1;

@end

