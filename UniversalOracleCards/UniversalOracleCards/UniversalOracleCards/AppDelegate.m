//
//  AppDelegate.m
//  UniversalOracleCards
//
//  Created by whiznic on 02/03/19.
//  Copyright © 2019 whiznic. All rights reserved.
//

#import "AppDelegate.h"
#import "SignInVC.h"
#import <Firebase.h>
#import "HomeVC.h"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>

static UIView *vwLoading;
@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
       [Fabric with:@[[Crashlytics class]]];
    [self initVwLoading];
    NSUserDefaults *userDefaults1 = [NSUserDefaults standardUserDefaults];
    NSDictionary *dictData1 = [userDefaults1 objectForKey:@"user_data"];
    NSLog(@" dictData1 :- %@", dictData1);
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSString *dictData = [userDefaults objectForKey:@"username"];
    
    if ([dictData length] == 0) {
        if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone )
        {
            SignInVC *signVC = [[SignInVC alloc] initWithNibName:@"SignInVC" bundle:nil];
            self.navi1 = [[UINavigationController alloc] initWithRootViewController:signVC];
            self.navi1.navigationBarHidden = TRUE;
            self.window.rootViewController = self.navi1;
            
        } else {
            SignInVC *signVC = [[SignInVC alloc] initWithNibName:@"SignInVC_IPad" bundle:nil];
            self.navi1 = [[UINavigationController alloc] initWithRootViewController:signVC];
            self.navi1.navigationBarHidden = TRUE;
            self.window.rootViewController = self.navi1;
        }
        
    } else {
        if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone )
        {
            HomeVC *homeVC = [[HomeVC alloc] initWithNibName:@"HomeVC" bundle:nil];
            self.navi1 = [[UINavigationController alloc] initWithRootViewController:homeVC];
            self.navi1.navigationBarHidden = TRUE;
            self.window.rootViewController = self.navi1;
            
        } else {
            HomeVC *homeVC = [[HomeVC alloc] initWithNibName:@"HomeVC_IPad" bundle:nil];
            self.navi1 = [[UINavigationController alloc] initWithRootViewController:homeVC];
            self.navi1.navigationBarHidden = TRUE;
            self.window.rootViewController = self.navi1;
        }
    }
    
    [[AVAudioSession sharedInstance] setDelegate: self];
   // [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:nil];

    
    
    [GIDSignIn sharedInstance].clientID = @"281610612742-95ds0jv1j1ile75p4ansef3avg0n5bb7.apps.googleusercontent.com";
    [GIDSignIn sharedInstance].delegate = self;
    
    [FIRApp configure];
    
   
    
    [[FIRInstanceID instanceID] instanceIDWithHandler:^(FIRInstanceIDResult * _Nullable result,
                                                        NSError * _Nullable error) {
        if (error != nil) {
            NSLog(@"Error fetching remote instance ID: %@", error);
        } else {
            NSLog(@"Remote instance ID token: %@", result.token);
            NSString* message =
            [NSString stringWithFormat:@"Remote InstanceID token: %@", result.token];
            //self.instanceIDTokenMessage.text = message;
        }
    }];

    
    return YES;
}

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
    return [[GIDSignIn sharedInstance] handleURL:url
                               sourceApplication:sourceApplication
                                      annotation:annotation];
}

- (void)signIn:(GIDSignIn *)signIn
didSignInForUser:(GIDGoogleUser *)user
     withError:(NSError *)error {
    
    if (error == nil) {
        GIDAuthentication *authentication = user.authentication;
        FIRAuthCredential *credential =
        [FIRGoogleAuthProvider credentialWithIDToken:authentication.idToken
                                         accessToken:authentication.accessToken];
        NSString *userId = user.userID;                  // For client-side use only!
        NSString *idToken = user.authentication.idToken; // Safe to send to the server
        NSString *fullName = user.profile.name;
        NSString *givenName = user.profile.givenName;
        NSString *familyName = user.profile.familyName;
        NSString *email = user.profile.email;
    } else {
        // ...
    }
}

- (void)signIn:(GIDSignIn *)signIn
didDisconnectWithUser:(GIDGoogleUser *)user
     withError:(NSError *)error {
    // Perform any operations when the user disconnects from app here.
    // ...
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

+(void)addVwLoading {
    [AppDelegate removeVwLoading];
    [UIApplication.sharedApplication.delegate.window addSubview:vwLoading];
    [UIApplication.sharedApplication.delegate.window bringSubviewToFront:vwLoading];
}

+(void)addVwLoadingIntoVw:(UIView *)vw {
    [AppDelegate removeVwLoading];
    [vw addSubview:vwLoading];
    [vw bringSubviewToFront:vwLoading];
}

+(void)removeVwLoading {
    [vwLoading removeFromSuperview];
}

-(void)initVwSideMenuBg {
    vwSideMenuBg = [[UIView alloc] initWithFrame:UIScreen.mainScreen.bounds];
}

+(void)addVwSideMenuBgToVw:(UIView *)vw {
    vwSideMenuBg.backgroundColor = [UIColor colorWithWhite:0.0 alpha:1.0];
    vwSideMenuBg.alpha = 0.0;
    [vw addSubview:vwSideMenuBg];
}

+(void)removeVwSideMenuBg {
    vwSideMenuBg.alpha = 0.0;
    [vwSideMenuBg removeFromSuperview];
}

-(void)initVwLoading {
    CGRect rect = CGRectMake(0.0, 0.0, 50.0, 50.0);
    
    vwLoading = [[UIView alloc] initWithFrame:rect];
    vwLoading.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.33];
    
    rect = UIScreen.mainScreen.bounds;
    vwLoading.center = CGPointMake(rect.size.width/2.0, rect.size.height/2.0);
    vwLoading.layer.cornerRadius = vwLoading.frame.size.height/10.0;
    
    UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc]
                                                  initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    rect = vwLoading.frame;
    activityIndicator.center = CGPointMake(rect.size.width/2.0, rect.size.height/2.0);
    
    [vwLoading addSubview:activityIndicator];
    [activityIndicator startAnimating];
    

}

@end
